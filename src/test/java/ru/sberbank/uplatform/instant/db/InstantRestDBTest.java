package ru.sberbank.uplatform.instant.db;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.sberbank.uplatform.api.InstantAnswer;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by baryshnikov on 13.12.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring.xml"})
@Ignore
public class InstantRestDBTest {

    @Autowired
    private InstantAnswer db;

    @Test
    public void testSearch() throws Exception {
        Optional<String> resp = db.search("test");
        assertTrue(resp.isPresent());
        System.out.println(resp.get());
    }
}