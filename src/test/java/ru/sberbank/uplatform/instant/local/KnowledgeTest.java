package ru.sberbank.uplatform.instant.local;


import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Optional;

import static org.springframework.test.util.AssertionErrors.assertTrue;

/**
 * Created by baryshnikov on 30.11.15.
 */
@Ignore
public class KnowledgeTest {
    private Knowledge instance;

    @Before
    public void setUp() throws Exception {
        instance = new Knowledge();
        instance.setIndexFile("instant.json");
        instance.build();
    }

    @Test
    public void testSearch() throws Exception {
        Optional<String> search = instance.search("Сбт");
        assertTrue("Expect found", search.isPresent());
        assertTrue("Expect non-empty value", !search.get().isEmpty());
    }
}