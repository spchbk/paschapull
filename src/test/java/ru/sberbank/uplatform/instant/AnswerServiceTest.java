package ru.sberbank.uplatform.instant;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by baryshnikov on 18.01.16.
 */
@Ignore
public class AnswerServiceTest {

    @Test
    public void testGetAnswers() throws Exception {
        AnswerService service = new AnswerService();
        service.setServiceUrl("http://127.0.0.1:9016");
        service.setConnectionManager(new PoolingHttpClientConnectionManager());
        assertTrue(!service.getAnswers(0, 10000).isEmpty());
    }

    @Test
    public void testGetAnswersScanner() throws Exception {
        AnswerService service = new AnswerService();
        service.setServiceUrl("http://127.0.0.1:9016");
        service.setConnectionManager(new PoolingHttpClientConnectionManager());
        assertTrue(!service.getAnswersScanner("444").isEmpty());
    }

    @Test
    public void testGetAnswersCollectionScanner() throws Exception {
        AnswerService service = new AnswerService();
        service.setServiceUrl("http://127.0.0.1:9016");
        service.setConnectionManager(new PoolingHttpClientConnectionManager());
        assertTrue(!service.getAnswersCollectionScanner("Факты", "444").isEmpty());
    }
}