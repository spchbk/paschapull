package ru.sberbank.uplatform.geo;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.sberbank.uplatform.api.GeoDB;
import ru.sberbank.uplatform.api.geo.GeoInfo;
import ru.sberbank.uplatform.api.geo.GeoObject;

import java.util.Optional;

/**
 * Created by baryshnikov on 12.12.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring.xml"})
@Ignore
public class GeoDBTest {
    @Autowired
    private GeoDB geoDB;


    @Test
    public void testCount() throws Exception {
        GeoInfo obj = new GeoInfo();
        obj.setLatitude(133);
        obj.setLongitude(103);
        obj.setTag("test");
        Optional<Long> id = geoDB.create(obj);
        Assert.assertTrue(id.isPresent());

        Optional<GeoObject> full = geoDB.get(id.get());
        Assert.assertTrue(full.isPresent());

        System.out.println(full.get());
        System.out.println(geoDB.count().get());
        Assert.assertTrue(geoDB.remove(full.get().getId()));
    }
}