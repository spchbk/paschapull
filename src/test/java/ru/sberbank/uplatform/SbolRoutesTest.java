package ru.sberbank.uplatform;

import org.junit.Test;

import java.util.regex.Matcher;

import static org.junit.Assert.*;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class SbolRoutesTest {

    @Test
    public void testParse() throws Exception {
        String input = "100.000 usd";
        Matcher m = SbolRoutes.currencyPattern.matcher(input);
        System.out.println(SbolRoutes.currencyPattern.pattern());
        assertTrue(m.find());
        assertTrue(m.groupCount() > 3);
        assertTrue(m.groupCount() > 3);
        assertEquals(m.group(1), "100.000");
        assertEquals(m.group(3), "usd");
    }
}