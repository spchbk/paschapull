package ru.sberbank.uplatform;

import org.junit.Before;
import org.junit.Test;
import ru.sberbank.uplatform.sbol.api.Quote;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class CurrencyManagerTest {
    private CurrencyManager manager;

    @Before
    public void setUp() throws Exception {
        manager = new CurrencyManager();
        manager.setPrice(Currency.Euro, new Quote(60.0d, 6.0d));
        manager.setPrice(Currency.USD, new Quote(30.0d, 3.0d));
    }

    @Test
    public void testValue() throws Exception {
        Quote rub = manager.priceFor(Currency.Rub).get();
        Quote usd = manager.priceFor(Currency.USD).get();
        Quote euro = manager.priceFor(Currency.Euro).get();
        assertEquals(1.0d, rub.getBuy(), 0.0d);
        assertEquals(1.0d, rub.getSell(), 0.0d);

        assertEquals(30.0d, usd.getBuy(), 0.0d);
        assertEquals(3.0d, usd.getSell(), 0.0d);

        assertEquals(60.0d, euro.getBuy(), 0.0d);
        assertEquals(6.0d, euro.getSell(), 0.0d);
    }

    @Test
    public void testPriceTable() throws Exception {
        Map<Currency, Quote> table = manager.priceTable(Currency.USD);
        Quote rub = table.get(Currency.Rub);
        Quote euro = table.get(Currency.Euro);
        assertEquals(0.033333d, rub.getBuy(), 0.00001d);
        assertEquals(0.33333d, rub.getSell(), 0.00001d);

        assertEquals(2.0d, euro.getBuy(), 0.0d);
        assertEquals(2.0d, euro.getSell(), 0.0d);
        assertNull(table.get(Currency.USD));

    }
}