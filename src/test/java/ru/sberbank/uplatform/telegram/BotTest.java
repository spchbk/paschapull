package ru.sberbank.uplatform.telegram;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.sberbank.uplatform.message.reply.TextReply;

/**
 * Created by baryshnikov on 28.11.15.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring.xml"})
public class BotTest {

    @Autowired
    private API botReply;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void sendInvalidMessage() throws Exception {
        TextReply t = new TextReply();
        t.setChatID(123L);
        t.setText("Welcome");
        System.out.println(t.toString());
        botReply.send(t);
    }
}
