package ru.sberbank.uplatform.client;

import com.lambdaworks.redis.RedisClient;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.sberbank.uplatform.Client;
import ru.sberbank.uplatform.ClientState;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by baryshnikov on 02.12.15.
 * This test case required REDIS DB
 */
@Ignore //Remove it if you are sure, that test environment contains redis database
public class RedisClientManagerTest {
    private RedisClientManager<Client> manager;

    @Before
    public void setUp() throws Exception {
        manager = new RedisClientManager<>();
        manager.setRedisClient(RedisClient.create("redis://127.0.0.1:6379/0"));
        manager.setKeepAlive(2);
    }

    @Test
    public void testSetState() throws Exception {
        assertTrue("REDIS must save state", manager.setState("1234", new Client(ClientState.IDLE)));
    }

    @Test
    public void testGetState() throws Exception {
        assertTrue("REDIS must save state", manager.setState("1234", new Client(ClientState.IDLE)));
        Optional<Client> val = manager.getState("1234", Client.class);
        assertTrue("Value must exists", val.isPresent());
        assertEquals(ClientState.IDLE, val.get().getState());

    }

    @Test
    public void testGetExpiredState() throws Exception {
        //Set data
        testSetState();
        //Check that it exists
        testGetState();
        //Wait for some time
        Thread.sleep(1000 * manager.getKeepAlive() + 1000);
        assertFalse("Value must be removed", manager.getState("1234", Client.class).isPresent());
    }


}