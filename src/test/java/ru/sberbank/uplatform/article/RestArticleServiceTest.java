package ru.sberbank.uplatform.article;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.sberbank.uplatform.api.article.Article;
import ru.sberbank.uplatform.api.article.Book;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by baryshnikov on 27.12.15.
 */
@Ignore
public class RestArticleServiceTest {

    private RestArticleService service;

    @Before
    public void setUp() throws Exception {
        service = new RestArticleService();
        service.setServiceUrl("http://127.0.0.1:9015");
        service.setConnectionManager(new PoolingHttpClientConnectionManager());
    }

    @Test
    public void testGetRandomPage() throws Exception {
        Optional<Article> fact = service.getRandomPage("fact");
        assertTrue(fact.isPresent());
        System.out.println(fact.get());
    }

    @Test
    public void testGetBooks() throws Exception {
        List<Book> books = service.getBooks();
        assertTrue(!books.isEmpty());
        System.out.println(books);
    }
}