package ru.sberbank.uplatform.message;

import com.google.gson.Gson;
import org.junit.Test;

/**
 * Created by baryshnikov on 02.12.15.
 */
public class UpdateTest {

    @Test
    public void testBuildMessage() throws Exception {
        Update up = new Update();
        up.setUpdateId(0);
        Message msg = new Message();


        User user = new User();
        user.setId(0L);

        Chat chat = new Chat();
        chat.setId(100000000L);

        msg.setText("test");
        msg.setFrom(user);
        msg.setChat(chat);

        up.setMessage(msg);
        System.out.println(new Gson().toJson(up));
    }
}