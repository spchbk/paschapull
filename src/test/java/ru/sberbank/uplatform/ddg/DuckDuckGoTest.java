package ru.sberbank.uplatform.ddg;

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class DuckDuckGoTest {
    private DuckDuckGo instance;

    @Before
    public void setUp() throws Exception {
        instance = new DuckDuckGo();
    }

    @Test
    public void testSearch() throws Exception {
        Optional<String> val = instance.search("Владимир Путин");
        assertTrue("Request must be successfull", val.isPresent());
        assertTrue("Expect requested information", val.get().contains("Влади́мир"));
    }
}