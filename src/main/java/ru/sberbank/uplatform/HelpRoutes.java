package ru.sberbank.uplatform;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.camel.Header;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.api.AdviseService;
import ru.sberbank.uplatform.api.FactService;
import ru.sberbank.uplatform.message.Message;

import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by baryshnikov on 19.11.15.
 */
public class HelpRoutes extends RouteBuilder {
    private Cache<Long, HelpRandomCache> cache;

    @Autowired
    private FactService factService;

    @Autowired
    private AdviseService adviseService;

    @Autowired
    private ClientRoutes clientRoutes;//FIXME: DIRTY, UGLY, EXTREMELY BAD HACK!

    @Override
    public void configure() throws Exception {
        from(Endpoints.HELP_INIT.getEndpoint())
                .id(Endpoints.HELP_INIT.getId())
                .process(ex -> cache = CacheBuilder.newBuilder().expireAfterWrite(15, TimeUnit.MINUTES).build());

        from(Endpoints.HELP.getEndpoint())
                .id(Endpoints.HELP.getId())
                .setHeader("template", constant("templates/help.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("help_menu"))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.HELP_FACT.endpoint)
                .id(Endpoints.HELP_FACT.id)
                .process(ex -> {
                    factService.getRandomFact().ifPresent(x -> {
                        ex.getIn().setHeader("randFact", x);
                        ex.getIn().setHeader("index", x.getPage());
                        ex.getIn().setHeader("existsFact", true);
                    });
                })
                .setHeader("template", constant("templates/fact.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("help_menu"))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.HELP_RECOMMENDATION.endpoint)
                .id(Endpoints.HELP_RECOMMENDATION.id)
                .process(ex -> {
                    adviseService.getRandomAdvise().ifPresent(x -> {
                        ex.getIn().setHeader("randRecommendation", x);
                        ex.getIn().setHeader("index", x.getPage());
                        ex.getIn().setHeader("existsRecommendation", true);
                    });
                })
                .setHeader("keyboard", constant("help_menu"))
                .setHeader("template", constant("templates/recommendation.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);


        from(Endpoints.HELP_HOTLINE.endpoint)
                .id(Endpoints.HELP_HOTLINE.id)
                .setHeader("keyboard", constant("vote_menu"))
                .setHeader("template", constant("templates/hotline.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.HELP_VIDEO.endpoint)
                .id(Endpoints.HELP_VIDEO.id)
                .process(ex -> {
                    Object[] video = getRandomElement((ex.getProperty("message", Message.class)).getFrom().getId());
                    if (video != null) {
                        ex.getIn().setHeader("index", video[0]);
                        ex.getIn().setHeader("randVideo", video[1]);
                        ex.getIn().setHeader("existsVideo", true);
                    }
                })
                .setHeader("keyboard", constant("help_menu"))
                .setHeader("template", constant("templates/video.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);
    }

    public List<String> loadFile(@Header("file") String filename) throws Exception {
        Type frmt = new TypeToken<List<String>>() {
        }.getType();
        try (FileReader r = new FileReader(filename)) {
            return new Gson().fromJson(r, frmt);
        }
    }

    public Object[] getRandomElement(Long userId) {
        int size = clientRoutes.getVideos().size();
        if (size > 0) {
            HelpRandomCache help = cache.getIfPresent(userId);
            if (help == null) {
                help = new HelpRandomCache();
                cache.put(userId, help);
            }

            List<Integer> elements = help.getIndexes();
            if (elements.isEmpty()) {
                elements = IntStream.range(0, size).mapToObj(i -> i).collect(Collectors.toList());
                help.setIndexes(elements);
                cache.put(userId, help);
            }

            Integer next = elements.get(new Random().nextInt(elements.size()));
            if (elements.size() == size) {
                next = !next.equals(help.getLastIndex()) ? next : (size % (next + 1));
            } else if (elements.size() == 1) {
                help.setLastIndex(next);
            }

            help.remove(next);
            cache.put(userId, help);
            return new Object[]{next, clientRoutes.getVideos().get(next)};
        }

        return null;
    }

    private class HelpRandomCache {
        private List<Integer> videosIndexes;
        private Integer lastIndex;

        public HelpRandomCache() {
            videosIndexes = new LinkedList<>();
            lastIndex = -1;
        }

        public void setLastIndex(Integer index) {
            lastIndex = index;
        }

        public Integer getLastIndex() {
            return lastIndex;
        }

        public List<Integer> getIndexes() {
            return (videosIndexes != null) ? videosIndexes : Collections.<Integer>emptyList();
        }

        public void setIndexes(List<Integer> indexes) {
            videosIndexes = indexes;
        }

        public void remove(Integer index) {
            videosIndexes.remove(index);
        }

        @Override
        public int hashCode() {
            int result = videosIndexes != null ? videosIndexes.hashCode() : 0;
            result = 31 * result + (lastIndex != null ? lastIndex.hashCode() : 0);
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            HelpRandomCache that = (HelpRandomCache) o;
            return !(videosIndexes != null ? !videosIndexes.equals(that.videosIndexes) : that.videosIndexes != null)
                    && !(lastIndex != null ? !lastIndex.equals(that.lastIndex) : that.lastIndex != null);
        }

        @Override
        public String toString() {
            return "HelpRandomCache{" +
                    "videosIndexes=" + videosIndexes +
                    ", lastIndex=" + lastIndex +
                    '}';
        }
    }
}