package ru.sberbank.uplatform;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ru.sberbank.uplatform.message.inline.InlineQuery;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.sbol.geolocations.GeoObject;
import ru.sberbank.uplatform.sbol.geolocations.Response;
import ru.sberbank.uplatform.sbol.api.Branch;
import ru.sberbank.uplatform.sbol.api.InfoBlock;
import ru.sberbank.uplatform.sbol.api.Quote;
import ru.sberbank.uplatform.sbol.api.QuoteType;
import ru.sberbank.uplatform.sbol.yahoo.Fields;
import ru.sberbank.uplatform.sbol.yahoo.YahooResponse;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.UnmarshalException;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

/**
 * Created by baryshnikov on 14.11.15.
 */
public class SbolRoutes extends RouteBuilder {
    //TODO: Merge to normal class
    public class ExchangeQuote {
        private Double value;
        private Double change;

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        public Double getChange() {
            return change;
        }

        public void setChange(Double change) {
            this.change = change;
        }

        public ExchangeQuote(Double value, Double change) {
            this.value = value;
            this.change = change;
        }

        public ExchangeQuote() {
        }
    }

    private DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
    final static Pattern currencyPattern = Pattern.compile("([0-9]+(\\.|,)?[0-9]*)\\s*(" + Currency.any.getPattern() + ").*?");

    public String yesterday() {
        return LocalDate.now().minusDays(1).format(formatter);
    }

    public String tomorrow() {
        return LocalDate.now().plusDays(1).format(formatter);
    }

    private QuoteType[] quotes_ids;
    private Map<String, Quote> quote_cache = new ConcurrentHashMap<>();
    private CurrencyManager currencyManager = new CurrencyManager();
    Type responseRootFormat = new TypeToken<Map<String, InfoBlock>>() {
    }.getType();

    private Fields sbrfCache;
    private Fields sbrfPCache;
    private Fields oilBrentCache;
    private Fields oilWTICache;

    @Value("${branches_limit}")
    private Integer branchesLimit;

    private final Type listOfBranches = new TypeToken<List<Branch>>() {
    }.getType();

    public SbolRoutes() {
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator('.');
        formatSymbols.setGroupingSeparator(' ');
    }

    @Override
    public void configure() throws Exception {
        /**
         * Here we requesting info from Sberbank Online API and caching it if needs
         */

        from("direct:init-quotes")
                .id("init-quotes")
                .setBody(simple("${properties:sbol_quotes_ids}"))
                .process(exchange -> {
                    quotes_ids = Arrays.stream(exchange.getIn().getBody(String.class).split(",")).map(String::trim).map(QuoteType::byID).filter((qt) -> qt != null).toArray(QuoteType[]::new);
                    exchange.getIn().setBody(quotes_ids);
                    exchange.getContext().startRoute("sbol-update-quotes");
                })
                .log("quotes: ${body}");


        from("direct:sbol-api-request-quotes")
                .id("sbol-api-request-quotes")
                .setHeader("CamelHttpMethod", constant("GET"))
                .setHeader("to", method(this, "tomorrow"))
                .setHeader("from", method(this, "yesterday"))
                .process(ex -> {
                    ex.getIn().setHeader("qid", ex.getIn().getHeader("quote", QuoteType.class).quoteId);
                    ex.setProperty("id", ex.getIn().getHeader("quote", QuoteType.class).id);
                })
                .setHeader("Exchange.HTTP_QUERY", simple("_date_ato114=${headers.to}&_date_afrom114=${headers.from}&display=json&period=on&inf_block=${headers.infoBlock}&qid[]=${headers.qid}"))
                .setBody(constant(""))
                .to("http:{{sbol_quotes_url}}?copyHeaders=false")
                .to("log:quotes-raw")
                .process(ex -> {
                    //Manual parsing due to complex root type
                    Map<String, InfoBlock> resp = new Gson().fromJson(new InputStreamReader(ex.getIn().getBody(InputStream.class)), responseRootFormat);
                    ex.getIn().setBody(resp);
                })
                .to("log:quotes");

        from("direct:sbol-request-quotes")
                .id("sbol-request-quotes")
                .process(exchange2 -> {
                    exchange2.getIn().setBody(quote_cache);
                    quote_cache.forEach(exchange2.getIn()::setHeader);
                });


        from("direct:sbol-request-branches")
                .id("sbol-request-branches")
                .setHeader("Exchange.HTTP_QUERY", method(this, "getUrlForNearestBranches"))
                .setBody(constant(""))
                .to("log:request-branches?showHeaders=true")
                .to("http:{{sbol_geo_url}}?copyHeaders=false&bridgeEndpoint=true")
                .process(ex -> {
                    try {
                        Response resp = (Response) JAXBContext.newInstance(Response.class)
                                .createUnmarshaller().unmarshal((InputStream) ex.getIn().getBody());
                        resp.getGeoObjects().getGeoObjects().forEach(geo -> geo.setDistance(calculateDistance(
                                resp.getLon(), resp.getLat(), geo.getCoordinate().getLongitude(), geo.getCoordinate().getLatitude())));
                        List<GeoObject> objects = resp.getGeoObjects().getGeoObjects()
                                .stream()
                                .filter(o -> o.getDistance() <= 5000) // todo filter on SBOL server by max_radius parameter doesn't work
                                .sorted((o1, o2) -> (int) (o2.getDistance() - o1.getDistance()))
                                .distinct().collect(Collectors.toList());
                        if (objects.isEmpty()) {
                            ex.setProperty("response", null);
                        } else {
                            resp.getGeoObjects().setGeoObjects(objects);
                            ex.getIn().setHeader("response", resp);
                            ex.setProperty("response", resp);
                            ex.getIn().setBody(resp.getGeoObjects().getGeoObjects());
                        }
                    } catch (UnmarshalException e) {
                        log.info("Doesn't find SBOL object");
                        ex.setProperty("response", null);
                    }
                });


        from("timer:sbol-update-quotes?period={{sbol_quotes_cache_interval}}")
                .id("sbol-update-quotes")
                .autoStartup(false)
                .process(exchange -> {
                    exchange.getIn().setBody(quotes_ids);
                })
                .split(body())
                .setHeader("quote", body())
                .process(exchange2 -> exchange2.getIn().setHeader("infoBlock", exchange2.getIn().getBody(QuoteType.class).infoBlock))
                .log("fetching ${headers.quote}")
                .to("direct:sbol-api-request-quotes")
                .process(exchange1 -> {
                    Map<String, InfoBlock> quotes = exchange1.getIn().getBody(Map.class);
                    quotes.forEach((qid, info) -> {
                        QuoteType qt = QuoteType.byID(exchange1.getProperty("id", String.class));
                        if (qt != null) {
                            quote_cache.put(qt.name(), info.getLastQuote());
                            Currency.findCurrency(qt).ifPresent((c) -> currencyManager.setPrice(c, info.getLastQuote()));
                        }
                    });
                })
                .end()
                .log("quotes cache updated");


        /**
         * Here we requesting oil price and SBRF quotes price and then caching
         */
        from("direct:init-stock")
                .id("init-stock")
                .process(exchange -> {
                    exchange.getContext().startRoute("update-stock");
                });

        from("timer://update-stock?period={{stock_cache_interval}}")
                .id("update-stock")
                .autoStartup(false)
                .to("http:{{oil_brent_url}}")
                .process(ex -> {
                    YahooResponse oilBrent = new Gson().fromJson(new InputStreamReader(ex.getIn().getBody(InputStream.class)), YahooResponse.class);
                    oilBrentCache = oilBrent.getList().getResources().get(0).getResource().getFields();
                })
                .end().log("Brent oil cache updated")
                .to("http:{{oil_wti_url}}")
                .process(ex -> {
                    YahooResponse oilWTI = new Gson().fromJson(new InputStreamReader(ex.getIn().getBody(InputStream.class)), YahooResponse.class);
                    oilWTICache = oilWTI.getList().getResources().get(0).getResource().getFields();
                })
                .end().log("WTI oil cache updated")
                .to("http:{{sbrf_akcii_url}}")
                .process(ex -> {
                    YahooResponse sbrf = new Gson().fromJson(new InputStreamReader(ex.getIn().getBody(InputStream.class)), YahooResponse.class);
                    sbrfCache = sbrf.getList().getResources().get(0).getResource().getFields();
                })
                .to("http:{{sbrf_p_akcii_url}}")
                .process(ex -> {
                    YahooResponse sbrfP = new Gson().fromJson(new InputStreamReader(ex.getIn().getBody(InputStream.class)), YahooResponse.class);
                    sbrfPCache = sbrfP.getList().getResources().get(0).getResource().getFields();
                })
                .end().log("SBRF cache updated");

        from("direct:request-oil")
                .id("request-oil")
                .process(exchange -> {
                    exchange.getIn().setHeader("oilBrent", oilBrentCache);
                    exchange.getIn().setHeader("oilWTI", oilWTICache);
                });

        from("direct:request-sbrf")
                .id("request-sbrf")
                .process(exchange -> {
                    exchange.getIn().setHeader("sbrf", sbrfCache);
                    exchange.getIn().setHeader("sbrfp", sbrfPCache);
                });

        from("direct:sbol-request-exchange-currency")
                .id("sbol-request-exchange-currency")
                .setBody(method(this, "hasCurrencyExchangeRequest"))
                .to("log:currency?showHeaders=true");
    }

    public String getUrlForNearestBranches(@ExchangeProperty("message") Message msg, @ExchangeProperty("mode") String mode) throws UnsupportedEncodingException {
        return "lat=" + msg.getLocation().getLatitude() + "&lon=" + msg.getLocation().getLongitude() + "&count=" + branchesLimit + "&" + mode + "=1";
    }

    public boolean hasCurrencyExchangeRequest(@ExchangeProperty("message") Message msg, Exchange exchange) {
        String query = null;

        if (msg != null && msg.getText() != null) {
            query = msg.getText();
        } else {
            InlineQuery inlineQuery = exchange.getProperty(InlineModeRoutes.INLINE_QUERY, InlineQuery.class);
            if (inlineQuery != null && inlineQuery.getQuery()!= null) {
                query = inlineQuery.getQuery();
            }
        }
        
        if (query == null) {
            return false;
        }
        
        
        Matcher matcher = currencyPattern.matcher(query.trim().replace(" ", "").toLowerCase());
        if (!matcher.find() || matcher.groupCount() < 2) {
            return false;
        }
        double value = 0d;
        try {
            value = Double.valueOf(matcher.group(1).replace(",", "."));
        } catch (NumberFormatException ignored) {
            return false;
        }
        Optional<Currency> currency = Currency.findCurrency(matcher.group(3).trim());
        if (!currency.isPresent()) {
            return false;
        }
        try {
            String unit = matcher.group(4).trim();
            if (unit.matches("(кг|килограм).*")) {
                value *= 1000;
            }
        } catch (IllegalStateException | IndexOutOfBoundsException | PatternSyntaxException ignored) {
        }

        final double k = value;
        exchange.getIn().setHeader("currency", currency.get());
        exchange.getIn().setHeader("amount", value);
        currencyManager.sellTable(currency.get()).forEach((c, v) -> exchange.getIn().setHeader(c.name(), k * v.getSell()));
        return true;
    }

    // calculate distance by Haversine formula
    public Double calculateDistance(double lonA, double latA, double lonB, double latB) {
        double dLat = Math.toRadians(latB - latA);
        double dLon = Math.toRadians(lonB - lonA);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(latA))
                * Math.cos(Math.toRadians(latB)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return c * 6378137.0; //Equatorial radius of earth
    }
}
