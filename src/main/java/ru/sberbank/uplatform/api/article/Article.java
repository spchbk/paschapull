package ru.sberbank.uplatform.api.article;

import java.util.Date;

/**
 * Created by baryshnikov on 27.12.15.
 */
public class Article extends ArticleBase {
    private Long id;
    private Long hits;
    private Date created;
    private String book;
    private Long page;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHits() {
        return hits;
    }

    public void setHits(Long hits) {
        this.hits = hits;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", hits=" + hits +
                ", created=" + created +
                ", book='" + book + '\'' +
                ", page=" + page +
                "} " + super.toString();
    }
}
