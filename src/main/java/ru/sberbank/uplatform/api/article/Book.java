package ru.sberbank.uplatform.api.article;

/**
 * Created by baryshnikov on 27.12.15.
 */
public class Book {
    private String book;
    private Long pages;


    public Long getPages() {
        return pages;
    }

    public void setPages(Long pages) {
        this.pages = pages;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "Book{" +
                "book='" + book + '\'' +
                ", pages=" + pages +
                '}';
    }
}
