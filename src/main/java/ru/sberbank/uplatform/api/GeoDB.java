package ru.sberbank.uplatform.api;


import ru.sberbank.uplatform.api.geo.GeoInfo;
import ru.sberbank.uplatform.api.geo.GeoObject;
import ru.sberbank.uplatform.api.geo.GeoObjectWithDistance;

import java.util.List;
import java.util.Optional;

/**
 * Geo tips service
 *
 * Created by baryshnikov on 11.12.15.
 */
public interface GeoDB {
    /**
     * Find objects by Earth coordinate in selected circle region with fixed radius and limited return count.
     * Result is sorted by distance. First is nearest
     *
     * @param latitude
     * @param longitude
     * @param radius    Max distance in meters
     * @param limit     Limits of items in response
     * @return Always array of objects. If error occurred returns empty (not null) list
     */
    List<GeoObjectWithDistance> findByLocation(double latitude,
                                               double longitude,
                                               double radius,
                                               int limit);

    /**
     * Find objects by tag (short text metainfo)
     *
     * @param tag   Short (about 255 chars) info
     * @param limit Limits of items in response
     * @param page  Page of results. (like offset = limit * page)
     * @return Always array of objects. If error occurred returns empty (not null) list
     */
    List<GeoObject> findByTag(String tag, int limit, int page);

    /**
     * Get all objects
     *
     * @param limit Limits of items in response
     * @param page  Page of results. (like offset = limit * page)
     * @return Always array of objects. If error occurred returns empty (not null) list
     */
    List<GeoObject> list(int limit, int page);

    /**
     * Get count of geo objects in backend
     */
    Optional<Long> count();

    /**
     * Get object by id
     *
     * @param id Unique object id, generated when it is created
     */
    Optional<GeoObject> get(long id);

    /**
     * Create new object. Each objects must have unique locations.
     * If title, text or tag is null, it would be replaced by empty strings
     */
    Optional<Long> create(GeoInfo baseInfo);

    /**
     * Set new information of object by object id
     *
     * @param id      Unique object id, generated when it is created
     * @param newInfo Full new information
     */
    boolean update(long id, GeoInfo newInfo);

    /**
     * Remove object by id.
     *
     * @param id Unique object id, generated when it is created
     * @return true if no network or backend error occurred. May return true if object is not exists.
     */
    boolean remove(long id);

}
