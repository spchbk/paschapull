package ru.sberbank.uplatform.api.geo;

/**
 * Created by baryshnikov on 12.12.15.
 */
public class GeoObjectWithDistance extends GeoObject {
    private Double distance;//in meters

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "GeoObjectWithDistance{" +
                "distance=" + distance +
                '}';
    }
}
