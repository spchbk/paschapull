package ru.sberbank.uplatform.api.geo;

/**
 * Created by baryshnikov on 11.12.15.
 */
public class GeoInfo {
    private double longitude;
    private double latitude;
    private String tag;
    private String title;
    private String text;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "GeoInfo{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", tag='" + tag + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
