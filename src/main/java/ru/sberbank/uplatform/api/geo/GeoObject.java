package ru.sberbank.uplatform.api.geo;

import java.util.Date;

/**
 * Created by baryshnikov on 11.12.15.
 */
public class GeoObject extends GeoInfo {
    private Long id;


    private Long selected;
    private Date created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getSelected() {
        return selected;
    }

    public void setSelected(Long selected) {
        this.selected = selected;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "GeoObject{" +
                "created=" + created +
                ", selected=" + selected +
                ", id=" + id +
                "} " + super.toString();
    }
}
