package ru.sberbank.uplatform.api;

import java.util.Optional;

/**
 * Service for keeping client state
 *
 * Created by baryshnikov on 02.12.15.
 */
public interface ClientsManager<T> {
    /**
     * Get saved state by client ID
     *
     * @param clientID unique client identity
     * @return Saved state or nothing
     */
    Optional<T> getState(String clientID, Class<T> clasz);

    /**
     * Save client state
     *
     * @param clientID unique client identity
     * @param state    object which represents client state
     * @return true if state was saved
     */
    boolean setState(String clientID, T state);
}
