package ru.sberbank.uplatform.api;

import ru.sberbank.uplatform.api.flower.Invite;
import ru.sberbank.uplatform.api.flower.Recall;
import ru.sberbank.uplatform.dbo.OrderBean;

import java.util.List;
import java.util.Optional;

/**
 * API interface of the flower service.
 *
 * @author Pavel Tarasov
 * @since 01/03/2016.
 */
public interface FlowerService {

    /**
     * Get list of the invites for flower's block.
     *
     * @return list of the invites or {@code null} if error occurred.
     */
    List<Invite> getInvites();

    /**
     * Get last flower's order for the user.
     *
     * @param userId    telegram user id.
     * @return order record wrapped on the {@link Optional}
     *         or {@link Optional#EMPTY} if error occurred.
     */
    Optional<OrderBean> getLastOrder(long userId);

    /**
     * Get user recall for the order.
     *
     * @param orderId   order id.
     * @return order recall wrapped on the {@link Optional}
     *         or {@link Optional#EMPTY} if error occurred.
     */
    Optional<Recall> getRecallByOrderId(String orderId);

    /**
     * Add recall for the order.
     *
     * @param recall   {@link Recall} recall object.
     * @return recall id wrapped on the {@link Optional}
     *         or {@link Optional#EMPTY} if error occurred.
     */
    Optional<Long> addRecall(Recall recall);

    /**
     * Update recall for the order.
     *
     * @param recall   {@link Recall} recall object.
     * @return {@code false} if error occurred {@code false} otherwise.
     */
    boolean updateRecall(Recall recall);
}