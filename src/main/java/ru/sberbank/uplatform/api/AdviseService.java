package ru.sberbank.uplatform.api;

import ru.sberbank.uplatform.api.article.Article;

import java.util.Optional;

/**
 * Special case of article service for facts
 * <p/>
 * Created by baryshnikov on 27.12.15.
 */
public interface AdviseService {
    /**
     * Get random advise
     *
     * @return Advise or nothing
     */
    Optional<Article> getRandomAdvise();
}
