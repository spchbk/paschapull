package ru.sberbank.uplatform.api;

import ru.sberbank.uplatform.faq.FaqResponse;

import java.util.Optional;

/**
 * Service for answers from FAQ knowledge base.
 *
 * @author Pavel Tarasov
 * @since 26/05/2016.
 */
public interface FaqService {

    /**
     * Search answer by specified phrase from FAQ knowledge base.
     * Used determining the similarity of ASCII alphanumeric text based on trigram matching,
     * as well as index operator classes that support fast searching for similar strings.
     *
     * @param phrase    the searching phrase.
     * @return {@link FaqResponse} object which contains text of the answer or
     *         {@link Optional#EMPTY} if error occurred or answer not found.
     */
    Optional<FaqResponse> search(String phrase);
}