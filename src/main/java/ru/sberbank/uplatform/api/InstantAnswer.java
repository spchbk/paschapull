package ru.sberbank.uplatform.api;

import java.util.Optional;

/**
 * Short answers service
 *
 * Created by baryshnikov on 28.11.15.
 */
public interface InstantAnswer {
    /**
     * Search answer for query like search engine with minimal delay
     *
     * @param query Any text
     * @return Search result or nothing if error or not found
     */
    Optional<String> search(String query);
}
