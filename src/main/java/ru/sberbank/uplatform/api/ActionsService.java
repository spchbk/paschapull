package ru.sberbank.uplatform.api;

import ru.sberbank.uplatform.api.action.Action;
import ru.sberbank.uplatform.api.action.ActionBase;

import java.util.List;
import java.util.Optional;

/**
 * API interface of the "Spasibo" actions service.
 *
 * @author Pavel Tarasov
 * @since 24/12/2015.
 */
public interface ActionsService {

    /**
     * Get list of the action records.
     *
     * @param limit    no more than specified count of rows will be returned.
     * @param offset   skip specified count of rows before beginning to return rows.
     * @return list of action records or empty list if error occurred.
     */
    List<Action> getActions(int limit, int offset);

    /**
     * Get random action record.
     *
     * @return random action record wrapped on the {@link Optional}
     *         or {@link Optional#EMPTY} if error occurred.
     */
    Optional<Action> getRandomAction();

    /**
     * Get action record by id.
     *
     * @param id   action record id.
     * @return action record wrapped on the {@link Optional}
     *         or {@link Optional#EMPTY} if error occurred.
     */
    Optional<Action> getAction(long id);

    /**
     * Create action record.
     *
     * @param action    {@link ActionBase} object containing basic information.
     * @return id of the created action record.
     */
    Optional<Long> createAction(ActionBase action);

    /**
     * Update action record.
     *
     * @param id       id of the action record that will be updated.
     * @param action   {@link ActionBase} object containing basic information.
     * @return {@code true} if record updated or not exists {@code false} otherwise.
     */
    boolean updateAction(long id, ActionBase action);

    /**
     * Remove action record.
     *
     * @param id   action record id.
     * @return {@code true} if record removed or not exists {@code false} otherwise.
     */
    boolean removeAction(long id);
}
