package ru.sberbank.uplatform.api;

import ru.sberbank.uplatform.api.article.Article;

import java.util.Optional;

/**
 * Special case of article service for facts
 * <p/>
 * Created by baryshnikov on 27.12.15.
 */
public interface FactService {

    /**
     * Get random fact
     *
     * @return Fact or nothing
     */
    Optional<Article> getRandomFact();
}
