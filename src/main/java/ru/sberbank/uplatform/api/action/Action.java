package ru.sberbank.uplatform.api.action;

import java.util.Date;

/**
 * @author Pavel Tarasov
 * @since 24/12/2015.
 */
public class Action {
    private Long id;
    private String image;
    private String title;
    private String period;
    private String profit;
    private Date created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    @Override
    public String toString() {
        return "Action{" +
                "id='" + id + '\'' +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", period='" + period + '\'' +
                ", profit='" + profit + '\'' +
                ", created='" + created + '\'' +
                '}';
    }
}
