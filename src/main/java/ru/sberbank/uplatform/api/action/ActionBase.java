package ru.sberbank.uplatform.api.action;

/**
 * @author Pavel Tarasov
 * @since 24/12/2015.
 */
public class ActionBase {
    private String image;
    private String title;
    private String period;
    private String profit;

    public String getImage() {
        return image;
    }

    public ActionBase setImage(String image) {
        this.image = image;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ActionBase setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getPeriod() {
        return period;
    }

    public ActionBase setPeriod(String period) {
        this.period = period;
        return this;
    }

    public String getProfit() {
        return profit;
    }

    public ActionBase setProfit(String profit) {
        this.profit = profit;
        return this;
    }

    @Override
    public String toString() {
        return "ActionBase{" +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", period='" + period + '\'' +
                ", profit='" + profit + '\'' +
                '}';
    }
}
