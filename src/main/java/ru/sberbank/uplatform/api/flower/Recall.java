package ru.sberbank.uplatform.api.flower;

import java.util.Date;

/**
 * @author Pavel Tarasov
 * @since 09/03/2016.
 */
public class Recall {
    /** Recall id. */
    private Long id;

    /** Telegram user id. */
    private Long user;

    /** Order id. */
    private String order;
    private String comment;
    private Integer rating;
    private Date created;

    public Recall(String order) {
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recall recall = (Recall) o;

        if (id != null ? !id.equals(recall.id) : recall.id != null) return false;
        if (user != null ? !user.equals(recall.user) : recall.user != null) return false;
        if (order != null ? !order.equals(recall.order) : recall.order != null) return false;
        if (comment != null ? !comment.equals(recall.comment) : recall.comment != null) return false;
        if (rating != null ? !rating.equals(recall.rating) : recall.rating != null) return false;
        return !(created != null ? !created.equals(recall.created) : recall.created != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (order != null ? order.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Recall{" +
                "id=" + id +
                ", user=" + user +
                ", order='" + order + '\'' +
                ", comment='" + comment + '\'' +
                ", rating=" + rating +
                ", created=" + created +
                '}';
    }
}
