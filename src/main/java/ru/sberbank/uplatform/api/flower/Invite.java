package ru.sberbank.uplatform.api.flower;

import java.util.Date;

/**
 * @author Pavel Tarasov
 * @since 01/03/2016.
 */
public class Invite extends BaseInvite {
    private Long id;
    private Integer code;
    private Integer status;
    private Date created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "Invite{" +
                "id=" + id +
                ", code=" + code +
                ", status=" + status +
                ", created=" + created +
                '}';
    }
}
