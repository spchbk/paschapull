package ru.sberbank.uplatform.api.flower;

/**
 * @author Pavel Tarasov
 * @since 01/03/2016.
 */
public class BaseInvite {
    private String username;
    private String comment;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "BaseInvite{" +
                "username='" + username + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
