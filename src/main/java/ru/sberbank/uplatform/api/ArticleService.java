package ru.sberbank.uplatform.api;

import ru.sberbank.uplatform.api.article.Article;
import ru.sberbank.uplatform.api.article.Book;

import java.util.List;
import java.util.Optional;

/**
 * Simple service which contains texts in groups (like facts and advices)
 * <p/>
 * Created by baryshnikov on 27.12.15.
 */
public interface ArticleService {

    /**
     * Get one page by unique ID
     *
     * @param id Unique page id
     * @return Page or nothing for error or not found exception
     */
    Optional<Article> getPage(long id);

    /**
     * Get one page by random (service-side random generator) ID
     *
     * @param book Collection name
     * @return Page or nothing for error or not found exception
     */
    Optional<Article> getRandomPage(String book);

    /**
     * Get list of pages in collection ordered by time
     *
     * @param book   Collection name
     * @param offset skips count
     * @param limit  return limit
     * @return List of articles or empty list (not null!) if something goes wrong
     */
    List<Article> getBookPages(String book, long offset, long limit);

    /**
     * Get all known books in system
     *
     * @return List of books or empty list (not null!) if something goes wrong
     */
    List<Book> getBooks();
}
