package ru.sberbank.uplatform.api;

import java.util.Optional;

/**
 * Uploaded files cache service
 *
 * Created by baryshnikov on 24.12.15.
 */
public interface UploadCache {

    /**
     * Get cached generated ID for recently uploaded filename
     *
     * @param filename Path to file
     * @return Last ID for this file or nothing
     */
    Optional<String> getUploadedId(String filename);

    /**
     * Set filenames and generated id to cache
     *
     * @param filename Path to file
     * @param id       Generated ID for this file
     */
    void setUploadId(String filename, String id);
}
