package ru.sberbank.uplatform;

import java.math.BigDecimal;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class VelocityExtension {

    public Double bankRound(Double value, int digits) {
        try {
            return new BigDecimal(value).setScale(digits, BigDecimal.ROUND_HALF_UP).doubleValue();
        } catch (Exception ex) {
            return Double.NaN;
        }
    }

    public Object coalesc(Object... args) {
        for (Object arg : args) {
            if (arg != null) return arg;
        }
        return null;
    }
}
