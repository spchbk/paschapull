package ru.sberbank.uplatform;

/**
 * Created by baryshnikov on 18.11.15.
 */
public enum Endpoints {
//@formatter:off
    CR_NEW_ADD("cr-add"),
    CR_NEW_ADD_START("cr-add-start"),
    CR_NEW_SUB_MENU("cr-menu"),
    CR_NEW_ADD_FINISH("cr-new-add-finish"),
    
    ADD_CONSUMPTION_OR_REVENUE("add-consumption-or-revenue"),
    ADD_CONSUMPTION_OR_REVENUE_DB("add-consumption-or-revenue-db"),
    
    GET_CONSUMPTION_OR_REVENUE("get-consumption-or-revenue"),
    GET_CONSUMPTION_OR_REVENUE_DB("get-consumption-or-revenue-db"),
    GET_CONSUMPTION_OR_REVENUE_MAX("get-consumption-or-revenue-max"),
    GET_CONSUMPTION_OR_REVENUE_ALL("get-consumption-or-revenue-all"),
    GET_CONSUMPTION_OR_REVENUE_STAT("get-consumption-or-revenue-stat"),
    GET_CONSUMPTION_PROCESS(),
    CONSUMPTION_OR_REVENUE_DEMO("consumption-or-revenue-demo"),
    GET_BALANCE("get-balance"),
    GET_BALANCE_DB("get-balance-db"),
    ADD_CATEGORY_CONSUMPTION_OR_REVENUE("add-category-consumption-revenue"),
    SHOW_AGREEMENT_CONSUMPTION_OR_REVENUE("show-agreement-category-consumption-or-revenue"),
    SHOW_AGREEMENT_CONSUMPTION_OR_REVENUE2("show-agreement-category-consumption-or-revenue2"),
    CHECK_CATEGORY_CONSUMPTION_OR_REVENUE("check-category-consumption-or-revenue"),
    CHECK_INIT_CONSUMPTION_OR_REVENUE("check-init-consumption-or-revenue"),
    CHECK_INIT_CONSUMPTION_OR_REVENUE2("check-init-consumption-or-revenue2"),
    CONSUMPTION_OR_REVENUE_FINISH_ADDING("consumption-or-revenue-finish-adding"),
    GET_CHART_DB("get-chart-db"),
    GET_CHART_INTERNAL("get-chart-internal"),

    CALLBACK_QUERY_UPDATE("callback-process-updates"),

    INLINE_PROCESS_UPDATE("inline-process-updates"),
    INLINE_INIT("inline-init"),
    INLINE_GET_CURRENCY("inline-get-currency"),
    INLINE_CURRENCY_EXCHANGE("inline-currency-exchange"),
    INLINE_ROUTER("inline-router"),

    
    PROCESS_UPDATE("seda", "process-updates"),
    CLIENT_ASK_ATM("direct", "client-ask-atm"),
    CLIENT_ASK_FILIAL("direct", "client-ask-filial"),
    RENDER_NEAREST_BRANCH("direct", "render-nearest-branch"),
    INSTANT_SEARCH("direct", "instant-search"),
    NANOSEMANTICS_ASK("direct", "nanosemantics-ask"),
    FAQ_SEARCH("direct", "faq-search"),
    SITE("direct", "get-site"),
    HELP("direct", "get-help"),
    HELP_INIT("direct", "init-help"),
    HELP_FACT("direct", "get-help-fact"),
    HELP_RECOMMENDATION("direct", "get-help-recommendation"),
    HELP_HOTLINE("direct", "get-help-hotline"),
    HELP_VIDEO("direct", "get-help-video"),
    ADD_TEXT_REPLY("direct", "add-text-reply"),
    ADD_LOCATION_REPLY("direct", "add-location-reply"),
    ADD_VIDEO_REPLY("direct", "add-video-reply"),
    ADD_PHOTO_REPLY("direct", "add-photo-reply"),
    ADD_CHAT_ACTION_REPLY("direct", "add-chat-action-reply"),
    NOTIFICATION("direct", "notification"),
    SPASIBO("direct", "get-spasibo"),
    SPASIBO_INIT("direct", "init-spasibo"),
    SPASIBO_NEW("direct", "get-spasibo-new"),
    SPASIBO_ACTIONS("direct", "get-spasibo-actions"),
    SPASIBO_PARTNERS("direct", "get-spasibo-partners"),
    SBOL_CURRENCY_CONVERTER("direct", "get-currency-exchange"),
    CBR_CURRENCY_CONVERTER("direct", "get-cbr-currency-exchange"),
    ROUTER("direct", "router"),
    SOMETHING_STRANGE("direct", "get-something-strange"),
    NOT_FOUND_LOCATION("direct", "not-found-location"),
    GEO_TIP("direct", "geo-tip"),
    GEO_TIP_COMMENT("direct", "geo-tip-comment"),
    GEO_TIP_RECORD("direct", "geo-tip-record"),
    GEO_TIP_RATING("direct", "geo-tip-rating"),

    RENDER_TEMPLATE("render-template"),
    FIND_ENDPOINT_BY_CONTENT,
    
    LIVE_TEX_ADD("livetex-add"),
    LIVE_TEX_GET_UPDATES("livetex-get-updates"),
    LIVE_TEX_GET_FILE("livetex-get-file"),
    LIVE_TEX_SEND_MESSAGE("livetex-send-message"),
    LIVE_TEX_SEND_STICKER("livetex-send-sticker"),
    LIVE_TEX_DOWNLOAD_FILE("livetex-download-file");
//@formatter:on

    //TODO private
    public final String type, id;
    public final String endpoint;

    Endpoints() {
        this.type = "direct";
        this.id = name();
        this.endpoint = type + ':' + id;
    }
    
    /** "direct" route */
    Endpoints(String id) {
        this("direct", id);
    }
    
    Endpoints(String type, String id) {
        this.type = type;
        this.id = id;
        this.endpoint = type + ':' + id;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
