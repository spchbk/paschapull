package ru.sberbank.uplatform;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.api.ClientsManager;
import ru.sberbank.uplatform.dbo.Dialog;
import ru.sberbank.uplatform.message.Message;

import java.util.Optional;

/**
 * Created by baryshnikov on 14.11.15.
 */
public class DialogRoutes extends RouteBuilder {

    @Autowired
    private ClientsManager<Client> clients;

    @Override
    public void configure() throws Exception {
        /*
        Here we have to find dialogs from persistent storage or create new (new client).
        Also, each saved dialog may have a destination (aka next) route.
        Otherwise direct:new-dialog will be used

        This routes operates with:
        exchange:
            expected: chat_id
            provides: dialog

        there are no depends for headers or body
         */

        from("direct:get-dialog")
                .id("get-dialog")
                .log("searching dialogs...")
                .process(ex -> {
                    Message msg = ex.getProperty("message", Message.class);
                    Optional<Client> client = clients.getState(msg.getFrom().getId().toString(), Client.class);
                    ex.setProperty("client", client.orElseGet(() -> new Client(ClientState.IDLE)));
                });

        from("direct:save-dialog")
                .id("save-dialog")
                .log("${headers.client}")
                .process(ex -> {
                    Message msg = ex.getProperty("message", Message.class);
                    Client cl = ex.getProperty("client", Client.class);
                    String userId = ex.getProperty("userId", String.class);
                    if (userId == null || userId.isEmpty()) {
                        userId = msg.getFrom().getId().toString();
                    }
                    ex.getIn().setHeader("result", clients.setState(userId, cl));
                })
                .choice().when(header("result")).log("dialog saved")
                .otherwise().log("dialog NOT saved").end();
    }
}
