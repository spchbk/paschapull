package ru.sberbank.uplatform.article;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.APIError;
import ru.sberbank.uplatform.util.Utils;
import ru.sberbank.uplatform.api.AdviseService;
import ru.sberbank.uplatform.api.ArticleService;
import ru.sberbank.uplatform.api.FactService;
import ru.sberbank.uplatform.api.article.Article;
import ru.sberbank.uplatform.api.article.Book;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by baryshnikov on 27.12.15.
 */
public class RestArticleService implements ArticleService, FactService, AdviseService {
    public static final String FACT_BOOK = "fact", ADVISE_BOOK = "advise", NEWS = "news";
    private Logger logger = LoggerFactory.getLogger(RestArticleService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
    private Type articleListType = new TypeToken<List<Article>>() {
    }.getType();
    private Type bookListType = new TypeToken<List<Book>>() {
    }.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${article_url}")
    private String serviceUrl;


    @Override
    public Optional<Article> getPage(long id) {
        HttpGet get = new HttpGet(serviceUrl + "/" + String.valueOf(id));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Article.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get article by id: '" + String.valueOf(id) + "'", ex);
            return Optional.<Article>empty();
        }
    }

    @Override
    public Optional<Article> getRandomPage(String book) {
        HttpGet get = new HttpGet(serviceUrl + "/" + Utils.encode(book) + "/random");
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Article.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get random article from: '" + book + "'", ex);
            return Optional.<Article>empty();
        }
    }

    @Override
    public List<Article> getBookPages(String book, long offset, long limit) {
        HttpGet get = new HttpGet(serviceUrl + "/" + Utils.encode(book) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), articleListType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of articles", ex);
            return Collections.<Article>emptyList();
        }
    }

    @Override
    public List<Book> getBooks() {
        HttpGet get = new HttpGet(serviceUrl);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), bookListType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of articles", ex);
            return Collections.<Book>emptyList();
        }
    }

    //Special

    @Override
    public Optional<Article> getRandomAdvise() {
        return getRandomPage(ADVISE_BOOK);
    }

    @Override
    public Optional<Article> getRandomFact() {
        return getRandomPage(FACT_BOOK);
    }


    //For non-spring systems


    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }
}
