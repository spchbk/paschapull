package ru.sberbank.uplatform.action;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.APIError;
import ru.sberbank.uplatform.api.ActionsService;
import ru.sberbank.uplatform.api.action.Action;
import ru.sberbank.uplatform.api.action.ActionBase;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Pavel Tarasov
 * @since 24/12/2015.
 */
public class RestActionService implements ActionsService {
    private Logger logger = LoggerFactory.getLogger(RestActionService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
    private Type listType = new TypeToken<List<Action>>(){}.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${actions_url}")
    private String serviceUrl;

    @Override
    public List<Action> getActions(int limit, int offset) {
        HttpGet get = new HttpGet(serviceUrl + "/actions?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of actions", ex);
            return Collections.<Action>emptyList();
        }
    }

    @Override
    public Optional<Action> getRandomAction() {
        HttpGet get = new HttpGet(serviceUrl + "/actions?rand=true");
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                List<Action> result = gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
                return Optional.of(result.get(0));
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of actions", ex);
            return Optional.<Action>empty();
        }
    }

    @Override
    public Optional<Action> getAction(long id) {
        HttpGet get = new HttpGet(serviceUrl + "/action/" + String.valueOf(id));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Action.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get action by id: '" + String.valueOf(id) + "'", ex);
            return Optional.<Action>empty();
        }
    }

    @Override
    public Optional<Long> createAction(ActionBase action) {
        HttpPost post = new HttpPost(serviceUrl + "/action");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(action)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to create action", ex);
            return Optional.<Long>empty();
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Response.class).getId());
            }
        } catch (Exception ex) {
            logger.error("Failed to create action", ex);
            return Optional.<Long>empty();
        }
    }

    @Override
    public boolean updateAction(long id, ActionBase action) {
        HttpPut put = new HttpPut(serviceUrl + "/action/" + String.valueOf(id));
        put.setHeader("Content-type", "application/json");
        try {
            put.setEntity(new StringEntity(new Gson().toJson(action)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to update action by id: '" + String.valueOf(id) + "'", ex);
            return false;
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(put)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to update action by id: '" + String.valueOf(id) + "'", ex);
            return false;
        }
    }

    @Override
    public boolean removeAction(long id) {
        HttpDelete delete = new HttpDelete(serviceUrl + "/action/" + String.valueOf(id));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(delete)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to remove action by id: '" + String.valueOf(id) + "'", ex);
            return false;
        }
    }
}
