package ru.sberbank.uplatform.action;

/**
 * @author Pavel Tarasov
 * @since 24/12/2015.
 */
public class Response {
    private boolean success;
    private Long id;

    public boolean isSuccess() {
        return success;
    }

    public Long getId() {
        return id;
    }
}
