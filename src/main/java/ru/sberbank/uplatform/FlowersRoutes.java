package ru.sberbank.uplatform;

import org.apache.camel.ExchangeProperty;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.api.flower.Invite;
import ru.sberbank.uplatform.api.flower.Recall;
import ru.sberbank.uplatform.commerce.flowers.ExternalFlowerService;
import ru.sberbank.uplatform.commerce.flowers.RestFlowerService;
import ru.sberbank.uplatform.commerce.flowers.api.Flower;
import ru.sberbank.uplatform.commerce.flowers.api.FlowerOrder;
import ru.sberbank.uplatform.commerce.flowers.api.OrderResponse;
import ru.sberbank.uplatform.dbo.OrderBean;
import ru.sberbank.uplatform.message.Message;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Pavel Tarasov
 * @since 24/02/2016.
 */
public class FlowersRoutes extends RouteBuilder {
    @Autowired
    private ExternalFlowerService service;
    @Autowired
    private RestFlowerService flowerService;
    private List<Invite> invites = new ArrayList<>();
    private Optional<List<Flower>> flowers = Optional.empty();
    private Map<Long, FlowerOrder> orders = new HashMap<>();
    private Map<Long, Recall> recalls = new HashMap<>();


    @Override
    public void configure() throws Exception {
        from("direct:init-flowers")
                .id("init-flowers")
                .process(ex -> {
                    ex.getContext().startRoute("flowers-update-data");
                    ex.getContext().startRoute("flower-update-invites");
                });

        from("timer:flowers-update-data?period={{flowers_update_interval}}")
                .id("flowers-update-data")
                .autoStartup(false)
                .process(ex -> ex.getIn().setHeader("flowers", service.getFlowers(Optional.of("москва"))))
                .to("direct:flower-update-db")
                .log("Flowers price updated!");

        from("timer:flower-update-invites?period={{flowers_invites_update_interval}}")
                .id("flower-update-invites")
                .autoStartup(false)
                .process(ex -> {
                    List<Invite> newInvites = flowerService.getInvites();
                    if (newInvites != null) {
                        invites = newInvites;
                    }
                })
                .log("Flower invites updated");

        from("direct:update-flowers")
                .id("update-flowers")
                .process(ex -> {
                    @SuppressWarnings("unchecked")
                    Optional<List<Flower>> flowersTemp = (Optional<List<Flower>>) ex.getIn().getHeader("flowers");
                    updateFlowers(flowersTemp);
                });

        from("direct:init-flowers-booking")
                .id("init-flowers-booking")
                .choice()
                .when(method(this, "checkInvite"))
                    .process(ex -> {
                        orders.remove(ex.getProperty("message", Message.class).getFrom().getId());
                        ex.getProperty("client", Client.class).setState(ClientState.FLOWERS_INFO);
                    })
                    .setHeader("template", constant("templates/flower-init.vm"))
                    .setHeader("keyboard", constant("flowers_main_menu"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .otherwise()
                    .setHeader("template", constant("templates/flower-beta.vm"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .end();

        from(ClientState.FLOWERS_INFO.endpoint)
                .id(ClientState.FLOWERS_INFO.getId())
                .choice()
                .when(method(this, "checkCancel"))
                    .process(ex -> {
                        ex.getProperty("client", Client.class).setState(ClientState.IDLE);
                        orders.remove(ex.getProperty("message", Message.class).getFrom().getId());
                    })
                    .to(ClientRoutes.epNewClient)
                .otherwise()
                    .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.FLOWERS_VIEW))
                    .setHeader("template", constant("templates/flower-info.vm"))
                    .setHeader("keyboard", constant("flowers_main_menu"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .end();

        from(ClientState.FLOWERS_VIEW.endpoint)
                .id(ClientState.FLOWERS_VIEW.getId())
                .choice()
                .when(method(this, "isNextStep"))
                    .process(ex -> {
                        ex.getProperty("client", Client.class).setState(ClientState.FLOWERS_CHOICE);
                        Optional<Flower> flower = getFirstFlower();
                        ex.getIn().setHeader("flower", flower.get());
                        ex.getIn().setHeader("num", 1);
                        long userId = ex.getProperty("message", Message.class).getFrom().getId();
                        FlowerOrder order = new FlowerOrder()
                                .setUserId(userId)
                                .setFlower(flower.get());
                        orders.put(userId, order);
                    })
                    .setHeader("template", constant("templates/flower.vm"))
                    .setHeader("keyboard", constant("flowers_menu"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                    .setHeader("template", constant("templates/flower-price.vm"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .otherwise()
                    .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE))
                    .to(ClientRoutes.epNewClient)
                .end();

        from(ClientState.FLOWERS_CHOICE.endpoint)
                .id(ClientState.FLOWERS_CHOICE.getId())
                .choice()
                .when(method(this, "isNextStep"))
                    .to("direct:flowers-phone")
                .when(method(this, "isNextFlower"))
                    .to("direct:flowers-next")
                .otherwise()
                    .process(ex -> {
                        ex.getProperty("client", Client.class).setState(ClientState.IDLE);
                        orders.remove(ex.getProperty("message", Message.class).getFrom().getId());
                    })
                    .to(ClientRoutes.epNewClient)
                .end();

        from("direct:flowers-next")
                .id("flowers-next")
                .process(ex -> {
                    Message msg = ex.getProperty("message", Message.class);
                    long userId = msg.getFrom().getId();
                    int number = getFlowerNumber(msg.getText());
                    if (number != -1) {
                        Flower flower = flowers.get().get(number);
                        ex.getIn().setHeader("flower", flower);
                        ex.getIn().setHeader("num", number + 1);
                        orders.get(userId).setFlower(flower);
                    }
                })
                .setHeader("template", constant("templates/flower.vm"))
                .setHeader("keyboard", constant("flowers_menu"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .setHeader("template", constant("templates/flower-price.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from("direct:flowers-phone")
                .id("flowers-phone")
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.FLOWERS_PHONE_RECEIVER))
                .setHeader("template", constant("templates/flower-phone.vm"))
                .setHeader("keyboard", constant("cancel_menu"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(ClientState.FLOWERS_PHONE_RECEIVER.endpoint)
                .id(ClientState.FLOWERS_PHONE_RECEIVER.getId())
                .choice()
                .when(method(this, "checkPhone"))
                    .process(ex -> {
                        Message msg = ex.getProperty("message", Message.class);
                        ex.getProperty("client", Client.class).setState(ClientState.FLOWERS_ADDRESS);
                        orders.get(msg.getFrom().getId()).setPhone(getPhone(msg));
                    })
                    .setHeader("template", constant("templates/flower-phone-receiver.vm"))
                    .setHeader("keyboard", constant("flowers_repeat_menu"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .when(method(this, "checkCancel"))
                    .process(ex -> {
                        ex.getProperty("client", Client.class).setState(ClientState.IDLE);
                        orders.remove(ex.getProperty("message", Message.class).getFrom().getId());
                    })
                    .to(ClientRoutes.epNewClient)
                .otherwise()
                    .to("direct:bad-phone-format")
                .end();

        from(ClientState.FLOWERS_ADDRESS.endpoint)
                .id(ClientState.FLOWERS_ADDRESS.getId())
                .choice()
                .when(method(this, "checkPhone"))
                    .process(ex -> {
                        Message msg = ex.getProperty("message", Message.class);
                        ex.getProperty("client", Client.class).setState(ClientState.FLOWERS_BOOKING);
                        orders.get(msg.getFrom().getId()).setReceiverPhone(getPhone(msg));
                    })
                    .setHeader("template", constant("templates/flower-address.vm"))
                    .setHeader("keyboard", constant("cancel_menu"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .when(method(this, "checkCommandRepeat"))
                    .process(ex -> {
                        Message msg = ex.getProperty("message", Message.class);
                        ex.getProperty("client", Client.class).setState(ClientState.FLOWERS_BOOKING);
                        FlowerOrder order = orders.get(msg.getFrom().getId());
                        order.setReceiverPhone(order.getPhone());
                    })
                    .setHeader("template", constant("templates/flower-address.vm"))
                    .setHeader("keyboard", constant("cancel_menu"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .when(method(this, "checkCancel"))
                    .process(ex -> {
                        ex.getProperty("client", Client.class).setState(ClientState.IDLE);
                        orders.remove(ex.getProperty("message", Message.class).getFrom().getId());
                    })
                    .to(ClientRoutes.epNewClient)
                .otherwise()
                    .to("direct:bad-phone-format")
                .end();

        from(ClientState.FLOWERS_BOOKING.endpoint)
                .id(ClientState.FLOWERS_BOOKING.getId())
                .choice()
                .when(method(this, "checkCancel"))
                    .process(ex -> {
                        ex.getProperty("client", Client.class).setState(ClientState.IDLE);
                        orders.remove(ex.getProperty("message", Message.class).getFrom().getId());
                    })
                    .to(ClientRoutes.epNewClient)
                .otherwise()
                    .process(ex -> {
                        Message msg = ex.getProperty("message", Message.class);
                        long userId = msg.getFrom().getId();
                        orders.get(userId).setReceiverAddress(msg.getText());
                        ex.getIn().setHeader("order", orders.get(userId));
                        ex.getIn().setHeader("result", orders.get(userId).getFlower().getPrice().add(BigDecimal.valueOf(500)));
                        ex.getIn().setHeader("num", flowers.get().indexOf(orders.get(userId).getFlower()) + 1);
                    })
                    .setHeader("template", constant("templates/flower-booking.vm"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                    .to("direct:flower-order")
                .end();

        from("direct:flower-order")
                .id("flower-order")
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.FLOWER_AGREE_BOOKING))
                .setHeader("template", constant("templates/flower-order.vm"))
                .setHeader("keyboard", constant("flowers_agree_menu"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(ClientState.FLOWER_AGREE_BOOKING.endpoint)
                .id(ClientState.FLOWER_AGREE_BOOKING.getId())
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE))
                .choice()
                .when(method(this, "isNextStep"))
                    .process(ex -> {
                        FlowerOrder order = orders.get(ex.getProperty("message", Message.class).getFrom().getId());
                        OrderBean orderBean = new OrderBean(order);
                        boolean status = false;
                        String orderId = "-";
                        Optional<OrderResponse> response = service.sendOrder(order);
                        if (response.isPresent()) {
                            status = (response.get().getStatus() == 1);
                            orderId = status ? response.get().getOrderId() : orderId;
                        }
                        orderBean.setOrderId(orderId);
                        orderBean.setOrderStatus(status);
                        ex.getIn().setHeader("orderBean", Optional.of(orderBean));
                        ex.getIn().setHeader("order", orderId);
                    })
                    .to("direct:flower-order-db")
                    .setHeader("template", constant("templates/flower-accept.vm"))
                    .setHeader("keyboard", constant("main_menu"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .otherwise()
                    .process(ex -> orders.remove(ex.getProperty("message", Message.class).getFrom().getId()))
                    .to(ClientRoutes.epNewClient)
                .end();

        from("direct:bad-phone-format")
                .id("bad-phone-format")
                .setBody(simple("Не верный формат введенного телефонного номера. Введите номер еще раз."))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from("direct:init-flowers-recall")
                .id("init-flowers-recall")
                .choice()
                .when(method(this, "checkInvite"))
                    .process(ex -> {
                        Optional<OrderBean> order = flowerService.getLastOrder(ex.getProperty("message", Message.class).getFrom().getId());
                        if (order.isPresent()) {
                            ex.getIn().setHeader("order", order.get().getOrderId());
                            ex.getProperty("client", Client.class).setState(ClientState.FLOWER_RECALL);
                        }
                    })
                    .choice()
                    .when(header("order"))
                        .process(ex -> recalls.put(ex.getProperty("message", Message.class).getFrom().getId(),
                                new Recall(ex.getIn().getHeader("order", String.class))))
                        .setHeader("template", constant("templates/flower-recall.vm"))
                        .to("direct:render-template")
                        .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                    .otherwise()
                        .setBody(simple("Для оценки вам нужно сделать хотя бы один заказ."))
                        .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                    .endChoice()
                .otherwise()
                    .setHeader("template", constant("templates/flower-beta.vm"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .end();

        from(ClientState.FLOWER_RECALL.endpoint)
                .id(ClientState.FLOWER_RECALL.getId())
                .process(ex -> {
                    ex.getProperty("client", Client.class).setState(ClientState.FLOWER_RATING);
                    Message msg = ex.getProperty("message", Message.class);
                    recalls.get(msg.getFrom().getId()).setComment(msg.getText());
                })
                .setHeader("keyboard", constant("rating_menu"))
                .setBody(simple("Поставьте оценку."))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(ClientState.FLOWER_RATING.endpoint)
                .id(ClientState.FLOWER_RATING.getId())
                .process(ex -> {
                    ex.getProperty("client", Client.class).setState(ClientState.IDLE);
                    Message msg = ex.getProperty("message", Message.class);
                    Recall recall = recalls.remove(msg.getFrom().getId());
                    recall.setRating(getRating(msg.getText()));
                    Optional<Recall> rec = flowerService.getRecallByOrderId(recall.getOrder());
                    if (rec.isPresent()) {
                        flowerService.updateRecall(recall);
                    } else {
                        flowerService.addRecall(recall);
                    }
                })
                .setHeader("keyboard", constant("main_menu"))
                .setBody(simple("Спасибо, Ваш отзыв принят."))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);
    }

    private synchronized void updateFlowers(Optional<List<Flower>> flowers) {
        if (flowers.isPresent()) {
            this.flowers = flowers;
        }
    }

    private Optional<Flower> getFirstFlower() {
        return flowers.isPresent() ? Optional.of(flowers.get().get(0)) : Optional.empty();
    }

    private Optional<Flower> getNextFlower(int current) {
        Optional<Flower> flower = flowers.get().stream().filter(f -> f.getLocalId() > current).findFirst();
        return flower.isPresent() ? flower : getFirstFlower();
    }

    private int getFlowerNumber(String text) {
        text = text.replaceAll(".*№", "");
        try {
            return Integer.parseInt(text.trim()) - 1;
        } catch (Exception e) {
            return -1;
        }
    }

    public boolean checkInvite(@ExchangeProperty("message") Message msg) {
        return invites != null && !invites.isEmpty()
                && invites.stream().filter(i -> i.getUsername().equals(msg.getFrom().getUsername())).findFirst().isPresent();
    }

    public boolean isNextStep(@ExchangeProperty("message") Message msg) {
        return msg.getText() != null && (msg.getText().toLowerCase().equals("✅ далее")
                || msg.getText().toLowerCase().equals("✅ согласен(а)"));
    }

    public boolean isNextFlower(@ExchangeProperty("message") Message msg) {
        return msg.getText() != null && msg.getText().matches("(\uD83C\uDF37|\uD83D\uDC90|\uD83C\uDF39|)\\s+№\\d");
    }

    public boolean checkPhone(@ExchangeProperty("message") Message msg) {
        if (msg.getText() != null) {
            return clearPhone(msg.getText()).matches("\\d{11}");
        }
        if (msg.getContact() != null) {
            String phone = msg.getContact().getPhoneNumber();
            return phone != null && clearPhone(phone).matches("\\d{11}");
        }
        return false;
    }

    public boolean checkCommandRepeat(@ExchangeProperty("message") Message msg) {
        return msg.getText() != null && msg.getText().trim().replaceAll(" ", "").toLowerCase().matches("такойже");
    }

    public boolean checkCancel(@ExchangeProperty("message") Message msg) {
        return (msg.getText() != null && msg.getText().trim().toLowerCase().equals("отмена"));
    }

    private String getPhone(Message msg) {
        String text = msg.getText();
        return (text != null) ? clearPhone(text) : clearPhone(msg.getContact().getPhoneNumber());
    }

    private String clearPhone(String phone) {
        return phone.trim().replaceAll("\\+|-|\\.|,|\\)|\\(|\\s+", "");
    }

    private int getRating(String text) {
        int rating = 0;
        Pattern p = Pattern.compile("⭐");
        Matcher m = p.matcher(text);
        while(m.find()) rating++;
        return rating;
    }
}
