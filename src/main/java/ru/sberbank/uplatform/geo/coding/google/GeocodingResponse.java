package ru.sberbank.uplatform.geo.coding.google;

import java.util.Arrays;

public class GeocodingResponse {

	private GeocodingResult[] results;
	private Status status;

	public GeocodingResult[] getResults() {
		return results;
	}

	public void setResults(GeocodingResult[] results) {
		this.results = results;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return String.format("GeocodingResponse [results=%s, status=%s]", Arrays.toString(results), status);
	}

}
