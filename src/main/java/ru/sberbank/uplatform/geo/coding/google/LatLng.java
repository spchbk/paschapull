package ru.sberbank.uplatform.geo.coding.google;

public class LatLng {

	private Float lat;
	private Float lng;

	public Float getLat() {
		return lat;
	}

	public void setLat(Float lat) {
		this.lat = lat;
	}

	public Float getLng() {
		return lng;
	}

	public void setLng(Float lng) {
		this.lng = lng;
	}

	@Override
	public String toString() {
		return String.format("LatLng [lat=%s, lng=%s]", lat, lng);
	}

}
