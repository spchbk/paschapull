package ru.sberbank.uplatform.geo.coding;

import ru.sberbank.uplatform.message.Location;

/**
 * Geocoding is the process of converting addresses (like
 * "1600 Amphitheatre Parkway, Mountain View, CA") into geographic coordinates
 * (like latitude 37.423021 and longitude -122.083739).
 * 
 * @author Dmitriy_Gulyaev
 *
 */
public interface IGeocodingService {

    Location geocodeAddress(String address);
}
