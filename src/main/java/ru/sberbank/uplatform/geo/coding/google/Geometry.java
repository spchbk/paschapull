package ru.sberbank.uplatform.geo.coding.google;

import com.google.gson.annotations.SerializedName;

public class Geometry {

	private LatLng location;

	@SerializedName("location_type")
	private LocationType locationType;

	public LatLng getLocation() {
		return location;
	}

	public void setLocation(LatLng location) {
		this.location = location;
	}

	public LocationType getLocationType() {
		return locationType;
	}

	public void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}

	@Override
	public String toString() {
		return String.format("Geometry [location=%s, \n\t\tlocationType=%s]", location, getLocationType());
	}

}
