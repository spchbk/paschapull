package ru.sberbank.uplatform.geo.coding.google;

import com.google.gson.annotations.SerializedName;

public class GeocodingResult {

    /**
     * String containing the human-readable address of this location. Often this
     * address is equivalent to the "postal address," which sometimes differs
     * from country to country.
     */
    @SerializedName("formatted_address")
    private String formattedAddress;
    private Geometry geometry;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    @Override
    public String toString() {
        return String.format("\nGeocodingResult [formattedAddress=%s, \n\t\tgeometry=%s]\n", formattedAddress,
                geometry);
    }

}
