package ru.sberbank.uplatform.geo.coding.google;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;

import ru.sberbank.uplatform.geo.coding.IGeocodingService;
import ru.sberbank.uplatform.message.Location;

/**
 * Using of Google Maps Geocoding API to traslate manual-typed address to
 * lat\lon.
 * 
 * @see <a href=
 *      "https://developers.google.com/maps/documentation/geocoding/intro">
 *      Google Maps Geocoding API</a>
 * 
 * @author Dmitriy_Gulyaev
 * @since 15-Apr-2016
 */
public class GoogleMapsGeocodingService implements IGeocodingService {

    private static final Logger logger = LoggerFactory.getLogger(GoogleMapsGeocodingService.class);

    @Value("${geocode_api_url}")
    private String geocodeApiUrl;

    private Gson gson = new Gson();

    @Override
    public Location geocodeAddress(String address) {

        if (address == null || address.isEmpty()) {
            logger.warn("Empty address string.");
            return null;
        }

        // Disable geocoding if no url in config file.
        if (geocodeApiUrl == null || geocodeApiUrl.isEmpty()) {
            return null;
        }

        try {

            URL url = new URL(geocodeApiUrl + URLEncoder.encode(address, "UTF-8"));

            GeocodingResponse geocodingResponse;
            try (InputStream inputStream = url.openStream()) {
                Reader inputStreamReader = new InputStreamReader(inputStream);
                geocodingResponse = gson.fromJson(inputStreamReader, GeocodingResponse.class);
            }

            if (geocodingResponse.getStatus() != Status.OK) {
                logger.warn("Response for address '{}' = {}", address, geocodingResponse);
                return null;
            }

            if (logger.isTraceEnabled()) {
                logger.trace(geocodingResponse.toString());
            }

            return filter(geocodingResponse);
        } catch (Exception e) {
            logger.error("Error during invoke of Google Maps Geocoding API service", e);
            e.printStackTrace();
        }

        return null;
    }

    private Location filter(GeocodingResponse geocodingResponse) {
        // Not found;
        if (geocodingResponse == null || geocodingResponse.getResults().length == 0) {
            return null;
        }

        if (geocodingResponse.getResults().length == 1) {
            return extractLocation(geocodingResponse.getResults()[0]);
        } else {
            // Too ambiguous address in query.
            if (logger.isDebugEnabled()) {
                logger.debug("Too many ({}) results in response.", geocodingResponse.getResults().length);
            }
        }

        return null;
    }

    private Location extractLocation(GeocodingResult geocodingResult) {
        LatLng latLng = geocodingResult.getGeometry().getLocation();
        Location location = new Location();
        location.setLatitude(latLng.getLat());
        location.setLongitude(latLng.getLng());
        return location;
    }

}
