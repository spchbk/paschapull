package ru.sberbank.uplatform.geo.coding.google;

/**
 * The "status" field within the Geocoding response object contains the status
 * of the request, and may contain debugging information to help you track down
 * why geocoding is not working.
 * 
 * @author Dmitriy_Gulyaev
 *
 */

public enum Status {

    /**
     * Indicates that no errors occurred; the address was successfully parsed
     * and at least one geocode was returned.
     */
    OK,

    /**
     * Indicates that the geocode was successful but returned no results. This
     * may occur if the geocoder was passed a non-existent address.
     */
    ZERO_RESULTS,

    /** Indicates that you are over your quota. */
    OVER_QUERY_LIMIT,

    /** Indicates that your request was denied. */
    REQUEST_DENIED,

    /**
     * Generally indicates that the query (address, components or latlng) is
     * missing.
     */
    INVALID_REQUEST,

    /**
     * Indicates that the request could not be processed due to a server error.
     * The request may succeed if you try again.
     */
    UNKNOWN_ERROR
}
