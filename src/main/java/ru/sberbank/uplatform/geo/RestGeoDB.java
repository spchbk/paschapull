package ru.sberbank.uplatform.geo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.Consts;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.APIError;
import ru.sberbank.uplatform.api.geo.GeoInfo;
import ru.sberbank.uplatform.api.geo.GeoObject;
import ru.sberbank.uplatform.api.geo.GeoObjectWithDistance;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by baryshnikov on 12.12.15.
 */
public class RestGeoDB implements ru.sberbank.uplatform.api.GeoDB {

    static final ContentType TEXT_PLAIN = ContentType.create("text/plain", Consts.UTF_8);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();

    class Response {
        private boolean success;
        private Long id;

        public boolean isSuccess() {
            return success;
        }

        public Long getId() {
            return id;
        }

    }

    @Value("${geodb_url}")
    private String apiUrl;
    private Logger logger = LoggerFactory.getLogger(RestGeoDB.class);

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<GeoObjectWithDistance> findByLocation(double latitude, double longitude, double radius, int limit) {
        HttpGet get = new HttpGet(apiUrl + "/by-location?" +
                "limit=" + String.valueOf(limit) +
                "&r=" + String.valueOf(radius) +
                "&lat=" + String.valueOf(latitude) +
                "&lon=" + String.valueOf(longitude));

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), (new TypeToken<List<GeoObjectWithDistance>>() {
                }).getType());
            }
        } catch (Exception ex) {
            logger.error("Failed list geo objects", ex);
            return new ArrayList<>();
        }
    }

    @Override
    public List<GeoObject> findByTag(String tag, int limit, int page) {
        HttpGet get = new HttpGet(apiUrl + "/by-tag?limit=" + String.valueOf(limit) + "&page=" + String.valueOf(page) + "&tag=" + encode(tag));

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), (new TypeToken<List<GeoObject>>() {
                }).getType());
            }
        } catch (Exception ex) {
            logger.error("Failed list geo objects", ex);
            return new ArrayList<>();
        }
    }

    @Override
    public List<GeoObject> list(int limit, int page) {
        HttpGet get = new HttpGet(apiUrl + "/list?limit=" + String.valueOf(limit) + "&page=" + String.valueOf(page));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), (new TypeToken<List<GeoObject>>() {
                }).getType());
            }
        } catch (Exception ex) {
            logger.error("Failed list geo objects", ex);
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<Long> count() {
        HttpGet get = new HttpGet(apiUrl + "/count");

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Long.class));
            }
        } catch (Exception ex) {
            logger.error("Failed get count objects", ex);
            return Optional.empty();
        }
    }

    @Override
    public Optional<GeoObject> get(long id) {
        HttpGet get = new HttpGet(apiUrl + "/object/" + String.valueOf(id));

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), GeoObject.class));
            }
        } catch (Exception ex) {
            logger.error("Failed get geo object " + String.valueOf(id), ex);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Long> create(GeoInfo baseInfo) {
        HttpPost post = new HttpPost(apiUrl + "/object");
        MultipartEntityBuilder holder = MultipartEntityBuilder.create();
        holder.addTextBody("latitude", String.valueOf(baseInfo.getLatitude()), TEXT_PLAIN);
        holder.addTextBody("longitude", String.valueOf(baseInfo.getLongitude()), TEXT_PLAIN);
        if (baseInfo.getTag() != null) holder.addTextBody("tag", baseInfo.getTag(), TEXT_PLAIN);
        if (baseInfo.getText() != null) holder.addTextBody("text", baseInfo.getText(), TEXT_PLAIN);
        if (baseInfo.getTitle() != null) holder.addTextBody("title", baseInfo.getTitle(), TEXT_PLAIN);
        holder.setContentType(ContentType.MULTIPART_FORM_DATA);
        post.setEntity(holder.build());
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Response.class).getId());
            }
        } catch (Exception ex) {
            logger.error("Failed create geo object", ex);
            return Optional.empty();
        }
    }

    @Override
    public boolean update(long id, GeoInfo newInfo) {
        HttpPut post = new HttpPut(apiUrl + "/object/" + String.valueOf(id));
        MultipartEntityBuilder holder = MultipartEntityBuilder.create();
        holder.addTextBody("latitude", String.valueOf(newInfo.getLatitude()), TEXT_PLAIN);
        holder.addTextBody("longitude", String.valueOf(newInfo.getLongitude()), TEXT_PLAIN);
        if (newInfo.getTag() != null) holder.addTextBody("tag", newInfo.getTag(), TEXT_PLAIN);
        if (newInfo.getText() != null) holder.addTextBody("text", newInfo.getText(), TEXT_PLAIN);
        if (newInfo.getTitle() != null) holder.addTextBody("titile", newInfo.getTitle(), TEXT_PLAIN);
        holder.setContentType(ContentType.MULTIPART_FORM_DATA);
        post.setEntity(holder.build());
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), Response.class).isSuccess();
            }
        } catch (Exception ex) {
            logger.error("Failed update geo object", ex);
            return false;
        }
    }

    @Override
    public boolean remove(long id) {
        HttpDelete get = new HttpDelete(apiUrl + "/object/" + String.valueOf(id));

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed remove geo object " + String.valueOf(id), ex);
            return false;
        }
    }

    private String encode(String text) {
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch (Throwable t) {
            return "";
        }
    }


}
