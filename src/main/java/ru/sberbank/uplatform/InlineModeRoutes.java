package ru.sberbank.uplatform;

import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.Header;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ru.sberbank.uplatform.message.inline.*;
import ru.sberbank.uplatform.message.inline.input.InputTextMessageContent;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResultArticle;
import ru.sberbank.uplatform.telegram.API;
import ru.sberbank.uplatform.util.Utils;

/**
 * Interaction in <a href="https://core.telegram.org/bots/inline">inline mode</a>.
 * 
 * @author Dmitriy_Gulyaev
 * @since 03-mar-2016
 *
 */
public class InlineModeRoutes extends RouteBuilder {
    public final static String INLINE_QUERY = "inlineQuery";
    private final static String INLINE_QUERY_RESULT = "inlineQueryResult";
    private final static String INLINE_QUERY_CONFIG = "inlineQueryConfig";
    private final static String INLINE_QUERY_ROUTE = "inlineQueryRoute";
    private final static String ENDPOINT = "endpoint";

    private final static Pattern PLACE_HOLDER = Pattern.compile("\\$\\{(\\w+)}");

    private Map<String, InlineQueryConfig> selectors;

    @Autowired
    private API bot;

    @Override
    public void configure() throws Exception {

        from(Endpoints.INLINE_INIT.endpoint)
            .id(Endpoints.INLINE_INIT.id)
            .setHeader("file", simple("${properties:routes_inline_file}"))
            .bean(this, "loadRoutesInlineSelectors");

        from(Endpoints.CALLBACK_QUERY_UPDATE.endpoint)
                .id(Endpoints.CALLBACK_QUERY_UPDATE.id)
                .process(ex -> {
                    CallbackQuery query = ex.getProperty("callbackQuery", CallbackQuery.class);
                    log.info("CALLBACK MODE: " + query.toString());
                    AnswerCallbackQuery answer = new AnswerCallbackQuery();
                    answer.setCallbackQueryId(query.getId());
                    answer.setText("Stub. Data value: " + query.getData());
                    bot.answerCallbackQuery(answer);
                });

        from(Endpoints.INLINE_PROCESS_UPDATE.endpoint)
                .id(Endpoints.INLINE_PROCESS_UPDATE.id)
                .process(ex -> ex.setProperty(INLINE_QUERY_RESULT, new InlineQueryResultArticle()))
                .doTry().to(Endpoints.INLINE_ROUTER.endpoint)
                .doCatch(Throwable.class).to("log:panic?showAll=true");

        from(Endpoints.INLINE_ROUTER.endpoint)
            .id(Endpoints.INLINE_ROUTER.id)
            .process(this::findEndpointByContent)
            .choice()
                .when(header(ENDPOINT))
                    .recipientList(header(ENDPOINT))
                    .end()
                .otherwise()
                .end()
                    
            .log("after")
            .to("direct:send-reply-inline");

        from(Endpoints.INLINE_GET_CURRENCY.endpoint)
            .id(Endpoints.INLINE_GET_CURRENCY.id)
            .to("direct:sbol-request-quotes")
            .setHeader("template", constant("templates/currency.vm"))
            .to("direct:render-template")
            .bean(this, "addTextReply");

        from(Endpoints.INLINE_CURRENCY_EXCHANGE.endpoint)
            .id(Endpoints.INLINE_CURRENCY_EXCHANGE.id)
            .to("direct:sbol-request-exchange-currency")
            .choice()
                .when(body())
                    .setHeader("template", constant("templates/exchange.vm"))
                    .to("direct:render-template")
                    .bean(this, "addTextReply")
                .end();

        from("direct:send-reply-inline")
            .id("send-reply-inline")
            .bean(this,"prepareAndSendInlineQueryResponse")
            .setBody(constant(""));
    }

    public void prepareAndSendInlineQueryResponse(Exchange exchange) {
        InlineQuery inlineQuery = exchange.getProperty(INLINE_QUERY, InlineQuery.class);
        InlineQueryConfig inlineQueryConfig = exchange.getProperty(INLINE_QUERY_CONFIG, InlineQueryConfig.class);

        AnswerInlineQuery inlineQueryResponse = new AnswerInlineQuery();
        inlineQueryResponse.setInlineQueryId(inlineQuery.getId());

        if (inlineQueryConfig != null) {
            inlineQueryResponse.setCacheTime(inlineQueryConfig.getCacheTime());

            InlineQueryResultArticle result = exchange.getProperty(INLINE_QUERY_RESULT, InlineQueryResultArticle.class);
            result.setId("1");
            result.setTitle(inlineQueryConfig.getTitle());
            result.setDescription(format(inlineQueryConfig.getDescription(), exchange));
            result.setUrl(inlineQueryConfig.getUrl());
            inlineQueryResponse.getResults().add(result);
        }

        bot.answerInlineQuery(inlineQueryResponse);
    }

    public void addTextReply(Exchange ex,
            @ExchangeProperty(INLINE_QUERY_RESULT) InlineQueryResultArticle inlineQueryResult, @Body String body) {
        inlineQueryResult.setInputMessageContent(new InputTextMessageContent(body));
    }

    private void findEndpointByContent(Exchange exchange) {
        InlineQuery inlineQuery = exchange.getProperty(INLINE_QUERY, InlineQuery.class);
        String query0 = inlineQuery.getQuery().toLowerCase().trim();

        if (Utils.isNumeric(query0)) {
            query0 = query0 + " rub";
            inlineQuery.setQuery(query0);
        }
        String query = query0;

        System.out.println("inline query: " + query);
        Optional<Map.Entry<String, InlineQueryConfig>> first = selectors.entrySet().stream().filter((entry) -> {
            return Arrays.stream(entry.getValue().getCompiledPattern()).anyMatch((p) -> p.matcher(query).matches());
        }).findFirst();

        if (first.isPresent()) {
            InlineQueryConfig inlineQueryConfig = first.get().getValue();
            exchange.setProperty(INLINE_QUERY_CONFIG, inlineQueryConfig);
            exchange.setProperty(INLINE_QUERY_ROUTE, first.get().getKey());
            exchange.getIn().setHeader("endpoint", first.get().getKey());
        }
    }

    public void loadRoutesInlineSelectors(@Header("file") String filename) throws Exception {
        Map<String, InlineQueryConfig> rules;

        Type configType = new TypeToken<Map<String, InlineQueryConfig>>() {
        }.getType();

        try (FileReader reader = new FileReader(filename)) {
            rules = new Gson().fromJson(reader, configType);
        }
        selectors = new ConcurrentHashMap<>();

        for (String key : rules.keySet()) {
            List<Pattern> tempCompiledPatterns = new ArrayList<>();

            InlineQueryConfig inlineQueryConfig = rules.get(key);

            if (Endpoints.INLINE_CURRENCY_EXCHANGE.endpoint.equals(key)) {
                Pattern pattern = Pattern.compile(Currency.getExchangeAnyPattern());
                tempCompiledPatterns.add(pattern);
            }

            tempCompiledPatterns.addAll(inlineQueryConfig.getPattern().stream().map(Pattern::compile).collect(Collectors.toList()));
            inlineQueryConfig.setCompiledPattern(new Pattern[tempCompiledPatterns.size()]);
            tempCompiledPatterns.toArray(inlineQueryConfig.getCompiledPattern());
            selectors.put(key, inlineQueryConfig);
        }
    }

    private String format(String format, Exchange exchange) {
        String result = format;

        if (format.indexOf('{') != -1) {
            Matcher matcher = PLACE_HOLDER.matcher(format);
            while (matcher.find()) {
                String key0 = matcher.group(0);
                String key1 = matcher.group(1);
                Object value = exchange.getIn().getHeader(key1);
                if (value != null) {
                    int p;
                    while ((p = result.indexOf(key0)) != -1) {
                        result = result.substring(0, p) + value.toString() + result.substring(p + key0.length());
                    }
                }
            }
        }

        return result;
    }
}