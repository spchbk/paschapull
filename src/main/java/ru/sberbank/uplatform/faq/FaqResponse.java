package ru.sberbank.uplatform.faq;

/**
 * @author Pavel Tarasov
 * @since 26/05/2016.
 */
public class FaqResponse {
    private Boolean ok;
    private String answer;
    private Double rating;

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FaqResponse that = (FaqResponse) o;
        return !(ok != null ? !ok.equals(that.ok) : that.ok != null)
                && !(answer != null ? !answer.equals(that.answer) : that.answer != null)
                && !(rating != null ? !rating.equals(that.rating) : that.rating != null);
    }

    @Override
    public int hashCode() {
        int result = ok != null ? ok.hashCode() : 0;
        result = 31 * result + (answer != null ? answer.hashCode() : 0);
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FaqResponse{" +
                "ok=" + ok +
                ", answer='" + answer + '\'' +
                ", rating=" + rating +
                '}';
    }
}