package ru.sberbank.uplatform.faq;

import com.google.gson.Gson;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.APIError;
import ru.sberbank.uplatform.api.FaqService;
import ru.sberbank.uplatform.util.Utils;

import java.util.Optional;

/**
 * @author Pavel Tarasov
 * @since 26/05/2016.
 */
public class RestFaqService implements FaqService {
    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;
    private Logger logger = LoggerFactory.getLogger(RestFaqService.class);

    @Override
    public Optional<FaqResponse> search(String phrase) {
        HttpGet get = new HttpGet("http://127.0.0.1:9019/search?phrase=" + Utils.encode(phrase));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }

                FaqResponse resp = new Gson().fromJson(EntityUtils.toString(response.getEntity()), FaqResponse.class);
                return resp.getOk() ? Optional.of(resp) : Optional.<FaqResponse>empty();
            }
        } catch (Exception ex) {
            logger.error("Failed get FAQ answer for " + phrase);
            return Optional.<FaqResponse>empty();
        }
    }
}