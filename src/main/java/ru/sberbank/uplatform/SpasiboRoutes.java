package ru.sberbank.uplatform;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.Header;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.api.ActionsService;
import ru.sberbank.uplatform.api.action.Action;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.spasibo.SpasiboEngine;
import ru.sberbank.uplatform.spasibo.SpasiboState;
import ru.sberbank.uplatform.spasibo.api.APIError;
import ru.sberbank.uplatform.spasibo.api.PartnerLocation;
import ru.sberbank.uplatform.spasibo.request.ActionRequest;
import ru.sberbank.uplatform.spasibo.request.PartnersLocationRequest;
import ru.sberbank.uplatform.spasibo.response.Response;
import ru.sberbank.uplatform.spasibo.response.SpasiboResponse;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Pavel Tarasov
 * @since 25/11/2015.
 */
public class SpasiboRoutes extends RouteBuilder {
    @Autowired
    private SpasiboEngine engine;
    @Autowired
    private ActionsService actionsService;


    @Override
    public void configure() throws Exception {
        from(Endpoints.SPASIBO.endpoint)
                .id(Endpoints.SPASIBO.id)
                .setHeader("template", constant("templates/spasibo.vm"))
                .setHeader("keyboard", constant("spasibo_menu"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.SPASIBO_NEW.endpoint)
                .id(Endpoints.SPASIBO_NEW.id)
                .setHeader("template", constant("templates/spasibo-new.vm"))
                .setHeader("keyboard", constant("spasibo_menu"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.SPASIBO_ACTIONS.endpoint)
                .id(Endpoints.SPASIBO_ACTIONS.id)
                .process(ex -> {
                    Optional<Action> action = actionsService.getRandomAction();
                    action.ifPresent(a -> ex.getIn().setHeader("action", a));
                })
                .setHeader("template", constant("templates/spasibo-actions.vm"))
                .setHeader("keyboard", constant("spasibo_menu"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.SPASIBO_PARTNERS.endpoint)
                .id(Endpoints.SPASIBO_PARTNERS.id)
                .setHeader("mode", constant("partner"))
                .process(ex -> ex.getProperty("client", Client.class).setState(SpasiboState.FIND_PARTNERS.getEndpoint()))
                .to("direct:get-location");

//        from(Endpoints.SPASIBO_ACTIONS.endpoint)
//                .id(Endpoints.SPASIBO_ACTIONS.id)
//                .setHeader("mode", constant("action"))
//                .process(ex -> ex.getProperty("client", Client.class).setState(SpasiboState.FIND_ACTIONS.getEndpoint()))
//                .to("direct:get-location");

        from("direct:get-location")
                .id("get-location")
                .setHeader("template", simple("templates/help${headers.mode}s.vm"))
                .setHeader("keyboard", constant("spasibo_menu"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(SpasiboState.FIND_PARTNERS.getEndpoint())
                .id(SpasiboState.FIND_PARTNERS.getId())
                .choice()
                .when(simple("${exchangeProperty.message.location} == null"))
                    .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE))
                    .to(Endpoints.ROUTER.endpoint)
                .otherwise()
                    .setHeader("template", simple("templates/thankpartners.vm"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                    .setHeader("hasActions", constant(false))
                    .to("direct:find-partners")
                .choice()
                .when(simple("${exchangeProperty.count} == 0"))
                    .to(Endpoints.NOT_FOUND_LOCATION.endpoint)
                .otherwise()
                    .loop(header("count")).to("direct:add-partner")
                .end()
                .setHeader("template", simple("templates/location-footer.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE));

        from("direct:add-partner")
                .id("add-partner")
                .setHeader("partner", method(this, "getPartner"))
                .setHeader("template", simple("templates/spasibo-partners.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .setHeader("longitude", method(this, "getLon"))
                .setHeader("latitude", method(this, "getLat"))
                .to(Endpoints.ADD_LOCATION_REPLY.endpoint);

        from("direct:find-partners")
                .id("find-partners")
                .process(ex -> {
                    boolean hasActions = ex.getIn().getHeader("hasActions", Boolean.class);
                    List<PartnerLocation> partners = findPartnersByLocation(ex, hasActions);
                    Collections.reverse(partners);
                    ex.getIn().setBody(partners);
                    ex.getIn().setHeader("partners", partners);
                    ex.getIn().setHeader("count", partners.size());
                });


//        from(SpasiboState.FIND_ACTIONS.getEndpoint())
//                .id(SpasiboState.FIND_ACTIONS.getId())
//                .choice().when(simple("${exchangeProperty.message.location} == null")).to(Endpoints.SOMETHING_STRANGE.endpoint)
//                .otherwise()
//                .setHeader("hasActions", constant(true))
//                .to("direct:find-partners")
//                .process(ex -> {
//                    List<Action> actions = new ArrayList<>();
//                    List<PartnerLocation> partners = ex.getIn().getBody(List.class);
//                    partners.forEach(part -> actions.addAll(actions_cache.stream()
//                            .filter(a -> a.getPartner().getId().equals(part.getId()))
//                            .collect(Collectors.toList())));
//                    ex.getIn().setHeader("actions", actions);
//                })
//                .setHeader("template", constant("templates/spasibo-actions.vm"))
//                .setHeader("keyboard", constant("spasibo_menu"))
//                .to("direct:render-template")
//                .to(Endpoints.ADD_TEXT_REPLY.endpoint);
    }

    public List<PartnerLocation> findPartnersByLocation(Exchange ex, boolean withActions) {
        try {
            Message msg = (Message)ex.getProperty("message");
            PartnersLocationRequest request = new PartnersLocationRequest()
                    .setLocation(msg.getLocation().getLatitude().toString(), msg.getLocation().getLongitude().toString())
                    .setDistance(3000)
                    .setPageSize(10);
            if (withActions) {
                request.setHasActions();
            }
            Response<SpasiboResponse<PartnerLocation>> resp = engine.sendRequest(request);
            List<PartnerLocation> result = resp.getResponse().getResults().stream()
                    .filter(distinctByKey(p -> p.getPartner().getId())).limit(5).collect(Collectors.toList());
            ex.setProperty("count", result.size());
            return result;
        } catch (APIError | IOException e) {
            log.error("Find Spasibo partners exception", e);
            return Collections.emptyList();
        }
    }

    public List<ru.sberbank.uplatform.spasibo.api.Action> getActions() {
        try {
            List<ru.sberbank.uplatform.spasibo.api.Action> actions = new ArrayList<>();
            ActionRequest request = new ActionRequest();
            while (true) {
                Response<SpasiboResponse<ru.sberbank.uplatform.spasibo.api.Action>> resp = engine.sendRequest(request);
                actions.addAll(resp.getResponse().getResults());
                String newPath = resp.getResponse().getNext();
                if (newPath == null || newPath.isEmpty()) {
                    break;
                }
                request.setPath(newPath);
            }
            return actions;
        } catch (APIError | IOException e) {
            log.error("Actions cache update exception", e);
            return Collections.emptyList();
        }
    }

    public Double getLon(@Header("partners") List<PartnerLocation> partners, @ExchangeProperty("CamelLoopIndex") int index) {
        return Double.valueOf(partners.get(index).getLocation().split(",")[1]);
    }

    public Double getLat(@Header("partners") List<PartnerLocation> partners, @ExchangeProperty("CamelLoopIndex") int index) {
        return Double.valueOf(partners.get(index).getLocation().split(",")[0]);
    }

    public PartnerLocation getPartner(@Header("partners") List<PartnerLocation> partners, @ExchangeProperty("CamelLoopIndex") int index) {
        return partners.get(index);
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T,Object> keyExtractor) {
        Map<Object,Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
