package ru.sberbank.uplatform;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import ru.sberbank.uplatform.geo.coding.IGeocodingService;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.ChatActionReply;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.messenger.MessengerAPI;
import ru.sberbank.uplatform.spasibo.SpasiboState;
import ru.sberbank.uplatform.telegram.API;
import ru.sberbank.uplatform.util.Utils;

/**
 * Main Bot routes of communication with the Telegram server.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class TelegramRoutes extends RouteBuilder {

    /** Implementation of the methods of Telegram Bot API. */
    @Autowired
    private API bot;

    @Autowired
    private MessengerAPI messengerSendAPI;

    /** Manager of the user states. */
    @Autowired
    private StatesManager statesManager;

    /** Geo coding service. */
    @Autowired
    private IGeocodingService geocodingService;

    /** Limits the number of updates to be retrieved. */
    @Value("${updates_limit}")
    private Integer limit;

    @Value("${queue_ahc_ws_endpoint}")
    private String queueAHCWSEndpoint;

    @Override
    public void configure() throws Exception {

        from("timer://get-updates?period={{updates_interval}}")
                .id("incoming-updates")
                .autoStartup(false)
                .process(ex -> ex.getIn().setBody(bot.getUpdates(limit)))
                .split(body()).streaming()
                .to(Endpoints.PROCESS_UPDATE.endpoint + "?waitForTaskToComplete=Never");

        if (!messengerSendAPI.isDisabled()) {
            from(queueAHCWSEndpoint)
                .process(ex -> ex.getIn().setBody(messengerSendAPI.jsonToUpdate(ex.getIn().getBody())))
                .split(body()).streaming()
                .to(Endpoints.PROCESS_UPDATE.endpoint + "?waitForTaskToComplete=Never");

            // Пинг (1) инициализирует соединение, (2) восстанавливает его при обрыве.
            // Без пинга, а именно без инициализации соединения, маршрут queueAHCWSEndpoint работать не будет.
            from("timer://queue-websocket-ping?period=60000")
                .log("queue-websocket-ping")
                .process(ex -> {ex.getIn().setBody("ping-body");})
                .to(queueAHCWSEndpoint);
        }

        from(Endpoints.PROCESS_UPDATE.endpoint + "?concurrentConsumers=16")
                .id(Endpoints.PROCESS_UPDATE.id)
                .choice()
                .when(body().isNull()).log("TRASH message")
                .when(simple("${body.inlineQuery} != null"))
                    .setProperty("inlineQuery", simple("${body.inlineQuery}"))
                    .to(Endpoints.INLINE_PROCESS_UPDATE.endpoint)
                .when(simple("${body.callbackQuery} != null"))
                    .setProperty("callbackQuery", simple("${body.callbackQuery}"))
                    .to(Endpoints.CALLBACK_QUERY_UPDATE.endpoint)
                .when(simple("${body.message} == null")).log("Bad message")
                .when(simple("${body.message.leftChatMember}")).log("control message")
                .otherwise()
                    .setProperty("message", simple("${body.message}"))
                    .setProperty("chat_id", simple("${body.message.chat.id}"))
                    .setProperty("update_id", simple("${body.updateId}"))
                .end()
                .process(ex -> ex.setProperty("replies", new ArrayList<Reply>()))
                .to(Endpoints.ADD_CHAT_ACTION_REPLY.endpoint)
                .transform(simple("${body.message}"))
                .doTry()
                    .log("Body: ${body}")
                    .to("direct:get-dialog")
                    .to(Endpoints.ROUTER.endpoint)
                .doCatch(Throwable.class).to("log:panic?showAll=true");

        from(Endpoints.ROUTER.endpoint)
                .id(Endpoints.ROUTER.id)
                .process(ex -> {
                    Message msg = ex.getProperty("message", Message.class);
                    Client client = ex.getProperty("client", Client.class);
                    if (statesManager.hasRouteForClient(client)) {
                        @SuppressWarnings("unchecked")
                        List<Reply> replies = ex.getProperty("replies", List.class);
                        if (!statesManager.route(client, msg, replies)) {
                            ex.getIn().setHeader("endpoint", Endpoints.SOMETHING_STRANGE.endpoint);
                        }
                    } else {
                        if (msg.getText() != null && client.isIdleState()) {
                            if (Utils.isNumeric(msg.getText())) {
                                ex.setProperty("numText", true);
                                msg.setText(msg.getText() + " rub");
                            }
                        }
//                        todo: Currently disabled because must be agreement with Google (uncomment or delete in future)
//                        checkLocation(client, msg);
                        ex.getIn().setHeader("endpoint", client.getState());
                    }
                })
                .choice()
                .when(header("endpoint"))
                    .recipientList(header("endpoint"))
                    .endChoice()
                .otherwise()
                    .log("used states route")
                .end()
                .log("message routed. sending reply: ${body}")
                .to("direct:send-reply")
                .log("saving dialog")
                .to("direct:save-dialog");

        from("direct:send-reply")
                .id("send-reply")
                .removeHeader("branches")
                .removeHeader("branch")
                .setBody(exchangeProperty("replies"))
                .choice().when(exchangeProperty("sent").isEqualTo(true)).log("already sent").endChoice()
                .otherwise().process((ex) -> ex.setProperty("sent", true)) //FIXME: This is workaround to prevent double sending
                .removeProperty("replies")
                .split(body()).streaming()
                .log("sending reply ${body}")
                .process((exchange) -> {
                    Reply reply = exchange.getIn().getBody(Reply.class);
                    reply.setChatID(exchange.getProperty("chat_id", Long.class));
                    Integer botType = reply.getBotType();
                    exchange.setProperty("bot-type", botType);
                    switch (botType) {
                        case BotType.FB:
                            messengerSendAPI.send(reply);
                            break;
                        case BotType.TELEGRAM:
                        default:
                            bot.send(reply);
                    }
                })
                .choice().when(bodyAs(Reply.class).isInstanceOf(ChatActionReply.class)).endChoice()
                .otherwise().to("direct:create-db-record").end()
                .end() //split
                .setBody(constant(""));

        from("direct:send-notification")
                .id("send-notification")
                .setBody(exchangeProperty("replies"))
                .removeProperty("replies")
                .split(body()).streaming()
                .process(ex -> {
                    Reply reply = ex.getIn().getBody(Reply.class);
                    reply.setChatID(ex.getIn().getHeader("chat", Long.class));
                    ex.setProperty("client", new Client(ClientState.NEWS));
                    ex.setProperty("userId", reply.getChatID().toString());
                    log.info("Send notification to user: " + reply.getChatID());
                    bot.send(reply);
                })
                .to("direct:save-dialog")
                .end()
                .setBody(constant(""));
    }

    /**
     * If expect location in message, but location is absent - try to geocode
     * not-empty address in message to location.
     *
     * @param client
     * @param message
     */
    @SuppressWarnings("unused")
    private void checkLocation(Client client, Message message) {
        String clientState = client.getState();
        if (clientState.equals(ClientState.WANT_ATM.getEndpoint()) ||
                clientState.equals(ClientState.WANT_FILIAL.getEndpoint()) ||
                clientState.equals(SpasiboState.FIND_PARTNERS.getEndpoint()))
        {
            if (message.getLocation() == null && message.getText() != null) {
                message.setLocation(geocodingService.geocodeAddress(message.getText()));
            }
        }
    }
}