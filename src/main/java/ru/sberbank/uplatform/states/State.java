package ru.sberbank.uplatform.states;

import ru.sberbank.uplatform.Client;
import ru.sberbank.uplatform.From;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.Reply;

import java.util.List;

/**
 * Created by baryshnikov on 02.12.15.
 */
public interface State {
    String getName();

    String onMessage(Client client, final Message message, List<Reply> replies) throws UnexpectedMessage, Exception;
}
