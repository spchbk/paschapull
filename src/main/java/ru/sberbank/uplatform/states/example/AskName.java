package ru.sberbank.uplatform.states.example;

import ru.sberbank.uplatform.Client;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.message.reply.TextReply;
import ru.sberbank.uplatform.states.State;
import ru.sberbank.uplatform.states.UnexpectedMessage;

import java.util.List;
import java.util.Optional;

/**
 * Created by baryshnikov on 02.12.15.
 */
public class AskName implements State {
    @Override
    public String getName() {
        return AskName.class.getCanonicalName();
    }

    @Override
    public String onMessage(Client client, Message message, List<Reply> replies) throws UnexpectedMessage, Exception {
        Optional<Context> ctx = client.getContent(Context.class);
        if (!ctx.isPresent()) throw new UnexpectedMessage();

        ctx.get().setName(message.getText());
        //Do not forget save context again
        client.setContent(ctx.get());
        replies.add(new TextReply("Ok " + ctx.get().getName() + ", and when are born?"));
        return AskYear.class.getCanonicalName();
    }
}
