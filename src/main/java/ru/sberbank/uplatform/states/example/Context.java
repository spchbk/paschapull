package ru.sberbank.uplatform.states.example;

/**
 * Created by baryshnikov on 02.12.15.
 */
public class Context {
    private String name;
    private Integer year;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
