package ru.sberbank.uplatform.states.example;

import ru.sberbank.uplatform.Client;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.message.reply.TextReply;
import ru.sberbank.uplatform.states.State;
import ru.sberbank.uplatform.states.UnexpectedMessage;

import java.util.List;

/**
 * Created by baryshnikov on 02.12.15.
 */
public class Hello implements State {

    @Override
    public String getName() {
        //It can be used if object is a singleton
        return Hello.class.getCanonicalName();
    }

    @Override
    public String onMessage(Client client, Message message, List<Reply> replies) throws UnexpectedMessage, Exception {
        replies.add(new TextReply("What is your name?"));
        //Never save variables in class. Because next route may be called on other bot instance
        client.setContent(new Context());
        return AskName.class.getCanonicalName(); //Go to next singleton
    }
}
