package ru.sberbank.uplatform.states.example;

import ru.sberbank.uplatform.Client;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.message.reply.TextReply;
import ru.sberbank.uplatform.states.State;
import ru.sberbank.uplatform.states.UnexpectedMessage;

import java.util.*;

/**
 * Created by baryshnikov on 02.12.15.
 */
public class AskYear implements State {
    @Override
    public String getName() {
        return AskYear.class.getCanonicalName();
    }

    @Override
    public String onMessage(Client client, Message message, List<Reply> replies) throws UnexpectedMessage, Exception {
        Optional<Context> ctx = client.getContent(Context.class);
        if (!ctx.isPresent()) throw new UnexpectedMessage();
        Integer year = Integer.parseInt(message.getText());
        replies.add(new TextReply("Hello " + ctx.get().getName() + ", you are " + (new GregorianCalendar().get(Calendar.YEAR) - year) + " old"));
        return "";//Reset state
    }
}
