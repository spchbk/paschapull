package ru.sberbank.uplatform.states.greeteng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.Client;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.PhotoReply;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.message.reply.TextReply;
import ru.sberbank.uplatform.states.State;
import ru.sberbank.uplatform.states.UnexpectedMessage;

import javax.annotation.PostConstruct;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * @author Paul Tarasov
 * @since 05.03.2016
 */
public class GreetingCardReply implements State {
    private final static Logger log = LoggerFactory.getLogger(GreetingCardReply.class);
    public static final String photoExtension = ".jpg";
    @Value("${photo_dir}")
    private String photoDir;
    private List<CardPhoto> cards;
    private Font font;
    // coefficient of font size and text layout for 100x100 picture
    private final double k = 0.35125;
    // indent from the edges for 100x100 picture
    private final int indent = 25;

    private final double K = k * 8;
    private final int I = indent * 8;


    private class CardPhoto {
        private String photoName;
        private Color color;

        public CardPhoto(String photoName, Color color) {
            this.photoName = photoName;
            this.color = color;
        }
    }

    public GreetingCardReply() {
        cards = new ArrayList<>();

        // woman's day
        cards.add(new CardPhoto("8marta", new Color(88, 108, 179)));

        // valentines cards
//      cards.add(new CardPhoto("valentines1", new Color(103, 167, 207)));
//      cards.add(new CardPhoto("valentines2", new Color(202, 71, 110)));
//      cards.add(new CardPhoto("valentines3", new Color(66, 117, 99)));
    }

    @PostConstruct
    public void init() {
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, new File(photoDir + "/SERIF_ITALIC_BOLD.ttf"));
        } catch (FontFormatException | IOException e) {
            log.error("Font doesn't load", e);
            font = new Font("Serif", Font.ITALIC + Font.BOLD, 36);
        }
    }

    @Override
    public String getName() {
        return GreetingCardReply.class.getCanonicalName();
    }

    @Override
    public String onMessage(Client client, Message message, List<Reply> replies) throws Exception {
        Optional<GreetingText> text = client.getContent(GreetingText.class);
        if (!text.isPresent()) throw new UnexpectedMessage();
        GreetingText greetingText = text.get();
        greetingText.setText(message.getText());
        if (!greetingText.checkLength()) {
            replies.add(new TextReply("Слишком длинный текст. Попробуйте еще раз"));
            return GreetingCardReply.class.getCanonicalName();
        } else {
            PhotoReply reply = new PhotoReply();
            reply.setFile(photoDir + "/" + getCardPhoto(message) + photoExtension);
            replies.add(reply);
            return "";
        }
    }

    private CardPhoto randomCard() {
        Random r = new Random();
        int elem = r.nextInt(cards.size());
        return cards.get(elem);
    }

    public String getCardPhoto(Message msg) {
        String text = msg.getText();
        return (text.isEmpty()) ? randomCard().photoName : createTempPhoto(msg, text.trim());
    }

    private String createTempPhoto(Message msg, String text) {
        CardPhoto photo = randomCard();
        try {
            font = font.deriveFont(getFontSize(text));
            BufferedImage img = ImageIO.read(new File(photoDir + "/" + photo.photoName + photoExtension));
            addTextToImage(img, font, photo.color, text);
            File temp = new File(photoDir, "/greeting-card" + msg.getFrom().getId() +
                    System.currentTimeMillis() + photoExtension);

            JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(Locale.getDefault());
            jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            jpegParams.setCompressionQuality(1);
            // hack for alpha converting
            ImageWriter imgWriter = ImageIO.getImageWritersByFormatName("png").next();
            ImageOutputStream ioStream = ImageIO.createImageOutputStream(temp);
            imgWriter.setOutput(ioStream);
            imgWriter.write(null, new IIOImage(img, null, null), jpegParams);

            String filename = temp.getName();
            int index = filename.lastIndexOf(".");
            return filename.substring(0, index);
        } catch (IOException e) {
            return photo.photoName;
        }
    }

    private float getFontSize(String text) {
        String[] words = text.split(" ");
        if (text.length() < 10)
            return (float) (21 * K);
        else if (text.length() < 12 && words.length >= 2)
            return (float) (20 * K);
        else if (text.length() < 17 && countLongLengthWords(words) >= 2)
            return (float) (18 * K);
        else if (text.length() < 25)
            return (float) (16 * K);
        else if (text.length() < 30)
            return (float) (15 * K);
        else
            return (float) (14 * K);
    }

    private int countLongLengthWords(String[] words) {
        int i = 0;
        for (String word : words) {
            if (word.length() >= 3) i++;
        }
        return i;
    }

    private void addTextToImage(BufferedImage img, Font font, Color color, String text) {
        final double X, Y;
        X = Y = I * K;

        final Graphics graphics = img.getGraphics();
        graphics.setFont(font);
        final FontMetrics fontMetrics = graphics.getFontMetrics();
        graphics.dispose();

        double lineHeight = fontMetrics.getHeight() + 20 * K;
        String[] words = text.split(" ");
        String line = "";
        List<String> lines = new ArrayList<>();
        for (String word : words) {
            if (fontMetrics.stringWidth(line + word) * 1.45 > X || (line.length() > 5 && word.toLowerCase().equals("с"))) {
                if (!line.isEmpty()) {
                    lines.add(line);
                    line = "";
                }
            }
            line += word + " ";
        }
        lines.add(line);

        int i = 0;
        for (String l : lines) {
            // hard code.
            double x = (X - fontMetrics.stringWidth(l)) / 2;
            double y = lineHeight + i * lineHeight + ((Y / 2) - (lineHeight * i / 2)) - lines.size() * (indent - 5) * K;
            addLineTextToImage(img, l, (2 * indent * K + x), (y + font.getSize() * lines.size()), 1, font, color);
            i++;
        }
    }

    private void addLineTextToImage(BufferedImage img, String text, double topX, double topY, float alpha, Font font, Color color) {
        Graphics2D g = img.createGraphics();
        g.setColor(color);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
        g.setFont(font);
        g.drawString(text, (int)topX, (int)topY);
        g.dispose();
    }
}
