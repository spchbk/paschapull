package ru.sberbank.uplatform.states.greeteng;

import ru.sberbank.uplatform.Client;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.message.reply.TextReply;
import ru.sberbank.uplatform.states.State;
import ru.sberbank.uplatform.states.UnexpectedMessage;

import java.util.List;

/**
 * @author Paul Tarasov
 * @since 05.03.2016
 */
public class GreetingCard implements State {
    @Override
    public String getName() {
        return GreetingCard.class.getCanonicalName();
    }

    @Override
    public String onMessage(Client client, Message message, List<Reply> replies) throws Exception {
        replies.add(new TextReply("Введите текст поздравления (не более 40 символов)"));
        client.setContent(new GreetingText());
        return GreetingCardReply.class.getCanonicalName();
    }
}
