package ru.sberbank.uplatform.states.greeteng;

/**
 * @author Paul Tarasov
 * @since 05.03.2016
 */
public class GreetingText {
    public static final int MAX_LENGTH = 40;
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean checkLength() {
        return text.length() <= MAX_LENGTH;
    }
}