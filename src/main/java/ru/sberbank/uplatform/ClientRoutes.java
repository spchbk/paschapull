package ru.sberbank.uplatform;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.Header;
import org.apache.camel.component.velocity.VelocityConstants;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import ru.sberbank.uplatform.api.FaqService;
import ru.sberbank.uplatform.api.GeoDB;
import ru.sberbank.uplatform.api.InstantAnswer;
import ru.sberbank.uplatform.api.geo.GeoObjectWithDistance;
import ru.sberbank.uplatform.cbr.converter.CbrConverterManager;
import ru.sberbank.uplatform.cbr.data.BiCurrencyValue;
import ru.sberbank.uplatform.cbr.data.InternationalReserve;
import ru.sberbank.uplatform.faq.FaqResponse;
import ru.sberbank.uplatform.message.ChatActions;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.keyboard.ForceReply;
import ru.sberbank.uplatform.message.keyboard.InlineKeyboardMarkup;
import ru.sberbank.uplatform.message.keyboard.ReplyKeyboard;
import ru.sberbank.uplatform.message.keyboard.ReplyKeyboardHide;
import ru.sberbank.uplatform.message.keyboard.ReplyKeyboardMarkup;
import ru.sberbank.uplatform.message.reply.ChatActionReply;
import ru.sberbank.uplatform.message.reply.LocationReply;
import ru.sberbank.uplatform.message.reply.PhotoReply;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.message.reply.TextReply;
import ru.sberbank.uplatform.message.reply.VideoReply;
import ru.sberbank.uplatform.ns.NanosemanticsService;
import ru.sberbank.uplatform.route.CustomRouteBuilder;
import ru.sberbank.uplatform.route.Keyboard;
import ru.sberbank.uplatform.sbol.geolocations.GeoObject;
import ru.sberbank.uplatform.sbol.geolocations.Response;
import ru.sberbank.uplatform.sbol.yahoo.Fields;
import ru.sberbank.uplatform.util.RuntimeTypeAdapterFactory;

/**
 * Created by baryshnikov on 14.11.15.
 */
public class ClientRoutes extends CustomRouteBuilder {
    public static final String epNewClient = "direct:new-client",
            epInitClient = "direct:init-client",
            epNotFound = "direct:command-not-found";
    public static final String videoExtension = "mp4", photoExtension = "jpg";
    private Map<String, Pattern[]> selectors;
    private MathTool mathTools = new MathTool();
    private NumberTool numTools = new NumberTool();
    private VelocityExtension exNumTools = new VelocityExtension();
    private Map<String, ReplyKeyboard> keyBoardsMarkup;
    private final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy"),
            timeFormat = DateTimeFormatter.ofPattern("HH:mm"),
            sbrfFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    /**
     * Регулярка с периодами функционирования расходов и доходов
     */
    private String balancePeriod = "";

    @Value("${video_dir}")
    private String videoDir;

    @Value("${video_instant_dir}")
    private String instVideoDir;

    @Value("${photo_dir}")
    private String photoDir;

    @Autowired
    private GeoDB geoDB;

    @Value("${geo_radius}")
    private Double geoMaxRadius;

    @Autowired
    private InstantAnswer instantAnswer;

    private VirtualDirectory videos, instVideos, photos;

    public VirtualDirectory getVideos() {
        return videos;
    }

    @Autowired
    private NanosemanticsService nanosemanticsService;

    @Autowired
    private FaqService faqService;

    /**
     * Scan videos and photos before run
     */
    @PostConstruct
    public void init() {
        videos = new VirtualDirectory(videoExtension, videoDir);
        instVideos = new VirtualDirectory(videoExtension, instVideoDir);
        photos = new VirtualDirectory(photoExtension, photoDir);
        try {
            videos.scan();
        } catch (Exception ex) {
            log.error("Failed scan videos folder due to", ex);
        }
        try {
            instVideos.scan();
        } catch (Exception ex) {
            log.error("Failed scan videos folder due to", ex);
        }
        try {
            photos.scan();
        } catch (Exception ex) {
            log.error("Failed scan photos folder due to", ex);
        }
    }

    @Override
    public void configure() throws Exception {


        /**
         * Here contains routes for clients - greeting,
         * content-based routers and anything else
         */
        from(epInitClient)
                .id("init-client")
                .setHeader("file", simple("${properties:routes_file}"))
                .bean(this, "loadRoutesSelectors")
                .setHeader("file", simple("${properties:keyboards_file}"))
                .bean(this, "loadKeyboardMarkups")
                .setBody(constant(keyBoardsMarkup))
                .log("Keyboards: ${body}");

        from(epNewClient)
                .id("client-new-client")
                .setHeader("template", constant("templates/newclient.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu_full"))
                .bean(this, "addTextReply");

        from(ClientState.IDLE.endpoint)
                .id(ClientState.IDLE.getId())
                .log("routing message by content")
                .setHeader("keyboard", constant("main_menu"))
                .choice()
                .when(simple("${exchangeProperty.message.location} != null")).to(Endpoints.GEO_TIP.endpoint)
                .when(simple("${exchangeProperty.message.text} == null")).to(Endpoints.SOMETHING_STRANGE.endpoint)
                .otherwise().to(Endpoints.FIND_ENDPOINT_BY_CONTENT.endpoint);

        from(Endpoints.FIND_ENDPOINT_BY_CONTENT.endpoint).recipientList(method(this, "findEndpointByContent"));

        // Make action on absolutely invalid message (without saving state).
        from(Endpoints.SOMETHING_STRANGE.endpoint)
                .id(Endpoints.SOMETHING_STRANGE.id)
                .to(epNotFound)
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE));

        // Make action on absolutely invalid message (with saving state).
        from(epNotFound)
                .id("command-not-found")
                .setHeader("template", constant("templates/somethingstrange.vm"))
                .setProperty("not-answered", constant(true))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from(Endpoints.SBOL_CURRENCY_CONVERTER.endpoint)
                .id(Endpoints.SBOL_CURRENCY_CONVERTER.id)
                .to("direct:sbol-request-exchange-currency")
                .choice()
                .when(body()).setHeader("template", constant("templates/exchange.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .bean(this, "addTextReply")
                .otherwise().to(Endpoints.SOMETHING_STRANGE.endpoint);

        from(Endpoints.CBR_CURRENCY_CONVERTER.endpoint)
                .id(Endpoints.CBR_CURRENCY_CONVERTER.id)
                .to("direct:cbr-request-exchange-currency")
                .choice()
                .when(body()).setHeader("template", constant("templates/exchange-cbr.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .bean(this, "addTextReply")
                .otherwise().to(Endpoints.SOMETHING_STRANGE.endpoint);

        from("direct:get-metall")
                .id("get-metall")
                .to("direct:sbol-request-quotes")
                .to("direct:request-oil")
                .setHeader("template", constant("templates/metall.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .bean(this, "addTextReply");

        from("direct:get-premier")
                .id("get-premier")
                .to("direct:sbol-request-quotes")
                .setHeader("template", constant("templates/premier.vm"))
                .setHeader("keyboard", constant("currencies_menu"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from("direct:get-sbrf-akcii")
                .id("get-sbrf-akcii")
                .to("direct:request-sbrf")
                .process(ex -> {
                    Fields sbrf = ex.getIn().getHeader("sbrf", Fields.class);
                    if (sbrf != null) {
                        LocalDateTime dt = LocalDateTime.parse(sbrf.getUtctime(), sbrfFormat).plusHours(3);
                        ex.getIn().setHeader("sbrfdate", dt.format(dateFormat));
                        ex.getIn().setHeader("sbrftime", dt.format(timeFormat));
                    }
                })
                .setHeader("template", constant("templates/sbrf.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .bean(this, "addTextReply");

        from(Endpoints.SITE.getEndpoint())
                .id(Endpoints.SITE.getId())
                .setHeader("template", constant("templates/site.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .bean(this, "addTextReply");

        from(Endpoints.CLIENT_ASK_ATM.getEndpoint())
                .id(Endpoints.CLIENT_ASK_ATM.getId())
                .setHeader("mode", constant("atm"))
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.WANT_ATM))
                .to("direct:get-branches");

        from(Endpoints.CLIENT_ASK_FILIAL.getEndpoint())
                .id(Endpoints.CLIENT_ASK_FILIAL.getId())
                .setHeader("mode", constant("filial"))
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.WANT_FILIAL))
                .to("direct:get-branches");

        from("direct:get-branches")
                .id("get-branches")
                .setHeader("template", simple("templates/help${headers.mode}s.vm"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from(Endpoints.GEO_TIP.endpoint)
                .id(Endpoints.GEO_TIP.id)
                .log("tip by geo location")
                .process(ex -> {
                    Message msg = ex.getProperty("message", Message.class);
                    List<GeoObjectWithDistance> res = geoDB.findByLocation(msg.getLocation().getLatitude(), msg.getLocation().getLongitude(), geoMaxRadius, 1);
                    if (!res.isEmpty())
                        ex.getIn().setHeader("geo", res.get(0));
                })
                .choice()
                .when(header("geo"))
                .setHeader("template", simple("templates/geotip.vm"))
                .to("direct:render-template")
                .bean(this, "addTextReply")
                .endChoice()
                .otherwise()
                .to(ClientState.WANT_FILIAL.endpoint);

        from(Endpoints.GEO_TIP_COMMENT.endpoint)
                .id(Endpoints.GEO_TIP_COMMENT.id)
                .log("set comment by geo-tip location")
                .setHeader("template", simple("templates/geotip-stub.vm"))
                .setHeader("keyboard", constant("main_menu"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from(Endpoints.GEO_TIP_RECORD.endpoint)
                .id(Endpoints.GEO_TIP_RECORD.id)
                .log("set comment by geo-tip location")
                .setHeader("template", simple("templates/geotip-stub.vm"))
                .setHeader("keyboard", constant("main_menu"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from(Endpoints.GEO_TIP_RATING.endpoint)
                .id(Endpoints.GEO_TIP_RATING.id)
                .log("set comment by geo-tip location")
                .setHeader("template", simple("templates/geotip-stub.vm"))
                .setHeader("keyboard", constant("main_menu"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from(ClientState.WANT_FILIAL.endpoint)
                .id(ClientState.WANT_FILIAL.getId())
                .log("searching for filial")
                .choice().when(simple("${exchangeProperty.message.location} == null"))
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE))
                .to(Endpoints.ROUTER.endpoint).endChoice()
                .otherwise()
                .setProperty("mode", constant("branch"))
                .to(Endpoints.RENDER_NEAREST_BRANCH.getEndpoint());

        from(ClientState.WANT_ATM.endpoint)
                .id(ClientState.WANT_ATM.getId())
                .log("searching for atm")
                .choice().when(simple("${exchangeProperty.message.location} == null"))
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE))
                .to(Endpoints.ROUTER.endpoint).endChoice()
                .otherwise()
                .setProperty("mode", constant("atm"))
                .to(Endpoints.RENDER_NEAREST_BRANCH.getEndpoint());


        from(Endpoints.RENDER_NEAREST_BRANCH.getEndpoint())
                .id(Endpoints.RENDER_NEAREST_BRANCH.getId())
                .setHeader("template", simple("templates/thank${exchangeProperty.mode}.vm"))
                .to("direct:render-template")
                .bean(this, "addTextReply")
                .to("direct:sbol-request-branches") // Get
                .choice().when(simple("${exchangeProperty.response} == null")).to(Endpoints.NOT_FOUND_LOCATION.endpoint)
                .otherwise()
                .split(body()).streaming().to("direct:add-other-branch")
                .end()
                .process(setKeyboardProcessor(Keyboard.main_menu))
                .setHeader("template", simple("templates/last${exchangeProperty.mode}.vm"))
                .to("direct:render-template")
                .bean(this, "addTextReply")
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE));

        from(Endpoints.NOT_FOUND_LOCATION.getEndpoint())
                .id(Endpoints.NOT_FOUND_LOCATION.getId())
                .setHeader("template", constant("templates/location-not-found.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from("direct:add-other-branch")
                .id("add-other-branch")
                .setHeader("other-branch", body())
                .setHeader("template", simple("templates/branch.vm"))
                .to("direct:render-template")
                .bean(this, "addTextReply")
                .setHeader("longitude", method(this, "getBranchLon"))
                .setHeader("latitude", method(this, "getBranchLat"))
                .bean(this, "addLocationReply");

        from("direct:get-currency")
                .id("get-currency")
                .to("direct:sbol-request-quotes")
                .setHeader("template", constant("templates/currency.vm"))
                .setHeader("keyboard", constant("currencies_menu"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from("direct:get-cbr-currencies")
                .id("get-cbr-currencies")
                .to("direct:get-cbr-currencies-rates")
                .setHeader("template", constant("templates/cbr-currencies.vm"))
                .setHeader("keyboard", constant("currencies_menu"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from("direct:get-cbr-bi-currency-info")
                .id("get-cbr-bi-currency-info")
                .to("direct:get-cbr-bi-currency-bucket")
                .process(ex -> {
                    BiCurrencyValue.BiValue value = ex.getIn().getHeader("bivalue", BiCurrencyValue.BiValue.class);
                    if (value != null) {
                        DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
                        LocalDate date = LocalDate.from(timeFormatter.parse(value.getDate()));
                        ex.getIn().setHeader("cbrdate", date.format(dateFormat));
                    }
                })
                .setHeader("template", constant("templates/cbr-bi-currency.vm"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from("direct:get-international-reserves")
                .id("get-international-reserves")
                .to("direct:get-cbr-international-reserves")
                .process(ex -> {
                    InternationalReserve.ReserveInfo value = ex.getIn().getHeader("reserves", InternationalReserve.ReserveInfo.class);
                    if (value != null) {
                        DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
                        LocalDate date = LocalDate.from(timeFormatter.parse(value.getDate()));
                        ex.getIn().setHeader("cbrdate", date.format(dateFormat));
                    }
                })
                .setHeader("template", constant("templates/cbr-international-reserves.vm"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from("direct:get-refinancing-rate")
                .id("get-refinancing-rate")
                .to("direct:get-cbr-refinancing-rate")
                .setHeader("template", constant("templates/cbr-refinancing-rate.vm"))
                .to("direct:render-template")
                .bean(this, "addTextReply");

        from(Endpoints.ADD_TEXT_REPLY.endpoint)
                .id(Endpoints.ADD_TEXT_REPLY.id)
                .bean(this, "addTextReply");

        from(Endpoints.ADD_VIDEO_REPLY.endpoint)
                .id(Endpoints.ADD_VIDEO_REPLY.id)
                .bean(this, "addVideoReply");

        from(Endpoints.ADD_LOCATION_REPLY.endpoint)
                .id(Endpoints.ADD_LOCATION_REPLY.id)
                .bean(this, "addLocationReply");

        from(Endpoints.ADD_PHOTO_REPLY.endpoint)
                .id(Endpoints.ADD_PHOTO_REPLY.id)
                .bean(this, "addPhotoReply");

        from(Endpoints.ADD_CHAT_ACTION_REPLY.endpoint)
                .id(Endpoints.ADD_CHAT_ACTION_REPLY.id)
                .bean(this, "addChatActionReply");

        from(Endpoints.NOTIFICATION.endpoint)
                .id(Endpoints.NOTIFICATION.id)
                .bean(this, "notification")
                .to("direct:send-notification");

        from(Endpoints.INSTANT_SEARCH.endpoint)
                .id(Endpoints.INSTANT_SEARCH.id)
                .setHeader("instant", exchangeProperty("instant"))
                .setHeader("template", constant("templates/instant.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .bean(this, "addTextReply");

        from(Endpoints.FAQ_SEARCH.endpoint)
                .id(Endpoints.FAQ_SEARCH.id)
                .setHeader("faq", exchangeProperty("faq"))
                .setHeader("template", constant("templates/faq.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .bean(this, "addTextReply");

        from(Endpoints.NANOSEMANTICS_ASK.endpoint)
                .id(Endpoints.NANOSEMANTICS_ASK.id)
                .setHeader("nsAnswer", exchangeProperty("nsAnswer"))
                .setHeader("template", constant("templates/nanosemantics.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .bean(this, "addTextReply");

        from(Endpoints.CHECK_CATEGORY_CONSUMPTION_OR_REVENUE.endpoint)
                .id(Endpoints.CHECK_CATEGORY_CONSUMPTION_OR_REVENUE.id)
                .process(exchange -> {
                    String nameCheckCategory = (String) exchange.getIn().getHeader("nameCheckCategory");
                    boolean flagCategoryExists = checkCategoryExists(nameCheckCategory,
                            exchange.getProperty("message", Message.class));

                    exchange.getIn().setHeader("flagCategoryExists", flagCategoryExists);
                })
                .to(Endpoints.ADD_CATEGORY_CONSUMPTION_OR_REVENUE.endpoint);

        from(ClientState.BALANCE_CONSUMPTION.endpoint)
                .id(ClientState.BALANCE_CONSUMPTION.getId())
                .process(exchange -> shortRequestConsumptionOrRevenue(exchange, "Расходы "))
                .to(Endpoints.ROUTER.endpoint);

        from(ClientState.BALANCE_REVENUE.endpoint)
                .id(ClientState.BALANCE_REVENUE.getId())
                .process(exchange -> shortRequestConsumptionOrRevenue(exchange, "Доходы "))
                .to(Endpoints.ROUTER.endpoint);

        from("direct:render-template")
                .id("render-template")
                .setHeader("math", constant(mathTools))
                .process(ex -> {
                    LocalDateTime now = LocalDateTime.now();
                    ex.getIn().setHeader("date", dateFormat.format(now));
                    ex.getIn().setHeader("time", timeFormat.format(now));
                    @SuppressWarnings("unchecked")
                    //This allows add video or something else from template
                    Attachment attachment = new Attachment(videos, instVideos, photos, null, ex.getProperty("replies", List.class), keyBoardsMarkup);
                    ex.setProperty(Attachment.HEADER, attachment);
                    Map<String, Object> variableMap = new HashMap<>();
                    variableMap.put("headers", ex.getIn().getHeaders());
                    variableMap.put("body", ex.getIn().getBody());
                    variableMap.put("properties", ex.getProperties());
                    variableMap.put("math", mathTools);
                    variableMap.put("attachment", attachment);
                    variableMap.put("exchange", ex);
                    variableMap.put("num", numTools);
                    variableMap.put("tools", exNumTools);
                    VelocityContext velocityContext = new VelocityContext(variableMap);
                    ex.getIn().setHeader(VelocityConstants.VELOCITY_CONTEXT, velocityContext);
                })
                .setHeader("CamelVelocityResourceUri", simple("file:${headers.template}"))
                .recipientList(simple("velocity:file:${headers.template}?encoding=utf-8&propertiesFile=distrib/velocity.properties"));
    }

    private void shortRequestConsumptionOrRevenue(Exchange exchange, String prefixRequest) {
        Message message = exchange.getProperty("message", Message.class);
        if (textIsConsumptionOrRevenue(message.getText())) {
            message.setText(prefixRequest + message.getText());
        }
        exchange.getProperty("client", Client.class).setState(ClientState.IDLE);
    }

    private boolean textIsConsumptionOrRevenue(String messageText) {
        Pattern pattern = Pattern.compile(balancePeriod, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        try {
            return pattern.matcher(messageText).matches();
        } catch (Exception e) {
            log.error("Failed matching balance period: ", e);
            return false;
        }
    }

    private boolean checkCategoryExists(String categoriesMenu, Message message) {
        if (message != null && message.getText() != null && !message.getText().isEmpty()) {
            ReplyKeyboard keyboard = keyBoardsMarkup.get(categoriesMenu);
            if (keyboard instanceof ReplyKeyboardMarkup) {
                return ((ReplyKeyboardMarkup) keyBoardsMarkup.get(categoriesMenu))
                        .getKeyboard().stream().flatMap(Collection::stream)
                        .filter(button -> {
                            return button.getText().equals(message.getText());
                        })
                        .findFirst().isPresent();
            }
        }

        return false;
    }

    public void addLocationReply(Exchange ex,
                                 @ExchangeProperty("message") Message msg,
                                 @ExchangeProperty("replies") List<Reply> replies,
                                 @Header("longitude") Float longitude,
                                 @Header("latitude") Float latitude,
                                 @Header("keyboard") String keyBoardName,
                                 @ExchangeProperty(Attachment.HEADER) Attachment attachment) {
        LocationReply reply = new LocationReply();
        reply.setBotType(msg.getBotType());
        reply.setLongitude(longitude);
        reply.setLatitude(latitude);
        reply.setReplyMarkup(getKeyBoard(keyBoardName, msg));
        replies.add(reply);
        processAttachment(reply, ex, attachment);

    }

    public void addTextReply(Exchange ex,
                             @ExchangeProperty("message") Message msg,
                             @ExchangeProperty("replies") List<Reply> replies,
                             @Body String body,
                             @Header("keyboard") String keyBoardName,
                             @ExchangeProperty(Attachment.HEADER) Attachment attachment) {
        if (body != null && !body.isEmpty()) {
            TextReply reply = new TextReply();
            reply.setBotType(msg.getBotType());
            reply.setText(body);
            reply.setReplyMarkup(getKeyBoard(keyBoardName, msg));
            replies.add(reply);
            processAttachment(reply, ex, attachment);
        } else {
            processAttachment(null, ex, attachment);
        }
    }

    public void addVideoReply(Exchange ex,
                              @ExchangeProperty("message") Message msg,
                              @ExchangeProperty("replies") List<Reply> replies,
                              @Body String body,
                              @Header("keyboard") String keyBoardName,
                              @Header("caption") String caption,
                              @ExchangeProperty(Attachment.HEADER) Attachment attachment) {
        VideoReply reply = new VideoReply();
        reply.setFile(body);
        reply.setCaption(caption);
        reply.setReplyMarkup(getKeyBoard(keyBoardName, msg));
        replies.add(reply);
        processAttachment(reply, ex, attachment);
    }

    public void addPhotoReply(Exchange ex,
                              @ExchangeProperty("replies") List<Reply> replies,
                              @ExchangeProperty("message") Message msg,
                              @Body String body,
                              @Header("keyboard") String keyBoardName,
                              @Header("caption") String caption,
                              @ExchangeProperty(Attachment.HEADER) Attachment attachment) {
        PhotoReply reply = new PhotoReply();
        reply.setBotType(msg.getBotType());
        reply.setFile(body);
        reply.setCaption(caption);
        reply.setReplyMarkup(getKeyBoard(keyBoardName, msg));
        replies.add(reply);
        processAttachment(reply, ex, attachment);
    }

    public void addChatActionReply(Exchange ex,
                                   @ExchangeProperty("message") Message msg,
                                   @ExchangeProperty("replies") List<Reply> replies,
                                   @Header("keyboard") String keyBoardName,
                                   @ExchangeProperty(Attachment.HEADER) Attachment attachment) {
        ChatActionReply reply = new ChatActionReply(ChatActions.TEXT);
        reply.setReplyMarkup(getKeyBoard(keyBoardName, msg));
        reply.setBotType(msg.getBotType());
        replies.add(reply);
        processAttachment(reply, ex, attachment);
    }

    private void processAttachment(Reply reply, Exchange ex, Attachment attachment) {
        if (attachment != null) {
            attachment.end();
            ex.removeProperty(Attachment.HEADER);
            if (attachment.getKeyboard() != null && reply != null) {
                Message message = ex.getProperty("message", Message.class);
                reply.setReplyMarkup(getKeyBoard(attachment.getKeyboard(), message));
            }

            shiftKeyboard(attachment);
        }
    }

    /**
     * Method used to move the keyboard in place after the last response.
     * It's necessary if the photo has been append to the template.
     *
     * @param attachment    attachment of the template.
     */
    private void shiftKeyboard(Attachment attachment) {
        List<Reply> replies = attachment.getReplies();
        if (replies.size() > 1) {
            boolean hasKeyboard = attachment.getReplies().get(replies.size() - 1).getReplyMarkup() != null;
            if (!hasKeyboard) {
                int size = replies.size() - 2;
                for (int i = size; i >= 0; i--) {
                    ReplyKeyboard keyboard = replies.get(i).getReplyMarkup();
                    if (keyboard != null) {
                        replies.get(replies.size() - 1).setReplyMarkup(keyboard);
                        replies.get(i).setReplyMarkup(null);
                        return;
                    }
                }
            }
        }
    }

    public void notification(Exchange ex,
                             @ExchangeProperty("message") Message msg,
                             @ExchangeProperty("replies") List<Reply> replies,
                             @Body String body,
                             @Header("chat") Long chat,
                             @Header("keyboard") String keyBoardName,
                             @ExchangeProperty(Attachment.HEADER) Attachment attachment) {
        TextReply reply = new TextReply("");
        reply.setText(body);
        reply.setChatID(chat);
        reply.setReplyMarkup(getKeyBoard(keyBoardName, msg));
        replies.add(reply);
        processAttachment(reply, ex, attachment);
        ex.getIn().setBody(reply);
    }

    public void loadRoutesSelectors(@Header("file") String filename) throws Exception {
        Map<String, List<String>> rules0;
        Type rulesRootFormat = new TypeToken<Map<String, List<String>>>() {
        }.getType();
        try (InputStreamReader r = new InputStreamReader(new FileInputStream(filename), "utf-8")) {
            rules0 = new Gson().fromJson(r, rulesRootFormat);
        }
        selectors = new ConcurrentHashMap<>();
        Map<String, String> includeMap = new HashMap<>();
        Map<String, List<String>> rules = new LinkedHashMap<>();

        // Endpoints started with "_include_" used for replacement in other patterns. 
        rules0.forEach((endpoint, patterns) -> {
            if (endpoint.startsWith("_include_")) {
                includeMap.put(endpoint, patterns.get(0));
                if (endpoint.equals("_include_period")) {
                    balancePeriod = patterns.get(0);
                }
            } else {
                for (String includeKey : includeMap.keySet()) {
                    for (int i = 0; i < patterns.size(); i++) {
                        String pattern = patterns.get(i);
                        int pos = pattern.indexOf(includeKey);
                        if (pos != -1) {
                            // String.replace*() problem with removing of double
                            // slash
                            pattern = pattern.substring(0, pos) + includeMap.get(includeKey)
                                    + pattern.substring(pos + includeKey.length(), pattern.length());
                            patterns.set(i, pattern);
                        }
                    }
                }
                rules.put(endpoint, patterns);
            }
        });
        rules.forEach((endpoint, patterns) -> selectors.put(endpoint,
                patterns.stream().map(Pattern::compile).toArray(Pattern[]::new)));
        selectors.put(Endpoints.SBOL_CURRENCY_CONVERTER.endpoint,
                new Pattern[]{Pattern.compile(Currency.getExchangeAnyPattern())});
    }

    public void loadKeyboardMarkups(@Header("file") String filename) throws Exception {
        Type format = new TypeToken<Map<String, ReplyKeyboard>>() {
        }.getType();

        final RuntimeTypeAdapterFactory<ReplyKeyboard> typeFactory = RuntimeTypeAdapterFactory
                .of(ReplyKeyboard.class, "type")
                .registerSubtype(ReplyKeyboardMarkup.class)
                .registerSubtype(ReplyKeyboardHide.class)
                .registerSubtype(InlineKeyboardMarkup.class)
                .registerSubtype(ForceReply.class);

        final Gson gson = new GsonBuilder().registerTypeAdapterFactory(typeFactory).create();
        try (InputStreamReader r = new InputStreamReader(new FileInputStream(filename), "utf-8")) {
            keyBoardsMarkup = gson.fromJson(r, format);
        }
    }

    public String findEndpointByContent(@ExchangeProperty("message") Message msg, Exchange exc) {
        String norm = msg.getText().toLowerCase().trim();
        Optional<Map.Entry<String, Pattern[]>> first = selectors.entrySet().stream().filter((entry) -> {
            return Arrays.stream(entry.getValue()).anyMatch((p) -> {
                Matcher m = p.matcher(norm);
                boolean b = m.matches();
                if (b && m.groupCount() > 0) {
                    for (int i = 1; i <= m.groupCount(); i++) {
                        // Add captured groups for future use. To avoid double pattern match checking.
                        exc.setProperty("group" + i, m.group(i));
                    }
                }
                return b;
            });
        }).findFirst();
        if (first.isPresent()) {
            return first.get().getKey();
        }
        if (CbrConverterManager.getInstance().getDefaultPattern().matcher(norm).matches()) {
            return Endpoints.CBR_CURRENCY_CONVERTER.endpoint;
        }
        Optional<String> opt = instantAnswer.search(msg.getText());
        if (opt.isPresent()) {
            exc.setProperty("instant", opt.get());
            return Endpoints.INSTANT_SEARCH.endpoint;
        }
        Optional<FaqResponse> faq = faqService.search(msg.getText());
        if (faq.isPresent()) {
            exc.setProperty("faq", faq.get());
            return Endpoints.FAQ_SEARCH.endpoint;
        }
        Long chatId = exc.getProperty("chat_id", Long.class);
        Optional<String> nsAnswerOpt = nanosemanticsService.askQuestion(chatId, msg.getText());
        if (nsAnswerOpt.isPresent()) {
            exc.setProperty("nsAnswer", nsAnswerOpt.get());
            return Endpoints.NANOSEMANTICS_ASK.endpoint;
        }
        return epNotFound;
    }

    public Double getBranchLon(@Header("other-branch") GeoObject resp) {
        return resp.getCoordinate().getLongitude();
    }

    public Double getBranchLat(@Header("other-branch") GeoObject resp) {
        return resp.getCoordinate().getLatitude();
    }

    public GeoObject getBranch(@ExchangeProperty("response") Response resp, @ExchangeProperty("CamelLoopIndex") int index) {
        return resp.getGeoObjects().getGeoObjects().get(++index);
    }
    
    private ReplyKeyboard getKeyBoard(String keyBoardName, Message message) {
        if(keyBoardName == null){
            return null;
        }
        int botType = (message.getBotType() == null) ? BotType.TELEGRAM : message.getBotType();
        if (botType == BotType.TELEGRAM && Keyboard.main_menu.name().equals(keyBoardName)) {
            keyBoardName = Keyboard.main_menu_full.name();
        }
        return keyBoardsMarkup.get(keyBoardName);
    }

}