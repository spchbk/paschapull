package ru.sberbank.uplatform.ddg;

import com.google.gson.annotations.SerializedName;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class Response {
    private Meta meta;
    @SerializedName("AbstractUR")
    private String abstractURL;
    @SerializedName("Abstract")
    private String abstractContent;
    @SerializedName("AbstractText")
    private String abstractText;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public String getAbstractURL() {
        return abstractURL;
    }

    public void setAbstractURL(String abstractURL) {
        this.abstractURL = abstractURL;
    }

    public String getAbstractContent() {
        return abstractContent;
    }

    public void setAbstractContent(String abstractContent) {
        this.abstractContent = abstractContent;
    }

    public String getAbstractText() {
        return abstractText;
    }

    public void setAbstractText(String abstractText) {
        this.abstractText = abstractText;
    }

    public String getText() {
        return abstractContent != null ? abstractContent : (abstractText != null ? abstractText : "");
    }

    public boolean isRussian() {
        return meta != null &&
                meta.getOptions() != null &&
                meta.getOptions().getLanguage() != null &&
                meta.getOptions().getLanguage().toLowerCase().equals("ru");
    }
}
