package ru.sberbank.uplatform.ddg;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class SrcOptions {
    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
