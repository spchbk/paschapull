package ru.sberbank.uplatform.ddg;

import com.google.gson.Gson;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.api.InstantAnswer;

import java.net.URLEncoder;
import java.util.Optional;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class DuckDuckGo implements InstantAnswer {
    private static final String apiURL = "http://api.duckduckgo.com/?format=json&q=";//TODO: Move to external resource
    private Logger logger = LoggerFactory.getLogger(DuckDuckGo.class);

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Override
    public Optional<String> search(String query) {
        try {
            Response resp = get(query);
            if (resp.isRussian() && !resp.getText().isEmpty())
                return Optional.of(resp.getText());
        } catch (Exception ex) {
            logger.error("Failed get DDG request", ex);
        }
        return Optional.empty();
    }

    private Response get(String query) throws Exception {
        HttpGet request = new HttpGet(apiURL + URLEncoder.encode(query, "UTF-8"));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(request)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return new Gson().fromJson(EntityUtils.toString(response.getEntity()), Response.class);
            }
        }
    }
}
