package ru.sberbank.uplatform.ddg;

import com.google.gson.annotations.SerializedName;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class Meta {
    @SerializedName("src_options")
    private SrcOptions options;

    public SrcOptions getOptions() {
        return options;
    }

    public void setOptions(SrcOptions options) {
        this.options = options;
    }
}
