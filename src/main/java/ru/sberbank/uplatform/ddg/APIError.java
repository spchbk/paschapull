package ru.sberbank.uplatform.ddg;

/**
 * Created by baryshnikov on 28.11.15.
 */
public class APIError extends Exception {
    public APIError(String message) {
        super(message);
    }
}
