package ru.sberbank.uplatform.cbr.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * @author Pavel Tarasov
 * @since 26/01/2016.
 */
@XmlRootElement(name = "mmrf")
public class InternationalReserve {
    /** List of the monthly international reserves information. */
    private List<ReserveInfo> reserves;

    public List<ReserveInfo> getReserves() {
        return reserves;
    }

    @XmlElement(name = "mr")
    public void setReserves(List<ReserveInfo> reserves) {
        this.reserves = reserves;
    }

    @Override
    public String toString() {
        return "InternationalReserve{" +
                "reserves=" + reserves +
                '}';
    }


    /**
     * Monthly information of the international reserves.
     */
    public static class ReserveInfo implements Comparable<ReserveInfo> {
        /** Date relevance of information. */
        private String date;
        /** Total value of the reserves. */
        private Double all;
        /** Total value of the currency reserves (part of reserves). */
        private Double currencies;
        /** Value of the foreign currency reserves (part of currencies reserves). */
        private Double foreignCurrency;
        /** Value of the special drawing rights - XDR (part of currencies reserves). */
        private Double XDR;
        /** Value of the International Monetary Fund reserves (part of the currencies reserves). */
        private Double IMF;
        /** Total value of the gold reserves (part of reserves). */
        private Double gold;

        public Optional<Calendar> getCalendarDate() {
            try {
                return Optional.of(DatatypeFactory.newInstance().newXMLGregorianCalendar(date).toGregorianCalendar());
            } catch (DatatypeConfigurationException | NullPointerException e) {
                return Optional.empty();
            }
        }

        public String getDate() {
            return date;
        }

        @XmlElement(name="D0")
        public void setDate(String date) {
            this.date = date;
        }

        public Double getAll() {
            return all;
        }

        @XmlElement(name="p1")
        public void setAll(Double all) {
            this.all = all;
        }

        public Double getCurrencies() {
            return currencies;
        }

        @XmlElement(name="p2")
        public void setCurrencies(Double currencies) {
            this.currencies = currencies;
        }

        public Double getForeignCurrency() {
            return foreignCurrency;
        }

        @XmlElement(name="p3")
        public void setForeignCurrency(Double foreignCurrency) {
            this.foreignCurrency = foreignCurrency;
        }

        public Double getXDR() {
            return XDR;
        }

        @XmlElement(name="p4")
        public void setXDR(Double XDR) {
            this.XDR = XDR;
        }

        public Double getIMF() {
            return IMF;
        }

        @XmlElement(name="p5")
        public void setIMF(Double IMF) {
            this.IMF = IMF;
        }

        public Double getGold() {
            return gold;
        }

        @XmlElement(name="p6")
        public void setGold(Double gold) {
            this.gold = gold;
        }

        @Override
        public int compareTo(ReserveInfo o) {
            if (!getCalendarDate().isPresent() && !o.getCalendarDate().isPresent()) {
                return 0;
            } else if (!getCalendarDate().isPresent()) {
                return -1;
            } else if (!o.getCalendarDate().isPresent()) {
                return 1;
            } else {
                return getCalendarDate().get().compareTo(o.getCalendarDate().get());
            }
        }

        @Override
        public String toString() {
            return "ReserveInfo{" +
                    "date='" + date + '\'' +
                    ", all=" + all +
                    ", currencies=" + currencies +
                    ", foreignCurrency=" + foreignCurrency +
                    ", XDR=" + XDR +
                    ", IMF=" + IMF +
                    ", gold=" + gold +
                    '}';
        }
    }
}
