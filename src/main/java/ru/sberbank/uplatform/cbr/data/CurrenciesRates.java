package ru.sberbank.uplatform.cbr.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class of the currency price information.
 *
 * @author Pavel Tarasov
 * @since 19/01/2016.
 */
@XmlRootElement(name = "ValuteCursOnDate")
public class CurrenciesRates {
    private String ruName;
    private Integer nominal;
    private Double rate;
    private Integer numCode;
    private String charCode;

    public String getRuName() {
        return ruName;
    }

    @XmlElement(name = "Vname")
    public void setRuName(String ruName) {
        this.ruName = ruName;
    }

    public Integer getNominal() {
        return nominal;
    }

    @XmlElement(name = "Vnom")
    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public Double getRate() {
        return rate;
    }

    @XmlElement(name = "Vcurs")
    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Integer getNumCode() {
        return numCode;
    }

    @XmlElement(name = "Vcode")
    public void setNumCode(Integer numCode) {
        this.numCode = numCode;
    }

    public String getCharCode() {
        return charCode;
    }

    @XmlElement(name = "VchCode")
    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }

    @Override
    public String toString() {
        return "CurrenciesRates{" +
                "ruName='" + ruName + '\'' +
                ", nominal=" + nominal +
                ", rate=" + rate +
                ", numCode=" + numCode +
                ", charCode='" + charCode + '\'' +
                '}';
    }
}