package ru.sberbank.uplatform.cbr.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * Class of bi-currency bucket which contains the whole history of the buckets info.
 *
 * @author Pavel Tarasov
 * @since 19/01/2016.
 */
@XmlRootElement(name = "BiCurBacket")
public class BiCurrencyBucket {
    /** List of the {@link Bucket} history buckets. */
    private List<Bucket> buckets;

    public List<Bucket> getBuckets() {
        return buckets;
    }

    @XmlElement(name = "BC")
    public void setBuckets(List<Bucket> buckets) {
        this.buckets = buckets;
    }

    @Override
    public String toString() {
        return "BiCurrencyBucket{" +
                "buckets=" + buckets +
                '}';
    }

    /**
     * Class of bi-currency bucket info.
     */
    public static class Bucket implements Comparable<Bucket> {
        /** Date of the establishment of currencies proportions. */
        private String date;
        /** The USD proportion. */
        private Double usd;
        /** The EUR proportion. */
        private Double eur;

        public Optional<Calendar> getCalendarDate() {
            try {
                return Optional.of(DatatypeFactory.newInstance().newXMLGregorianCalendar(date).toGregorianCalendar());
            } catch (DatatypeConfigurationException | NullPointerException e) {
                return Optional.empty();
            }
        }

        public String getDate() {
            return date;
        }

        @XmlElement(name="D0")
        public void setDate(String date) {
            this.date = date;
        }

        public Double getUsd() {
            return usd;
        }

        @XmlElement(name="USD")
        public void setUsd(Double usd) {
            this.usd = usd;
        }

        public Double getEur() {
            return eur;
        }

        @XmlElement(name="EUR")
        public void setEur(Double eur) {
            this.eur = eur;
        }

        @Override
        public int compareTo(Bucket o) {
            if (!getCalendarDate().isPresent() && !o.getCalendarDate().isPresent()) {
                return 0;
            } else if (!getCalendarDate().isPresent()) {
                return -1;
            } else if (!o.getCalendarDate().isPresent()) {
                return 1;
            } else {
                return getCalendarDate().get().compareTo(o.getCalendarDate().get());
            }
        }

        @Override
        public String toString() {
            return "Bucket{" +
                    "date='" + date + '\'' +
                    ", usd=" + usd +
                    ", eur=" + eur +
                    '}';
        }
    }
}