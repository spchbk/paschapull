package ru.sberbank.uplatform.cbr.data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * Class of bi-currency bucket prices info.
 *
 * @author Pavel Tarasov
 * @since 19/01/2016.
 */
@XmlRootElement(name = "BiCurBase")
public class BiCurrencyValue {
    /** List of the {@link BiValue} bucket prices info. */
    private List<BiValue> values;

    public List<BiValue> getValues() {
        return values;
    }

    @XmlElement(name = "BCB")
    public void setValues(List<BiValue> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "BiCurrencyValue{" +
                "values=" + values +
                '}';
    }

    /**
     * Class of bi-currency bucket price info.
     */
    public static class BiValue implements Comparable<BiValue> {
        /** Date of the establishment of the bucket price. */
        private String date;
        /** Bi-currency bucket price. */
        private Double value;

        public Optional<Calendar> getCalendarDate() {
            try {
                return Optional.of(DatatypeFactory.newInstance().newXMLGregorianCalendar(date).toGregorianCalendar());
            } catch (DatatypeConfigurationException | NullPointerException e) {
                return Optional.empty();
            }
        }

        public String getDate() {
            return date;
        }

        @XmlElement(name = "D0")
        public void setDate(String date) {
            this.date = date;
        }

        public Double getValue() {
            return value;
        }

        @XmlElement(name = "VAL")
        public void setValue(Double value) {
            this.value = value;
        }

        @Override
        public int compareTo(BiValue o) {
            if (!getCalendarDate().isPresent() && !o.getCalendarDate().isPresent()) {
                return 0;
            } else if (!getCalendarDate().isPresent()) {
                return -1;
            } else if (!o.getCalendarDate().isPresent()) {
                return 1;
            } else {
                return getCalendarDate().get().compareTo(o.getCalendarDate().get());
            }
        }

        @Override
        public String toString() {
            return "BiValue{" +
                    "date='" + date + '\'' +
                    ", value=" + value +
                    '}';
        }
    }
}
