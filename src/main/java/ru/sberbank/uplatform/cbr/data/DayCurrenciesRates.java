package ru.sberbank.uplatform.cbr.data;

import javax.xml.bind.annotation.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Day info for the all CBRF currencies.
 *
 * @author Pavel Tarasov
 * @since 19/01/2016.
 */
@XmlRootElement(name = "ValuteData")
public class DayCurrenciesRates {
    /** List of the currencies info. */
    private List<CurrenciesRates> rates;
    /** Date relevance of information in format "dd.MM.yyyy" */
    private String date;

    public List<CurrenciesRates> getRates() {
        return rates;
    }

    @XmlElement(name = "ValuteCursOnDate")
    public void setRates(List<CurrenciesRates> rates) {
        this.rates = rates;
    }

    public String getDate() {
        return date;
    }

    public Optional<LocalDate> getOptionalDate() {
        try {
            String[] parts = date.split(".");
            return Optional.of(LocalDate.of(Integer.valueOf(parts[2]),
                    Integer.valueOf(parts[1]), Integer.valueOf(parts[0])));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void setDate(String date) {
        this.date = new StringBuilder()
                .append(date.substring(6, 8)).append(".")
                .append(date.substring(4, 6)).append(".")
                .append(date.substring(0, 4)).toString();
    }

    @Override
    public String toString() {
        return "DayCurrenciesRates{" +
                "rates=" + rates +
                ", date='" + date + '\'' +
                '}';
    }
}