package ru.sberbank.uplatform.cbr.converter;

import ru.sberbank.uplatform.cbr.data.CurrenciesRates;

import java.util.regex.Pattern;

/**
 * Class of the currency price information
 * with search pattern by this currency.
 *
 * @author Pavel Tarasov
 * @since 25/01/2016.
 */
public class ExtendedCbrCurrency {
    private CurrenciesRates rates;
    private Pattern currencyPattern;

    public ExtendedCbrCurrency(CurrenciesRates rates, Pattern currencyPattern) {
        this.rates = rates;
        this.currencyPattern = currencyPattern;
    }

    public CurrenciesRates getRates() {
        return rates;
    }

    public void setRates(CurrenciesRates rates) {
        this.rates = rates;
    }

    public Pattern getCurrencyPattern() {
        return currencyPattern;
    }

    public void setCurrencyPattern(Pattern currencyPattern) {
        this.currencyPattern = currencyPattern;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExtendedCbrCurrency that = (ExtendedCbrCurrency) o;

        return !(rates != null ? !rates.equals(that.rates) : that.rates != null) &&
                !(currencyPattern != null ? !currencyPattern.equals(that.currencyPattern) : that.currencyPattern != null);
    }

    @Override
    public int hashCode() {
        int result = rates != null ? rates.hashCode() : 17;
        result = 31 * result + (currencyPattern != null ? currencyPattern.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ExtendedCbrCurrency{" +
                "rates=" + rates +
                ", currencyPattern=" + currencyPattern +
                '}';
    }
}
