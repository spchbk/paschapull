package ru.sberbank.uplatform.cbr.converter;

import ru.sberbank.uplatform.cbr.data.CurrenciesRates;
import ru.sberbank.uplatform.cbr.data.DayCurrenciesRates;

import java.util.*;
import java.util.regex.Pattern;


/**
 * Exchange converter manager for the CBRF currencies.
 *
 * @author Pavel Tarasov
 * @since 25/01/2016.
 */
public class CbrConverterManager {
    private static final CbrConverterManager INSTANCE = new CbrConverterManager();
    /**
     * Map of the currencies with patterns, where key is char code of
     * currency, value is {@link ExtendedCbrCurrency} currency info.
     */
    private Map<String, ExtendedCbrCurrency> currencies;
    private Optional<String> currenciesDate;

    /**
     * Creates manager and {@code currencies} map is filled of currencies
     * with default patterns and null currencies info.
     */
    private CbrConverterManager() {
        currenciesDate = Optional.empty();
        currencies = new HashMap<>();

        setDefaultPatterns();
    }

    public static CbrConverterManager getInstance() {
        return INSTANCE;
    }

    /**
     * Sets the default patterns for currencies which
     * specifies into {@link CurrencyPatterns} enum.
     */
    private void setDefaultPatterns() {
        Arrays.asList(CurrencyPatterns.values())
                .forEach(c -> currencies.put(c.name().toLowerCase(),
                        new ExtendedCbrCurrency(null, c.getPattern())));
    }

    /**
     * Update {@code currencies} currencies map and actual date.
     *
     * @param rates   {@link DayCurrenciesRates} latest currencies rates.
     */
    public synchronized void update(DayCurrenciesRates rates) {
        currenciesDate = Optional.of(rates.getDate());
        rates.getRates().forEach(rate -> {
            String charCode = rate.getCharCode().toLowerCase();
            ExtendedCbrCurrency currency = currencies.putIfAbsent(charCode, new ExtendedCbrCurrency(rate, Pattern.compile(charCode)));
            if (currency != null) {
                currency.setRates(rate);
                currencies.put(charCode, currency);
            }
        });
    }

    /**
     * Find currency by his regular expression pattern.
     *
     * @param matcher   the character sequence to be matched
     * @return the first found currencies rates info wrapped on
     *          {@link Optional} or empty Optional.
     */
    public synchronized Optional<CurrenciesRates> findCurrency(String matcher) {
        Optional<ExtendedCbrCurrency> currency = currencies.values().stream()
                .filter(c -> c.getCurrencyPattern().matcher(matcher).matches()).findFirst();
        return (currency.isPresent() && currency.get().getRates() != null) ? Optional.of(currency.get().getRates()) : Optional.empty();
    }

    /**
     * Get pattern value for all CBRF currencies.
     *
     * @return pattern for all currencies.
     */
    public synchronized Pattern getPattern() {
        StringBuilder sb = new StringBuilder("(");
        currencies.values().forEach(c -> sb.append(c.getCurrencyPattern().toString()).append("|"));
        return Pattern.compile("(\\d+|\\d+(\\.|,)\\d*)\\s*" + sb.substring(0, sb.length() - 1) + ")" + ".*");
    }

    public synchronized Pattern getDefaultPattern() {
        StringBuilder sb = new StringBuilder("(");
        currencies.values().forEach(c -> sb.append(c.getCurrencyPattern().toString()).append("|"));
        return Pattern.compile(".*" + sb.substring(0, sb.length() - 1) + ").*");
    }

    public String getCurrenciesDate() {
        return currenciesDate.isPresent() ? currenciesDate.get() : "";
    }
}
