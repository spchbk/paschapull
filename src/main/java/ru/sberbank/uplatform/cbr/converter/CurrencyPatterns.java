package ru.sberbank.uplatform.cbr.converter;

import java.util.regex.Pattern;

/**
 * Default patterns for CBRF currencies.
 *
 * @author Pavel Tarasov
 * @since 25/01/2016.
 */
public enum CurrencyPatterns {
    AUD("aud|австрал.*доллар.*"),
    AZN("azn|азербайджан.*манат.*"),
    AMD("amd|драм.*|армян.*драм.*"),
    BYR("byr|белорус.*рубл.*"),
    BGN("bgn|лев.*|болгарск.*лев.*"),
    BRL("brl|реал.*|бразил.*реал.*"),
    HUF("huf|форинт.*|венгерск.*форинт.*"),
    DKK("dkk|датск.*крон.*"),
    INR("inr|рупи.*|индийск.*рупи.*"),
    KZT("kzt|тенг.*|казахстанск.*тенг.*"),
    CAD("cad|канадск.*доллар.*"),
    KGS("kgs|сом|сомов|киргизск.*сом.*"),
    CNY("cny|юан.*|китайск.*юан.*"),
    MDL("mdl|молдавск.*лей|молдавск.*леев"),
    NOK("nok|норвежск.*крон.*"),
    PLN("pln|злоты.*|польск.*злоты.*"),
    RON("ron|румынск.*лей|румынск.*леев"),
    SGD("sgd|сингапурск.*доллар.*"),
    TJS("tjs|сомон.*|таджикск.*сомон.*"),
    TRY("try|лир.*|турецк.*лир.*"),
    TMT("tmt|туркмен.*манат.*"),
    UZS("uzs|узбекск.*сум.*"),
    UAH("uah|грив.*|украинск.*грив.*"),
    CZK("czk|чешск.*крон.*"),
    SEK("sek|шведск.*крон.*"),
    ZAR("zar|рэнд.*|.*африканск.*рэнд.*"),
    KRW("krw|корейск.*вон.*|вон.*республ.*коре.*"),
    JPY("jpy|[ий]ен.*|японск.*[ий]ен.*");


    private final Pattern pattern;

    CurrencyPatterns(String pattern) {
        this.pattern = Pattern.compile(pattern);
    }

    public Pattern getPattern() {
        return pattern;
    }
}
