package ru.sberbank.uplatform;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by baryshnikov on 23.11.15.
 */
public class VirtualDirectory {
    private List<String> files;
    private String extension;
    private String homeDir;

    public VirtualDirectory(String extension, String homeDir) {
        this.extension = extension;
        this.homeDir = homeDir;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    String getRealName(String name) {
        return new File(homeDir, name + "." + extension).getPath();
    }

    public String getExtension() {
        return extension;
    }

    public String random() {
        return get(new Random().nextInt(files.size()));
    }

    public void scan() {
        files = FileUtils.listFiles(new File(homeDir), new String[]{extension}, true).stream()
                .map(File::getName)
                .map(f -> f.substring(0, f.length() - extension.length() - 1))
                .collect(Collectors.toList());
    }

    public String getHomeDir() {
        return homeDir;
    }

    public String get(int index) {
        return files.get(index);
    }

    public int size() {
        return files.size();
    }
}
