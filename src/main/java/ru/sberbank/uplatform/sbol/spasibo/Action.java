package ru.sberbank.uplatform.sbol.spasibo;

/**
 * @author Pavel Tarasov
 * @since 25/11/2015.
 */
@Deprecated
public class Action {

    private String image;
    private String title;
    private String period;
    private String profit;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (period != null ? period.hashCode() : 0);
        result = 31 * result + (profit != null ? profit.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Action partner = (Action) o;

        if (image != null ? !image.equals(partner.image) : partner.image != null) return false;
        if (title != null ? !title.equals(partner.title) : partner.title != null) return false;
        if (period != null ? !period.equals(partner.period) : partner.period != null) return false;
        return !(profit != null ? !profit.equals(partner.profit) : partner.profit != null);
    }

    @Override
    public String toString() {
        return "Action{" +
                "image=" + image +
                ", title=" + title +
                ", period=" + period +
                ", profit=" + profit +
                '}';
    }
}
