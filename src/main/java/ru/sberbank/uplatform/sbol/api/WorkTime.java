package ru.sberbank.uplatform.sbol.api;

import java.util.List;

/**
 * Created by baryshnikov on 15.11.15.
 */
public class WorkTime {
    private String weekDayName;
    private List<String> timeList;
    private Integer weekDayNo;

    public String getWeekDayName() {
        return weekDayName;
    }

    public void setWeekDayName(String weekDayName) {
        this.weekDayName = weekDayName;
    }

    public List<String> getTimeList() {
        return timeList;
    }

    public void setTimeList(List<String> timeList) {
        this.timeList = timeList;
    }

    public Integer getWeekDayNo() {
        return weekDayNo;
    }

    public void setWeekDayNo(Integer weekDayNo) {
        this.weekDayNo = weekDayNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkTime workTime = (WorkTime) o;

        if (weekDayName != null ? !weekDayName.equals(workTime.weekDayName) : workTime.weekDayName != null)
            return false;
        if (timeList != null ? !timeList.equals(workTime.timeList) : workTime.timeList != null) return false;
        return !(weekDayNo != null ? !weekDayNo.equals(workTime.weekDayNo) : workTime.weekDayNo != null);

    }

    @Override
    public int hashCode() {
        int result = weekDayName != null ? weekDayName.hashCode() : 0;
        result = 31 * result + (timeList != null ? timeList.hashCode() : 0);
        result = 31 * result + (weekDayNo != null ? weekDayNo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WorkTime{" +
                "weekDayName='" + weekDayName + '\'' +
                ", timeList=" + timeList +
                ", weekDayNo=" + weekDayNo +
                '}';
    }
}
