package ru.sberbank.uplatform.sbol.api;

/**
 * Created by baryshnikov on 14.11.15.
 */
public class Quote {
    private Double buy;
    private Double sell;

    public Double getBuy() {
        return buy;
    }

    public void setBuy(Double buy) {
        this.buy = buy;
    }

    public Double getSell() {
        return sell;
    }

    public void setSell(Double sell) {
        this.sell = sell;
    }

    public Quote(Double buy, Double sell) {
        this.buy = buy;
        this.sell = sell;
    }

    public Quote() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Quote)) return false;

        Quote quote = (Quote) o;

        if (buy != null ? !buy.equals(quote.buy) : quote.buy != null) return false;
        return !(sell != null ? !sell.equals(quote.sell) : quote.sell != null);

    }

    @Override
    public int hashCode() {
        int result = buy != null ? buy.hashCode() : 0;
        result = 31 * result + (sell != null ? sell.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Quote{" +
                "buy='" + buy + '\'' +
                ", sell='" + sell + '\'' +
                '}';
    }
}
