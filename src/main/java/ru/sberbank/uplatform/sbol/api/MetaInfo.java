package ru.sberbank.uplatform.sbol.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by baryshnikov on 14.11.15.
 */
public class MetaInfo {
    @SerializedName("TITLE_ENG")
    private String TitleEng;
    @SerializedName("TITLE")
    private String Title;

    public String getTitleEng() {
        return TitleEng;
    }

    public void setTitleEng(String titleEng) {
        TitleEng = titleEng;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetaInfo metaInfo = (MetaInfo) o;

        if (TitleEng != null ? !TitleEng.equals(metaInfo.TitleEng) : metaInfo.TitleEng != null) return false;
        return !(Title != null ? !Title.equals(metaInfo.Title) : metaInfo.Title != null);

    }

    @Override
    public int hashCode() {
        int result = TitleEng != null ? TitleEng.hashCode() : 0;
        result = 31 * result + (Title != null ? Title.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MetaInfo{" +
                "TitleEng='" + TitleEng + '\'' +
                ", Title='" + Title + '\'' +
                '}';
    }
}
