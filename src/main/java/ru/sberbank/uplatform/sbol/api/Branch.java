package ru.sberbank.uplatform.sbol.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by baryshnikov on 15.11.15.
 */
public class Branch {
    private String type;
    private Coordinates coordinates;
    private Boolean mblt;
    private String name;
    private Boolean workOnWeeks;
    private Boolean transferNotOpen;
    @SerializedName("dist")
    private Double distance;
    private String metroStation;
    private Boolean naturalPerson;
    private String phone;
    private Requisites requisites;
    private String code;
    private Boolean individualSafes;
    private String jtype;
    private String postAddress;
    private List<WorkTime> workTimeList;
    private String address;
    private Boolean zona24;
    private Boolean extWorkTime;
    private List<String> naturalPersonServices;
    private Boolean legalPerson;
    private Long id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Boolean getMblt() {
        return mblt;
    }

    public void setMblt(Boolean mblt) {
        this.mblt = mblt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getWorkOnWeeks() {
        return workOnWeeks;
    }

    public void setWorkOnWeeks(Boolean workOnWeeks) {
        this.workOnWeeks = workOnWeeks;
    }

    public Boolean getTransferNotOpen() {
        return transferNotOpen;
    }

    public void setTransferNotOpen(Boolean transferNotOpen) {
        this.transferNotOpen = transferNotOpen;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getMetroStation() {
        return metroStation;
    }

    public void setMetroStation(String metroStation) {
        this.metroStation = metroStation;
    }

    public Boolean getNaturalPerson() {
        return naturalPerson;
    }

    public void setNaturalPerson(Boolean naturalPerson) {
        this.naturalPerson = naturalPerson;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Requisites getRequisites() {
        return requisites;
    }

    public void setRequisites(Requisites requisites) {
        this.requisites = requisites;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getIndividualSafes() {
        return individualSafes;
    }

    public void setIndividualSafes(Boolean individualSafes) {
        this.individualSafes = individualSafes;
    }

    public String getJtype() {
        return jtype;
    }

    public void setJtype(String jtype) {
        this.jtype = jtype;
    }

    public String getPostAddress() {
        return postAddress;
    }

    public void setPostAddress(String postAddress) {
        this.postAddress = postAddress;
    }

    public List<WorkTime> getWorkTimeList() {
        return workTimeList;
    }

    public void setWorkTimeList(List<WorkTime> workTimeList) {
        this.workTimeList = workTimeList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getZona24() {
        return zona24;
    }

    public void setZona24(Boolean zona24) {
        this.zona24 = zona24;
    }

    public Boolean getExtWorkTime() {
        return extWorkTime;
    }

    public void setExtWorkTime(Boolean extWorkTime) {
        this.extWorkTime = extWorkTime;
    }

    public List<String> getNaturalPersonServices() {
        return naturalPersonServices;
    }

    public void setNaturalPersonServices(List<String> naturalPersonServices) {
        this.naturalPersonServices = naturalPersonServices;
    }

    public Boolean getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(Boolean legalPerson) {
        this.legalPerson = legalPerson;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Branch branch = (Branch) o;

        if (type != null ? !type.equals(branch.type) : branch.type != null) return false;
        if (coordinates != null ? !coordinates.equals(branch.coordinates) : branch.coordinates != null) return false;
        if (mblt != null ? !mblt.equals(branch.mblt) : branch.mblt != null) return false;
        if (name != null ? !name.equals(branch.name) : branch.name != null) return false;
        if (workOnWeeks != null ? !workOnWeeks.equals(branch.workOnWeeks) : branch.workOnWeeks != null) return false;
        if (transferNotOpen != null ? !transferNotOpen.equals(branch.transferNotOpen) : branch.transferNotOpen != null)
            return false;
        if (distance != null ? !distance.equals(branch.distance) : branch.distance != null) return false;
        if (metroStation != null ? !metroStation.equals(branch.metroStation) : branch.metroStation != null)
            return false;
        if (naturalPerson != null ? !naturalPerson.equals(branch.naturalPerson) : branch.naturalPerson != null)
            return false;
        if (phone != null ? !phone.equals(branch.phone) : branch.phone != null) return false;
        if (requisites != null ? !requisites.equals(branch.requisites) : branch.requisites != null) return false;
        if (code != null ? !code.equals(branch.code) : branch.code != null) return false;
        if (individualSafes != null ? !individualSafes.equals(branch.individualSafes) : branch.individualSafes != null)
            return false;
        if (jtype != null ? !jtype.equals(branch.jtype) : branch.jtype != null) return false;
        if (postAddress != null ? !postAddress.equals(branch.postAddress) : branch.postAddress != null) return false;
        if (workTimeList != null ? !workTimeList.equals(branch.workTimeList) : branch.workTimeList != null)
            return false;
        if (address != null ? !address.equals(branch.address) : branch.address != null) return false;
        if (zona24 != null ? !zona24.equals(branch.zona24) : branch.zona24 != null) return false;
        if (extWorkTime != null ? !extWorkTime.equals(branch.extWorkTime) : branch.extWorkTime != null) return false;
        if (naturalPersonServices != null ? !naturalPersonServices.equals(branch.naturalPersonServices) : branch.naturalPersonServices != null)
            return false;
        if (legalPerson != null ? !legalPerson.equals(branch.legalPerson) : branch.legalPerson != null) return false;
        return !(id != null ? !id.equals(branch.id) : branch.id != null);

    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (coordinates != null ? coordinates.hashCode() : 0);
        result = 31 * result + (mblt != null ? mblt.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (workOnWeeks != null ? workOnWeeks.hashCode() : 0);
        result = 31 * result + (transferNotOpen != null ? transferNotOpen.hashCode() : 0);
        result = 31 * result + (distance != null ? distance.hashCode() : 0);
        result = 31 * result + (metroStation != null ? metroStation.hashCode() : 0);
        result = 31 * result + (naturalPerson != null ? naturalPerson.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (requisites != null ? requisites.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (individualSafes != null ? individualSafes.hashCode() : 0);
        result = 31 * result + (jtype != null ? jtype.hashCode() : 0);
        result = 31 * result + (postAddress != null ? postAddress.hashCode() : 0);
        result = 31 * result + (workTimeList != null ? workTimeList.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (zona24 != null ? zona24.hashCode() : 0);
        result = 31 * result + (extWorkTime != null ? extWorkTime.hashCode() : 0);
        result = 31 * result + (naturalPersonServices != null ? naturalPersonServices.hashCode() : 0);
        result = 31 * result + (legalPerson != null ? legalPerson.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Branch{" +
                "type='" + type + '\'' +
                ", coordinates=" + coordinates +
                ", mblt=" + mblt +
                ", name='" + name + '\'' +
                ", workOnWeeks=" + workOnWeeks +
                ", transferNotOpen=" + transferNotOpen +
                ", distance=" + distance +
                ", metroStation='" + metroStation + '\'' +
                ", naturalPerson=" + naturalPerson +
                ", phone='" + phone + '\'' +
                ", requisites=" + requisites +
                ", code='" + code + '\'' +
                ", individualSafes=" + individualSafes +
                ", jtype='" + jtype + '\'' +
                ", postAddress='" + postAddress + '\'' +
                ", workTimeList=" + workTimeList +
                ", address='" + address + '\'' +
                ", zona24=" + zona24 +
                ", extWorkTime=" + extWorkTime +
                ", naturalPersonServices=" + naturalPersonServices +
                ", legalPerson=" + legalPerson +
                ", id=" + id +
                '}';
    }
}
