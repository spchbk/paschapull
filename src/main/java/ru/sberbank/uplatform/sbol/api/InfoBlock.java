package ru.sberbank.uplatform.sbol.api;

import java.util.Map;

/**
 * Created by baryshnikov on 14.11.15.
 */
public class InfoBlock {
    private MetaInfo meta;
    private Map<String, Quote> quotes;
    private Quote lastQuote = null;

    public MetaInfo getMeta() {
        return meta;
    }

    public void setMeta(MetaInfo meta) {
        this.meta = meta;
    }

    public Map<String, Quote> getQuotes() {
        return quotes;
    }

    public void setQuotes(Map<String, Quote> quotes) {
        this.quotes = quotes;
    }

    public Quote getLastQuote() {
        if (lastQuote == null) {
            lastQuote = quotes.get(quotes.keySet().stream().sorted((o1, o2) -> o2.compareTo(o1)).findFirst().get());
        }
        return lastQuote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InfoBlock infoBlock = (InfoBlock) o;

        if (meta != null ? !meta.equals(infoBlock.meta) : infoBlock.meta != null) return false;
        return !(quotes != null ? !quotes.equals(infoBlock.quotes) : infoBlock.quotes != null);

    }

    @Override
    public int hashCode() {
        int result = meta != null ? meta.hashCode() : 0;
        result = 31 * result + (quotes != null ? quotes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InfoBlock{" +
                "meta=" + meta +
                ", quotes=" + quotes +
                '}';
    }
}
