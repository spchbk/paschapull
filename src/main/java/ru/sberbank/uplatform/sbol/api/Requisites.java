package ru.sberbank.uplatform.sbol.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by baryshnikov on 15.11.15.
 */
public class Requisites {
    private String kpp;
    private String inn;
    private String bik;
    @SerializedName("k_account")
    private String kAccount;

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getBik() {
        return bik;
    }

    public void setBik(String bik) {
        this.bik = bik;
    }

    public String getkAccount() {
        return kAccount;
    }

    public void setkAccount(String kAccount) {
        this.kAccount = kAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Requisites that = (Requisites) o;

        if (kpp != null ? !kpp.equals(that.kpp) : that.kpp != null) return false;
        if (inn != null ? !inn.equals(that.inn) : that.inn != null) return false;
        if (bik != null ? !bik.equals(that.bik) : that.bik != null) return false;
        return !(kAccount != null ? !kAccount.equals(that.kAccount) : that.kAccount != null);

    }

    @Override
    public int hashCode() {
        int result = kpp != null ? kpp.hashCode() : 0;
        result = 31 * result + (inn != null ? inn.hashCode() : 0);
        result = 31 * result + (bik != null ? bik.hashCode() : 0);
        result = 31 * result + (kAccount != null ? kAccount.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Requisites{" +
                "kpp='" + kpp + '\'' +
                ", inn='" + inn + '\'' +
                ", bik='" + bik + '\'' +
                ", kAccount='" + kAccount + '\'' +
                '}';
    }
}
