package ru.sberbank.uplatform.sbol.api;

/**
 * Created by baryshnikov on 15.11.15.
 */
public class Coordinates {
    private String qidb;
    private Double latitude, longitude;
    private Integer qid;

    public String getQidb() {
        return qidb;
    }

    public void setQidb(String qidb) {
        this.qidb = qidb;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getQid() {
        return qid;
    }

    public void setQid(Integer qid) {
        this.qid = qid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinates that = (Coordinates) o;

        if (qidb != null ? !qidb.equals(that.qidb) : that.qidb != null) return false;
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null) return false;
        return !(qid != null ? !qid.equals(that.qid) : that.qid != null);

    }

    @Override
    public int hashCode() {
        int result = qidb != null ? qidb.hashCode() : 0;
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (qid != null ? qid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "qidb='" + qidb + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", qid=" + qid +
                '}';
    }
}
