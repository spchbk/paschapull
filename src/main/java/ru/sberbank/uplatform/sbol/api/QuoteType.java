package ru.sberbank.uplatform.sbol.api;

/**
 * Created by baryshnikov on 14.11.15.
 */
public enum QuoteType {
    // metals
    Gold("1", "1", "223"),
    Silver("6", "6", "223"),
    Platinum("7", "28", "223"),
    Palladium("8", "29", "223"),
    // currencies
    Euro("2", "2", "123"),
    Dollar("3", "3", "123"),
    EuroPremier("4", "2", "11008976"),
    DollarPremier("5", "3", "11008976"),
    Pound("9", "12", "123"),
    Frank("10", "18", "123");

    public final String id;
    public final String quoteId;
    public final String infoBlock;

    QuoteType(String id, String quoteId, String infoBlock) {
        this.id = id;
        this.quoteId = quoteId;
        this.infoBlock = infoBlock;
    }

    public static QuoteType byID(String id) {
        if (Gold.id.equals(id)) return Gold;
        if (Euro.id.equals(id)) return Euro;
        if (Dollar.id.equals(id)) return Dollar;
        if (EuroPremier.id.equals(id)) return EuroPremier;
        if (DollarPremier.id.equals(id)) return DollarPremier;
        if (Silver.id.equals(id)) return Silver;
        if (Platinum.id.equals(id)) return Platinum;
        if (Palladium.id.equals(id)) return Palladium;
        if (Pound.id.equals(id)) return Pound;
        if (Frank.id.equals(id)) return Frank;
        return null;
    }

    @Override
    public String toString() {
        return "QuoteType{" +
                "name='" + name() + '\'' +
                ", id='" + id + '\'' +
                ", quoteId='" + quoteId + '\'' +
                ", infoBlock='" + infoBlock + '\'' +
                '}';
    }
}
