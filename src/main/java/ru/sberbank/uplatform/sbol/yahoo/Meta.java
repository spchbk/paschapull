package ru.sberbank.uplatform.sbol.yahoo;

/**
 * @author Pavel Tarasov
 * @since 08/12/2015.
 */
public class Meta {
    private String type;
    private Integer start;
    private Integer count;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Meta meta = (Meta) obj;

        if (type != null ? !type.equals(meta.type) : meta.type != null) return false;
        if (start != null ? !start.equals(meta.start) : meta.start != null) return false;
        return !(count != null ? !count.equals(meta.count) : meta.count != null);
    }

    @Override
    public String toString() {
        return "Meta{" +
                "type=" + type +
                ", start=" + start +
                ", count=" + count +
                '}';
    }
}
