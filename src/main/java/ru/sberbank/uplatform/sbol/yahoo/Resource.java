package ru.sberbank.uplatform.sbol.yahoo;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 08/12/2015.
 */
public class Resource {
    @SerializedName("fields")
    private Fields fields;
    @SerializedName("classname")
    private String classname;

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (fields != null ? fields.hashCode() : 0);
        result = 31 * result + (classname != null ? classname.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Resource res = (Resource) obj;

        return !(fields != null ? !fields.equals(res.fields) : res.fields != null) &&
                !(classname != null ? !classname.equals(res.classname) : res.classname != null);
    }

    @Override
    public String toString() {
        return "Resource{" +
                "fields=" + fields +
                ", classname=" + classname +
                '}';
    }
}
