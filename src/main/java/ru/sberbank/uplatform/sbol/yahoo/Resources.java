package ru.sberbank.uplatform.sbol.yahoo;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 08/12/2015.
 */
public class Resources {
    @SerializedName("resource")
    private Resource resource;

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (resource != null ? resource.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Resources res = (Resources) obj;
        return !(resource != null ? !resource.equals(res.resource) : res.resource != null);
    }

    @Override
    public String toString() {
        return "Resources{" +
                "resource=" + resource +
                '}';
    }
}
