package ru.sberbank.uplatform.sbol.yahoo;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 08/12/2015.
 */
public class YahooResponse {
    @SerializedName("list")
    private List list;

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (list != null ? list.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        YahooResponse yahooResponse = (YahooResponse) obj;
        return !(list != null ? !list.equals(yahooResponse.list) : yahooResponse.list != null);
    }

    @Override
    public String toString() {
        return "YahooResponse{" +
                "list=" + list +
                '}';
    }
}
