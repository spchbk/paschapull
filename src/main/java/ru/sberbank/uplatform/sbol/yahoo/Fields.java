package ru.sberbank.uplatform.sbol.yahoo;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 08/12/2015.
 */
public class Fields {
    @SerializedName("change")
    private Double change;
    @SerializedName("chg_percent")
    private Double changePercent;
    @SerializedName("day_high")
    private Double dayHigh;
    @SerializedName("day_low")
    private Double dayLow;
    @SerializedName("issuer_name")
    private String issuerName;
    @SerializedName("name")
    private String name;
    @SerializedName("symbol")
    private String symbol;
    @SerializedName("price")
    private Double price;
    @SerializedName("utctime")
    private String utctime;
    @SerializedName("year_high")
    private Double yearHigh;
    @SerializedName("year_low")
    private Double yearLow;

    public Double getChange() {
        return change;
    }

    public void setChange(Double change) {
        this.change = change;
    }

    public Double getChangePercent() {
        return changePercent;
    }

    public void setChangePercent(Double changePercent) {
        this.changePercent = changePercent;
    }

    public Double getDayHigh() {
        return dayHigh;
    }

    public void setDayHigh(Double dayHigh) {
        this.dayHigh = dayHigh;
    }

    public Double getDayLow() {
        return dayLow;
    }

    public void setDayLow(Double dayLow) {
        this.dayLow = dayLow;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUtctime() {
        return utctime;
    }

    public void setUtctime(String utctime) {
        this.utctime = utctime;
    }

    public Double getYearHigh() {
        return yearHigh;
    }

    public void setYearHigh(Double yearHigh) {
        this.yearHigh = yearHigh;
    }

    public Double getYearLow() {
        return yearLow;
    }

    public void setYearLow(Double yearLow) {
        this.yearLow = yearLow;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (change != null ? change.hashCode() : 0);
        result = 31 * result + (changePercent != null ? changePercent.hashCode() : 0);
        result = 31 * result + (dayHigh != null ? dayHigh.hashCode() : 0);
        result = 31 * result + (dayLow != null ? dayLow.hashCode() : 0);
        result = 31 * result + (issuerName != null ? issuerName.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (symbol != null ? symbol.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (utctime != null ? utctime.hashCode() : 0);
        result = 31 * result + (yearHigh != null ? yearHigh.hashCode() : 0);
        result = 31 * result + (yearLow != null ? yearLow.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        Fields fields = (Fields) obj;

        if (change != null ? !change.equals(fields.change) : fields.change != null) return false;
        if (changePercent != null ? !changePercent.equals(fields.changePercent) : fields.changePercent != null) return false;
        if (dayHigh != null ? !dayHigh.equals(fields.dayHigh) : fields.dayHigh != null) return false;
        if (dayLow != null ? !dayLow.equals(fields.dayLow) : fields.dayLow != null) return false;
        if (issuerName != null ? !issuerName.equals(fields.issuerName) : fields.issuerName != null) return false;
        if (name != null ? !name.equals(fields.name) : fields.name != null) return false;
        if (symbol != null ? !symbol.equals(fields.symbol) : fields.symbol != null) return false;
        if (price != null ? !price.equals(fields.price) : fields.price != null) return false;
        if (utctime != null ? !utctime.equals(fields.utctime) : fields.utctime != null) return false;
        if (yearHigh != null ? !yearHigh.equals(fields.yearHigh) : fields.yearHigh != null) return false;
        return !(yearLow != null ? !yearLow.equals(fields.yearLow) : fields.yearLow != null);
    }

    @Override
    public String toString() {
        return "Fields{" +
                "change=" + change +
                ", changePercent=" + changePercent +
                ", dayHigh=" + dayHigh +
                ", dayLow=" + dayLow +
                ", issuerName=" + issuerName +
                ", name=" + name +
                ", symbol=" + symbol +
                ", price=" + price +
                ", utctime=" + utctime +
                ", yearHigh=" + yearHigh +
                ", yearLow=" + yearLow +
                '}';
    }
}
