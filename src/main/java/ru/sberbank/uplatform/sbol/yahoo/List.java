package ru.sberbank.uplatform.sbol.yahoo;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 08/12/2015.
 */
public class List {
    @SerializedName("meta")
    private Meta meta;
    @SerializedName("resources")
    private java.util.List<Resources> resources;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public java.util.List<Resources> getResources() {
        return resources;
    }

    public void setResources(java.util.List<Resources> resources) {
        this.resources = resources;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (meta != null ? meta.hashCode() : 0);
        result = 31 * result + (resources != null ? resources.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        List list = (List) obj;

        return !(meta != null ? !meta.equals(list.meta) : list.meta != null) &&
                !(resources != null ? !resources.equals(list.resources) : list.resources != null);
    }

    @Override
    public String toString() {
        return "List{" +
                "meta=" + meta +
                ", resources=" + resources +
                '}';
    }
}
