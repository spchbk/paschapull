package ru.sberbank.uplatform.sbol.geolocations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baryshnikov on 21.11.15.
 */
@XmlRootElement(name = "state")
public class State {
    private String name;
    private Integer opcode;

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "opcode")
    public Integer getOpcode() {
        return opcode;
    }

    public void setOpcode(Integer opcode) {
        this.opcode = opcode;
    }
}
