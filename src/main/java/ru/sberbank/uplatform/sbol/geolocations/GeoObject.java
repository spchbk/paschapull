package ru.sberbank.uplatform.sbol.geolocations;

import ru.sberbank.uplatform.sbol.api.Location;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baryshnikov on 18.11.15.
 */
@XmlRootElement(name = "geo-object")
public class GeoObject {

    private String objType;
    private String companyId;
    private String phone;
    private String name;
    private String street;
    private String house;
    private String accessMode;
    private String workingTime;
    private Location coordinate;
    private Double distance;
    private State state;

    public String getObjType() {
        return objType;
    }

    @XmlElement(name = "state")
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @XmlElement(name = "obj-type")
    public void setObjType(String objType) {
        this.objType = objType;
    }

    public String getCompanyId() {
        return companyId;
    }

    @XmlElement(name = "company_id")
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getPhone() {
        return phone;
    }

    @XmlElement(name = "phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    @XmlElement(name = "street")
    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    @XmlElement(name = "house")
    public void setHouse(String house) {
        this.house = house;
    }

    public String getAccessMode() {
        return accessMode;
    }

    @XmlElement(name = "access_mode")
    public void setAccessMode(String accessMode) {
        this.accessMode = accessMode;
    }

    public Location getCoordinate() {
        return coordinate;
    }

    @XmlElement(name = "coordinate")
    public void setCoordinate(Location coordinate) {
        this.coordinate = coordinate;
    }

    public String getWorkingTime() {
        return workingTime;
    }

    @XmlElement(name = "working_time")
    public void setWorkingTime(String workingTime) {
        this.workingTime = workingTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoObject geoObject = (GeoObject) o;

        if (objType != null ? !objType.equals(geoObject.objType) : geoObject.objType != null) return false;
        return coordinate.equals(geoObject.coordinate);

    }

    @Override
    public int hashCode() {
        int result = objType != null ? objType.hashCode() : 0;
        result = 31 * result + coordinate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "GeoObject{" +
                "objType='" + objType + '\'' +
                ", companyId='" + companyId + '\'' +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", street='" + street + '\'' +
                ", house='" + house + '\'' +
                ", accessMode='" + accessMode + '\'' +
                ", workingTime='" + workingTime + '\'' +
                ", coordinate=" + coordinate +
                ", distance=" + distance +
                '}';
    }
}
