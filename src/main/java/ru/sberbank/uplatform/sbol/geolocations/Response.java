package ru.sberbank.uplatform.sbol.geolocations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by baryshnikov on 18.11.15.
 */
@XmlRootElement
public class Response {
    Double lon; //longitude of sender
    Double lat; //latitude of sender
    GeoObjects geoObjects;

    public Double getLon() {
        return lon;
    }

    @XmlElement(name = "lon")
    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    @XmlElement(name = "lat")
    public void setLat(Double lat) {
        this.lat = lat;
    }

    public GeoObjects getGeoObjects() {
        return geoObjects;
    }

    @XmlElement(name = "geo-objects")
    public void setGeoObjects(GeoObjects geoObjects) {
        this.geoObjects = geoObjects;
    }

    @Override
    public String toString() {
        return "Response{" +
                "lon=" + lon +
                ", lat=" + lat +
                ", geoObjects=" + geoObjects +
                '}';
    }
}
