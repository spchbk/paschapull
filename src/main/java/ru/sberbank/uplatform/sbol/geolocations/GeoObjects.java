package ru.sberbank.uplatform.sbol.geolocations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 19/11/2015.
 */
@XmlRootElement(name = "geo-objects")
public class GeoObjects {
    private List<GeoObject> geoObjects;

    public List<GeoObject> getGeoObjects() {
        return geoObjects;
    }

    @XmlElement(name = "geo-object")
    public void setGeoObjects(List<GeoObject> geoObjects) {
        this.geoObjects = geoObjects;
    }

    @Override
    public String toString() {
        return "GeoObjects{" +
                "geoObjects=" + geoObjects +
                '}';
    }
}
