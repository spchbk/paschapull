package ru.sberbank.uplatform;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;

import ru.sberbank.uplatform.consumption.ConsumptionBean;
import ru.sberbank.uplatform.consumption.ConsumptionStrConv;
import ru.sberbank.uplatform.route.CustomRouteBuilder;
import ru.sberbank.uplatform.route.Keyboard;

/**
 * Доходы и расходы: меню
 * 
 * https://app.asana.com/0/82615734087262/138327778231152/f
 * 
 * @author Dmitriy_Gulyaev
 *
 */

public class BalanceRoutes2 extends CustomRouteBuilder {

    private Map<Long, ConsumptionBean> consumptionMap = new HashMap<>();

//@formatter:off
    @Override
    public void configure() {

        from(Endpoints.CR_NEW_ADD)
            .to(Endpoints.CHECK_INIT_CONSUMPTION_OR_REVENUE2.endpoint)
            
            .choice()
            .when(header(KEY_SHOW_CR_AGREEMENT).isEqualTo(Boolean.TRUE))
                .to(Endpoints.SHOW_AGREEMENT_CONSUMPTION_OR_REVENUE2.endpoint)
            .otherwise()
                .to(Endpoints.CR_NEW_ADD_START.endpoint)
            .end();

        from(Endpoints.CR_NEW_ADD_START)
            .process(exchange -> {
                setKeyboard(exchange, Keyboard.main_menu);
                ConsumptionBean consumptionBean = getConsumptionBean(exchange);
                if (consumptionBean.getConsumptionMode() == null) {
                    consumptionBean.setConsumptionMode(ConsumptionStrConv.getConsumptionModeSimple(getMessageText(exchange)));
                    saveConsumptionBean(consumptionBean);
                }
                setTemplate(exchange, "cr_add_step_1");
                setKeyboard(exchange, Keyboard.hide_menu);
                setHeader(exchange, KEY_CR_BEAN, consumptionBean); 
            })
            .to(Endpoints.RENDER_TEMPLATE.endpoint)
            .process(setStateProcessor(ClientState.CR_ADD_STEP_1))
            .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.SHOW_AGREEMENT_CONSUMPTION_OR_REVENUE2)
                .process(exchange -> {
                        ConsumptionBean consumptionBean = getConsumptionBean(exchange);
                        consumptionBean.setConsumptionMode(ConsumptionStrConv.getConsumptionModeSimple(getMessageText(exchange)));
                        saveConsumptionBean(consumptionBean);
                        setState(exchange, ClientState.CR_ADD_CHECK_AGREEMENT_ACCEPT);
                        setTemplate(exchange, "add-consumption-or-revenue-agreement");
                        setKeyboard(exchange, Keyboard.consumption_agreement);
                    })
                .to(Endpoints.RENDER_TEMPLATE.endpoint)
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(ClientState.CR_ADD_CHECK_AGREEMENT_ACCEPT)
                .process(setStateProcessor(ClientState.IDLE))
                .choice()
                .when(method(IsAgreementAcceptedBean.class))
                    .to(Endpoints.CR_NEW_ADD_START.endpoint)
                .otherwise()
                    .process(exchange -> {
                        setTemplate(exchange, "add-consumption-or-revenue-agreement-refuse");
                        setKeyboard(exchange, Keyboard.main_menu);
                    })
                    .to(Endpoints.RENDER_TEMPLATE.endpoint)
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .end();

        from(ClientState.CR_ADD_STEP_1)
            .process(exchange -> {
            String text = getMessageText(exchange);
            try {
                BigDecimal bigDecimal = new BigDecimal(text);
                ConsumptionBean consumptionBean = getConsumptionBean(exchange);
                consumptionBean.setValue(bigDecimal);
                saveConsumptionBean(consumptionBean);
            } catch(Exception e){
                e.printStackTrace();
                setHeader(exchange, KEY_WRONG_SUM_FORMAT, Boolean.TRUE);
            }
        })
            .choice()
            .when(header(KEY_WRONG_SUM_FORMAT).isEqualTo(Boolean.TRUE))
                .to(Endpoints.CR_NEW_ADD_START.endpoint)
            .otherwise()
                .process(exchange -> {
                    setHeader(exchange, KEY_CR_BEAN, getConsumptionBean(exchange));
                    setTemplate(exchange, "cr_add_step_2");
                    setKeyboard(exchange, Keyboard.hide_menu);
                    ConsumptionBean consumptionBean = getConsumptionBean(exchange);
                    setState(exchange,consumptionBean.getConsumptionMode() ? ClientState.CR_ADD_STEP_2 : ClientState.CR_ADD_STEP_2_REVENUE);   
                })
                .to(Endpoints.RENDER_TEMPLATE.endpoint)
                .to(Endpoints.ADD_TEXT_REPLY.endpoint)
            .end();

        from(ClientState.CR_ADD_STEP_2_REVENUE)
            .process(exchange -> {
                ConsumptionBean consumptionBean = getConsumptionBean(exchange);
                consumptionBean.setPurpose(getMessageText(exchange));
                saveConsumptionBean(consumptionBean);
            })
            .to(Endpoints.CR_NEW_ADD_FINISH.endpoint);

        from(ClientState.CR_ADD_STEP_2)
            .process(exchange -> {
                ConsumptionBean consumptionBean = getConsumptionBean(exchange);
                consumptionBean.setPurpose(getMessageText(exchange));
                saveConsumptionBean(consumptionBean);

                setHeader(exchange, KEY_CR_BEAN, consumptionBean);
                setTemplate(exchange, "cr_add_step_3");
                setKeyboard(exchange, consumptionBean.getConsumptionMode() ? Keyboard.consumption_category_menu
                    : Keyboard.revenue_category_menu);
                setState(exchange, ClientState.CR_ADD_STEP_3);
            })
            .to(Endpoints.RENDER_TEMPLATE.endpoint)
            .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(ClientState.CR_ADD_STEP_3)
            .process(exchange -> {
                ConsumptionBean consumptionBean = getConsumptionBean(exchange);
                consumptionBean.setCategory(getMessageText(exchange));
                saveConsumptionBean(consumptionBean);
            })
            .to(Endpoints.CR_NEW_ADD_FINISH.endpoint);

        from(Endpoints.CR_NEW_ADD_FINISH)
            .process(exchange -> {
                setHeader(exchange, KEY_CR_BEAN, getConsumptionBean(exchange));
                setTemplate(exchange, "add-consumption-or-revenue");
                setState(exchange, ClientState.IDLE);
                setKeyboard(exchange, Keyboard.consumption_or_revenue_menu);
            })
            .to(Endpoints.ADD_CONSUMPTION_OR_REVENUE_DB.endpoint)
            .choice()
            .when(header(KEY_WRONG_CR_CATEGORY).isEqualTo(Boolean.TRUE))
                .to(ClientState.CR_ADD_STEP_2.endpoint)
             .otherwise()
                 .process(exchange -> {removeConsumptionBean(exchange);})
                 .to(Endpoints.RENDER_TEMPLATE.endpoint)
                 .to(Endpoints.ADD_TEXT_REPLY.endpoint)
             .end();


        from(Endpoints.CR_NEW_SUB_MENU).process(exchange -> {
            setTemplate(exchange, "choose-operation");
            setKeyboard(exchange, Keyboard.consumption_or_revenue_menu);
        })
        .to(Endpoints.RENDER_TEMPLATE.endpoint)
        .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.GET_CONSUMPTION_OR_REVENUE_STAT).process(exchange -> {
            boolean isCons = ConsumptionStrConv.getConsumptionModeSimple(getMessageText(exchange));
            setMessageText(exchange, "неделю");
            setState(exchange, isCons ? ClientState.STAT_CONSUMPTION : ClientState.STAT_REVENUE);
            setKeyboard(exchange, Keyboard.consumption_or_revenue_sub_menu);
        }).to(Endpoints.ROUTER.endpoint);
        
        from(ClientState.STAT_CONSUMPTION).process(exchange -> {
            processCVSubMenu(exchange, "расходы");
        })
        .to(Endpoints.FIND_ENDPOINT_BY_CONTENT.endpoint);

        from(ClientState.STAT_REVENUE).process(exchange -> {
            processCVSubMenu(exchange, "доходы");
        })
        .to(Endpoints.FIND_ENDPOINT_BY_CONTENT.endpoint);

    }
//@formatter:on

    private void processCVSubMenu(Exchange exchange, String opName) {
        String text = getMessageText(exchange);
        if (text.equalsIgnoreCase(BUTTON_TEXT_BACK)) {
            setMessageText(exchange, BUTTON_TEXT_CV_MENU);
            setState(exchange, ClientState.IDLE);
        } else {
            setMessageText(exchange, opName + ' ' + getMessageText(exchange));
        }
    }

    private ConsumptionBean getConsumptionBean(Exchange exchange) {
        Long userId = getUserId(exchange);
        ConsumptionBean consumptionBean = consumptionMap.get(userId);
        if (consumptionBean == null) {
            consumptionBean = new ConsumptionBean();
            consumptionBean.setUserId(userId);
        }
        return consumptionBean;
    }

    private ConsumptionBean removeConsumptionBean(Exchange exchange) {
        Long userId = getUserId(exchange);
        ConsumptionBean consumptionBean = consumptionMap.remove(userId);
        return consumptionBean;
    }

    private void saveConsumptionBean(ConsumptionBean consumptionBean) {
        consumptionMap.put(consumptionBean.getUserId(), consumptionBean);
    }

}
