package ru.sberbank.uplatform;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.states.State;
import ru.sberbank.uplatform.states.UnexpectedMessage;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by baryshnikov on 02.12.15.
 */
public class StatesManager {
    @Autowired
    private List<State> states; //List of all states, created by Spring
    private Map<String, State> statesMap = new HashMap<>();

    @PostConstruct
    public void build() {
        states.forEach(x -> statesMap.put(x.getName(), x));
    }

    /**
     * Detect, that exists state with required name
     *
     * @param cl
     * @return
     */
    public boolean hasRouteForClient(Client cl) {
        return statesMap.containsKey(cl.getState());
    }

    public boolean route(Client client, final Message message, List<Reply> replies) {
        if (!statesMap.containsKey(client.getState())) {
            return false;
        }
        State st = statesMap.get(client.getState());
        try {
            String nextRoute = st.onMessage(client, message, replies);
            client.setState(nextRoute);
            return true;
        } catch (Exception ex) {
            client.setState("");
            return false;
        }
    }


    public List<State> getStates() {
        return states;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }
}
