package ru.sberbank.uplatform.client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.SetArgs;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.api.ClientsManager;

import java.lang.reflect.Type;
import java.util.Optional;

/**
 * Created by baryshnikov on 02.12.15.
 */
public class RedisClientManager<T> implements ClientsManager<T> {

    @Value("${client_state_keep_alive}")
    private long keepAlive;
    @Autowired
    @Qualifier("clientStateDb")
    private RedisClient redisClient;
    private Gson converter = new Gson();
    private Logger logger = LoggerFactory.getLogger(RedisClientManager.class);

    @Override
    public Optional<T> getState(String clientID, Class<T> clasz) {
        StatefulRedisConnection<String, String> connection = null;
        RedisCommands<String, String> syncCommands = null;
        try {
            connection = redisClient.connect();
            syncCommands = connection.sync();
            String payload = syncCommands.get(clientID);
            if (payload == null || payload.isEmpty()) return Optional.empty();
            return Optional.of(converter.fromJson(payload, clasz));
        } catch (Exception ex) {
            logger.error("Failed get state", ex);
            return Optional.empty();
        } finally {
            //This is dirty and old exception processor due to
            //redis library does not support AutoCloseable and I am not
            //sure that library not raise RuntimeException
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                    logger.error("Failed close redis connection", ex);
                }
            }

        }
    }

    @Override
    public boolean setState(String clientID, T state) {
        StatefulRedisConnection<String, String> connection = null;
        RedisCommands<String, String> syncCommands = null;
        try {
            connection = redisClient.connect();
            syncCommands = connection.sync();
            String payload = converter.toJson(state);
            return "OK".equals(syncCommands.set(clientID, payload, new SetArgs().ex(keepAlive)));
        } catch (Exception ex) {
            logger.error("Failed set state", ex);
            return false;
        } finally {
            //This is dirty and old exception processor due to
            //redis library does not support AutoCloseable and I am not
            //sure that library not raise RuntimeException
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                    logger.error("Failed close redis connection", ex);
                }
            }

        }
    }

    /**
     * Get delay in seconds when client state will be removed
     *
     * @return
     */
    public long getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(long keepAlive) {
        this.keepAlive = keepAlive;
    }

    public RedisClient getRedisClient() {
        return redisClient;
    }

    public void setRedisClient(RedisClient redisClient) {
        this.redisClient = redisClient;
    }
}
