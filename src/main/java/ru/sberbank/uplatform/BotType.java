package ru.sberbank.uplatform;

/**
 * @author Pavel Tarasov
 * @since 12/05/2016.
 */
public final class BotType {
    public static final int TELEGRAM = 1;
    public static final int FB = 2;
}
