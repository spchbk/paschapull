package ru.sberbank.uplatform.upload;

import com.google.gson.Gson;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.SetArgs;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.api.UploadCache;
import ru.sberbank.uplatform.client.RedisClientManager;

import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by baryshnikov on 24.12.15.
 */
public class RedisCache implements UploadCache {
    @Autowired
    @Qualifier("fileCacheDb")
    private RedisClient redisClient;
    private Gson converter = new Gson();
    private Logger logger = LoggerFactory.getLogger(RedisClientManager.class);
    private ReadWriteLock accessLock = new ReentrantReadWriteLock();

    @Override
    public Optional<String> getUploadedId(String filename) {
        Lock lock = accessLock.readLock();
        lock.lock();
        Optional<String> s = get(filename);
        lock.unlock();
        return s;
    }

    @Override
    public void setUploadId(String filename, String id) {
        Lock lock = accessLock.writeLock();
        lock.lock();
        if (get(filename).isPresent()) {
            lock.unlock();
            return;
        }

        StatefulRedisConnection<String, String> connection = null;
        RedisCommands<String, String> syncCommands = null;
        try {
            connection = redisClient.connect();
            syncCommands = connection.sync();
            syncCommands.set(filename, id);
        } catch (Exception ex) {
            logger.error("Failed set state", ex);
        } finally {
            //This is dirty and old exception processor due to
            //redis library does not support AutoCloseable and I am not
            //sure that library not raise RuntimeException
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                    logger.error("Failed close redis connection", ex);
                }
            }
            lock.unlock();
        }
    }

    private Optional<String> get(String id) {
        StatefulRedisConnection<String, String> connection = null;
        RedisCommands<String, String> syncCommands = null;
        try {
            connection = redisClient.connect();
            syncCommands = connection.sync();
            String payload = syncCommands.get(id);
            if (payload == null || payload.isEmpty()) return Optional.empty();
            return Optional.of(payload);
        } catch (Exception ex) {
            logger.error("Failed get state", ex);
            return Optional.empty();
        } finally {
            //This is dirty and old exception processor due to
            //redis library does not support AutoCloseable and I am not
            //sure that library not raise RuntimeException
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                    logger.error("Failed close redis connection", ex);
                }
            }


        }
    }


}
