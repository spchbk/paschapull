package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents a venue.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class Venue {

    /** Venue location. */
    private Location location;

    /** Name of the venue. */
    private String title;

    /** Address of the venue. */
    private String address;

    /** Optional. Foursquare identifier of the venue. */
    @SerializedName("foursquare_id")
    private String foursquare;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFoursquare() {
        return foursquare;
    }

    public void setFoursquare(String foursquare) {
        this.foursquare = foursquare;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Venue venue = (Venue) o;

        if (address != null ? !address.equals(venue.address) : venue.address != null) return false;
        if (foursquare != null ? !foursquare.equals(venue.foursquare) : venue.foursquare != null) return false;
        if (location != null ? !location.equals(venue.location) : venue.location != null) return false;
        if (title != null ? !title.equals(venue.title) : venue.title != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = location != null ? location.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (foursquare != null ? foursquare.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Venue{" +
                "location=" + location +
                ", title='" + title + '\'' +
                ", address='" + address + '\'' +
                ", foursquare='" + foursquare + '\'' +
                '}';
    }
}
