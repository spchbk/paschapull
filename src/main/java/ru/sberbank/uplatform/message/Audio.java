package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents an audio file to be treated as music by the Telegram clients.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class Audio {

    /** Unique identifier for this file. */
    @SerializedName("file_id")
    private String fileID;

    /** Duration of the audio in seconds as defined by sender. */
    private Integer duration;

    /** Optional. Performer of the audio as defined by sender or by audio tags. */
    private String performer;

    /** Optional. Title of the audio as defined by sender or by audio tags. */
    private String title;

    /** Optional. MIME type of the file as defined by sender. */
    @SerializedName("mime_type")
    private String mimeType;

    /** Optional. File size. */
    @SerializedName("file_size")
    private Integer fileSize;

    public String getFileID() {
        return fileID;
    }

    public void setFileID(String fileID) {
        this.fileID = fileID;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Audio audio = (Audio) o;

        if (!fileID.equals(audio.fileID)) return false;
        if (duration != null ? !duration.equals(audio.duration) : audio.duration != null) return false;
        if (performer != null ? !performer.equals(audio.performer) : audio.performer != null) return false;
        if (title != null ? !title.equals(audio.title) : audio.title != null) return false;
        if (mimeType != null ? !mimeType.equals(audio.mimeType) : audio.mimeType != null) return false;
        return !(fileSize != null ? !fileSize.equals(audio.fileSize) : audio.fileSize != null);

    }

    @Override
    public int hashCode() {
        int result = fileID.hashCode();
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (performer != null ? performer.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Audio{" +
                "fileID='" + fileID + '\'' +
                ", duration=" + duration +
                ", performer='" + performer + '\'' +
                ", title='" + title + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}