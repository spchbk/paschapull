package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents one size of a photo or a file/sticker thumbnail.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class PhotoSize {

    /** Unique identifier for this file. */
    @SerializedName("file_id")
    private String fileID;

    /** Photo width. */
    private Integer width;

    /** Photo height. */
    private Integer height;

    /** Optional. File size. */
    @SerializedName("file_size")
    private Integer fileSize;

    public String getFileID() {
        return fileID;
    }

    public void setFileID(String fileID) {
        this.fileID = fileID;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhotoSize photoSize = (PhotoSize) o;

        if (!fileID.equals(photoSize.fileID)) return false;
        if (width != null ? !width.equals(photoSize.width) : photoSize.width != null) return false;
        if (height != null ? !height.equals(photoSize.height) : photoSize.height != null) return false;
        return !(fileSize != null ? !fileSize.equals(photoSize.fileSize) : photoSize.fileSize != null);

    }

    @Override
    public int hashCode() {
        int result = fileID.hashCode();
        result = 31 * result + (width != null ? width.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PhotoSize{" +
                "fileID='" + fileID + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", fileSize=" + fileSize +
                '}';
    }
}