package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents a voice note.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class Voice {

    /** Unique identifier for this file. */
    @SerializedName("file_id")
    private String fileID;

    /** Duration of the audio in seconds as defined by sender. */
    private Integer duration;

    /** Optional. MIME type of the file as defined by sender. */
    @SerializedName("mime_type")
    private String mimeType;

    /** Optional. File size. */
    @SerializedName("file_size")
    private Integer fileSize;

    public String getFileID() {
        return fileID;
    }

    public void setFileID(String fileID) {
        this.fileID = fileID;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Voice voice = (Voice) o;

        if (!fileID.equals(voice.fileID)) return false;
        if (duration != null ? !duration.equals(voice.duration) : voice.duration != null) return false;
        if (mimeType != null ? !mimeType.equals(voice.mimeType) : voice.mimeType != null) return false;
        return !(fileSize != null ? !fileSize.equals(voice.fileSize) : voice.fileSize != null);
    }

    @Override
    public int hashCode() {
        int result = fileID.hashCode();
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Voice{" +
                "fileID='" + fileID + '\'' +
                ", duration=" + duration +
                ", mimeType='" + mimeType + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}