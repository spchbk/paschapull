package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a contact with a phone number. By default, this contact will be sent
 * by the user. Alternatively, you can use {@code input_message_content} to send a
 * message with the specified content instead of the contact.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultContact extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "contact";

    /** Contact's phone number. */
    @SerializedName("phone_number")
    private String phoneNumber;

    /** Contact's first name. */
    @SerializedName("first_name")
    private String firstName;

    /** Optional. Contact's last name. */
    @SerializedName("last_name")
    private String lastName;

    /** Optional. Thumbnail width. */
    @SerializedName("thumb_width")
    private Integer thumbWidth;

    /** Optional. Thumbnail height. */
    @SerializedName("thumb_height")
    private Integer thumbHeight;

    public String getType() {
        return type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(Integer thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public Integer getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(Integer thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultContact that = (InlineQueryResultContact) o;

        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        if (thumbHeight != null ? !thumbHeight.equals(that.thumbHeight) : that.thumbHeight != null) return false;
        if (thumbWidth != null ? !thumbWidth.equals(that.thumbWidth) : that.thumbWidth != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (thumbWidth != null ? thumbWidth.hashCode() : 0);
        result = 31 * result + (thumbHeight != null ? thumbHeight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultContact{" +
                "type='" + type + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", thumbWidth=" + thumbWidth +
                ", thumbHeight=" + thumbHeight +
                "} " + super.toString();
    }
}