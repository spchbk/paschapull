package ru.sberbank.uplatform.message.inline.input;

import com.google.gson.annotations.SerializedName;

/**
 * Represents the content of a text message to be sent as the result of an inline query.
 *
 * @author Tarasov Pavel.
 * @since 26.04.16.
 */
public class InputTextMessageContent implements InputMessageContent {

    /** Text of the message to be sent, 1-4096 characters. */
    @SerializedName("message_text")
    private String messageText;

    /** Optional. Send message with HTML style syntax. */
    @SerializedName("parse_mode")
    private String parseMode = "HTML";

    /** Optional. Disables link previews for links in the sent message. */
    @SerializedName("disable_web_page_preview")
    private Boolean disableWebPagePreview = Boolean.TRUE;

    public InputTextMessageContent(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getParseMode() {
        return parseMode;
    }

    public void setParseMode(String parseMode) {
        this.parseMode = parseMode;
    }

    public Boolean getDisableWebPagePreview() {
        return disableWebPagePreview;
    }

    public void setDisableWebPagePreview(Boolean disableWebPagePreview) {
        this.disableWebPagePreview = disableWebPagePreview;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InputTextMessageContent that = (InputTextMessageContent) o;

        if (disableWebPagePreview != null ? !disableWebPagePreview.equals(that.disableWebPagePreview) : that.disableWebPagePreview != null) return false;
        if (messageText != null ? !messageText.equals(that.messageText) : that.messageText != null) return false;
        return !(parseMode != null ? !parseMode.equals(that.parseMode) : that.parseMode != null);
    }

    @Override
    public int hashCode() {
        int result = messageText != null ? messageText.hashCode() : 0;
        result = 31 * result + (parseMode != null ? parseMode.hashCode() : 0);
        result = 31 * result + (disableWebPagePreview != null ? disableWebPagePreview.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InputTextMessageContent{" +
                "messageText='" + messageText + '\'' +
                ", parseMode='" + parseMode + '\'' +
                ", disableWebPagePreview=" + disableWebPagePreview +
                '}';
    }
}