package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a link to a video animation (H.264/MPEG-4 AVC video without sound).
 * By default, this animated MPEG-4 file will be sent by the user with optional
 * caption. Alternatively, you can use {@code input_message_content} to send a
 * message with the specified content instead of the animation.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultMpeg4Gif extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "mpeg4_gif";

    /** A valid URL for the MP4 file. File size must not exceed 1MB. */
    @SerializedName("mpeg4_url")
    private String mpeg4Url;

    /** Optional. Video width. */
    @SerializedName("mpeg4_width")
    private Integer mpeg4Width;

    /** Optional. Video height. */
    @SerializedName("mpeg4_height")
    private Integer mpeg4Height;

    /** Optional. Caption of the MPEG-4 file to be sent, 0-200 characters. */
    private String caption;

    public String getType() {
        return type;
    }

    public String getMpeg4Url() {
        return mpeg4Url;
    }

    public void setMpeg4Url(String mpeg4Url) {
        this.mpeg4Url = mpeg4Url;
    }

    public Integer getMpeg4Width() {
        return mpeg4Width;
    }

    public void setMpeg4Width(Integer mpeg4Width) {
        this.mpeg4Width = mpeg4Width;
    }

    public Integer getMpeg4Height() {
        return mpeg4Height;
    }

    public void setMpeg4Height(Integer mpeg4Height) {
        this.mpeg4Height = mpeg4Height;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultMpeg4Gif that = (InlineQueryResultMpeg4Gif) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (mpeg4Height != null ? !mpeg4Height.equals(that.mpeg4Height) : that.mpeg4Height != null) return false;
        if (mpeg4Url != null ? !mpeg4Url.equals(that.mpeg4Url) : that.mpeg4Url != null) return false;
        if (mpeg4Width != null ? !mpeg4Width.equals(that.mpeg4Width) : that.mpeg4Width != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (mpeg4Url != null ? mpeg4Url.hashCode() : 0);
        result = 31 * result + (mpeg4Width != null ? mpeg4Width.hashCode() : 0);
        result = 31 * result + (mpeg4Height != null ? mpeg4Height.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultMpeg4Gif{" +
                "type='" + type + '\'' +
                ", mpeg4Url='" + mpeg4Url + '\'' +
                ", mpeg4Width=" + mpeg4Width +
                ", mpeg4Height=" + mpeg4Height +
                ", caption='" + caption + '\'' +
                "} " + super.toString();
    }
}