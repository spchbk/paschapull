package ru.sberbank.uplatform.message.inline.editor;

/**
 * Used in method of the change an existing caption in messages.
 *
 * @author Tarasov Pavel.
 * @since 29.04.16.
 */
public class EditMessageCaption extends EditMessageReplyMarkup {

    /** Optional. New caption of the message. */
    private String caption;

    @Override
    public String getMethodName() {
        return "editMessageCaption";
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EditMessageCaption that = (EditMessageCaption) o;
        return !(caption != null ? !caption.equals(that.caption) : that.caption != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EditMessageCaption{" +
                "caption='" + caption + '\'' +
                "} " + super.toString();
    }
}