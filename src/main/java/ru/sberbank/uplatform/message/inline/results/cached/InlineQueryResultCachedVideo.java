package ru.sberbank.uplatform.message.inline.results.cached;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResult;

/**
 * Represents a link to a video file stored on the Telegram servers. By default,
 * this video file will be sent by the user with an optional caption. Alternatively,
 * you can use {@code input_message_content} to send a message with the specified
 * content instead of the video.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultCachedVideo extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "video";

    /** A valid file identifier for the video file. */
    @SerializedName("video_file_id")
    private String videoFileId;

    /** Optional. Short description of the result. */
    private String description;

    /** Optional. Caption of the video to be sent, 0-200 characters. */
    private String caption;

    public String getType() {
        return type;
    }

    public String getVideoFileId() {
        return videoFileId;
    }

    public void setVideoFileId(String videoFileId) {
        this.videoFileId = videoFileId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultCachedVideo that = (InlineQueryResultCachedVideo) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (!type.equals(that.type)) return false;
        return !(videoFileId != null ? !videoFileId.equals(that.videoFileId) : that.videoFileId != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (videoFileId != null ? videoFileId.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultCachedVideo{" +
                "type='" + type + '\'' +
                ", videoFileId='" + videoFileId + '\'' +
                ", description='" + description + '\'' +
                ", caption='" + caption + '\'' +
                "} " + super.toString();
    }
}