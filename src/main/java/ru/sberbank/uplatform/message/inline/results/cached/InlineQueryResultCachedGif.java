package ru.sberbank.uplatform.message.inline.results.cached;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResult;

/**
 * Represents a link to an animated GIF file stored on the Telegram servers.
 * By default, this animated GIF file will be sent by the user with an optional
 * caption. Alternatively, you can use {@code input_message_content} to send a
 * message with specified content instead of the animation.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultCachedGif extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "gif";

    /** A valid file identifier for the GIF file. */
    @SerializedName("gif_file_id")
    private String gifFileId;

    /** Optional. Caption of the GIF file to be sent, 0-200 characters. */
    private String caption;

    public String getType() {
        return type;
    }

    public String getGifFileId() {
        return gifFileId;
    }

    public void setGifFileId(String gifFileId) {
        this.gifFileId = gifFileId;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultCachedGif that = (InlineQueryResultCachedGif) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (gifFileId != null ? !gifFileId.equals(that.gifFileId) : that.gifFileId != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (gifFileId != null ? gifFileId.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultCachedGif{" +
                "type='" + type + '\'' +
                ", gifFileId='" + gifFileId + '\'' +
                ", caption='" + caption + '\'' +
                "} " + super.toString();
    }
}