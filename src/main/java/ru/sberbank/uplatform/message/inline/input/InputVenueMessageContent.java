package ru.sberbank.uplatform.message.inline.input;

import com.google.gson.annotations.SerializedName;

/**
 * Represents the content of a venue message to be sent as the result of an inline query.
 *
 * @author Tarasov Pavel.
 * @since 26.04.16.
 */
public class InputVenueMessageContent implements InputMessageContent {

    /** Latitude of the location in degrees. */
    private Float latitude;

    /** Longitude of the location in degrees. */
    private Float longitude;

    /** Name of the venue. */
    private String title;

    /** Address of the venue. */
    private String address;

    /** Optional. Foursquare identifier of the venue, if known. */
    @SerializedName("foursquare_id")
    private String foursquareId;

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFoursquareId() {
        return foursquareId;
    }

    public void setFoursquareId(String foursquareId) {
        this.foursquareId = foursquareId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InputVenueMessageContent that = (InputVenueMessageContent) o;

        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (foursquareId != null ? !foursquareId.equals(that.foursquareId) : that.foursquareId != null) return false;
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null) return false;
        return !(title != null ? !title.equals(that.title) : that.title != null);
    }

    @Override
    public int hashCode() {
        int result = latitude != null ? latitude.hashCode() : 0;
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (foursquareId != null ? foursquareId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InputVenueMessageContent{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", title='" + title + '\'' +
                ", address='" + address + '\'' +
                ", foursquareId='" + foursquareId + '\'' +
                '}';
    }
}