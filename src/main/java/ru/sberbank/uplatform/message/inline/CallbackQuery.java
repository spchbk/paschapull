package ru.sberbank.uplatform.message.inline;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.User;

/**
 * This object represents an incoming callback query
 * from a callback button in an inline keyboard.
 *
 * If the button that originated the query was attached to a message
 * sent by the bot, the field message will be presented.
 * If the button was attached to a message sent via the bot (in inline mode),
 * the field inline_message_id will be presented.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class CallbackQuery {

    /** Unique identifier for this query. */
    private String id;

    /** Sender. */
    private User from;

    /**
     * Optional. Message with the callback button that originated the query.
     * Note that message content and message date will not be available
     * if the message is too old.
     */
    private Message message;

    /**
     * Optional. Identifier of the message sent via the bot in inline mode,
     * that originated the query.
     */
    @SerializedName("inline_message_id")
    private String inlineMessageId;

    /**
     * Data associated with the callback button.
     * Be aware that a bad client can send arbitrary data in this field.
     */
    private String data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getInlineMessageId() {
        return inlineMessageId;
    }

    public void setInlineMessageId(String inlineMessageId) {
        this.inlineMessageId = inlineMessageId;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CallbackQuery that = (CallbackQuery) o;

        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (from != null ? !from.equals(that.from) : that.from != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (inlineMessageId != null ? !inlineMessageId.equals(that.inlineMessageId) : that.inlineMessageId != null)
            return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (from != null ? from.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (inlineMessageId != null ? inlineMessageId.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CallbackQuery{" +
                "id='" + id + '\'' +
                ", from=" + from +
                ", message=" + message +
                ", inlineMessageId='" + inlineMessageId + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
