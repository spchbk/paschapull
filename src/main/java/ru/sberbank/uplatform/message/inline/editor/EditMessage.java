package ru.sberbank.uplatform.message.inline.editor;

/**
 * Interface for all classes of the edit message.
 *
 * @author Tarasov Pavel.
 * @since 29.04.16.
 */
public interface EditMessage {

    /** Name of the Telegram API method. */
    String getMethodName();
}
