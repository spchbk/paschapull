package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a link to a file. By default, this file will be sent by the user with
 * an optional caption. Alternatively, you can use {@code input_message_content} to
 * send a message with the specified content instead of the file. Currently, only
 * PDF and ZIP files can be sent using this method.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultDocument extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "document";

    /** Optional. Caption of the document to be sent, 0-200 characters. */
    private String caption;

    /** A valid URL for the file. */
    @SerializedName("document_url")
    private String documentUrl;

    /** Mime type of the content of the file, either "application/pdf" or "application/zip". */
    @SerializedName("mime_type")
    private String mimeType;

    /** Optional. Short description of the result. */
    private String description;

    /** Optional. Thumbnail width. */
    @SerializedName("thumb_width")
    private Integer thumbWidth;

    /** Optional. Thumbnail height. */
    @SerializedName("thumb_height")
    private Integer thumbHeight;

    public String getType() {
        return type;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(Integer thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public Integer getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(Integer thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultDocument that = (InlineQueryResultDocument) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (documentUrl != null ? !documentUrl.equals(that.documentUrl) : that.documentUrl != null) return false;
        if (mimeType != null ? !mimeType.equals(that.mimeType) : that.mimeType != null) return false;
        if (thumbHeight != null ? !thumbHeight.equals(that.thumbHeight) : that.thumbHeight != null) return false;
        if (thumbWidth != null ? !thumbWidth.equals(that.thumbWidth) : that.thumbWidth != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        result = 31 * result + (documentUrl != null ? documentUrl.hashCode() : 0);
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (thumbWidth != null ? thumbWidth.hashCode() : 0);
        result = 31 * result + (thumbHeight != null ? thumbHeight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultDocument{" +
                "type='" + type + '\'' +
                ", caption='" + caption + '\'' +
                ", documentUrl='" + documentUrl + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", thumbWidth=" + thumbWidth +
                ", thumbHeight=" + thumbHeight +
                "} " + super.toString();
    }
}