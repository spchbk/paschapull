package ru.sberbank.uplatform.message.inline;

import com.google.gson.annotations.SerializedName;

/**
 * Object which represents callback query parameters for answer method.
 *
 * @author Tarasov Pavel.
 * @since 28.04.16.
 */
public class AnswerCallbackQuery {

    /** Unique identifier for the query to be answered. */
    @SerializedName("callback_query_id")
    private String callbackQueryId;

    /**
     * Optional. Text of the notification. If not specified,
     * nothing will be shown to the user.
     */
    private String text;

    /**
     * Optional. If true, an alert will be shown by the client instead of
     * a notification at the top of the chat screen. Defaults to false.
     */
    @SerializedName("show_alert")
    private Boolean showAlert;

    public String getCallbackQueryId() {
        return callbackQueryId;
    }

    public void setCallbackQueryId(String callbackQueryId) {
        this.callbackQueryId = callbackQueryId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getShowAlert() {
        return showAlert;
    }

    public void setShowAlert(Boolean showAlert) {
        this.showAlert = showAlert;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AnswerCallbackQuery that = (AnswerCallbackQuery) o;

        if (callbackQueryId != null ? !callbackQueryId.equals(that.callbackQueryId) : that.callbackQueryId != null) return false;
        if (showAlert != null ? !showAlert.equals(that.showAlert) : that.showAlert != null) return false;
        return !(text != null ? !text.equals(that.text) : that.text != null);
    }

    @Override
    public int hashCode() {
        int result = callbackQueryId != null ? callbackQueryId.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (showAlert != null ? showAlert.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AnswerCallbackQuery{" +
                "callbackQueryId='" + callbackQueryId + '\'' +
                ", text='" + text + '\'' +
                ", showAlert=" + showAlert +
                '}';
    }
}