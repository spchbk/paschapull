package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a link to an animated GIF file. By default, this animated GIF file
 * will be sent by the user with optional caption. Alternatively, you can use
 * {@code input_message_content} to send a message with the specified content
 * instead of the animation.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultGif extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "gif";

    /** A valid URL for the GIF file. File size must not exceed 1MB. */
    @SerializedName("gif_url")
    private String gifUrl;

    /** Optional. Width of the GIF. */
    @SerializedName("gif_width")
    private Integer gifWidth;

    /** Optional. Height of the GIF. */
    @SerializedName("gif_height")
    private Integer gifHeight;

    /** Optional. Caption of the GIF file to be sent, 0-200 characters. */
    private String caption;

    public String getType() {
        return type;
    }

    public String getGifUrl() {
        return gifUrl;
    }

    public void setGifUrl(String gifUrl) {
        this.gifUrl = gifUrl;
    }

    public Integer getGifWidth() {
        return gifWidth;
    }

    public void setGifWidth(Integer gifWidth) {
        this.gifWidth = gifWidth;
    }

    public Integer getGifHeight() {
        return gifHeight;
    }

    public void setGifHeight(Integer gifHeight) {
        this.gifHeight = gifHeight;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultGif that = (InlineQueryResultGif) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (gifHeight != null ? !gifHeight.equals(that.gifHeight) : that.gifHeight != null) return false;
        if (gifUrl != null ? !gifUrl.equals(that.gifUrl) : that.gifUrl != null) return false;
        if (gifWidth != null ? !gifWidth.equals(that.gifWidth) : that.gifWidth != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (gifUrl != null ? gifUrl.hashCode() : 0);
        result = 31 * result + (gifWidth != null ? gifWidth.hashCode() : 0);
        result = 31 * result + (gifHeight != null ? gifHeight.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultGif{" +
                "type='" + type + '\'' +
                ", gifUrl='" + gifUrl + '\'' +
                ", gifWidth=" + gifWidth +
                ", gifHeight=" + gifHeight +
                ", caption='" + caption + '\'' +
                "} " + super.toString();
    }
}