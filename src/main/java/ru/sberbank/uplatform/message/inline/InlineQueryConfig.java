package ru.sberbank.uplatform.message.inline;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Inline mode commands settings.
 * 
 * @author Dmitriy Gulyaev
 * @since 03.03.16
 */
public class InlineQueryConfig {

    private List<String> pattern;
    private Pattern[] compiledPattern;
    private Integer cacheTime;
    private String description;
    private String title;
    private String url;

    public List<String> getPattern() {
        if (pattern == null) {
            pattern = new ArrayList<>();
        }
        return pattern;
    }

    public void setPattern(List<String> pattern) {
        this.pattern = pattern;
    }

    public Integer getCacheTime() {
        return cacheTime;
    }

    public void setCacheTime(Integer cacheTime) {
        this.cacheTime = cacheTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Pattern[] getCompiledPattern() {
        return compiledPattern;
    }

    public void setCompiledPattern(Pattern[] compiledPattern) {
        this.compiledPattern = compiledPattern;
    }

    @Override
    public String toString() {
        return String.format(
                "InlineQueryConfig [pattern=%s, compiledPattern=%s, cacheTime=%s, description=%s, title=%s, url=%s]",
                pattern, Arrays.toString(compiledPattern), cacheTime, description, title, url);
    }
}
