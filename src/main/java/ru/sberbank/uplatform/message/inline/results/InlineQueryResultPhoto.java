package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a link to a photo. By default, this photo will be sent by the user
 * with optional caption. Alternatively, you can use input_message_content to
 * send a message with the specified content instead of the photo.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultPhoto extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "photo";

    /**
     * A valid URL of the photo. Photo must be in jpeg format.
     * Photo size must not exceed 5MB.
     */
    @SerializedName("photo_url")
    private String photoUrl;

    /** Optional. Width of the photo. */
    @SerializedName("photo_width")
    private Integer photoWidth;

    /** Optional. Height of the photo. */
    @SerializedName("photo_height")
    private Integer photoHeight;

    /** Optional. Short description of the result. */
    private String description;

    /** Optional. Caption of the photo to be sent, 0-200 characters. */
    private String caption;

    public String getType() {
        return type;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Integer getPhotoWidth() {
        return photoWidth;
    }

    public void setPhotoWidth(Integer photoWidth) {
        this.photoWidth = photoWidth;
    }

    public Integer getPhotoHeight() {
        return photoHeight;
    }

    public void setPhotoHeight(Integer photoHeight) {
        this.photoHeight = photoHeight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultPhoto that = (InlineQueryResultPhoto) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (photoHeight != null ? !photoHeight.equals(that.photoHeight) : that.photoHeight != null) return false;
        if (photoUrl != null ? !photoUrl.equals(that.photoUrl) : that.photoUrl != null) return false;
        if (photoWidth != null ? !photoWidth.equals(that.photoWidth) : that.photoWidth != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (photoUrl != null ? photoUrl.hashCode() : 0);
        result = 31 * result + (photoWidth != null ? photoWidth.hashCode() : 0);
        result = 31 * result + (photoHeight != null ? photoHeight.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultPhoto{" +
                "type='" + type + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", photoWidth=" + photoWidth +
                ", photoHeight=" + photoHeight +
                ", description='" + description + '\'' +
                ", caption='" + caption + '\'' +
                "} " + super.toString();
    }
}