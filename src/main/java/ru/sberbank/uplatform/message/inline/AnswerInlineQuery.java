package ru.sberbank.uplatform.message.inline;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResult;

/**
 * Object which represents inline query parameters for answer method.
 *
 * @author Dmitriy Gulyaev
 * @since 03.03.16
 */
public class AnswerInlineQuery {

	/** Unique identifier for the answered query. */
	@SerializedName("inline_query_id")
	private String inlineQueryId;

	/** A JSON-serialized array of results for the inline query. */
	private List<InlineQueryResult> results;

	/**
	 * Optional. The maximum amount of time in seconds that the result of the
	 * inline query may be cached on the server. Defaults to 300.
	 */
	@SerializedName("cache_time")
	private Integer cacheTime;

	/**
	 * Optional. Pass True, if results may be cached on the server side only for
	 * the user that sent the query. By default, results may be returned to any
	 * user who sends the same query.
	 */
	@SerializedName("is_personal")
	private Boolean isPersonal;

	/**
	 * Optional. Pass the offset that a client should send in the next query with
	 * the same text to receive more results. Pass an empty string if there are no
	 * more results or if you don‘t support pagination. Offset length can’t exceed
	 * 64 bytes.
	 */
	@SerializedName("next_offset")
	private String nextOffset;

	/**
	 * Optional. If passed, clients will display a button with specified text that
	 * switches the user to a private chat with the bot and sends the bot a start
	 * message with the parameter {@code switch_pm_parameter}.
	 */
	@SerializedName("switch_pm_text")
	private String switchPmText;

	/**
	 * Optional. Parameter for the start message sent to the bot when user presses
	 * the switch button.
	 */
	@SerializedName("switch_pm_parameter")
	private String switchPmParameter;

	public String getInlineQueryId() {
		return inlineQueryId;
	}

	public void setInlineQueryId(String inlineQueryId) {
		this.inlineQueryId = inlineQueryId;
	}

	public List<InlineQueryResult> getResults() {
		if (results == null) {
			results = new ArrayList<>();
		}
		return results;
	}

	public void setResults(List<InlineQueryResult> results) {
		this.results = results;
	}

	public Integer getCacheTime() {
		return cacheTime;
	}

	public void setCacheTime(Integer cacheTime) {
		this.cacheTime = cacheTime;
	}

	public Boolean getIsPersonal() {
		return isPersonal;
	}

	public void setIsPersonal(Boolean isPersonal) {
		this.isPersonal = isPersonal;
	}

	public String getNextOffset() {
		return nextOffset;
	}

	public void setNextOffset(String nextOffset) {
		this.nextOffset = nextOffset;
	}

	public String getSwitchPmText() {
		return switchPmText;
	}

	public void setSwitchPmText(String switchPmText) {
		this.switchPmText = switchPmText;
	}

	public String getSwitchPmParameter() {
		return switchPmParameter;
	}

	public void setSwitchPmParameter(String switchPmParameter) {
		this.switchPmParameter = switchPmParameter;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AnswerInlineQuery that = (AnswerInlineQuery) o;

		if (cacheTime != null ? !cacheTime.equals(that.cacheTime) : that.cacheTime != null) return false;
		if (inlineQueryId != null ? !inlineQueryId.equals(that.inlineQueryId) : that.inlineQueryId != null) return false;
		if (isPersonal != null ? !isPersonal.equals(that.isPersonal) : that.isPersonal != null) return false;
		if (nextOffset != null ? !nextOffset.equals(that.nextOffset) : that.nextOffset != null) return false;
		if (results != null ? !results.equals(that.results) : that.results != null) return false;
		if (switchPmParameter != null ? !switchPmParameter.equals(that.switchPmParameter) : that.switchPmParameter != null) return false;
		if (switchPmText != null ? !switchPmText.equals(that.switchPmText) : that.switchPmText != null) return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = inlineQueryId != null ? inlineQueryId.hashCode() : 0;
		result = 31 * result + (results != null ? results.hashCode() : 0);
		result = 31 * result + (cacheTime != null ? cacheTime.hashCode() : 0);
		result = 31 * result + (isPersonal != null ? isPersonal.hashCode() : 0);
		result = 31 * result + (nextOffset != null ? nextOffset.hashCode() : 0);
		result = 31 * result + (switchPmText != null ? switchPmText.hashCode() : 0);
		result = 31 * result + (switchPmParameter != null ? switchPmParameter.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "AnswerInlineQuery{" +
				"inlineQueryId='" + inlineQueryId + '\'' +
				", results=" + results +
				", cacheTime=" + cacheTime +
				", isPersonal=" + isPersonal +
				", nextOffset='" + nextOffset + '\'' +
				", switchPmText='" + switchPmText + '\'' +
				", switchPmParameter='" + switchPmParameter + '\'' +
				'}';
	}
}