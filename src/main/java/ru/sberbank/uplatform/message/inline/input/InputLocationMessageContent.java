package ru.sberbank.uplatform.message.inline.input;

/**
 * Represents the content of a location message to be sent as the result of an inline query.
 *
 * @author Tarasov Pavel.
 * @since 26.04.16.
 */
public class InputLocationMessageContent implements InputMessageContent {

    /** Latitude of the location in degrees. */
    private Float latitude;

    /** Longitude of the location in degrees. */
    private Float longitude;

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InputLocationMessageContent that = (InputLocationMessageContent) o;
        return !(latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) &&
                !(longitude != null ? !longitude.equals(that.longitude) : that.longitude != null);
    }

    @Override
    public int hashCode() {
        int result = latitude != null ? latitude.hashCode() : 0;
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InputLocationMessageContent{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}