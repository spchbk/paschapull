package ru.sberbank.uplatform.message.inline.results.cached;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResult;

/**
 * Represents a link to a sticker stored on the Telegram servers.
 * By default, this sticker will be sent by the user. Alternatively,
 * you can use {@code input_message_content} to send a message with
 * the specified content instead of the sticker.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultCachedSticker extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "sticker";

    /** A valid file identifier of the sticker. */
    @SerializedName("sticker_file_id")
    private String stickerFileId;

    public String getType() {
        return type;
    }

    public String getStickerFileId() {
        return stickerFileId;
    }

    public void setStickerFileId(String stickerFileId) {
        this.stickerFileId = stickerFileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultCachedSticker that = (InlineQueryResultCachedSticker) o;
        return !(stickerFileId != null ? !stickerFileId.equals(that.stickerFileId)
                : that.stickerFileId != null) && type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (stickerFileId != null ? stickerFileId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultCachedSticker{" +
                "type='" + type + '\'' +
                ", stickerFileId='" + stickerFileId + '\'' +
                "} " + super.toString();
    }
}