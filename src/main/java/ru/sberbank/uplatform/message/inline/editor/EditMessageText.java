package ru.sberbank.uplatform.message.inline.editor;

import com.google.gson.annotations.SerializedName;

/**
 * Used in method of the change an existing text in messages.
 *
 * @author Tarasov Pavel.
 * @since 29.04.16.
 */
public class EditMessageText extends EditMessageReplyMarkup {

    /** Required. New text of the message. */
    private String text;

    /** Optional. Send message with HTML style syntax. */
    @SerializedName("parse_mode")
    private String parseMode = "HTML";

    /** Optional. Disables link previews for links in this message. */
    @SerializedName("disable_web_page_preview")
    private Boolean disableWebPagePreview = Boolean.TRUE;

    /** {@inheritDoc} */
    @Override
    public String getMethodName() {
        return "editMessageText";
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getParseMode() {
        return parseMode;
    }

    public void setParseMode(String parseMode) {
        this.parseMode = parseMode;
    }

    public Boolean getDisableWebPagePreview() {
        return disableWebPagePreview;
    }

    public void setDisableWebPagePreview(Boolean disableWebPagePreview) {
        this.disableWebPagePreview = disableWebPagePreview;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EditMessageText that = (EditMessageText) o;

        if (disableWebPagePreview != null ? !disableWebPagePreview.equals(that.disableWebPagePreview) : that.disableWebPagePreview != null) return false;
        if (parseMode != null ? !parseMode.equals(that.parseMode) : that.parseMode != null) return false;
        return !(text != null ? !text.equals(that.text) : that.text != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (parseMode != null ? parseMode.hashCode() : 0);
        result = 31 * result + (disableWebPagePreview != null ? disableWebPagePreview.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EditMessageText{" +
                "text='" + text + '\'' +
                ", parseMode='" + parseMode + '\'' +
                ", disableWebPagePreview=" + disableWebPagePreview +
                "} " + super.toString();
    }
}