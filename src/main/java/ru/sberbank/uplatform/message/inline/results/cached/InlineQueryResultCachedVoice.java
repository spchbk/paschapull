package ru.sberbank.uplatform.message.inline.results.cached;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResult;

/**
 * Represents a link to a voice message stored on the Telegram servers.
 * By default, this voice message will be sent by the user. Alternatively,
 * you can use {@code input_message_content} to send a message with the
 * specified content instead of the voice message.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultCachedVoice extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "voice";

    /** A valid file identifier for the voice message. */
    @SerializedName("voice_file_id")
    private String voiceFileId;

    public String getType() {
        return type;
    }

    public String getVoiceFileId() {
        return voiceFileId;
    }

    public void setVoiceFileId(String voiceFileId) {
        this.voiceFileId = voiceFileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultCachedVoice that = (InlineQueryResultCachedVoice) o;
        return !(voiceFileId != null ? !voiceFileId.equals(that.voiceFileId) : that.voiceFileId != null)
                && type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (voiceFileId != null ? voiceFileId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultCachedVoice{" +
                "type='" + type + '\'' +
                ", voiceFileId='" + voiceFileId + '\'' +
                "} " + super.toString();
    }
}