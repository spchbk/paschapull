package ru.sberbank.uplatform.message.inline.results.cached;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResult;

/**
 * Represents a link to a photo stored on the Telegram servers. By default, this
 * photo will be sent by the user with an optional caption. Alternatively, you can
 * use {@code input_message_content} to send a message with the specified content
 * instead of the photo.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultCachedPhoto extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "photo";

    /** A valid file identifier of the photo. */
    @SerializedName("photo_file_id")
    private String photoFileId;

    /** Optional. Short description of the result. */
    private String description;

    /** Optional. Caption of the photo to be sent, 0-200 characters. */
    private String caption;

    public String getType() {
        return type;
    }

    public String getPhotoFileId() {
        return photoFileId;
    }

    public void setPhotoFileId(String photoFileId) {
        this.photoFileId = photoFileId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultCachedPhoto that = (InlineQueryResultCachedPhoto) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (photoFileId != null ? !photoFileId.equals(that.photoFileId) : that.photoFileId != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (photoFileId != null ? photoFileId.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultCachedPhoto{" +
                "type='" + type + '\'' +
                ", photoFileId='" + photoFileId + '\'' +
                ", description='" + description + '\'' +
                ", caption='" + caption + '\'' +
                "} " + super.toString();
    }
}