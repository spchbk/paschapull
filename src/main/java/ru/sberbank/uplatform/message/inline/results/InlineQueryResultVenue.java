package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a venue. By default, the venue will be sent by the user.
 * Alternatively, you can use {@code input_message_content} to send a
 * message with the specified content instead of the venue.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultVenue extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "venue";

    /** Latitude of the venue location in degrees. */
    private Float latitude;

    /** Longitude of the venue location in degrees. */
    private Float longitude;

    /** Address of the venue. */
    private String address;

    /** Optional. Foursquare identifier of the venue if known. */
    @SerializedName("foursquare_id")
    private String foursquare;

    /** Optional. Thumbnail width. */
    @SerializedName("thumb_width")
    private Integer thumbWidth;

    /** Optional. Thumbnail height. */
    @SerializedName("thumb_height")
    private Integer thumbHeight;

    public String getType() {
        return type;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFoursquare() {
        return foursquare;
    }

    public void setFoursquare(String foursquare) {
        this.foursquare = foursquare;
    }

    public Integer getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(Integer thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public Integer getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(Integer thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultVenue that = (InlineQueryResultVenue) o;

        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (foursquare != null ? !foursquare.equals(that.foursquare) : that.foursquare != null) return false;
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null) return false;
        if (thumbHeight != null ? !thumbHeight.equals(that.thumbHeight) : that.thumbHeight != null) return false;
        if (thumbWidth != null ? !thumbWidth.equals(that.thumbWidth) : that.thumbWidth != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (foursquare != null ? foursquare.hashCode() : 0);
        result = 31 * result + (thumbWidth != null ? thumbWidth.hashCode() : 0);
        result = 31 * result + (thumbHeight != null ? thumbHeight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultVenue{" +
                "type='" + type + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", address='" + address + '\'' +
                ", foursquare='" + foursquare + '\'' +
                ", thumbWidth=" + thumbWidth +
                ", thumbHeight=" + thumbHeight +
                "} " + super.toString();
    }
}