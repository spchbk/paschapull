package ru.sberbank.uplatform.message.inline;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.Location;
import ru.sberbank.uplatform.message.User;

/**
 * Represents a result of an inline query that was chosen
 * by the user and sent to their chat partner.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class ChosenInlineResult {

    /** The unique identifier for the result that was chosen. */
    @SerializedName("result_id")
    private String resultId;

    /** The user that chose the result. */
    private User from;

    /** Optional. Sender location, only for bots that require user location. */
    private Location location;

    /**
     * Optional. Identifier of the sent inline message. Available only if there
     * is an inline keyboard attached to the message. Will be also received in
     * callback queries and can be used to edit the message.
     */
    @SerializedName("inline_message_id")
    private String inlineMessageId;

    /** The query that was used to obtain the result. */
    private String query;

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getInlineMessageId() {
        return inlineMessageId;
    }

    public void setInlineMessageId(String inlineMessageId) {
        this.inlineMessageId = inlineMessageId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChosenInlineResult that = (ChosenInlineResult) o;

        if (from != null ? !from.equals(that.from) : that.from != null) return false;
        if (inlineMessageId != null ? !inlineMessageId.equals(that.inlineMessageId) : that.inlineMessageId != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (query != null ? !query.equals(that.query) : that.query != null) return false;
        if (resultId != null ? !resultId.equals(that.resultId) : that.resultId != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = resultId != null ? resultId.hashCode() : 0;
        result = 31 * result + (from != null ? from.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (inlineMessageId != null ? inlineMessageId.hashCode() : 0);
        result = 31 * result + (query != null ? query.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ChosenInlineResult{" +
                "resultId='" + resultId + '\'' +
                ", from=" + from +
                ", location=" + location +
                ", inlineMessageId='" + inlineMessageId + '\'' +
                ", query='" + query + '\'' +
                '}';
    }
}