package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a link to a voice recording in an .ogg container encoded with OPUS.
 * By default, this voice recording will be sent by the user. Alternatively, you
 * can use {@code input_message_content} to send a message with the specified
 * content instead of the the voice message.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultVoice extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "voice";

    /** A valid URL for the voice recording. */
    @SerializedName("voice_url")
    private String voiceUrl;

    /** Optional. Recording duration in seconds. */
    @SerializedName("voice_duration")
    private Integer voiceDuration;

    public String getType() {
        return type;
    }

    public String getVoiceUrl() {
        return voiceUrl;
    }

    public void setVoiceUrl(String voiceUrl) {
        this.voiceUrl = voiceUrl;
    }

    public Integer getVoiceDuration() {
        return voiceDuration;
    }

    public void setVoiceDuration(Integer voiceDuration) {
        this.voiceDuration = voiceDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultVoice that = (InlineQueryResultVoice) o;

        if (!type.equals(that.type)) return false;
        if (voiceDuration != null ? !voiceDuration.equals(that.voiceDuration) : that.voiceDuration != null) return false;
        return !(voiceUrl != null ? !voiceUrl.equals(that.voiceUrl) : that.voiceUrl != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (voiceUrl != null ? voiceUrl.hashCode() : 0);
        result = 31 * result + (voiceDuration != null ? voiceDuration.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultVoice{" +
                "type='" + type + '\'' +
                ", voiceUrl='" + voiceUrl + '\'' +
                ", voiceDuration=" + voiceDuration +
                "} " + super.toString();
    }
}