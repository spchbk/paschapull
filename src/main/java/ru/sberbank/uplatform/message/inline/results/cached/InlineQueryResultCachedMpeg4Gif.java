package ru.sberbank.uplatform.message.inline.results.cached;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResult;

/**
 * Represents a link to a video animation (H.264/MPEG-4 AVC video without sound) stored
 * on the Telegram servers. By default, this animated MPEG-4 file will be sent by the
 * user with an optional caption. Alternatively, you can use {@code input_message_content}
 * to send a message with the specified content instead of the animation.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultCachedMpeg4Gif extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "mpeg4_gif";

    /** A valid file identifier for the MP4 file. */
    @SerializedName("mpeg4_file_id")
    private String mpeg4FileId;

    /** Optional. Caption of the MPEG-4 file to be sent, 0-200 characters. */
    private String caption;

    public String getType() {
        return type;
    }

    public String getMpeg4FileId() {
        return mpeg4FileId;
    }

    public void setMpeg4FileId(String mpeg4FileId) {
        this.mpeg4FileId = mpeg4FileId;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultCachedMpeg4Gif that = (InlineQueryResultCachedMpeg4Gif) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (mpeg4FileId != null ? !mpeg4FileId.equals(that.mpeg4FileId) : that.mpeg4FileId != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (mpeg4FileId != null ? mpeg4FileId.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultCachedMpeg4Gif{" +
                "type='" + type + '\'' +
                ", mpeg4FileId='" + mpeg4FileId + '\'' +
                ", caption='" + caption + '\'' +
                "} " + super.toString();
    }
}