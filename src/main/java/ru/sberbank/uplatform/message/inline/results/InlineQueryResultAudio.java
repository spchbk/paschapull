package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a link to an mp3 audio file. By default, this audio file will be
 * sent by the user. Alternatively, you can use {@code input_message_content}
 * to send a message with the specified content instead of the audio.
 *
 * @author Tarasov Pavel.
 * @since 26.04.16.
 */
public class InlineQueryResultAudio extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "audio";

    /** A valid URL for the audio file. */
    @SerializedName("audio_url")
    private String audioUrl;

    /** Optional. Performer. */
    private String performer;

    /** Optional. Audio duration in seconds. */
    @SerializedName("audio_duration")
    private Integer audioDuration;

    public String getType() {
        return type;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public Integer getAudioDuration() {
        return audioDuration;
    }

    public void setAudioDuration(Integer audioDuration) {
        this.audioDuration = audioDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultAudio that = (InlineQueryResultAudio) o;

        if (audioDuration != null ? !audioDuration.equals(that.audioDuration) : that.audioDuration != null) return false;
        if (audioUrl != null ? !audioUrl.equals(that.audioUrl) : that.audioUrl != null) return false;
        if (performer != null ? !performer.equals(that.performer) : that.performer != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (audioUrl != null ? audioUrl.hashCode() : 0);
        result = 31 * result + (performer != null ? performer.hashCode() : 0);
        result = 31 * result + (audioDuration != null ? audioDuration.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultAudio{" +
                "type='" + type + '\'' +
                ", audioUrl='" + audioUrl + '\'' +
                ", performer='" + performer + '\'' +
                ", audioDuration=" + audioDuration +
                "} " + super.toString();
    }
}