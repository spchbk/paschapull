package ru.sberbank.uplatform.message.inline.results.cached;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.results.InlineQueryResult;

/**
 * Represents a link to an mp3 audio file stored on the Telegram servers.
 * By default, this audio file will be sent by the user. Alternatively,
 * you can use {@code input_message_content} to send a message with the
 * specified content instead of the audio.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultCachedAudio extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "audio";

    /** A valid file identifier for the audio file. */
    @SerializedName("audio_file_id")
    private String audioFileId;

    public String getType() {
        return type;
    }

    public String getAudioFileId() {
        return audioFileId;
    }

    public void setAudioFileId(String audioFileId) {
        this.audioFileId = audioFileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultCachedAudio that = (InlineQueryResultCachedAudio) o;
        return !(audioFileId != null ? !audioFileId.equals(that.audioFileId) : that.audioFileId != null)
                && type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (audioFileId != null ? audioFileId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultCachedAudio{" +
                "type='" + type + '\'' +
                ", audioFileId='" + audioFileId + '\'' +
                "} " + super.toString();
    }
}