package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.inline.input.InputMessageContent;
import ru.sberbank.uplatform.message.keyboard.InlineKeyboardMarkup;

/**
 * This object represents one result of an inline query.
 * Telegram clients currently version 2.0 is support results of the 19 types.
 *
 * @author Dmitriy Gulyaev
 * @since 03.03.16
 */
public class InlineQueryResult {

	/** Unique identifier for this result, 1-64 bytes. */
	private String id;

	/** Optional. Title for the result. */
	private String title;

	/** Content of the message to be sent. */
	@SerializedName("input_message_content")
	private InputMessageContent inputMessageContent;

	/** Optional. Inline keyboard attached to the message. */
	@SerializedName("reply_markup")
	private InlineKeyboardMarkup replyMarkup;

	/** Optional. Url of the thumbnail for the result. */
	@SerializedName("thumb_url")
	private String thumbUrl;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public InputMessageContent getInputMessageContent() {
		return inputMessageContent;
	}

	public void setInputMessageContent(InputMessageContent inputMessageContent) {
		this.inputMessageContent = inputMessageContent;
	}

	public InlineKeyboardMarkup getReplyMarkup() {
		return replyMarkup;
	}

	public void setReplyMarkup(InlineKeyboardMarkup replyMarkup) {
		this.replyMarkup = replyMarkup;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		InlineQueryResult that = (InlineQueryResult) o;

		if (id != null ? !id.equals(that.id) : that.id != null) return false;
		if (inputMessageContent != null ? !inputMessageContent.equals(that.inputMessageContent) : that.inputMessageContent != null) return false;
		if (replyMarkup != null ? !replyMarkup.equals(that.replyMarkup) : that.replyMarkup != null) return false;
		if (thumbUrl != null ? !thumbUrl.equals(that.thumbUrl) : that.thumbUrl != null) return false;
		return !(title != null ? !title.equals(that.title) : that.title != null);
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (title != null ? title.hashCode() : 0);
		result = 31 * result + (inputMessageContent != null ? inputMessageContent.hashCode() : 0);
		result = 31 * result + (replyMarkup != null ? replyMarkup.hashCode() : 0);
		result = 31 * result + (thumbUrl != null ? thumbUrl.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "InlineQueryResult{" +
				"id='" + id + '\'' +
				", title='" + title + '\'' +
				", inputMessageContent=" + inputMessageContent +
				", replyMarkup=" + replyMarkup +
				", thumbUrl='" + thumbUrl + '\'' +
				'}';
	}
}