package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a link to a page containing an embedded video player or a video file.
 * By default, this video file will be sent by the user with an optional caption.
 * Alternatively, you can use {@code input_message_content} to send a message with
 * the specified content instead of the video.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultVideo extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "video";

    /** A valid URL for the embedded video player or video file. */
    @SerializedName("video_url")
    private String videoUrl;

    /** Mime type of the content of video url, “text/html” or “video/mp4”. */
    @SerializedName("mime_type")
    private String mimeType;

    /** Optional. Caption of the video to be sent, 0-200 characters. */
    private String caption;

    /** Optional. Video width. */
    @SerializedName("video_width")
    private Integer videoWidth;

    /** Optional. Video height. */
    @SerializedName("video_height")
    private Integer videoHeight;

    /** Optional. Video duration in seconds. */
    @SerializedName("video_duration")
    private Integer videoDuration;

    /** Optional. Short description of the result. */
    private String description;

    public String getType() {
        return type;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Integer getVideoWidth() {
        return videoWidth;
    }

    public void setVideoWidth(Integer videoWidth) {
        this.videoWidth = videoWidth;
    }

    public Integer getVideoHeight() {
        return videoHeight;
    }

    public void setVideoHeight(Integer videoHeight) {
        this.videoHeight = videoHeight;
    }

    public Integer getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(Integer videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultVideo that = (InlineQueryResultVideo) o;

        if (caption != null ? !caption.equals(that.caption) : that.caption != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (mimeType != null ? !mimeType.equals(that.mimeType) : that.mimeType != null) return false;
        if (!type.equals(that.type)) return false;
        if (videoDuration != null ? !videoDuration.equals(that.videoDuration) : that.videoDuration != null) return false;
        if (videoHeight != null ? !videoHeight.equals(that.videoHeight) : that.videoHeight != null) return false;
        if (videoUrl != null ? !videoUrl.equals(that.videoUrl) : that.videoUrl != null) return false;
        return !(videoWidth != null ? !videoWidth.equals(that.videoWidth) : that.videoWidth != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (videoUrl != null ? videoUrl.hashCode() : 0);
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        result = 31 * result + (videoWidth != null ? videoWidth.hashCode() : 0);
        result = 31 * result + (videoHeight != null ? videoHeight.hashCode() : 0);
        result = 31 * result + (videoDuration != null ? videoDuration.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultVideo{" +
                "type='" + type + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", caption='" + caption + '\'' +
                ", videoWidth=" + videoWidth +
                ", videoHeight=" + videoHeight +
                ", videoDuration=" + videoDuration +
                ", description='" + description + '\'' +
                "} " + super.toString();
    }
}