package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a location on a map. By default, the location will be sent
 * by the user. Alternatively, you can use {@code input_message_content}
 * to send a message with the specified content instead of the location.
 *
 * @author Tarasov Pavel.
 * @since 27.04.16.
 */
public class InlineQueryResultLocation extends InlineQueryResult {

    /** Type of the result. */
    private final String type = "location";

    /** Location latitude in degrees. */
    private Float latitude;

    /** Location longitude in degrees. */
    private Float longitude;

    /** Optional. Thumbnail width. */
    @SerializedName("thumb_width")
    private Integer thumbWidth;

    /** Optional. Thumbnail height. */
    @SerializedName("thumb_height")
    private Integer thumbHeight;

    public String getType() {
        return type;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Integer getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(Integer thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public Integer getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(Integer thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InlineQueryResultLocation that = (InlineQueryResultLocation) o;

        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null) return false;
        if (thumbHeight != null ? !thumbHeight.equals(that.thumbHeight) : that.thumbHeight != null) return false;
        if (thumbWidth != null ? !thumbWidth.equals(that.thumbWidth) : that.thumbWidth != null) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type.hashCode());
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (thumbWidth != null ? thumbWidth.hashCode() : 0);
        result = 31 * result + (thumbHeight != null ? thumbHeight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineQueryResultLocation{" +
                "type='" + type + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", thumbWidth=" + thumbWidth +
                ", thumbHeight=" + thumbHeight +
                "} " + super.toString();
    }
}