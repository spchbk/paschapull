package ru.sberbank.uplatform.message.inline.input;

/**
 * This interface represents the content of a message to be sent as a result of an inline query.
 *
 * @author Tarasov Pavel.
 * @since 26.04.16.
 */
public interface InputMessageContent {
}
