package ru.sberbank.uplatform.message.inline.results;

import com.google.gson.annotations.SerializedName;

/**
 * Represents a link to an article or web page.
 * 
 * @author Dmitriy Gulyaev
 * @since 03.03.16
 */
public class InlineQueryResultArticle extends InlineQueryResult {

	/** Type of the result. */
	private final String type = "article";

	/** Optional. URL of the result. */
	private String url;

	/** Optional. Pass True, if you don't want the URL to be shown in the message. */
	@SerializedName("hide_url")
	private String hideUrl;

	/** Optional. Short description of the result. */
	private String description;

	/** Optional. Thumbnail width. */
	@SerializedName("thumb_width")
	private Integer thumbWidth;

	/** Optional. Thumbnail height. */
	@SerializedName("thumb_height")
	private Integer thumbHeight;

	public String getType() {
		return type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHideUrl() {
		return hideUrl;
	}

	public void setHideUrl(String hideUrl) {
		this.hideUrl = hideUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getThumbWidth() {
		return thumbWidth;
	}

	public void setThumbWidth(Integer thumbWidth) {
		this.thumbWidth = thumbWidth;
	}

	public Integer getThumbHeight() {
		return thumbHeight;
	}

	public void setThumbHeight(Integer thumbHeight) {
		this.thumbHeight = thumbHeight;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		InlineQueryResultArticle that = (InlineQueryResultArticle) o;

		if (description != null ? !description.equals(that.description) : that.description != null) return false;
		if (hideUrl != null ? !hideUrl.equals(that.hideUrl) : that.hideUrl != null) return false;
		if (thumbHeight != null ? !thumbHeight.equals(that.thumbHeight) : that.thumbHeight != null) return false;
		if (thumbWidth != null ? !thumbWidth.equals(that.thumbWidth) : that.thumbWidth != null) return false;
		if (!type.equals(that.type)) return false;
		return !(url != null ? !url.equals(that.url) : that.url != null);

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (type.hashCode());
		result = 31 * result + (url != null ? url.hashCode() : 0);
		result = 31 * result + (hideUrl != null ? hideUrl.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (thumbWidth != null ? thumbWidth.hashCode() : 0);
		result = 31 * result + (thumbHeight != null ? thumbHeight.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "InlineQueryResultArticle{" +
				"type='" + type + '\'' +
				", url='" + url + '\'' +
				", hideUrl='" + hideUrl + '\'' +
				", description='" + description + '\'' +
				", thumbWidth=" + thumbWidth +
				", thumbHeight=" + thumbHeight +
				"} " + super.toString();
	}
}