package ru.sberbank.uplatform.message.inline.editor;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.message.keyboard.InlineKeyboardMarkup;

/**
 * Base class of updating messages. Used in method of the change an
 * existing messages. This object used for edit only the reply markup.
 *
 * @author Tarasov Pavel.
 * @since 29.04.16.
 */
public class EditMessageReplyMarkup implements EditMessage {

    /**
     * Unique identifier for the target chat.
     * Required if {@code inline_message_id} is not specified.
     */
    @SerializedName("chat_id")
    private Long chatID;

    /**
     * Unique identifier of the sent message.
     * Required if {@code inline_message_id} is not specified.
     */
    @SerializedName("message_id")
    private Integer messageID;

    /**
     * Identifier of the inline message. Required if
     * {@code chat_id} and {@code message_id} are not specified.
     */
    @SerializedName("inline_message_id")
    private String inlineMessageID;

    /**
     * Optional. A JSON-serialized object for an inline keyboard.
     */
    @SerializedName("reply_markup")
    private InlineKeyboardMarkup replyMarkup;

    /** {@inheritDoc} */
    @Override
    public String getMethodName() {
        return "editMessageReplyMarkup";
    }

    public Long getChatID() {
        return chatID;
    }

    public void setChatID(Long chatID) {
        this.chatID = chatID;
    }

    public Integer getMessageID() {
        return messageID;
    }

    public void setMessageID(Integer messageID) {
        this.messageID = messageID;
    }

    public String getInlineMessageID() {
        return inlineMessageID;
    }

    public void setInlineMessageID(String inlineMessageID) {
        this.inlineMessageID = inlineMessageID;
    }

    public InlineKeyboardMarkup getReplyMarkup() {
        return replyMarkup;
    }

    public void setReplyMarkup(InlineKeyboardMarkup replyMarkup) {
        this.replyMarkup = replyMarkup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EditMessageReplyMarkup that = (EditMessageReplyMarkup) o;
        if (chatID != null ? !chatID.equals(that.chatID) : that.chatID != null) return false;
        if (inlineMessageID != null ? !inlineMessageID.equals(that.inlineMessageID) : that.inlineMessageID != null) return false;
        if (messageID != null ? !messageID.equals(that.messageID) : that.messageID != null) return false;
        return !(replyMarkup != null ? !replyMarkup.equals(that.replyMarkup) : that.replyMarkup != null);
    }

    @Override
    public int hashCode() {
        int result = chatID != null ? chatID.hashCode() : 0;
        result = 31 * result + (messageID != null ? messageID.hashCode() : 0);
        result = 31 * result + (inlineMessageID != null ? inlineMessageID.hashCode() : 0);
        result = 31 * result + (replyMarkup != null ? replyMarkup.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EditMessage{" +
                "chatID=" + chatID +
                ", messageID=" + messageID +
                ", inlineMessageID='" + inlineMessageID + '\'' +
                ", replyMarkup=" + replyMarkup +
                '}';
    }
}