package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents a chat.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class Chat {

    /** Unique identifier for this chat. */
    private Long id;

    /** Type of chat, can be either “private”, or “group”, or “channel”. */
    private String type;

    /** Optional. Title, for channels and group chats. */
    private String title;

    /** Optional. Username, for private chats and channels if available. */
    private String username;

    /** Optional. First name of the other party in a private chat. */
    @SerializedName("first_name")
    private String firstName;

    /** Optional. Last name of the other party in a private chat. */
    @SerializedName("last_name")
    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Chat chat = (Chat) o;

        if (!id.equals(chat.id)) return false;
        if (type != null ? !type.equals(chat.type) : chat.type != null) return false;
        if (title != null ? !title.equals(chat.title) : chat.title != null) return false;
        if (username != null ? !username.equals(chat.username) : chat.username != null) return false;
        if (firstName != null ? !firstName.equals(chat.firstName) : chat.firstName != null) return false;
        return !(lastName != null ? !lastName.equals(chat.lastName) : chat.lastName != null);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}