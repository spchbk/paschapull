package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents a video file.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class Video {

    /** Unique identifier for this file. */
    @SerializedName("file_id")
    private String fileID;

    /** Video width as defined by sender. */
    private Integer width;

    /** Video height as defined by sender. */
    private Integer height;

    /** Duration of the video in seconds as defined by sender. */
    private Integer duration;

    /** Optional. Video thumbnail. */
    private PhotoSize thumb;

    /** Optional. Mime type of a file as defined by sender. */
    @SerializedName("mime_type")
    private String mimeType;

    /** Optional. File size. */
    @SerializedName("file_size")
    private Integer fileSize;

    public String getFileID() {
        return fileID;
    }

    public void setFileID(String fileID) {
        this.fileID = fileID;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public PhotoSize getThumb() {
        return thumb;
    }

    public void setThumb(PhotoSize thumb) {
        this.thumb = thumb;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Video video = (Video) o;

        if (!fileID.equals(video.fileID)) return false;
        if (width != null ? !width.equals(video.width) : video.width != null) return false;
        if (height != null ? !height.equals(video.height) : video.height != null) return false;
        if (duration != null ? !duration.equals(video.duration) : video.duration != null) return false;
        if (thumb != null ? !thumb.equals(video.thumb) : video.thumb != null) return false;
        if (mimeType != null ? !mimeType.equals(video.mimeType) : video.mimeType != null) return false;
        return !(fileSize != null ? !fileSize.equals(video.fileSize) : video.fileSize != null);
    }

    @Override
    public int hashCode() {
        int result = fileID.hashCode();
        result = 31 * result + (width != null ? width.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (thumb != null ? thumb.hashCode() : 0);
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Video{" +
                "fileID='" + fileID + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", duration=" + duration +
                ", thumb=" + thumb +
                ", mimeType='" + mimeType + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}