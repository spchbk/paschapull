package ru.sberbank.uplatform.message.reply;

import com.google.gson.annotations.SerializedName;
import org.apache.http.entity.mime.MultipartEntityBuilder;

/**
 * Use this reply to send text messages.
 *
 * @author Baryshnikov Alexander.
 * @since 14.11.15.
 */
public class TextReply extends Reply {

    /** Text of the message to be sent. */
    private String text;

    /** Optional. Send message with HTML style syntax. */
    @SerializedName("parse_mode")
    private String parseMode = "HTML";

    /** Optional. Disables link previews for links in this message. */
    @SerializedName("disable_web_page_preview")
    private Boolean disableWebPagePreview = Boolean.TRUE;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getParseMode() {
        return parseMode;
    }

    public void setParseMode(String parseMode) {
        this.parseMode = parseMode;
    }

    public Boolean getDisableWebPagePreview() {
        return disableWebPagePreview;
    }

    public void setDisableWebPagePreview(Boolean disableWebPagePreview) {
        this.disableWebPagePreview = disableWebPagePreview;
    }

    public TextReply(String text) {
        this.text = text;
    }

    public TextReply() {
        // empty.
    }

    @Override
    public final String getActionName() {
        return "sendMessage";
    }

    @Override
    public void serialize(MultipartEntityBuilder holder) throws Exception {
        super.serialize(holder);
        if (text != null) holder.addTextBody("text", text, TEXT_PLAIN);
        if (parseMode != null) holder.addTextBody("parse_mode", parseMode, TEXT_PLAIN);
        if (disableWebPagePreview != null)
            holder.addTextBody("disable_web_page_preview", disableWebPagePreview.toString(), TEXT_PLAIN);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TextReply textReply = (TextReply) o;

        if (!getChatID().equals(textReply.getChatID())) return false;
        if (!this.text.equals(textReply.text)) return false;
        if (parseMode != null ? !parseMode.equals(textReply.parseMode) : textReply.parseMode != null) return false;
        if (disableWebPagePreview != null ? !disableWebPagePreview.equals(textReply.disableWebPagePreview) : textReply.disableWebPagePreview != null) return false;
        if (getReplyMarkup() != null ? !getReplyMarkup().equals(textReply.getReplyMarkup()) : textReply.getReplyMarkup() != null);
        return !(getReplyToMessageID() != null ? !getReplyToMessageID().equals(textReply.getReplyToMessageID()) : textReply.getReplyToMessageID() != null);
    }

    @Override
    public int hashCode() {
        int result = getChatID().hashCode();
        result = 31 * result + text.hashCode();
        result = 31 * result + (parseMode != null ? parseMode.hashCode() : 0);
        result = 31 * result + (disableWebPagePreview != null ? disableWebPagePreview.hashCode() : 0);
        result = 31 * result + (getReplyToMessageID() != null ? getReplyToMessageID().hashCode() : 0);
        result = 31 * result + (getReplyMarkup() != null ? getReplyMarkup().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TextReply{" +
                "chatID='" + getChatID() + '\'' +
                ", text='" + text + '\'' +
                ", parseMode='" + parseMode + '\'' +
                ", disableWebPagePreview=" + disableWebPagePreview +
                ", replyToMessageID=" + getReplyToMessageID() +
                '}';
    }
}