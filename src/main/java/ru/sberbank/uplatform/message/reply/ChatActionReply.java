package ru.sberbank.uplatform.message.reply;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import ru.sberbank.uplatform.message.ChatActions;

/**
 * Use this reply when you need to tell the user that something is happening
 * on the bot's side. The status is set for 5 seconds or less (when a message
 * arrives from your bot, Telegram clients clear its typing status).
 *
 * @author Pavel Tarasov
 * @since 04/02/2016.
 */
public class ChatActionReply extends Reply {

    /** Type of action to broadcast. */
    private String action;

    public ChatActionReply(ChatActions action) {
        this.action = action.getActionType();
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String getActionName() {
        return "sendChatAction";
    }

    @Override
    public void serialize(MultipartEntityBuilder holder) throws Exception {
        super.serialize(holder);
        if (action != null) {
            holder.addTextBody("action", action, TEXT_PLAIN);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ChatActionReply that = (ChatActionReply) o;
        return !(action != null ? !action.equals(that.action) : that.action != null);
    }

    @Override
    public int hashCode() {
        return 31 * 17 + (action != null ? action.hashCode() : 0);
    }
}