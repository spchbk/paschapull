package ru.sberbank.uplatform.message.reply;

import org.apache.http.Consts;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

/**
 * Reply builder.
 *
 * @author Baryshnikov Alexander.
 * @since 22.11.15.
 */
public interface Builder {
    static final ContentType TEXT_PLAIN = ContentType.create("text/plain", Consts.UTF_8);

    void serialize(MultipartEntityBuilder holder) throws Exception;

    String getActionName();
}
