package ru.sberbank.uplatform.message.reply;

import org.apache.http.entity.mime.MultipartEntityBuilder;

/**
 * Use this reply to send point on the map.
 *
 * @author Baryshnikov Alexander.
 * @since 15.11.15.
 */
public class LocationReply extends Reply {

    /** Latitude of location. */
    private Float latitude;

    /** Longitude of location. */
    private Float longitude;

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    @Override
    public void serialize(MultipartEntityBuilder holder) throws Exception {
        super.serialize(holder);
        if (latitude != null) holder.addTextBody("latitude", latitude.toString(), TEXT_PLAIN);
        if (longitude != null) holder.addTextBody("longitude", longitude.toString(), TEXT_PLAIN);
    }

    @Override
    public final String getActionName() {
        return "sendLocation";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocationReply that = (LocationReply) o;

        if (!getChatID().equals(that.getChatID())) return false;
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null) return false;
        if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null) return false;
        return !(getReplyToMessageID() != null ? !getReplyToMessageID().equals(that.getReplyToMessageID()) : that.getReplyToMessageID() != null);
    }

    @Override
    public int hashCode() {
        int result = getChatID().hashCode();
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (getReplyToMessageID() != null ? getReplyToMessageID().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LocationReply{" +
                "chat_id=" + getChatID() +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", reply_to_message_id=" + getReplyToMessageID() +
                '}';
    }
}