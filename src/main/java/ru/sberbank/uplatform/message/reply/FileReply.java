package ru.sberbank.uplatform.message.reply;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import ru.sberbank.uplatform.util.Utils;
import ru.sberbank.uplatform.message.Message;

import java.io.File;

/**
 * Abstract class for file replies.
 *
 * @author Baryshnikov Alexander.
 * @since 23.11.15.
 */
public abstract class FileReply extends Reply {

    /** File caption (may also be used when resending file by file_id), 0-200 characters. */
    private String caption;

    /** File entity. */
    private String entity;

    /** File-based reply. */
    private boolean fileBased = true;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getEntity() {
        return entity;
    }

    public boolean isFileBased() {
        return fileBased;
    }

    public void setFile(String fileName) {
        fileBased = true;
        entity = fileName;
    }

    public void setFileId(String fileId) {
        fileBased = false;
        entity = fileId;
    }

    protected abstract String getEntityFieldName();

    public abstract String getFileIdFromReply(Message message);

    @Override
    public void serialize(MultipartEntityBuilder holder) throws Exception {
        super.serialize(holder);
        if (entity != null) {
            if (isExternalURL()) {
                holder.addBinaryBody(getEntityFieldName(), Utils.getExternalURLAsInputStream(entity), ContentType.DEFAULT_BINARY, "photo.png");
            } else if (fileBased) {
                holder.addBinaryBody(getEntityFieldName(), new File(entity));
            } else {
                holder.addTextBody(getEntityFieldName(), entity, TEXT_PLAIN);
            }
        }
        if (caption != null) {
            holder.addTextBody("caption", caption, TEXT_PLAIN);
        }
    }

    @Override
    public String toString() {
        return "FileReply{" +
                "caption='" + caption + '\'' +
                ", " + getEntityFieldName() + "='" + entity + '\'' +
                ", fileBased=" + fileBased +
                "} " + super.toString();
    }

    public boolean isExternalURL(){
        return entity != null && entity.startsWith("http"); 
    }
}
