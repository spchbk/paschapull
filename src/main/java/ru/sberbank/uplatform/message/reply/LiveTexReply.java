package ru.sberbank.uplatform.message.reply;

import org.apache.http.entity.mime.MultipartEntityBuilder;

/**
 * Ugly hack. Because f**king LiveTex.
 *
 * Problems with deserialize keyboard from query parameters
 * thus we have used the already serialized Keyboard.
 *
 * @author Pavel Tarasov
 * @since 13/05/2016.
 */
public class LiveTexReply extends TextReply {

    /** Serialized keyboard. */
    private String serializeMarkup;

    public LiveTexReply(String text) {
        super(text);
    }

    @Override
    public void serialize(MultipartEntityBuilder holder) throws Exception {
        super.serialize(holder);
        if (serializeMarkup != null) holder.addTextBody("reply_markup", serializeMarkup, TEXT_PLAIN);
    }

    public String getSerializeMarkup() {
        return serializeMarkup;
    }

    public void setSerializeMarkup(String serializeMarkup) {
        this.serializeMarkup = serializeMarkup;
    }
}