package ru.sberbank.uplatform.message.reply;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import ru.sberbank.uplatform.BotType;
import ru.sberbank.uplatform.message.keyboard.ReplyKeyboard;

/**
 * A common for the all types replies object.
 *
 * @author Baryshnikov Alexander.
 * @since 17.11.15.
 */
public abstract class Reply implements Builder {

    /** Unique identifier for the target chat. */
    @SerializedName("chat_id")
    private Long chatID;

    /**
     * Optional. Additional interface options.A JSON-serialized object for a custom
     * reply keyboard, instructions to hide keyboard or to force a reply from the user.
     */
    @SerializedName("reply_markup")
    private ReplyKeyboard replyMarkup;

    /** Optional. If the message is a reply, ID of the original message. */
    @SerializedName("reply_to_message_id")
    private Integer replyToMessageID;

    /**
     * Optional. Sends the message silently. iOS users will not receive a notification,
     * Android users will receive a notification with no sound.
     */
    @SerializedName("disable_notification")
    private Boolean disableNotification = Boolean.FALSE;
    
    private Integer botType;

    public Integer getReplyToMessageID() {
        return replyToMessageID;
    }

    public void setReplyToMessageID(Integer replyToMessageID) {
        this.replyToMessageID = replyToMessageID;
    }

    public ReplyKeyboard getReplyMarkup() {
        return replyMarkup;
    }

    public void setReplyMarkup(ReplyKeyboard replyMarkup) {
        this.replyMarkup = replyMarkup;
    }

    public Long getChatID() {
        return chatID;
    }

    public void setChatID(Long chatID) {
        this.chatID = chatID;
    }

    public Boolean getDisableNotification() {
        return disableNotification;
    }

    public void setDisableNotification(Boolean disableNotification) {
        this.disableNotification = disableNotification;
    }

    public Integer getBotType() {
        return (botType != null) ? botType : BotType.TELEGRAM;
    }

    public void setBotType(Integer botType) {
        this.botType = botType;
    }

    @Override
    public void serialize(MultipartEntityBuilder holder) throws Exception {
        if (replyMarkup != null) holder.addTextBody("reply_markup", new Gson().toJson(replyMarkup), TEXT_PLAIN);
        if (chatID != null) holder.addTextBody("chat_id", chatID.toString(), TEXT_PLAIN);
        if (replyToMessageID != null) holder.addTextBody("reply_to_message_id", replyToMessageID.toString(), TEXT_PLAIN);
        if (disableNotification != null) holder.addTextBody("disable_notification", disableNotification.toString(), TEXT_PLAIN);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reply)) return false;

        Reply reply = (Reply) o;

        return !(replyMarkup != null ? !replyMarkup.equals(reply.replyMarkup) : reply.replyMarkup != null)
                && !(chatID != null ? !chatID.equals(reply.chatID) : reply.chatID != null);
    }

    @Override
    public int hashCode() {
        int result = replyMarkup != null ? replyMarkup.hashCode() : 0;
        result = 31 * result + (chatID != null ? chatID.hashCode() : 0);
        result = 31 * result + (botType != null ? botType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "chatID=" + chatID +
                ", replyMarkup=" + replyMarkup +
                ", replyToMessageID=" + replyToMessageID +
                ", disableNotification=" + disableNotification +
                ", botType=" + botType +
                '}';
    }


}