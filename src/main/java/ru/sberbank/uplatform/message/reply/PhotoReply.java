package ru.sberbank.uplatform.message.reply;

import ru.sberbank.uplatform.message.Message;

/**
 * Use this reply to send photos.
 *
 * @author Baryshnikov Alexander.
 * @since 23.11.15.
 */
public class PhotoReply extends FileReply {

    @Override
    protected String getEntityFieldName() {
        return "photo";
    }

    @Override
    public String getActionName() {
        return "sendPhoto";
    }

    @Override
    public String toString() {
        return "PhotoReply{} " + super.toString();
    }

    @Override
    public String getFileIdFromReply(Message message) {
        return message.getPhoto().get(0).getFileID();
    }
}