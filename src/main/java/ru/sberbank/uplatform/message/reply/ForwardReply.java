package ru.sberbank.uplatform.message.reply;

import com.google.gson.annotations.SerializedName;
import org.apache.http.entity.mime.MultipartEntityBuilder;

/**
 * Use this reply to forward messages of any kind.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class ForwardReply extends Reply {

    /** Unique identifier for the chat where the original message was sent. */
    @SerializedName("from_chat_id")
    private Long fromChatId;

    /** Unique message identifier. */
    @SerializedName("message_id")
    private Integer messageId;

    public Long getFromChatId() {
        return fromChatId;
    }

    public void setFromChatId(Long fromChatId) {
        this.fromChatId = fromChatId;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    @Override
    public String getActionName() {
        return "forwardMessage";
    }

    @Override
    public void serialize(MultipartEntityBuilder holder) throws Exception {
        super.serialize(holder);
        if (fromChatId != null) holder.addTextBody("from_chat_id", fromChatId.toString(), TEXT_PLAIN);
        if (messageId != null) holder.addTextBody("message_id", messageId.toString(), TEXT_PLAIN);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ForwardReply that = (ForwardReply) o;
        return !(fromChatId != null ? !fromChatId.equals(that.fromChatId) : that.fromChatId != null)
                && !(messageId != null ? !messageId.equals(that.messageId) : that.messageId != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (fromChatId != null ? fromChatId.hashCode() : 0);
        result = 31 * result + (messageId != null ? messageId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ForwardReply{" +
                "fromChatId=" + fromChatId +
                ", messageId=" + messageId +
                '}';
    }
}