package ru.sberbank.uplatform.message.reply;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import ru.sberbank.uplatform.message.Message;

/**
 * Use this reply to send video files.
 * Telegram clients support mp4 videos (other formats may be sent as Document).
 * Bots can currently send video files of up to 50 MB in size.
 *
 * @author Baryshnikov Alexander.
 * @since 22.11.15.
 */
public class VideoReply extends FileReply {

    /** Optional. Duration of sent video in second. */
    private Integer duration;

    /** Optional. Video width. */
    private Integer width;

    /** Optional. Video height. */
    private Integer height;

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public void serialize(MultipartEntityBuilder holder) throws Exception {
        super.serialize(holder);
        if (duration != null) holder.addTextBody("duration", duration.toString(), TEXT_PLAIN);
        if (width != null) holder.addTextBody("width", width.toString(), TEXT_PLAIN);
        if (height != null) holder.addTextBody("height", height.toString(), TEXT_PLAIN);
    }

    @Override
    protected String getEntityFieldName() {
        return "video";
    }

    @Override
    public String getActionName() {
        return "sendVideo";
    }

    @Override
    public String getFileIdFromReply(Message message) {
        return message.getVideo().getFileID();
    }

    @Override
    public String toString() {
        return "VideoReply{" +
                "video='" + getEntity() + '\'' +
                ", caption='" + getCaption() + '\'' +
                ", duration=" + duration +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}