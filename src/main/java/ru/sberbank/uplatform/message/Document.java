package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents a general file
 * (as opposed to photos, voice messages and audio files).
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class Document {

    /** Unique file identifier. */
    @SerializedName("file_id")
    private String fileID;

    /** Optional. Document thumbnail as defined by sender. */
    private PhotoSize thumb;

    /** Optional. Original filename as defined by sender. */
    @SerializedName("file_name")
    private String fileName;

    /** Optional. MIME type of the file as defined by sender. */
    @SerializedName("mime_type")
    private String mimeType;

    /** Optional. File size. */
    @SerializedName("file_size")
    private Integer fileSize;

    public String getFileID() {
        return fileID;
    }

    public void setFileID(String fileID) {
        this.fileID = fileID;
    }

    public PhotoSize getThumb() {
        return thumb;
    }

    public void setThumb(PhotoSize thumb) {
        this.thumb = thumb;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Document document = (Document) o;

        if (!fileID.equals(document.fileID)) return false;
        if (thumb != null ? !thumb.equals(document.thumb) : document.thumb != null) return false;
        if (fileName != null ? !fileName.equals(document.fileName) : document.fileName != null) return false;
        if (mimeType != null ? !mimeType.equals(document.mimeType) : document.mimeType != null) return false;
        return !(fileSize != null ? !fileSize.equals(document.fileSize) : document.fileSize != null);

    }

    @Override
    public int hashCode() {
        int result = fileID.hashCode();
        result = 31 * result + (thumb != null ? thumb.hashCode() : 0);
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Document{" +
                "fileID='" + fileID + '\'' +
                ", thumb=" + thumb +
                ", fileName='" + fileName + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}