package ru.sberbank.uplatform.message;

/**
 * @author Pavel Tarasov
 * @since 04/02/2016.
 */
public enum ChatActions {
    TEXT("typing"),
    PHOTO("upload_photo"),
    RECORD_VIDEO("record_video"),
    UPLOAD_VIDEO("upload_video"),
    RECORD_AUDIO("record_audio"),
    UPLOAD_AUDIO("upload_audio"),
    GENERAL_FILES("upload_document"),
    LOCATION("find_location");

    private String actionType;

    ChatActions(String actionType) {
        this.actionType = actionType;
    }

    public String getActionType() {
        return actionType;
    }
}
