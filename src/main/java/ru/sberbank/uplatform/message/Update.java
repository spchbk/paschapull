package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

import ru.sberbank.uplatform.message.inline.ChosenInlineResult;
import ru.sberbank.uplatform.message.inline.InlineQuery;
import ru.sberbank.uplatform.message.inline.CallbackQuery;

/**
 * This object represents an incoming update.
 * Only one of the optional parameters can be present in any given update.
 *
 * @author Baryshnikov Alexander.
 * @since 14.11.15.
 */
public class Update {

    /**
     * The update‘s unique identifier. Update identifiers start from a certain
     * positive number and increase sequentially. This ID becomes especially
     * handy if you’re using Webhooks, since it allows you to ignore repeated
     * updates or to restore the correct update sequence, should they get out of order.
     */
    @SerializedName("update_id")
    private Integer updateId;

    /** Optional. New incoming message of any kind — text, photo, sticker, etc. */
    private Message message;

    /** Optional. New incoming inline query. */
    @SerializedName("inline_query")
    private InlineQuery inlineQuery;

    /**
     * Optional. The result of an inline query that was chosen by a user and
     * sent to their chat partner.
     */
    @SerializedName("chosen_inline_result")
    private ChosenInlineResult chosenInlineResult;

    /** Optional. New incoming callback query. */
    @SerializedName("callback_query")
    private CallbackQuery callbackQuery;

	public Integer getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Integer updateId) {
        this.updateId = updateId;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public InlineQuery getInlineQuery() {
        return inlineQuery;
    }

    public void setInlineQuery(InlineQuery inlineQuery) {
        this.inlineQuery = inlineQuery;
    }

    public ChosenInlineResult getChosenInlineResult() {
        return chosenInlineResult;
    }

    public void setChosenInlineResult(ChosenInlineResult chosenInlineResult) {
        this.chosenInlineResult = chosenInlineResult;
    }

    public CallbackQuery getCallbackQuery() {
        return callbackQuery;
    }

    public void setCallbackQuery(CallbackQuery callbackQuery) {
        this.callbackQuery = callbackQuery;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Update update = (Update) o;

        if (callbackQuery != null ? !callbackQuery.equals(update.callbackQuery) : update.callbackQuery != null) return false;
        if (chosenInlineResult != null ? !chosenInlineResult.equals(update.chosenInlineResult)
                : update.chosenInlineResult != null) return false;
        if (inlineQuery != null ? !inlineQuery.equals(update.inlineQuery) : update.inlineQuery != null) return false;
        if (message != null ? !message.equals(update.message) : update.message != null) return false;
        if (updateId != null ? !updateId.equals(update.updateId) : update.updateId != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = updateId != null ? updateId.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (inlineQuery != null ? inlineQuery.hashCode() : 0);
        result = 31 * result + (chosenInlineResult != null ? chosenInlineResult.hashCode() : 0);
        result = 31 * result + (callbackQuery != null ? callbackQuery.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Update{" +
                "updateId=" + updateId +
                ", message=" + message +
                ", inlineQuery=" + inlineQuery +
                ", chosenInlineResult=" + chosenInlineResult +
                ", callbackQuery=" + callbackQuery +
                '}';
    }
}