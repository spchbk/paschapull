package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.BotType;

import java.util.List;

/**
 * This object represents a message.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class Message {

    /** Unique message identifier. */
    @SerializedName("message_id")
    private Integer id;

    /** Optional. Sender, can be empty for messages sent to channels. */
    private User from;

    /** Date the message was sent in Unix time. */
    private Integer date;

    /** Conversation the message belongs to. */
    private Chat chat;

    /** Optional. For forwarded messages, sender of the original message. */
    @SerializedName("forward_from")
    private User forwardFrom;

    /** Optional. For forwarded messages, date the original message was sent in Unix time. */
    @SerializedName("forward_date")
    private Integer forwardDate;

    /**
     * Optional. For replies, the original message. Note that the Message object in this field
     * will not contain further {@code reply_to_message} fields even if it itself is a reply.
     */
    @SerializedName("reply_to_message")
    private Message replyToMessage;

    /** Optional. For text messages, the actual UTF-8 text of the message, 0-4096 characters.*/
    private String text;

    /**
     * Optional. For text messages, special entities like usernames, URLs, bot commands,
     * etc. that appear in the text.
     */
    private List<MessageEntity> entities;

    /** Optional. Message is an audio file, information about the file. */
    private Audio audio;

    /** Optional. Message is a general file, information about the file. */
    private Document document;

    /** Optional. Message is a photo, available sizes of the photo. */
    private List<PhotoSize> photo;

    /** Optional. Message is a sticker, information about the sticker. */
    private Sticker sticker;

    /** Optional. Message is a video, information about the video. */
    private Video video;

    /** Optional. Message is a voice message, information about the file. */
    private Voice voice;

    /** Optional. Caption for the document, photo or video, 0-200 characters. */
    private String caption;

    /** Optional. Message is a shared contact, information about the contact. */
    private Contact contact;

    /** Optional. Message is a shared location, information about the location. */
    private Location location;

    /** Optional. Message is a venue, information about the venue. */
    private Venue venue;

    /**
     * Optional. A new member was added to the group, information about them
     * (this member may be the bot itself).
     */
    @SerializedName("new_chat_member")
    private User newChatMember;

    /**
     * Optional. A member was removed from the group, information about them
     * (this member may be the bot itself).
     */
    @SerializedName("left_chat_member")
    private User leftChatMember;

    /** Optional. A chat title was changed to this value. */
    @SerializedName("new_chat_title")
    private String newChatTitle;

    /** Optional. A chat photo was change to this value. */
    @SerializedName("new_chat_photo")
    private List<PhotoSize> newChatPhotoSize;

    /** Optional. Service message: the chat photo was deleted. */
    @SerializedName("delete_chat_photo")
    private Boolean deleteChatPhoto;

    /** Optional. Service message: the group has been created. */
    @SerializedName("group_chat_created")
    private Boolean groupChatCreated;

    /** Optional. Service message: the super group has been created. */
    @SerializedName("supergroup_chat_created")
    private Boolean superGroupChatCreated;

    /** Optional. Service message: the channel has been created. */
    @SerializedName("channel_chat_created")
    private Boolean channelChatCreated;

    /** Optional. The group has been migrated to a super group with the specified identifier. */
    @SerializedName("migrate_to_chat_id")
    private Long migrateToChatId;

    /** Optional. The super group has been migrated from a group with the specified identifier. */
    @SerializedName("migrate_from_chat_id")
    private Long migrateFromChatId;

    /**
     * Optional. Specified message was pinned. Note that the Message object in this field will
     * not contain further {@code reply_to_message} fields even if it is itself a reply.
     */
    @SerializedName("pinned_message")
    private Message pinnedMessage;

    /** For internal use.*/
    private Integer botType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public User getForwardFrom() {
        return forwardFrom;
    }

    public void setForwardFrom(User forwardFrom) {
        this.forwardFrom = forwardFrom;
    }

    public Integer getForwardDate() {
        return forwardDate;
    }

    public void setForwardDate(Integer forwardDate) {
        this.forwardDate = forwardDate;
    }

    public Message getReplyToMessage() {
        return replyToMessage;
    }

    public void setReplyToMessage(Message replyToMessage) {
        this.replyToMessage = replyToMessage;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<MessageEntity> getEntities() {
        return entities;
    }

    public void setEntities(List<MessageEntity> entities) {
        this.entities = entities;
    }

    public Audio getAudio() {
        return audio;
    }

    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public List<PhotoSize> getPhoto() {
        return photo;
    }

    public void setPhoto(List<PhotoSize> photo) {
        this.photo = photo;
    }

    public Sticker getSticker() {
        return sticker;
    }

    public void setSticker(Sticker sticker) {
        this.sticker = sticker;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public User getNewChatMember() {
        return newChatMember;
    }

    public void setNewChatMember(User newChatMember) {
        this.newChatMember = newChatMember;
    }

    public User getLeftChatMember() {
        return leftChatMember;
    }

    public void setLeftChatMember(User leftChatMember) {
        this.leftChatMember = leftChatMember;
    }

    public String getNewChatTitle() {
        return newChatTitle;
    }

    public void setNewChatTitle(String newChatTitle) {
        this.newChatTitle = newChatTitle;
    }

    public List<PhotoSize> getNewChatPhotoSize() {
        return newChatPhotoSize;
    }

    public void setNewChatPhotoSize(List<PhotoSize> newChatPhotoSize) {
        this.newChatPhotoSize = newChatPhotoSize;
    }

    public Boolean getDeleteChatPhoto() {
        return deleteChatPhoto;
    }

    public void setDeleteChatPhoto(Boolean deleteChatPhoto) {
        this.deleteChatPhoto = deleteChatPhoto;
    }

    public Boolean getGroupChatCreated() {
        return groupChatCreated;
    }

    public void setGroupChatCreated(Boolean groupChatCreated) {
        this.groupChatCreated = groupChatCreated;
    }

    public Boolean getSuperGroupChatCreated() {
        return superGroupChatCreated;
    }

    public void setSuperGroupChatCreated(Boolean superGroupChatCreated) {
        this.superGroupChatCreated = superGroupChatCreated;
    }

    public Boolean getChannelChatCreated() {
        return channelChatCreated;
    }

    public void setChannelChatCreated(Boolean channelChatCreated) {
        this.channelChatCreated = channelChatCreated;
    }

    public Long getMigrateToChatId() {
        return migrateToChatId;
    }

    public void setMigrateToChatId(Long migrateToChatId) {
        this.migrateToChatId = migrateToChatId;
    }

    public Long getMigrateFromChatId() {
        return migrateFromChatId;
    }

    public void setMigrateFromChatId(Long migrateFromChatId) {
        this.migrateFromChatId = migrateFromChatId;
    }

    public Message getPinnedMessage() {
        return pinnedMessage;
    }

    public void setPinnedMessage(Message pinnedMessage) {
        this.pinnedMessage = pinnedMessage;
    }

    public Integer getBotType() {
        return (botType != null) ? botType : BotType.TELEGRAM;
    }

    public void setBotType(Integer botType) {
        this.botType = botType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (audio != null ? !audio.equals(message.audio) : message.audio != null) return false;
        if (caption != null ? !caption.equals(message.caption) : message.caption != null) return false;
        if (channelChatCreated != null ? !channelChatCreated.equals(message.channelChatCreated) : message.channelChatCreated != null) return false;
        if (chat != null ? !chat.equals(message.chat) : message.chat != null) return false;
        if (contact != null ? !contact.equals(message.contact) : message.contact != null) return false;
        if (date != null ? !date.equals(message.date) : message.date != null) return false;
        if (deleteChatPhoto != null ? !deleteChatPhoto.equals(message.deleteChatPhoto) : message.deleteChatPhoto != null) return false;
        if (document != null ? !document.equals(message.document) : message.document != null) return false;
        if (entities != null ? !entities.equals(message.entities) : message.entities != null) return false;
        if (forwardDate != null ? !forwardDate.equals(message.forwardDate) : message.forwardDate != null) return false;
        if (forwardFrom != null ? !forwardFrom.equals(message.forwardFrom) : message.forwardFrom != null) return false;
        if (from != null ? !from.equals(message.from) : message.from != null) return false;
        if (groupChatCreated != null ? !groupChatCreated.equals(message.groupChatCreated) : message.groupChatCreated != null) return false;
        if (id != null ? !id.equals(message.id) : message.id != null) return false;
        if (leftChatMember != null ? !leftChatMember.equals(message.leftChatMember) : message.leftChatMember != null) return false;
        if (location != null ? !location.equals(message.location) : message.location != null) return false;
        if (migrateFromChatId != null ? !migrateFromChatId.equals(message.migrateFromChatId) : message.migrateFromChatId != null) return false;
        if (migrateToChatId != null ? !migrateToChatId.equals(message.migrateToChatId) : message.migrateToChatId != null) return false;
        if (newChatMember != null ? !newChatMember.equals(message.newChatMember) : message.newChatMember != null) return false;
        if (newChatPhotoSize != null ? !newChatPhotoSize.equals(message.newChatPhotoSize) : message.newChatPhotoSize != null) return false;
        if (newChatTitle != null ? !newChatTitle.equals(message.newChatTitle) : message.newChatTitle != null) return false;
        if (photo != null ? !photo.equals(message.photo) : message.photo != null) return false;
        if (pinnedMessage != null ? !pinnedMessage.equals(message.pinnedMessage) : message.pinnedMessage != null) return false;
        if (replyToMessage != null ? !replyToMessage.equals(message.replyToMessage) : message.replyToMessage != null) return false;
        if (sticker != null ? !sticker.equals(message.sticker) : message.sticker != null) return false;
        if (superGroupChatCreated != null ? !superGroupChatCreated.equals(message.superGroupChatCreated) : message.superGroupChatCreated != null) return false;
        if (text != null ? !text.equals(message.text) : message.text != null) return false;
        if (venue != null ? !venue.equals(message.venue) : message.venue != null) return false;
        if (video != null ? !video.equals(message.video) : message.video != null) return false;
        if (voice != null ? !voice.equals(message.voice) : message.voice != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (from != null ? from.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (chat != null ? chat.hashCode() : 0);
        result = 31 * result + (forwardFrom != null ? forwardFrom.hashCode() : 0);
        result = 31 * result + (forwardDate != null ? forwardDate.hashCode() : 0);
        result = 31 * result + (replyToMessage != null ? replyToMessage.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (entities != null ? entities.hashCode() : 0);
        result = 31 * result + (audio != null ? audio.hashCode() : 0);
        result = 31 * result + (document != null ? document.hashCode() : 0);
        result = 31 * result + (photo != null ? photo.hashCode() : 0);
        result = 31 * result + (sticker != null ? sticker.hashCode() : 0);
        result = 31 * result + (video != null ? video.hashCode() : 0);
        result = 31 * result + (voice != null ? voice.hashCode() : 0);
        result = 31 * result + (caption != null ? caption.hashCode() : 0);
        result = 31 * result + (contact != null ? contact.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (venue != null ? venue.hashCode() : 0);
        result = 31 * result + (newChatMember != null ? newChatMember.hashCode() : 0);
        result = 31 * result + (leftChatMember != null ? leftChatMember.hashCode() : 0);
        result = 31 * result + (newChatTitle != null ? newChatTitle.hashCode() : 0);
        result = 31 * result + (newChatPhotoSize != null ? newChatPhotoSize.hashCode() : 0);
        result = 31 * result + (deleteChatPhoto != null ? deleteChatPhoto.hashCode() : 0);
        result = 31 * result + (groupChatCreated != null ? groupChatCreated.hashCode() : 0);
        result = 31 * result + (superGroupChatCreated != null ? superGroupChatCreated.hashCode() : 0);
        result = 31 * result + (channelChatCreated != null ? channelChatCreated.hashCode() : 0);
        result = 31 * result + (migrateToChatId != null ? migrateToChatId.hashCode() : 0);
        result = 31 * result + (migrateFromChatId != null ? migrateFromChatId.hashCode() : 0);
        result = 31 * result + (pinnedMessage != null ? pinnedMessage.hashCode() : 0);
        result = 31 * result + (botType != null ? botType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", from=" + from +
                ", date=" + date +
                ", chat=" + chat +
                ", forwardFrom=" + forwardFrom +
                ", forwardDate=" + forwardDate +
                ", replyToMessage=" + replyToMessage +
                ", text='" + text + '\'' +
                ", entities=" + entities +
                ", audio=" + audio +
                ", document=" + document +
                ", photo=" + photo +
                ", sticker=" + sticker +
                ", video=" + video +
                ", voice=" + voice +
                ", caption='" + caption + '\'' +
                ", contact=" + contact +
                ", location=" + location +
                ", venue=" + venue +
                ", newChatMember=" + newChatMember +
                ", leftChatMember=" + leftChatMember +
                ", newChatTitle='" + newChatTitle + '\'' +
                ", newChatPhotoSize=" + newChatPhotoSize +
                ", deleteChatPhoto=" + deleteChatPhoto +
                ", groupChatCreated=" + groupChatCreated +
                ", superGroupChatCreated=" + superGroupChatCreated +
                ", channelChatCreated=" + channelChatCreated +
                ", migrateToChatId=" + migrateToChatId +
                ", migrateFromChatId=" + migrateFromChatId +
                ", pinnedMessage=" + pinnedMessage +
                ", botType=" + botType +
                '}';
    }

}