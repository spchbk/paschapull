package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents a sticker.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class Sticker {

    /** Unique identifier for this file. */
    @SerializedName("file_id")
    private String fileID;

    /** Sticker width. */
    private Integer width;

    /** Sticker height. */
    private Integer height;

    /** Optional. Sticker thumbnail in .webp or .jpg format. */
    private PhotoSize thumb;

    /** Optional. Emoji associated with the sticker. */
    private String emoji;

    /** Optional. File size. */
    @SerializedName("file_size")
    private Integer fileSize;

    public String getFileID() {
        return fileID;
    }

    public void setFileID(String fileID) {
        this.fileID = fileID;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public PhotoSize getThumb() {
        return thumb;
    }

    public void setThumb(PhotoSize thumb) {
        this.thumb = thumb;
    }

    public String getEmoji() {
        return emoji;
    }

    public void setEmoji(String emoji) {
        this.emoji = emoji;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sticker sticker = (Sticker) o;

        if (!fileID.equals(sticker.fileID)) return false;
        if (width != null ? !width.equals(sticker.width) : sticker.width != null) return false;
        if (height != null ? !height.equals(sticker.height) : sticker.height != null) return false;
        if (thumb != null ? !thumb.equals(sticker.thumb) : sticker.thumb != null) return false;
        if (emoji != null ? !emoji.equals(sticker.emoji) : sticker.emoji != null) return false;
        return !(fileSize != null ? !fileSize.equals(sticker.fileSize) : sticker.fileSize != null);
    }

    @Override
    public int hashCode() {
        int result = fileID.hashCode();
        result = 31 * result + (width != null ? width.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        result = 31 * result + (thumb != null ? thumb.hashCode() : 0);
        result = 31 * result + (emoji != null ? emoji.hashCode() : 0);
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Sticker{" +
                "fileID='" + fileID + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", thumb=" + thumb +
                ", emoji='" + emoji + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}