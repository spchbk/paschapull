package ru.sberbank.uplatform.message.keyboard;

/**
 * A common interface for all keyboards replies.
 *
 * @author Pavel Tarasov
 * @since 17.11.2015.
 */
public interface ReplyKeyboard {
}