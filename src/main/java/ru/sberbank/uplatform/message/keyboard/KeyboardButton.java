package ru.sberbank.uplatform.message.keyboard;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents one button of the reply keyboard.
 * For simple text buttons String can be used instead of this object to
 * specify text of the button. Optional fields are mutually exclusive.
 *
 * Note: request_contact and request_location options will only work in
 * Telegram versions released after 9 April, 2016.
 * Older clients will ignore them.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class KeyboardButton {

    /**
     * Text of the button. If none of the optional fields are used,
     * it will be sent to the bot as a message when the button is pressed.
     */
    private String text;

    /**
     * Optional. If True, the user's phone number will be sent as a contact
     * when the button is pressed. Available in private chats only.
     */
    @SerializedName("request_contact")
    private Boolean requestContact;

    /**
     * Optional. If True, the user's current location will be sent when the
     * button is pressed. Available in private chats only.
     */
    @SerializedName("request_location")
    private Boolean requestLocation;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getRequestContact() {
        return requestContact;
    }

    public void setRequestContact(Boolean requestContact) {
        this.requestContact = requestContact;
    }

    public Boolean getRequestLocation() {
        return requestLocation;
    }

    public void setRequestLocation(Boolean requestLocation) {
        this.requestLocation = requestLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeyboardButton that = (KeyboardButton) o;
        if (requestContact != null ? !requestContact.equals(that.requestContact) : that.requestContact != null) return false;
        if (requestLocation != null ? !requestLocation.equals(that.requestLocation) : that.requestLocation != null) return false;
        return !(text != null ? !text.equals(that.text) : that.text != null);
    }

    @Override
    public int hashCode() {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (requestContact != null ? requestContact.hashCode() : 0);
        result = 31 * result + (requestLocation != null ? requestLocation.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "KeyboardButton{" +
                "text='" + text + '\'' +
                ", requestContact=" + requestContact +
                ", requestLocation=" + requestLocation +
                '}';
    }
}