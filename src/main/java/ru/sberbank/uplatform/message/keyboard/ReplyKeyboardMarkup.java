package ru.sberbank.uplatform.message.keyboard;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * This object represents a custom keyboard with reply options.
 *
 * @author Pavel Tarasov
 * @since 17.11.2015.
 */
public class ReplyKeyboardMarkup implements ReplyKeyboard {

    /**
     * Array of button rows, each represented by an Array of
     * {@link KeyboardButton} objects.
     */
    @SerializedName("keyboard")
    private List<List<KeyboardButton>> keyboard;

    /**
     * Optional. Requests clients to resize the keyboard vertically for optimal
     * fit (e.g., make the keyboard smaller if there are just two rows of buttons).
     * Defaults to {@code false}, in which case the custom keyboard is always of the same
     * height as the app's standard keyboard.
     */
    @SerializedName("resize_keyboard")
    private Boolean resizeKeyboard;

    /**
     * Optional. Requests clients to hide the keyboard as soon as it's been used.
     * The keyboard will still be available, but clients will automatically display
     * the usual letter-keyboard in the chat – the user can press a special button
     * in the input field to see the custom keyboard again. Defaults to {@code false}.
     */
    @SerializedName("one_time_keyboard")
    private Boolean oneTimeKeyboard;

    /**
     * Optional. Use this parameter if you want to show the keyboard to specific users only.
     */
    private Boolean selective;

    public List<List<KeyboardButton>> getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(List<List<KeyboardButton>> keyboard) {
        this.keyboard = keyboard;
    }

    public Boolean getResizeKeyboard() {
        return resizeKeyboard;
    }

    public void setResizeKeyboard(Boolean resizeKeyboard) {
        this.resizeKeyboard = resizeKeyboard;
    }

    public Boolean getOneTimeKeyboard() {
        return oneTimeKeyboard;
    }

    public void setOneTimeKeyboard(Boolean oneTimeKeyboard) {
        this.oneTimeKeyboard = oneTimeKeyboard;
    }

    public Boolean getSelective() {
        return selective;
    }

    public void setSelective(Boolean selective) {
        this.selective = selective;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReplyKeyboardMarkup markup = (ReplyKeyboardMarkup) o;

        if (keyboard != null ? !keyboard.equals(markup.keyboard) : markup.keyboard != null) return false;
        if (oneTimeKeyboard != null ? !oneTimeKeyboard.equals(markup.oneTimeKeyboard) : markup.oneTimeKeyboard != null) return false;
        if (resizeKeyboard != null ? !resizeKeyboard.equals(markup.resizeKeyboard) : markup.resizeKeyboard != null) return false;
        return !(selective != null ? !selective.equals(markup.selective) : markup.selective != null);
    }

    @Override
    public int hashCode() {
        int result = keyboard != null ? keyboard.hashCode() : 0;
        result = 31 * result + (resizeKeyboard != null ? resizeKeyboard.hashCode() : 0);
        result = 31 * result + (oneTimeKeyboard != null ? oneTimeKeyboard.hashCode() : 0);
        result = 31 * result + (selective != null ? selective.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReplyKeyboardMarkup{" +
                "keyboard=" + keyboard +
                ", resizeKeyboard=" + resizeKeyboard +
                ", oneTimeKeyboard=" + oneTimeKeyboard +
                ", selective=" + selective +
                '}';
    }
}