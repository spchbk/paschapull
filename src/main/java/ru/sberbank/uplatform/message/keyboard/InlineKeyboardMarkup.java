package ru.sberbank.uplatform.message.keyboard;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * This object represents an inline keyboard that appears right next
 * to the message it belongs to.
 *
 * Note: This will only work in Telegram versions released after 9 April, 2016.
 * Older clients will display unsupported message.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class InlineKeyboardMarkup implements ReplyKeyboard {

    /**
     * Array of button rows, each represented by an
     * Array of {@link InlineKeyboardButton} objects.
     */
    @SerializedName("inline_keyboard")
    private List<List<InlineKeyboardButton>> inlineKeyboard;

    public List<List<InlineKeyboardButton>> getInlineKeyboard() {
        return inlineKeyboard;
    }

    public void setInlineKeyboard(List<List<InlineKeyboardButton>> inlineKeyboard) {
        this.inlineKeyboard = inlineKeyboard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InlineKeyboardMarkup that = (InlineKeyboardMarkup) o;
        return !(inlineKeyboard != null ?
                !inlineKeyboard.equals(that.inlineKeyboard) : that.inlineKeyboard != null);
    }

    @Override
    public int hashCode() {
        return inlineKeyboard != null ? inlineKeyboard.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "InlineKeyboardMarkup{" +
                "inlineKeyboard=" + inlineKeyboard +
                '}';
    }
}