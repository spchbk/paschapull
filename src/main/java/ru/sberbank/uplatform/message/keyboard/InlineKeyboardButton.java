package ru.sberbank.uplatform.message.keyboard;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents one button of an inline keyboard.
 * You must use exactly one of the optional fields.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class InlineKeyboardButton {

    /** Label text on the button. */
    private String text;

    /** Optional. HTTP url to be opened when button is pressed. */
    private String url;

    /**
     * Optional. Data to be sent in a callback query
     * {@link ru.sberbank.uplatform.message.inline.CallbackQuery}
     * to the bot when button is pressed, 1-64 bytes.
     */
    @SerializedName("callback_data")
    private String callbackData;

    /**
     * Optional. If set, pressing the button will prompt the user to select
     * one of their chats, open that chat and insert the bot‘s username and
     * the specified inline query in the input field. Can be empty, in which
     * case just the bot’s username will be inserted.
     */
    @SerializedName("switch_inline_query")
    private String switchInlineQuery;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCallbackData() {
        return callbackData;
    }

    public void setCallbackData(String callbackData) {
        this.callbackData = callbackData;
    }

    public String getSwitchInlineQuery() {
        return switchInlineQuery;
    }

    public void setSwitchInlineQuery(String switchInlineQuery) {
        this.switchInlineQuery = switchInlineQuery;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InlineKeyboardButton that = (InlineKeyboardButton) o;

        if (callbackData != null ? !callbackData.equals(that.callbackData) : that.callbackData != null) return false;
        if (switchInlineQuery != null ? !switchInlineQuery.equals(that.switchInlineQuery) : that.switchInlineQuery != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        return !(url != null ? !url.equals(that.url) : that.url != null);
    }

    @Override
    public int hashCode() {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (callbackData != null ? callbackData.hashCode() : 0);
        result = 31 * result + (switchInlineQuery != null ? switchInlineQuery.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InlineKeyboardButton{" +
                "text='" + text + '\'' +
                ", url='" + url + '\'' +
                ", callbackData='" + callbackData + '\'' +
                ", switchInlineQuery='" + switchInlineQuery + '\'' +
                '}';
    }
}