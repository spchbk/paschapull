package ru.sberbank.uplatform.message.keyboard;

import com.google.gson.annotations.SerializedName;

/**
 * Upon receiving a message with this object, Telegram clients will display
 * a reply interface to the user (act as if the user has selected the bot‘s
 * message and tapped ’Reply'). This can be extremely useful if you want to
 * create user-friendly step-by-step interfaces without having to sacrifice
 * privacy mode.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class ForceReply implements ReplyKeyboard {

    /**
     * Shows reply interface to the user, as if they manually selected the
     * bot‘s message and tapped ’Reply'.
     */
    @SerializedName("force_reply")
    private Boolean forceReply = Boolean.TRUE;

    /**
     * Optional. Use this parameter if you want to hide keyboard for
     * specific users only.
     */
    private Boolean selective;

    public Boolean getForceReply() {
        return forceReply;
    }

    public Boolean getSelective() {
        return selective;
    }

    public void setSelective(Boolean selective) {
        this.selective = selective;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ForceReply that = (ForceReply) o;
        return !(forceReply != null ? !forceReply.equals(that.forceReply) : that.forceReply != null)
                && !(selective != null ? !selective.equals(that.selective) : that.selective != null);
    }

    @Override
    public int hashCode() {
        int result = forceReply != null ? forceReply.hashCode() : 0;
        result = 31 * result + (selective != null ? selective.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ForceReply{" +
                "forceReply=" + forceReply +
                ", selective=" + selective +
                '}';
    }
}