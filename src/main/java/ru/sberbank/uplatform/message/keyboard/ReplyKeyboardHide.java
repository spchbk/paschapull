package ru.sberbank.uplatform.message.keyboard;

import com.google.gson.annotations.SerializedName;

/**
 * Upon receiving a message with this object, Telegram clients will hide
 * the current custom keyboard and display the default letter-keyboard.
 * By default, custom keyboards are displayed until a new keyboard is sent
 * by a bot. An exception is made for one-time keyboards that are hidden
 * immediately after the user presses a button.
 *
 * @author Pavel Tarasov
 * @since 17.11.2015.
 */
public class ReplyKeyboardHide implements ReplyKeyboard {

    /** Requests clients to hide the custom keyboard. */
    @SerializedName("hide_keyboard")
    private Boolean hideKeyboard = Boolean.TRUE;

    /**
     * Optional. Use this parameter if you want to hide keyboard for
     * specific users only.
     */
    private Boolean selective;

    public Boolean getHideKeyboard() {
        return hideKeyboard;
    }

    public Boolean getSelective() {
        return selective;
    }

    public void setSelective(Boolean selective) {
        this.selective = selective;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReplyKeyboardHide hide = (ReplyKeyboardHide) o;
        return !(hideKeyboard != null ? !hideKeyboard.equals(hide.hideKeyboard) : hide.hideKeyboard != null)
                && !(selective != null ? !selective.equals(hide.selective) : hide.selective != null);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (hideKeyboard != null ? hideKeyboard.hashCode() : 0);
        result = 31 * result + (selective != null ? selective.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReplyKeyboardHide{" +
                "hideKeyboard=" + hideKeyboard +
                ", selective=" + selective +
                '}';
    }
}