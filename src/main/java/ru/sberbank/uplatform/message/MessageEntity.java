package ru.sberbank.uplatform.message;


/**
 * This object represents one special entity in a text message.
 * For example, hashtags, usernames, URLs, etc.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class MessageEntity {

    /**
     * Type of the entity. One of mention (@username), hashtag, bot_command, url,
     * email, bold (bold text), italic (italic text), code (monowidth string),
     * pre (monowidth block), text_link (for clickable text URLs).
     */
    private String type;

    /** Offset in UTF-16 code units to the start of the entity. */
    private Integer offset;

    /** Length of the entity in UTF-16 code units. */
    private Integer length;

    /** Optional. For “text_link” only, url that will be opened after user taps on the text. */
    private String url;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageEntity that = (MessageEntity) o;

        if (length != null ? !length.equals(that.length) : that.length != null) return false;
        if (offset != null ? !offset.equals(that.offset) : that.offset != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (offset != null ? offset.hashCode() : 0);
        result = 31 * result + (length != null ? length.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MessageEntity{" +
                "type='" + type + '\'' +
                ", offset=" + offset +
                ", length=" + length +
                ", url='" + url + '\'' +
                '}';
    }
}