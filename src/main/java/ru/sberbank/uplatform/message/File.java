package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

/**
 * This object represents a file ready to be downloaded.
 * The file can be downloaded via the link
 * <a href="https://api.telegram.org/file/bot<token>/<file_path>"></a>
 * It is guaranteed that the link will be valid for at least 1 hour.
 * When the link expires, a new one can be requested by calling getFile.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class File {

    /** Unique identifier for this file. */
    @SerializedName("file_id")
    private String fileId;

    /** Optional. File size, if known. */
    @SerializedName("file_size")
    private Integer fileSize;

    /** Optional. File path. */
    @SerializedName("file_path")
    private String filePath;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        File file = (File) o;

        if (fileId != null ? !fileId.equals(file.fileId) : file.fileId != null) return false;
        if (filePath != null ? !filePath.equals(file.filePath) : file.filePath != null) return false;
        if (fileSize != null ? !fileSize.equals(file.fileSize) : file.fileSize != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = fileId != null ? fileId.hashCode() : 0;
        result = 31 * result + (fileSize != null ? fileSize.hashCode() : 0);
        result = 31 * result + (filePath != null ? filePath.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "File{" +
                "fileId='" + fileId + '\'' +
                ", fileSize=" + fileSize +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}