package ru.sberbank.uplatform.message;

/**
 * This object represents a point on the map.
 *
 * @author Baryshnikov Alexander.
 * @since 13.11.15.
 */
public class Location {

    /** Longitude as defined by sender. */
    private Float longitude;

    /** Latitude as defined by sender. */
    private Float latitude;

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;
        return !(longitude != null ? !longitude.equals(location.longitude) : location.longitude != null)
                && !(latitude != null ? !latitude.equals(location.latitude) : location.latitude != null);
    }

    @Override
    public int hashCode() {
        int result = longitude != null ? longitude.hashCode() : 0;
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LocationReply{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}