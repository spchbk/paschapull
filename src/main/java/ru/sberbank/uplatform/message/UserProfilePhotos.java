package ru.sberbank.uplatform.message;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * This object represent a user's profile pictures.
 *
 * @author Tarasov Pavel.
 * @since 25.04.16.
 */
public class UserProfilePhotos {

    /** Total number of profile pictures the target user has. */
    @SerializedName("total_count")
    private Integer totalCount;

    /** Requested profile pictures (in up to 4 sizes each). */
    private List<List<PhotoSize>> photos;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public List<List<PhotoSize>> getPhotos() {
        return photos;
    }

    public void setPhotos(List<List<PhotoSize>> photos) {
        this.photos = photos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserProfilePhotos that = (UserProfilePhotos) o;
        return !(photos != null ? !photos.equals(that.photos) : that.photos != null)
                && !(totalCount != null ? !totalCount.equals(that.totalCount) : that.totalCount != null);
    }

    @Override
    public int hashCode() {
        int result = totalCount != null ? totalCount.hashCode() : 0;
        result = 31 * result + (photos != null ? photos.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserProfilePhotos{" +
                "totalCount=" + totalCount +
                ", photos=" + photos +
                '}';
    }
}