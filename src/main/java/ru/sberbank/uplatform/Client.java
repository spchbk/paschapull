package ru.sberbank.uplatform;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.Optional;

/**
 * Created by baryshnikov on 18.11.15.
 */
public class Client {
    private String state;
    private JsonElement content;

    public Client() {
        this(ClientState.IDLE.getEndpoint());
    }

    public Client(String state) {
        this.state = state;
    }

    public Client(ClientState state) {
        this.state = state.getEndpoint();
    }

    public String getState() {
        return (state == null || state.isEmpty() ? ClientState.IDLE.getEndpoint() : state);
    }

    public void setState(ClientState state) {
        this.state = state.getEndpoint();
    }

    public void setState(From endpoint) {
        this.state = endpoint.endpoint;
    }

    public void setState(String s) {
        this.state = s;
    }

    public void setContent(Object content) {
        this.content = new Gson().toJsonTree(content);
    }

    public <T> Optional<T> getContent(Class<T> clasz) {
        if (content == null) return Optional.empty();
        T t = new Gson().fromJson(content, clasz);
        if (t.getClass().equals(clasz))//Happy  runtime check
            return Optional.of(t);
        return Optional.empty();
    }

    public boolean isIdleState() {
        return "".equals(state) || state == null || ClientState.IDLE.getEndpoint().equals(state);
    }
}
