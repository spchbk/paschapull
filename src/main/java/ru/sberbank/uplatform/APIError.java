package ru.sberbank.uplatform;

/**
 * Created by baryshnikov on 22.11.15.
 */
public class APIError extends Exception {
    public APIError(String message) {
        super(message);
    }
}
