package ru.sberbank.uplatform.util;

import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.text.ParsePosition;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * Created by baryshnikov on 27.12.15.
 */
public class Utils {
    public static String encode(String text) {
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch (Throwable t) {
            return "";
        }
    }

    public static boolean isNumeric(String str) {
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }

    public static InputStream getExternalURLAsInputStream(String uriStr) {
        try {
            URI uri = new URI(uriStr);
            HttpPost post = new HttpPost(new URI(uri.getScheme(), uri.getHost(), uri.getPath(), null));
            post.setEntity(new StringEntity(uri.getQuery(), "UTF-8"));

            HttpClient client = HttpClientBuilder.create().build();
            HttpResponse response = client.execute(post);

            return response.getEntity().getContent();

        } catch (Exception e) {
            return null;
        }
    }

}
