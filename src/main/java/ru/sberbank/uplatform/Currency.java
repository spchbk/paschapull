package ru.sberbank.uplatform;

import ru.sberbank.uplatform.sbol.api.QuoteType;

import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Created by baryshnikov on 28.11.15.
 */
public enum Currency {
    //TODO: Think how to move it to external file
    Euro("(eur|евро|€)", "€"),
    Rub("(rub|руб|руб\\.|рублей|рубль|рубля|р\\.|Ꝑ)", "Ꝑ"),
    USD("(usd|долар|доллар|dollar|dolar|\\$|бакс)", "$"),
    GBP("(gbp|фунт|фунт.*стерлинг.*|£)", "£"),
    CHF("(chf|франк|швейцарск.*франк.*|₣)", "₣"),
    GOLD("(г|кг|грам|килограм)(.*?золот|gold)", "GLD"),
    SILVER("(г|кг|грам|килограм)(.*?серебр|silver)", "SLV"),
    PLATINUM("(г|кг|грам|килограм)(.*?платин|platin.*)", "PLT"),
    PALLADIUM("(г|кг|грам|килограм)(.*?паллади|pallad.*)", "PLD"),
    any("(" + Euro.getPattern() + "|" + Rub.getPattern() + "|" + USD.getPattern() + "|" + GBP.getPattern() + "|" + CHF.getPattern()
            + "|" + GOLD.getPattern() + "|" + SILVER.getPattern() + "|" + PLATINUM.getPattern() + "|" + PALLADIUM.getPattern() + ")", "");


    private final Pattern pattern;
    private final String symbol;


    Currency(String pattern, String symbol) {
        this.pattern = Pattern.compile(pattern);
        this.symbol = symbol;
    }

    public boolean isMatch(String possibleCurrencyName) {
        return pattern.matcher(possibleCurrencyName).matches();
    }

    public String getPattern() {
        return pattern.pattern();
    }

    public Pattern getCompiled() {
        return pattern;
    }

    public String getSymbol() {
        return symbol;
    }

    public static Optional<Currency> findCurrency(String possibleCurrencyName) {
        if (Euro.isMatch(possibleCurrencyName)) return Optional.of(Euro);
        if (Rub.isMatch(possibleCurrencyName)) return Optional.of(Rub);
        if (USD.isMatch(possibleCurrencyName)) return Optional.of(USD);
        if (GBP.isMatch(possibleCurrencyName)) return Optional.of(GBP);
        if (CHF.isMatch(possibleCurrencyName)) return Optional.of(CHF);
        if (GOLD.isMatch(possibleCurrencyName)) return Optional.of(GOLD);
        if (SILVER.isMatch(possibleCurrencyName)) return Optional.of(SILVER);
        if (PLATINUM.isMatch(possibleCurrencyName)) return Optional.of(PLATINUM);
        if (PALLADIUM.isMatch(possibleCurrencyName)) return Optional.of(PALLADIUM);
        return Optional.empty();
    }

    /**
     * Get currency from quote type. This is backward capability change
     */
    public static Optional<Currency> findCurrency(QuoteType quoteType) {
        if (quoteType == QuoteType.Dollar) return Optional.of(USD);
        if (quoteType == QuoteType.Euro) return Optional.of(Euro);
        if (quoteType == QuoteType.Pound) return Optional.of(GBP);
        if (quoteType == QuoteType.Frank) return Optional.of(CHF);
        if (quoteType == QuoteType.Gold) return Optional.of(GOLD);
        if (quoteType == QuoteType.Silver) return Optional.of(SILVER);
        if (quoteType == QuoteType.Palladium) return Optional.of(PALLADIUM);
        if (quoteType == QuoteType.Platinum) return Optional.of(PLATINUM);
        return Optional.empty();
    }
    
    public static String getExchangeAnyPattern() {
        return "(\\d+|\\d+(\\.|,)\\d+)\\s*" + Currency.any.getPattern() + ".*";
    }
    
}
