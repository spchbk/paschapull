package ru.sberbank.uplatform;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import ru.sberbank.uplatform.consumption.ConsumptionBean;
import ru.sberbank.uplatform.consumption.ConsumptionStrConv;
import ru.sberbank.uplatform.consumption.GetConsumptionRequest;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.route.CustomRouteBuilder;

/**
 * Created by Anton Lenok on 15.04.16.
 * Маршрут для работы с доходами и расходами
 */
public class BalanceRoutes extends CustomRouteBuilder {

    private Map<Long, ConsumptionBean> consumptionMap = new HashMap<>();

    @Override
    public void configure() {
        //ADD_CONSUMPTION_OR_REVENUE -> CHECK_INIT_CONSUMPTION_OR_REVENUE ->
        //(SHOW_AGREEMENT_CONSUMPTION_OR_REVENUE -> ACCEPT_OR_REJECT_CONSUMPTION_AGREEMENT ) ->
        //CONSUMPTION_OR_REVENUE_FINISH_ADDING -> BALANCE_?_CATEGORY
        from(Endpoints.ADD_CONSUMPTION_OR_REVENUE.getEndpoint())
                .id(Endpoints.ADD_CONSUMPTION_OR_REVENUE.getId())
                .process(exchange -> {
                    String mode = (String) exchange.getProperty("group2");
                    String purpose1 = (String) exchange.getProperty("group4");
                    String value = (String) exchange.getProperty("group5");
                    String purpose2 = (String) exchange.getProperty("group11");

                    Long chatId = exchange.getProperty("chat_id", Long.class);
                    ConsumptionBean consumptionBean = new ConsumptionBean();
                    consumptionBean.setUserId(chatId);
                    consumptionBean.setConsumptionMode(ConsumptionStrConv.getConsumptionMode(mode));
                    consumptionBean.setValue(new BigDecimal(value));

                    if (purpose1 != null && !purpose1.isEmpty()) {
                        consumptionBean.setPurpose(purpose1);
                    } else if (purpose2 != null && !purpose2.isEmpty()) {
                        consumptionBean.setPurpose(purpose2);
                    }

                    exchange.getIn().setHeader("consumptionBean", consumptionBean);
                    //При добавлении доходов категории не выводятся
                    boolean isRevenue = Boolean.FALSE.equals(consumptionBean.getConsumptionMode());
                    if (isRevenue) {
                        exchange.getIn().setHeader("addBalanceWithoutCategory", true);
                        exchange.getIn().setHeader("flagCategoryExists", true);
                    }
                    consumptionMap.put(chatId, consumptionBean);
                })
                //Go to DBRoutes
                .to(Endpoints.CHECK_INIT_CONSUMPTION_OR_REVENUE.endpoint);

        from(Endpoints.CONSUMPTION_OR_REVENUE_FINISH_ADDING.endpoint)
                .id(Endpoints.CONSUMPTION_OR_REVENUE_FINISH_ADDING.id)
                .choice()
                .when(header("addBalanceWithoutCategory").isEqualTo(true))
                .to(Endpoints.ADD_CATEGORY_CONSUMPTION_OR_REVENUE.endpoint)

                .otherwise()
                .process(exchange -> {
                    Long chatId = exchange.getProperty("chat_id", Long.class);
                    ConsumptionBean consumptionBean = consumptionMap.get(chatId);

                    setStatusCategoryConsumptionOrRevenue(exchange, consumptionBean.getConsumptionMode());
                    boolean isConsumption = consumptionBean.getConsumptionMode();
                    if (isConsumption) {
                        exchange.getIn().setHeader("keyboard", "consumption_category_menu");
                    } else {
                        exchange.getIn().setHeader("keyboard", "revenue_category_menu");
                    }
                })
                .setHeader("template", constant("templates/consumption-or-revenue-show-categories.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .end();

        from(Endpoints.SHOW_AGREEMENT_CONSUMPTION_OR_REVENUE.endpoint)
                .id(Endpoints.SHOW_AGREEMENT_CONSUMPTION_OR_REVENUE.id)
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.ACCEPT_OR_REJECT_CONSUMPTION_AGREEMENT))
                .setHeader("template", constant("templates/add-consumption-or-revenue-agreement.vm"))
                .setHeader("keyboard", constant("consumption_agreement"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(ClientState.ACCEPT_OR_REJECT_CONSUMPTION_AGREEMENT.endpoint)
                .id(ClientState.ACCEPT_OR_REJECT_CONSUMPTION_AGREEMENT.getId())
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE))
                .choice()
                .when(method(IsAgreementAcceptedBean.class))
                // consumption agreement accepted
                .process(exchange -> {
                    Long chatId = exchange.getProperty("chat_id", Long.class);
                    ConsumptionBean consumptionBean = consumptionMap.get(chatId);
                    exchange.getIn().setHeader("consumptionBean", consumptionBean);

                    //При добавлении доходов категории не выводятся
                    boolean isRevenue = Boolean.FALSE.equals(consumptionBean.getConsumptionMode());
                    if (isRevenue) {
                        exchange.getIn().setHeader("addBalanceWithoutCategory", true);
                        exchange.getIn().setHeader("flagCategoryExists", true);
                    }
                })
                .to(Endpoints.CONSUMPTION_OR_REVENUE_FINISH_ADDING.endpoint)
                .otherwise()
                // consumption agreement rejected
                .setHeader("template", constant("templates/add-consumption-or-revenue-agreement-refuse.vm"))
                .setHeader("keyboard", constant("main_menu"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .end();


        //BALANCE_?_CATEGORY -> (CHECK_CATEGORY_CONSUMPTION_OR_REVENUE) -> ADD_CATEGORY_CONSUMPTION_OR_REVENUE
        from(ClientState.BALANCE_CONSUMPTION_CATEGORY.endpoint)
                .id(ClientState.BALANCE_CONSUMPTION_CATEGORY.getId())
                .process(exchange -> exchange.getIn().setHeader("nameCheckCategory", "consumption_category_menu"))
                //Go to ClientRoutes
                .to(Endpoints.CHECK_CATEGORY_CONSUMPTION_OR_REVENUE.endpoint);
        from(ClientState.BALANCE_REVENUE_CATEGORY.endpoint)
                .id(ClientState.BALANCE_REVENUE_CATEGORY.getId())
                .process(exchange -> exchange.getIn().setHeader("flagCategoryExists", true))
                .to(Endpoints.ADD_CATEGORY_CONSUMPTION_OR_REVENUE.endpoint);

        Processor processorConsumptionOrRevenueCategory = exchange -> {
            Long chatId = exchange.getProperty("chat_id", Long.class);
            Message message = exchange.getProperty("message", Message.class);
            ConsumptionBean consumptionBean = consumptionMap.get(chatId);
            consumptionBean.setCategory(message.getText());

            exchange.getIn().setHeader("consumptionBean", consumptionBean);
        };

        from(Endpoints.ADD_CATEGORY_CONSUMPTION_OR_REVENUE.endpoint)
                .id(Endpoints.ADD_CATEGORY_CONSUMPTION_OR_REVENUE.id)
                .choice()
                .when(header("flagCategoryExists").isEqualTo(true))
                .process(processorConsumptionOrRevenueCategory)
                .to(Endpoints.ADD_CONSUMPTION_OR_REVENUE_DB.getEndpoint())
                .setHeader("template", constant("templates/add-consumption-or-revenue.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("consumption_or_revenue_sub_menu"))
                .process(exchange -> {
                    Long chatId = exchange.getProperty("chat_id", Long.class);
                    ConsumptionBean consumptionBean = consumptionMap.get(chatId);
                    setStatusConsumptionOrRevenue(exchange, consumptionBean.getConsumptionMode());

                    if (consumptionMap.containsKey(chatId)) {
                        consumptionMap.remove(chatId);
                    }
                })
                .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .otherwise()
                .process(exchange -> {
                    exchange.getProperty("client", Client.class).setState(ClientState.IDLE);
                    Long chatId = exchange.getProperty("chat_id", Long.class);
                    if (consumptionMap.containsKey(chatId)) {
                        consumptionMap.remove(chatId);
                    }
                })
                .to(Endpoints.ROUTER.endpoint)
                .end();


        from(Endpoints.GET_CONSUMPTION_OR_REVENUE.getEndpoint())
                .id(Endpoints.GET_CONSUMPTION_OR_REVENUE.getId())
                .process(ex -> {
                    String type = (String) ex.getProperty("group2");
                    String purpose = null;//We can't distinguish purpose
                    String preposition = (String) ex.getProperty("group4");
                    String interval = (String) ex.getProperty("group5");
                    String days = (String) ex.getProperty("group8");

                    GetConsumptionRequest consumptionRequest = new GetConsumptionRequest(type, purpose, preposition, interval, days, false);
                    ex.getIn().setHeader(KEY_CONS_REQ, consumptionRequest);
                    ex.getIn().setHeader(KEY_CONS_MOD, consumptionRequest.isConsumptionMode());
                    setStatusConsumptionOrRevenue(ex, consumptionRequest.isConsumptionMode());
                })
                .to(Endpoints.GET_CONSUMPTION_PROCESS.getEndpoint());

        from(Endpoints.GET_CONSUMPTION_OR_REVENUE_ALL)
                .process(ex -> {
                    GetConsumptionRequest consumptionRequest = new GetConsumptionRequest(ConsumptionStrConv.getConsumptionModeSimple(getMessageText(ex)));
                    setHeader(ex, KEY_CONS_REQ, consumptionRequest);
                    setHeader(ex, KEY_CONS_MOD, consumptionRequest.isConsumptionMode());
                    setStatusConsumptionOrRevenue(ex, consumptionRequest.isConsumptionMode());
                })
                .to(Endpoints.GET_CONSUMPTION_PROCESS.getEndpoint());

        from(Endpoints.GET_CONSUMPTION_PROCESS)
                .to(Endpoints.GET_CHART_DB.getEndpoint())
                .to(Endpoints.GET_CHART_INTERNAL.getEndpoint())
                .to(Endpoints.GET_CONSUMPTION_OR_REVENUE_DB.getEndpoint())
                .setHeader("template", constant("templates/get-consumption-or-revenue.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("consumption_or_revenue_sub_menu"))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.GET_CONSUMPTION_OR_REVENUE_MAX.getEndpoint())
                .id(Endpoints.GET_CONSUMPTION_OR_REVENUE_MAX.getId())
                .process(ex -> {
                    String type = (String) ex.getProperty("group3");
                    String preposition = (String) ex.getProperty("group5");
                    String interval = (String) ex.getProperty("group6");
                    String days = (String) ex.getProperty("group9");

                    GetConsumptionRequest consumptionRequest = new GetConsumptionRequest(type, null, preposition, interval, days, true);
                    ex.getIn().setHeader(KEY_CONS_REQ, consumptionRequest);
                    setStatusConsumptionOrRevenue(ex, consumptionRequest.isConsumptionMode());
                })
                .to(Endpoints.GET_CONSUMPTION_OR_REVENUE_DB.getEndpoint())
                .setHeader("template", constant("templates/get-consumption-or-revenue-max.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("consumption_or_revenue_menu"))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.GET_BALANCE.getEndpoint())
                .id(Endpoints.GET_BALANCE.getId())
                .process(ex -> {
                    GetConsumptionRequest consumptionRequest = new GetConsumptionRequest("баланс", null, null, null, null, false);
                    ex.getIn().setHeader(KEY_CONS_REQ, consumptionRequest);
                })
                .to(Endpoints.GET_CHART_DB.getEndpoint())
                .to(Endpoints.GET_CHART_INTERNAL.getEndpoint())
                .to(Endpoints.GET_BALANCE_DB.getEndpoint())
                .setHeader("template", constant("templates/get-balance.vm"))
                .to("direct:render-template")
                .setHeader("keyboard", constant("main_menu"))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(Endpoints.GET_CHART_INTERNAL.getEndpoint())
                .id(Endpoints.GET_CHART_INTERNAL.getId())
                .log("Body: ${body}")
                .choice()
                .when(header("chartURL"))
                .setBody(header("chartURL"))
                .to(Endpoints.ADD_PHOTO_REPLY.endpoint)
                .end();

        from(Endpoints.CONSUMPTION_OR_REVENUE_DEMO.endpoint)
                .id(Endpoints.CONSUMPTION_OR_REVENUE_DEMO.id)
                .process(exchange -> {
                    List<Map<String, Object>> purposes = Arrays.asList(map("метро", 100), map("обед", 300), map("булочки", 90), map("молоко", 100), map("хлеб", 80));
                    exchange.getIn().setHeader("purposes", purposes);
                    exchange.getIn().setHeader("sum", 670);
                    GetConsumptionRequest consumptionRequest = new GetConsumptionRequest(true, true);
                    exchange.getIn().setHeader(KEY_CONS_REQ, consumptionRequest);
                })
                .to(Endpoints.GET_CHART_DB.getEndpoint())
                .to(Endpoints.GET_CHART_INTERNAL.getEndpoint())
                .setHeader("template", constant("templates/get-consumption-or-revenue.vm"))
                .to("direct:render-template")
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

    }

    private Map<String, Object> map(String purpose, int money) {
        Map<String, Object> purposes = new HashMap<>();
        purposes.put("purpose", purpose);
        purposes.put("sum_value", money);
        return purposes;
    }

    private void setStatusCategoryConsumptionOrRevenue(Exchange exchange, boolean isConsumption) {
        boolean isRevenue = !isConsumption;
        if (isConsumption) {
            exchange.getProperty("client", Client.class).setState(ClientState.BALANCE_CONSUMPTION_CATEGORY);
        }
        if (isRevenue) {
            exchange.getProperty("client", Client.class).setState(ClientState.BALANCE_REVENUE_CATEGORY);
        }
    }

    private void setStatusConsumptionOrRevenue(Exchange exchange, boolean isConsumption) {
        boolean isRevenue = !isConsumption;
        if (isConsumption) {
            exchange.getProperty("client", Client.class).setState(ClientState.STAT_CONSUMPTION);
        }
        if (isRevenue) {
            exchange.getProperty("client", Client.class).setState(ClientState.STAT_REVENUE);
        }
    }

}