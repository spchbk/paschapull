package ru.sberbank.uplatform;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.LinkedBlockingQueue;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import ru.sberbank.uplatform.commerce.flowers.api.Flower;
import ru.sberbank.uplatform.consumption.ConsumptionBean;
import ru.sberbank.uplatform.consumption.ConsumptionChartURLBuilder;
import ru.sberbank.uplatform.consumption.GetConsumptionRequest;
import ru.sberbank.uplatform.consumption.SumPerPeriod;
import ru.sberbank.uplatform.consumption.SumPerPeriodRowMapper;
import ru.sberbank.uplatform.dbo.MetricBean;
import ru.sberbank.uplatform.dbo.OrderBean;
import ru.sberbank.uplatform.dbo.QueueBean;
import ru.sberbank.uplatform.dbo.UserBean;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.User;
import ru.sberbank.uplatform.route.CustomRouteBuilder;

/**
 * @author Pavel Tarasov
 * @since 23/11/2015.
 */
public class DBRoutes extends CustomRouteBuilder {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final Integer REVENUE_FAKE_CATEGORY = 20;
    private LinkedBlockingQueue<QueueBean> queue = new LinkedBlockingQueue<>();
    private DataSource dataSource;

    private final static String SQL_GET_DATES = "SELECT "
            + " MAX(create_date) as stopDate, MIN(create_date) as strtDate"
            + " FROM consumptions WHERE ";

    private final static String SQL_CONSUMPTION_EXIST = "SELECT EXISTS (SELECT 1 FROM consumptions WHERE user_id = ?)";

    @Override
    public void configure() throws Exception {
        from("direct:init-db")
                .id("init-db")
                .process(ex -> {
                    ex.getContext().startRoute("db-insert");
                    ex.getContext().startRoute("flowers-update");
                });

        from("direct:create-db-record")
                .id("create-db-record")
                .process(ex -> {
                    Message msg = ex.getProperty("message", Message.class);
                    Boolean notAnswered = ex.getProperty("not-answered", Boolean.class);
                    Integer botType = ex.getProperty("bot-type", Integer.class);
                    User user = msg.getFrom();
                    Long chatId = msg.getChat().getId();

                    String command;
                    if (msg.getLocation() != null) {
                        command = "send location";
                    } else if (ex.getProperty("command") != null) {
                        command = ex.getProperty("command", String.class);
                    } else {
                        command = (msg.getText() != null) ? msg.getText() : "";
                    }

                    MetricBean metricBean = new MetricBean(command, (notAnswered == null));
                    UserBean userBean = new UserBean(user.getId(), chatId, user.getFirstName(), user.getLastName(),
                            user.getUsername(), botType, null, null, null);
                    queue.add(new QueueBean(metricBean, userBean));
                });

        from("timer:db-insert?period={{db_insert_interval}}")
                .id("db-insert")
                .process(ex -> insertValues());

        from("timer:flowers-update?period={{flowers_update_interval}}")
                .id("flowers-update")
                .process(ex -> {
                    List<Flower> flowersTemp = new ArrayList<>();
                    List<Map<String, Object>> rows = new JdbcTemplate(dataSource).queryForList("SELECT * FROM flowers");
                    for (Map<String, Object> m : rows) {
                        Flower f = new Flower();
                        f.setLocalId((Integer) m.get("id"));
                        f.setProductId((Integer) m.get("product_id"));
                        f.setName((String) m.get("name"));
                        f.setDescription((String) m.get("description"));
                        f.setPhoto((String) m.get("photo"));
                        f.setPrice((BigDecimal) m.get("price"));
                        flowersTemp.add(f);
                    }

                    Optional<List<Flower>> flowers = Optional.of(flowersTemp);
                    if (flowers.isPresent()) {
                        flowers.get().sort((o1, o2) -> o1.getLocalId().compareTo(o2.getLocalId()));
                    }
                    ex.getIn().setHeader("flowers", flowers);
                })
                .to("direct:update-flowers")
                .log("Flowers updated");

        from("direct:get-metrics-users")
                .id("get-metrics-users")
                .process(ex -> ex.getIn().setHeader("users", getUsersChatsList()));

        from("direct:flower-order-db")
                .id("flower-order-db")
                .process(ex -> {
                    @SuppressWarnings("unchecked")
                    Optional<OrderBean> order = (Optional<OrderBean>) ex.getIn().getHeader("orderBean");
                    order.ifPresent(this::addFlowerOrder);
                });

        from("direct:flower-update-db")
                .id("flower-update-db")
                .process(ex -> {
                    @SuppressWarnings("unchecked")
                    List<Flower> flowers = (List<Flower>) ex.getIn().getHeader("flowers");
                    flowers.forEach(this::updateFlower);
                });

        from(Endpoints.ADD_CONSUMPTION_OR_REVENUE_DB)
                .process(exchange -> {
                    ConsumptionBean consumption = exchange.getIn().getHeader(KEY_CR_BEAN, ConsumptionBean.class);
                    if (!addConsumption(consumption)) {
                        setHeader(exchange, KEY_WRONG_CR_CATEGORY, Boolean.TRUE);
                    }
                });

        from(Endpoints.CHECK_INIT_CONSUMPTION_OR_REVENUE)
                .process(exchange -> {
                    ConsumptionBean consumptionBean = exchange.getIn().getHeader(KEY_CR_BEAN, ConsumptionBean.class);
                    boolean showConsumptionAgreement = !checkFirstConsumptionRequest(consumptionBean.getUserId());
                    setHeader(exchange, KEY_SHOW_CR_AGREEMENT, showConsumptionAgreement);
                })
                .choice()
                .when(header(KEY_SHOW_CR_AGREEMENT).isEqualTo(Boolean.TRUE))
                    .to(Endpoints.SHOW_AGREEMENT_CONSUMPTION_OR_REVENUE.endpoint)
                .otherwise()
                    .to(Endpoints.CONSUMPTION_OR_REVENUE_FINISH_ADDING.endpoint)
                .end();

        from(Endpoints.CHECK_INIT_CONSUMPTION_OR_REVENUE2)
                .process(exchange -> {
                    boolean showConsumptionAgreement = !checkFirstConsumptionRequest(getUserId(exchange));
                    setHeader(exchange, KEY_SHOW_CR_AGREEMENT, showConsumptionAgreement);
                });

        from(Endpoints.GET_CONSUMPTION_OR_REVENUE_DB)
                .process(exchange -> {
                    GetConsumptionRequest getConsumptionRequest = exchange.getIn().getHeader(KEY_CONS_REQ, GetConsumptionRequest.class);
                    getConsumptionOrRevenue(getConsumptionRequest, exchange);
                });

        from(Endpoints.GET_BALANCE_DB).process(this::getBalance);

        from(Endpoints.GET_CHART_DB).process(this::getChartURL);
    }

    public void insertValues() {
        final String checkQuery = "SELECT id FROM users WHERE user_id = ? AND platform = ?";
        final String insertMetric = "INSERT INTO metrics(id, user_id, command_text, answered, created_at) VALUES(default, ?, ?, ?, default)";
        final String insertUser = "INSERT INTO users(id, user_id, chat_id, firstname, lastname, username, platform," +
                " latitude, longitude, gender, created) VALUES(default, ?, ?, ?, ?, ?, ?, ?, ?, ?, default) RETURNING id";
        final String updateUser = "UPDATE users SET chat_id=?, firstname=?, lastname=?, username=? WHERE id=?";

        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        List<QueueBean> records = new ArrayList<>();
        queue.drainTo(records);
        if (!records.isEmpty()) {
            records.forEach(rec -> {
                UserBean user = rec.getUser();
                MetricBean metric = rec.getMetric();
                List<Long> ids = jdbc.queryForList(checkQuery, Long.class, user.getUserId(), user.getPlatform());
                long id;
                if (ids.isEmpty()) {
                    id = jdbc.queryForObject(insertUser, Long.class, user.getInsertValues());
                } else {
                    id = ids.get(0);
                    // TODO temporary solution to fill the data for existing users.
                    if (BotType.FB != user.getPlatform()) {
                        jdbc.update(updateUser, user.getUpdateValues(id));
                    }
                }

                metric.setUserId(id);
                jdbc.update(insertMetric, metric.getValues());
            });
        }
    }

    public List<Long> getUsersChatsList() {
        return new JdbcTemplate(dataSource).queryForList("SELECT chat_id FROM users WHERE platform=1", Long.class);
    }

    private void addFlowerOrder(OrderBean order) {
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        List<Object[]> params = new ArrayList<>();
        params.add(order.getValues());
        jdbc.batchUpdate("INSERT INTO flower_orders VALUES (default, ?, ?, ?, default, default, default, ?, ?)", params);
    }

    private void updateFlower(Flower flower) {
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        jdbc.update("UPDATE flowers SET price = ? WHERE product_id=?", flower.getPrice(), flower.getProductId());
    }

    /**
     * @param userId    ID of the user.
     * @return true - there is at least 1 consumption\revenue record in DB for
     * given user, false - none.
     */
    private boolean checkFirstConsumptionRequest(Long userId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject(SQL_CONSUMPTION_EXIST, Boolean.class, userId);
    }

    /**
     * Add new consumption\revenue to DB. If it`s first consumption\revenue for
     * user - skip adding till agreement accepting.
     *
     * @param consumption
     */
    private boolean addConsumption(ConsumptionBean consumption) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Integer categoryId;
        boolean isFakeCategory = !consumption.getConsumptionMode();
        if (isFakeCategory) {
            categoryId = REVENUE_FAKE_CATEGORY;
        } else {
            String categoryWithoutImage = consumption.getCategory().replaceAll("\\p{So}+", "").trim();
            try {
            categoryId = jdbcTemplate.queryForObject(
                    "SELECT id FROM consumption_categories WHERE category_name = ? AND is_consumption = ?",
                    Integer.class, categoryWithoutImage, consumption.getConsumptionMode());
            } catch(EmptyResultDataAccessException e){
                return false;
            }
        }
        jdbcTemplate.update(
                "INSERT INTO consumptions (user_id, value, purpose, category_id, is_consumption) VALUES (?, ?, ?, ?, ?)",
                consumption.getUserId(), consumption.getValue(), consumption.getPurpose(), categoryId,
                consumption.getConsumptionMode());
        return true;
    }

    private void getBalance(Exchange exchange) {
        Long chatId = getUserId(exchange);
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        BigDecimal balance = jdbc.queryForObject(
                "SELECT "
                        + "COALESCE((SELECT SUM(value) FROM consumptions WHERE user_id = ? AND is_consumption = false), 0)"
                        + " - "
                        + "COALESCE((SELECT SUM(value) FROM consumptions WHERE user_id = ? AND is_consumption = true), 0)",
                BigDecimal.class, chatId, chatId);
        exchange.getIn().setHeader("balance", balance);
    }

    private void getConsumptionOrRevenue(GetConsumptionRequest consumptionRequest, Exchange exchange) {
        Long chatId = getUserId(exchange);
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);

        List<Object> params = new ArrayList<>();
        params.add(chatId);
        params.add(consumptionRequest.isConsumptionMode());

        String functionName = (consumptionRequest.isMaxMode()) ? "max" : "sum";
        String sql = " FROM consumptions WHERE user_id = ? and is_consumption = ?";

        if (consumptionRequest.getFromDate() != null) {
            sql = sql + " and create_date >= ?";
            params.add(consumptionRequest.getFromDate());
        }

        if (consumptionRequest.getToDate() != null) {
            sql = sql + " and create_date < ?";
            params.add(consumptionRequest.getToDate());
        }

        if (consumptionRequest.getPurpose() != null && !consumptionRequest.getPurpose().isEmpty()) {
            sql = sql + " and purpose = ?";
            params.add(consumptionRequest.getPurpose());
        }
        String sqlSum = "SELECT " + functionName + "(value)" + sql;
        String sqlPurposes = "SELECT purpose, SUM(value) AS sum_value " + sql + " GROUP BY purpose ORDER BY purpose";
        String sqlMax = "SELECT DISTINCT purpose " + sql + " and value = ? ORDER BY purpose";

        BigDecimal sum = jdbc.queryForObject(sqlSum, BigDecimal.class, params.toArray());
        if (sum == null) {
            sum = new BigDecimal(0);
        }
        exchange.getIn().setHeader("sum", sum);

        //purpose's list
        List<Map<String, Object>> purposes;
        if (consumptionRequest.isMaxMode()) {
            params.add(sum);
            purposes = jdbc.queryForList(sqlMax, params.toArray());
        } else {
            purposes = jdbc.queryForList(sqlPurposes, params.toArray());
        }
        exchange.getIn().setHeader("purposes", purposes);
    }

    private void getChartURL(Exchange exchange) {
        GetConsumptionRequest getConsumptionRequest = (GetConsumptionRequest) exchange.getIn().getHeader(KEY_CONS_REQ);
        String chartURL;
        if (getConsumptionRequest.isDemoMode()) {
            chartURL = ConsumptionChartURLBuilder.buildURLForDemo(getConsumptionRequest.isConsumptionMode());
        } else {
            JdbcTemplate jdbc = new JdbcTemplate(dataSource);
            Long chatId = getUserId(exchange);
            List<Object> params = new ArrayList<>(Arrays.asList(getConsumptionRequest.isConsumptionMode(),
                    getConsumptionRequest.isConsumptionMode() == null, chatId));

            String conditionFilter = "(is_consumption = ? or (? = true)) AND user_id = ?";
            if (getConsumptionRequest.getPurpose() != null && !getConsumptionRequest.getPurpose().isEmpty()) {
                conditionFilter += " AND purpose = ?";
                params.add(getConsumptionRequest.getPurpose());
            }

            Date startDate, stopDate;
            if (getConsumptionRequest.getFromDate() != null) {
                startDate = getConsumptionRequest.getFromDate();
                if (getConsumptionRequest.getToDate() != null) {
                    stopDate = getConsumptionRequest.getToDate();
                } else {
                    stopDate = new Date();
                }
            } else {
                Map datesMap = jdbc.queryForMap(SQL_GET_DATES + conditionFilter, params.toArray());
                startDate = (Date) datesMap.get("strtDate");
                stopDate = (Date) datesMap.get("stopDate");
            }

            if (startDate == null || stopDate == null) {
                return;
            }

            String startDateStr = DATE_FORMAT.format(startDate);
            String stopDateStr = DATE_FORMAT.format(stopDate);
            String GS_START = "(to_date('" + startDateStr + "','YYYY-MM-DD'))::date";
            String GS_STOP = "(to_date('" + stopDateStr + "','YYYY-MM-DD'))::date";
            String GS_STEP = "'1 day'";

            String sumBody = (getConsumptionRequest.isConsumptionMode() == null)
                    ? " * (CASE WHEN c.is_consumption = true THEN -1 ELSE 1 END)" : "";

            String sql = "SELECT SUM(c.value" + sumBody + ") sumValue, dt.series as createdate" + " FROM consumptions c"
                    + " RIGHT OUTER JOIN (SELECT GENERATE_SERIES(" + GS_START + "," + GS_STOP + " , " + GS_STEP
                    + ")::date AS series" + " ) AS dt on DATE_TRUNC('day', c.create_date) = dt.series"
                    + " AND " + conditionFilter + " GROUP BY dt.series ORDER BY dt.series ASC";

            List<SumPerPeriod> list = jdbc.query(sql, params.toArray(), new SumPerPeriodRowMapper());

            if (list.size() <= 1) {
                return;
            }

            chartURL = ConsumptionChartURLBuilder.buildURL(list, getConsumptionRequest.isConsumptionMode());
        }
        exchange.getIn().setHeader("chartURL", chartURL);
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}