package ru.sberbank.uplatform;

import org.apache.camel.ExchangeProperty;

import ru.sberbank.uplatform.message.Message;

public class IsAgreementAcceptedBean {

    public boolean m(@ExchangeProperty("message") Message msg) {
        return msg.getText() != null && (msg.getText().toLowerCase().equals("✅ продолжить"));
    }

}
