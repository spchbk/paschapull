package ru.sberbank.uplatform.commerce.flowers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.APIError;
import ru.sberbank.uplatform.commerce.flowers.api.BouquetsResponse;
import ru.sberbank.uplatform.commerce.flowers.api.Flower;
import ru.sberbank.uplatform.commerce.flowers.api.FlowerOrder;
import ru.sberbank.uplatform.commerce.flowers.api.OrderResponse;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Flower service which interacts with API is provided of Rus-Buket.
 *
 * @author Pavel Tarasov
 * @since 25/02/2016.
 */
public class ExternalFlowerService {
    private Logger log = LoggerFactory.getLogger(ExternalFlowerService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;
    @Value("${flowers_api_url}")
    private String urlAPI;


    /**
     * Send order for the delivery of the bouquet.
     *
     * @param order   {@link FlowerOrder} order data.
     * @return response value {@link OrderResponse} wrapped on the {@link Optional}.
     */
    public Optional<OrderResponse> sendOrder(FlowerOrder order) {
        HttpPost post = new HttpPost(urlAPI + createURI(order));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), OrderResponse.class));
            }
        } catch (Exception e) {
            log.error("Failed to send flowers order");
            return Optional.empty();
        }
    }

    /**
     * Get list of available flowers for specified city.
     *
     * @param city   city of delivery wrapped on the {@link Optional} or {@link Optional#EMPTY}.
     * @return list of the information by available flowers.
     */
    public List<Flower> getFlowers(Optional<String> city) {
        String uri = "&action=GetPrice";
        if (city.isPresent()) {
            uri += ("&delivery_city=" + city.get());
        }
        HttpGet get = new HttpGet(urlAPI + uri);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                BouquetsResponse resp = gson.fromJson(EntityUtils.toString(response.getEntity()), BouquetsResponse.class);
                return (resp != null) ? resp.getBouquets() : Collections.<Flower>emptyList();
            }
        } catch (Exception ex) {
            log.error("Failed to get list of flowers", ex);
            return Collections.<Flower>emptyList();
        }
    }

    /**
     * Creates order URL parameters.
     *
     * @param order   {@link FlowerOrder} order data.
     * @return URL parameters for specified order.
     */
    private String createURI(FlowerOrder order) {
        StringBuilder uri = new StringBuilder("&action=CreateOrder")
                .append("&product_id=").append(order.getFlower().getProductId())
                .append("&phone=").append(order.getPhone())
                .append("&delivery_phone=").append(order.getReceiverPhone());
        try {
            uri.append("&delivery_address=").append(URLEncoder.encode(order.getReceiverAddress(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("Failed append delivery address", e);
        }

        if (order.getDeliveryCity().isPresent()) {
            uri.append("&delivery_city=").append(order.getDeliveryCity().get());
        }
        if (order.getBillingCity().isPresent()) {
            uri.append("&billing_city=").append(order.getBillingCity().get());
        }
        if (order.getDeliveryDate().isPresent()) {
            uri.append("&delivery_date=").append(new SimpleDateFormat("dd.MM.yyyy").format(order.getDeliveryDate().get()));
        }
        return uri.toString();
    }
}