package ru.sberbank.uplatform.commerce.flowers.api;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * @author Pavel Tarasov
 * @since 24/02/2016.
 */
public class Flower {
    /** Flower id in database. */
    private Integer localId;

    /** Flower id in provider service. */
    @SerializedName("id")
    private Integer productId;

    /** Flower name. */
    @SerializedName("title")
    private String name;

    /** Flower description. */
    @SerializedName("desc")
    private String description;

    /** Link of the flower photo. */
    private String photo;

    /** Flower price. */
    private BigDecimal price;

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flower flower = (Flower) o;

        if (localId != null ? !localId.equals(flower.localId) : flower.localId != null) return false;
        if (productId != null ? !productId.equals(flower.productId) : flower.productId != null) return false;
        if (name != null ? !name.equals(flower.name) : flower.name != null) return false;
        if (description != null ? !description.equals(flower.description) : flower.description != null) return false;
        if (photo != null ? !photo.equals(flower.photo) : flower.photo != null) return false;
        return !(price != null ? !price.equals(flower.price) : flower.price != null);

    }

    @Override
    public int hashCode() {
        int result = localId != null ? localId.hashCode() : 0;
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (photo != null ? photo.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "id=" + localId +
                ", product_id=" + productId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", photo='" + photo + '\'' +
                ", price=" + price +
                '}';
    }
}
