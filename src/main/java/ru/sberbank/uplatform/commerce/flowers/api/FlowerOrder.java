package ru.sberbank.uplatform.commerce.flowers.api;

import java.util.Date;
import java.util.Optional;

/**
 * @author Pavel Tarasov
 * @since 25/02/2016.
 */
public class FlowerOrder {
    private Flower flower;
    private Long userId;
    private String phone;
    private String receiverPhone;
    private String receiverAddress;
    private String deliveryCity;
    private String billingCity;
    private Date deliveryDate;

    public Flower getFlower() {
        return flower;
    }

    public FlowerOrder setFlower(Flower flower) {
        this.flower = flower;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public FlowerOrder setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public FlowerOrder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public FlowerOrder setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
        return this;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public FlowerOrder setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
        return this;
    }

    public Optional<String> getDeliveryCity() {
        return (deliveryCity != null) ? Optional.of(deliveryCity) : Optional.<String>empty();
    }

    public FlowerOrder setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
        return this;
    }

    public Optional<String> getBillingCity() {
        return (billingCity != null) ? Optional.of(billingCity) : Optional.<String>empty();
    }

    public FlowerOrder setBillingCity(String billingCity) {
        this.billingCity = billingCity;
        return this;
    }

    public Optional<Date> getDeliveryDate() {
        return (deliveryDate != null) ? Optional.of(deliveryDate) : Optional.<Date>empty();
    }

    public FlowerOrder setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
        return this;
    }

    @Override
    public String toString() {
        return "FlowerOrder{" +
                "flower=" + flower +
                ", userId=" + userId +
                ", phone='" + phone + '\'' +
                ", receiverPhone='" + receiverPhone + '\'' +
                ", receiverAddress='" + receiverAddress + '\'' +
                ", deliveryCity='" + deliveryCity + '\'' +
                ", billingCity='" + billingCity + '\'' +
                ", deliveryDate=" + deliveryDate +
                '}';
    }
}
