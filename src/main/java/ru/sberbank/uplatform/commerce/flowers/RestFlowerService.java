package ru.sberbank.uplatform.commerce.flowers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.APIError;
import ru.sberbank.uplatform.action.Response;
import ru.sberbank.uplatform.api.FlowerService;
import ru.sberbank.uplatform.api.flower.Invite;
import ru.sberbank.uplatform.api.flower.Recall;
import ru.sberbank.uplatform.dbo.OrderBean;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

/**
 * @author Pavel Tarasov
 * @since 01/03/2016.
 */
public class RestFlowerService implements FlowerService {
    private Logger logger = LoggerFactory.getLogger(RestFlowerService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
    private Type listType = new TypeToken<List<Invite>>(){}.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${flowers_url}")
    private String serviceUrl;


    @Override
    public List<Invite> getInvites() {
        HttpGet get = new HttpGet(serviceUrl + "/invites");
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of invites", ex);
            return null;
        }
    }

    @Override
    public Optional<OrderBean> getLastOrder(long userId) {
        HttpGet get = new HttpGet(serviceUrl + "/lastorder/" + userId);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), OrderBean.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get last order", ex);
            return Optional.<OrderBean>empty();
        }
    }

    @Override
    public Optional<Recall> getRecallByOrderId(String orderId) {
        HttpGet get = new HttpGet(serviceUrl + "/recalls/order/" + orderId);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                Recall recall = gson.fromJson(EntityUtils.toString(response.getEntity()), Recall.class);
                return (recall != null) ? Optional.of(recall) : Optional.<Recall>empty();
            }
        } catch (Exception ex) {
            logger.error("Failed to get recall by order id", ex);
            return Optional.<Recall>empty();
        }
    }

    @Override
    public Optional<Long> addRecall(Recall recall) {
        HttpPost post = new HttpPost(serviceUrl + "/recall");
        post.setHeader("Content-type", "application/json");
        post.setEntity(new StringEntity(new Gson().toJson(recall), Charset.forName("UTF-8")));

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Response.class).getId());
            }
        } catch (Exception ex) {
            logger.error("Failed to add recall", ex);
            return Optional.<Long>empty();
        }
    }

    @Override
    public boolean updateRecall(Recall recall) {
        HttpPut put = new HttpPut(serviceUrl + "/recalls/order/" + recall.getOrder());
        put.setHeader("Content-type", "application/json");
        put.setEntity(new StringEntity(new Gson().toJson(recall), Charset.forName("UTF-8")));

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(put)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Response: " + response);
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to update recall by order id: '" + recall.getOrder() + "'", ex);
            return false;
        }
    }
}
