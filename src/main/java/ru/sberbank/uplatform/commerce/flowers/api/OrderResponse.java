package ru.sberbank.uplatform.commerce.flowers.api;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 25/02/2016.
 */
public class OrderResponse {
    /** Action type (CreateOrder). */
    private String action;

    /** Response status (error: 0, ok: 1). */
    private Integer status;

    @SerializedName("order_id")
    private String orderId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "OrderResponse{" +
                "action='" + action + '\'' +
                ", status=" + status +
                ", orderId='" + orderId + '\'' +
                '}';
    }
}
