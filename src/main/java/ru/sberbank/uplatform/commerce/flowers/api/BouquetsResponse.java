package ru.sberbank.uplatform.commerce.flowers.api;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 31/03/2016.
 */
public class BouquetsResponse {
    /** Action type (GetPrice). */
    private String action;

    /** Response status (error: 0, ok: 1). */
    private Integer status;

    /** List of the bouquets. */
    private List<Flower> bouquets;

    /** Delivery cost. */
    @SerializedName("delivery_cost")
    private Double delivery;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Flower> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<Flower> bouquets) {
        this.bouquets = bouquets;
    }

    public Double getDelivery() {
        return delivery;
    }

    public void setDelivery(Double delivery) {
        this.delivery = delivery;
    }

    @Override
    public String toString() {
        return "BouquetsResponse{" +
                "action='" + action + '\'' +
                ", status=" + status +
                ", bouquets=" + bouquets +
                ", delivery=" + delivery +
                '}';
    }
}
