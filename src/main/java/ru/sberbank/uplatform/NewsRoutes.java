package ru.sberbank.uplatform;

import org.apache.camel.ExchangeProperty;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.api.article.Article;
import ru.sberbank.uplatform.article.RestArticleService;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.reply.Reply;

import java.util.*;

/**
 * @author Tarasov Pavel.
 * @since 13.04.16.
 */
public class NewsRoutes extends RouteBuilder {
    @Autowired
    private RestArticleService service;
    private List<Article> news = new ArrayList<>();
    private Map<Long, Integer> newsCounter = new HashMap<>();
    private List<Long> chats = new ArrayList<>();

    @Override
    public void configure() throws Exception {

        from("direct:push-news")
                .id("push-news")
                .to("direct:update-news")
                .to("direct:get-metrics-users")
                .process(ex -> {
                    String param = ex.getIn().getBody(String.class);
                    if (param != null && !param.isEmpty()) {
                        param = param.replace("user=", "");
                        if (param.equals("all")) {
                            //noinspection unchecked
                            chats = ex.getIn().getHeader("users", List.class);
                        } else {
                            try {
                                chats = Collections.singletonList(Long.parseLong(param));
                            } catch (Exception ignored) {
                            }
                        }
                    }
                    ex.getIn().setHeader("count", chats.size());
                })
                .removeHeader("users")
                .loop(header("count"))
                    .process(ex -> {
                        Thread.sleep(25); // max 40 messages per second
                        int index = ex.getProperty("CamelLoopIndex", Integer.class);
                        long chatId = chats.get(index);
                        ex.getIn().setHeader("chat", chatId);
                        ex.getIn().setHeader("news", news.get(0));
                        ex.setProperty("replies", new ArrayList<Reply>());
                        newsCounter.put(chatId, 0);
                    })
                    .setHeader("keyboard", constant("news_menu"))
                    .setHeader("template", constant("templates/news.vm"))
                    .to("direct:render-template")
                    .to(Endpoints.NOTIFICATION.endpoint)
                .end();

        from("direct:update-news")
                .id("update-news")
                .process(ex -> news = service.getBookPages(RestArticleService.NEWS, 0L, 1000L));

        from(ClientState.NEWS.endpoint)
                .id(ClientState.NEWS.getId())
                .choice()
                .when(method(this, "checkMore"))
                    .choice()
                    .when(method(this, "checkLike"))
                        .setProperty("command", constant("like news"))
                        .process(ex -> ex.getIn().setBody("Спасибо за оценку \uD83D\uDC4D"))
                        .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                    .otherwise()
                        .setProperty("command", constant("more news"))
                    .endChoice()
                    .to("direct:create-db-record")
                    .process(ex -> {
                        long chatId = ex.getProperty("message", Message.class).getChat().getId();
                        int nextNews = (newsCounter.getOrDefault(chatId, 0) + 1);
                        if (nextNews == news.size() - 1) {
                            ex.getIn().setHeader("keyboard", "main_menu");
                            ex.getProperty("client", Client.class).setState(ClientState.IDLE);
                            removeUserCounter(chatId);
                        } else {
                            ex.getIn().setHeader("keyboard", "news_menu");
                            newsCounter.put(chatId, nextNews);
                        }
                        ex.getIn().setHeader("news", news.get(nextNews));
                    })
                    .setHeader("template", constant("templates/news.vm"))
                    .to("direct:render-template")
                    .to(Endpoints.ADD_TEXT_REPLY.endpoint)
                .otherwise()
                    .process(ex -> {
                        ex.getProperty("client", Client.class).setState(ClientState.IDLE);
                        removeUserCounter(ex.getProperty("message", Message.class).getChat().getId());
                    })
                    .to(ClientRoutes.epNewClient)
                .end();
        }

    public boolean checkCancel(@ExchangeProperty("message") Message msg) {
        return (msg.getText() != null && msg.getText().trim().toLowerCase().equals("отмена"));
    }

    public boolean checkMore(@ExchangeProperty("message") Message msg) {
        return (msg.getText() != null && msg.getText().trim().toLowerCase().matches("еще|\uD83D\uDC4D"));
    }

    public boolean checkLike(@ExchangeProperty("message") Message msg) {
        return (msg.getText() != null && msg.getText().trim().toLowerCase().equals("\uD83D\uDC4D"));
    }

    private synchronized void removeUserCounter(long chatId) {
        newsCounter.remove(chatId);
    }
}
