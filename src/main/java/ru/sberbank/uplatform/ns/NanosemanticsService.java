package ru.sberbank.uplatform.ns;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;

import ru.sberbank.uplatform.ns.model.ChatInitRequest;
import ru.sberbank.uplatform.ns.model.ChatRequestRequest;
import ru.sberbank.uplatform.ns.model.ChatResponse;
import ru.sberbank.uplatform.ns.session.ChatSession;
import ru.sberbank.uplatform.ns.session.ISessionHolder;
import ru.sberbank.uplatform.ns.session.InMemorySessionHolder;

/**
 * Interaction with Nanosemantics WEB Service.
 *
 * @author Dmitriy_Gulyaev
 * @see <a href="http://nanosemantics.ru/">Site</a>
 * @since 12 April 2016
 */

public class NanosemanticsService {

    private static final Logger logger = LoggerFactory.getLogger(NanosemanticsService.class);

    /**
     * Inf identifier.
     */
    @Value("${ns_uuid}")
    private String nsUUID;

    /**
     * Public key.
     */
    @Value("${ns_pkey}")
    private String nsPKey;

    /**
     * Scheme and Host of Nanosemantics WEB Service.
     */
    @Value("${ns_scheme_with_host}")
    private String nsSchemeWhHost;

    /**
     * Path to "Chat.init" service.
     */
    @Value("${ns_path_chat_init}")
    private String nsPathChatInit;

    /**
     * Path to "Chat.request" service.
     */
    @Value("${ns_path_chat_rqst}")
    private String nsPathChatRqst;

    private URI uriChatInit;
    private URI uriChatRqst;

    /**
     * Max. session inactivity time, in seconds, with 1 min. reserve.
     */
    private final static long MAX_INACTIVITY_TIME = 1000 * 60 * (30 - 1);

    private Gson gson = new Gson();

    CloseableHttpClient httpClient = HttpClients.createDefault();

    ISessionHolder holder = new InMemorySessionHolder();

    private URI getURIChatInit() {
        if (uriChatInit == null) {
            uriChatInit = URI.create(nsSchemeWhHost + nsPathChatInit);
        }
        return uriChatInit;
    }

    private URI getURIChatRqst() {
        if (uriChatRqst == null) {
            uriChatRqst = URI.create(nsSchemeWhHost + nsPathChatRqst);
        }
        return uriChatRqst;
    }

    /**
     * Ask question to inf.
     *
     * @param userId
     * @param question
     * @return
     */
    public Optional<String> askQuestion(Long userId, String question) {
        try {
            ChatSession chatSession = holder.getSession(userId);
            if (!isValidChatSession(chatSession)) {
                String previousCuid = chatSession == null ? null : chatSession.getCuid();
                String signature = generateNewSignature(previousCuid);
                String cuid = chatInit(signature);
                signature = generateNewSignature(cuid);
                chatSession = new ChatSession(cuid, signature);
                holder.startSession(userId, chatSession);
                logger.debug("Started new session {}", cuid);
            }
            chatSession.setLastAccessTime(System.currentTimeMillis());

            ChatResponse chatResponse = chatRequest(chatSession, question);

            if (isValidInfAnswer(chatResponse)) {
                return Optional.of(chatResponse.getResult().getText().getValue());
            }
        } catch (IOException e) {
            logger.error("Error during invoke of NANOSEMANTICS`s service", e);
        }
        return Optional.empty();
    }

    /**
     * Initialize new chat (session). Send POST JSON request
     * <p>
     * {"uuid": inf-id, "cuid": optional-chat-id, "sign": signature}
     *
     * @param signature
     * @return new chat (session) id.
     * @throws IOException
     */
    private String chatInit(String signature) throws IOException {
        ChatInitRequest chatInitRequest = new ChatInitRequest(nsUUID, signature);
        ChatResponse chatResponse = executeHTTPRequest(chatInitRequest, getURIChatInit(), "Chat.init");
        return chatResponse.getResult().getCuid();
    }

    /**
     * Ask Inf for question. Send POST JSON request
     * <p>
     * { "cuid": chat-id,"text": question, "sign": signature}
     *
     * @param chatSession Previously opened session.
     * @param question
     * @return
     * @throws IOException
     */
    private ChatResponse chatRequest(ChatSession chatSession, String question) throws IOException {
        ChatRequestRequest chatRequestRequest = new ChatRequestRequest(chatSession, question);
        ChatResponse chatResponse = executeHTTPRequest(chatRequestRequest, getURIChatRqst(), "Chat.request");
        return chatResponse;
    }

    private ChatResponse executeHTTPRequest(Object object, URI uri, String opId) throws IOException {
        HttpPost post = new HttpPost(uri);
        post.setEntity(new StringEntity(gson.toJson(object), ContentType.APPLICATION_JSON));
        logger.trace("Invoke of {} with param {}", opId, object);
        try (CloseableHttpResponse response = httpClient.execute(post)) {
            String entity = EntityUtils.toString(response.getEntity());
            logger.trace("Response for {} = {}", opId, entity);
            ChatResponse resp = gson.fromJson(entity, ChatResponse.class);
            return resp;
        }
    }

    /**
     * Generate signature string
     * <p>
     * signature = md5(nsUUID.сuid.nsPKey);
     *
     * @param сuid
     * @return
     */
    private String generateNewSignature(String сuid) {
        StringBuffer stringBuffer = new StringBuffer(128);
        stringBuffer.append(nsUUID);
        stringBuffer.append('.');
        if (сuid != null) {
            stringBuffer.append(сuid);
        }
        stringBuffer.append('.');
        stringBuffer.append(nsPKey);

        return DigestUtils.md5Hex(stringBuffer.toString());
    }

    private boolean isValidChatSession(ChatSession chatSession) {
        if (chatSession == null) {
            return false;
        }
        return System.currentTimeMillis() - chatSession.getLastAccessTime() < MAX_INACTIVITY_TIME;
    }

    /**
     * Check if question to inf was recognized by inf.
     *
     * @param chatResponse
     * @return
     */
    private boolean isValidInfAnswer(ChatResponse chatResponse) {
        return !chatResponse.getResult().getText().getValue().contains("NOTHING");
    }

}
