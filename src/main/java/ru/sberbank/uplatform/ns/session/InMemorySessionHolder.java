package ru.sberbank.uplatform.ns.session;

import java.util.HashMap;
import java.util.Map;

public class InMemorySessionHolder implements ISessionHolder {

	private Map<Long, ChatSession> holder = new HashMap<Long, ChatSession>();

	@Override
	public ChatSession getSession(Long userId) {
		return holder.get(userId);
	}

	@Override
	public void startSession(Long userId, ChatSession chatSession) {
		holder.put(userId, chatSession);
	}


}
