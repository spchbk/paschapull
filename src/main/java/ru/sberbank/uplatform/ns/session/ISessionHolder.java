package ru.sberbank.uplatform.ns.session;

public interface ISessionHolder {

    ChatSession getSession(Long userId);

    void startSession(Long userId, ChatSession chatSession);

}
