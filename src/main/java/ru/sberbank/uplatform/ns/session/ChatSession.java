package ru.sberbank.uplatform.ns.session;

public class ChatSession {

	private String cuid;
	private long lastAccessTime;
	private String signature;

	public ChatSession() {

	}

	public ChatSession(String cuid, String signature) {
		this.cuid = cuid;
		this.signature = signature;
	}

	public String getCuid() {
		return cuid;
	}

	public void setCuid(String cuid) {
		this.cuid = cuid;
	}

	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public void setLastAccessTime(long lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return String.format("ChatSession [cuid=%s, lastAccessTime=%s, signature=%s]", cuid, lastAccessTime, signature);
	}

}
