package ru.sberbank.uplatform.ns.model;

public class BaseRequest {
	/** Chat ID, optional. */
	private String cuid;

	/** Signature. */
	protected String sign;

	public String getCuid() {
		return cuid;
	}

	public void setCuid(String cuid) {
		this.cuid = cuid;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return String.format("BaseRequest [cuid=%s, sign=%s]", cuid, sign);
	}

}
