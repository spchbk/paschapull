package ru.sberbank.uplatform.ns.model;

/**
 * Request to "Chat.init" service.
 * 
 * @author Dmitriy_Gulyaev
 *
 */

public class ChatInitRequest extends BaseRequest {

	/** Inf ID. */
	private String uuid;

	public ChatInitRequest() {

	}

	public ChatInitRequest(String uuid, String sign) {
		this.uuid = uuid;
		this.sign = sign;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
