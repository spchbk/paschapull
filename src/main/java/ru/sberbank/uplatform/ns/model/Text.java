package ru.sberbank.uplatform.ns.model;

public class Text {
    private Integer delay;
    private Boolean showRate;
    private Integer status;
    private String value;

    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }

    public Boolean getShowRate() {
        return showRate;
    }

    public void setShowRate(Boolean showRate) {
        this.showRate = showRate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("Text [delay=%s, showRate=%s, status=%s, value=%s]", delay, showRate, status, value);
    }

}
