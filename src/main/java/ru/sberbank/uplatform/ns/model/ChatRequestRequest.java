package ru.sberbank.uplatform.ns.model;

import ru.sberbank.uplatform.ns.session.ChatSession;

/**
 * Request to "Chat.request" service.
 * 
 * @author Dmitriy_Gulyaev
 *
 */
public class ChatRequestRequest extends BaseRequest {
	/** Question text. */
	private String text;

	public ChatRequestRequest() {

	}

	public ChatRequestRequest(ChatSession chatSession, String question) {
		this.setCuid(chatSession.getCuid());
		this.setSign(chatSession.getSignature());
		this.setText(question);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return String.format("ChatRequestRequest [text=%s]", text);
	}

}
