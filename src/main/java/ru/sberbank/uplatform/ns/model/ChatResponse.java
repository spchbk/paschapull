package ru.sberbank.uplatform.ns.model;

/**
 * Represent response from Nanosemantics WEB Service.
 * @author Dmitriy_Gulyaev
 *
 */
public class ChatResponse {
	private String id;
	private BaseResult result;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BaseResult getResult() {
		return result;
	}

	public void setResult(BaseResult result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return String.format("ChatResponse [id=%s, result=%s]", id, result);
	}

}
