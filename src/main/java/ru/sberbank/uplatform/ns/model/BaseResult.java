package ru.sberbank.uplatform.ns.model;

/**
 * Represent answer from Nanosemantics for different types of requests.
 *
 * @author Dmitriy_Gulyaev
 */
public class BaseResult {
    private String cuid;
    private Text text;
    private String token;

    public String getCuid() {
        return cuid;
    }

    public void setCuid(String cuid) {
        this.cuid = cuid;
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return String.format("BaseResult [cuid=%s, text=%s, token=%s]", cuid, text, token);
    }

}
