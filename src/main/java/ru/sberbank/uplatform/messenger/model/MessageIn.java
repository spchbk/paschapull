package ru.sberbank.uplatform.messenger.model;

import java.util.Arrays;

public class MessageIn extends Message {
    private Attachment[] attachments;

    public Attachment[] getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachment[] attachments) {
        this.attachments = attachments;
    }

    @Override
    public String toString() {
        return String.format("MessageIn [attachments=%s]", Arrays.toString(attachments));
    }
}
