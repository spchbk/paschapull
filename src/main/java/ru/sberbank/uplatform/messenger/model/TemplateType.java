package ru.sberbank.uplatform.messenger.model;

public enum TemplateType {
    generic, button, receipt
}
