package ru.sberbank.uplatform.messenger.model;

public class Button {

    private ButtonType type;
    private String title;
    private String url;
    private String payload;

    public ButtonType getType() {
        return type;
    }

    public void setType(ButtonType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return String.format("Button [type=%s, title=%s, url=%s, payload=%s]", type, title, url, payload);
    }

}
