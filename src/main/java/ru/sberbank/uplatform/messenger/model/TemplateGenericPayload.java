package ru.sberbank.uplatform.messenger.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TemplateGenericPayload extends Payload {

    @SerializedName("template_type")
    private TemplateType templateType = TemplateType.generic;
    private List<Bubble> elements;

    public TemplateType getTemplateType() {
        return templateType;
    }

    public void setTemplateType(TemplateType templateType) {
        this.templateType = templateType;
    }

    public List<Bubble> getElements() {
        if (elements == null) {
            elements = new ArrayList<Bubble>();
        }
        return elements;
    }

    public void setElements(List<Bubble> elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return String.format("TemplateGenericPayload [templateType=%s, elements=%s]", templateType, elements);
    }

}
