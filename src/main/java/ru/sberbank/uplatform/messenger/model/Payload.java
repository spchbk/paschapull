package ru.sberbank.uplatform.messenger.model;

public class Payload {

    // TODO Правильно ли хранить координаты здесь?
    private Coordinates coordinates;

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return String.format("Payload [coordinates=%s]", coordinates);
    }
}
