package ru.sberbank.uplatform.messenger.model;

import com.google.gson.annotations.SerializedName;

public class Response {
    private Recipient recipient;
    private MessageOut message;

    @SerializedName("notification_type")
    private NotificationType notificationType;

    public Response() {

    }

    public Response(long recipientId, String text) {
        this.setRecipient(new Recipient());
        this.getRecipient().setId(recipientId);

        this.setMessage(new MessageOut());
        this.getMessage().setText(text);
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public MessageOut getMessage() {
        return message;
    }

    public void setMessage(MessageOut message) {
        this.message = message;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    @Override
    public String toString() {
        return String.format("Response [recipient=%s, message=%s, notificationType=%s]", recipient, message,
                notificationType);
    }

}
