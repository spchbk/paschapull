package ru.sberbank.uplatform.messenger.model;

import java.util.Arrays;

public class MessagesEvent {

    private String object;
    private Entry[] entry;

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Entry[] getEntry() {
        return entry;
    }

    public void setEntry(Entry[] entry) {
        this.entry = entry;
    }

    @Override
    public String toString() {
        return String.format("MessagesEvent [object=%s, entry=%s]", object, Arrays.toString(entry));
    }
    
}
