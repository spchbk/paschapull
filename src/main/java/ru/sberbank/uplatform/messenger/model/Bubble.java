package ru.sberbank.uplatform.messenger.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Bubble {

    private String title;

    @SerializedName("item_url")
    private String itemUrl;

    @SerializedName("image_url")
    private String imageUrl;

    private String subtitle;

    private List<Button> buttons;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public void setItemUrl(String itemUrl) {
        this.itemUrl = itemUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public List<Button> getButtons() {
        if (buttons == null) {
            buttons = new ArrayList<Button>();
        }
        return buttons;
    }

    public void setButtons(List<Button> buttons) {
        this.buttons = buttons;
    }

    @Override
    public String toString() {
        return String.format("Bubble [title=%s, itemUrl=%s, imageUrl=%s, subtitle=%s, buttons=%s]", title, itemUrl,
                imageUrl, subtitle, buttons);
    }

}
