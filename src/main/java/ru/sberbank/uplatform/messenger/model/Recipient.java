package ru.sberbank.uplatform.messenger.model;

import com.google.gson.annotations.SerializedName;

public class Recipient {
    private Long id;

    @SerializedName("phone_number")
    private String phoneNumber;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return String.format("Recipient [id=%s, phoneNumber=%s]", id, phoneNumber);
    }

}
