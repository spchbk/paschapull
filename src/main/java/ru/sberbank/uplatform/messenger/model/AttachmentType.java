package ru.sberbank.uplatform.messenger.model;

public enum AttachmentType {
    image, template, location, fallback, audio, file
}
