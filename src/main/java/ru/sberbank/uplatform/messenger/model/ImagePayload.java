package ru.sberbank.uplatform.messenger.model;

public class ImagePayload extends Payload {

    private String url;

    public ImagePayload() {

    }

    public ImagePayload(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return String.format("ImagePayload [url=%s]", url);
    }
}
