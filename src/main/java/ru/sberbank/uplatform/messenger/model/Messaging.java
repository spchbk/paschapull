package ru.sberbank.uplatform.messenger.model;

public class Messaging {
    private Sender sender;
    private Recipient recipient;
    private long timestamp;
    private MessageIn message;
    private Postback postback;

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public MessageIn getMessage() {
        return message;
    }

    public void setMessage(MessageIn message) {
        this.message = message;
    }

    public Postback getPostback() {
        return postback;
    }

    public void setPostback(Postback postback) {
        this.postback = postback;
    }

    @Override
    public String toString() {
        return String.format("Messaging [sender=%s, recipient=%s, timestamp=%s, message=%s, postback=%s]", sender,
                recipient, timestamp, message, postback);
    }

}
