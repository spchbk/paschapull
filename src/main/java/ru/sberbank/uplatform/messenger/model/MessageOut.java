package ru.sberbank.uplatform.messenger.model;

public class MessageOut extends Message {
    private Attachment attachment;

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public String toString() {
        return String.format("MessageOut [attachment=%s]", attachment);
    }

}
