package ru.sberbank.uplatform.messenger.model;

import com.google.gson.annotations.SerializedName;

public class SendResponse {
    @SerializedName("recipient_id")
    private Long recipientId;

    @SerializedName("message_id")
    private String messageId;

}
