package ru.sberbank.uplatform.messenger.model;

public class Postback {
    private String payload;

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
