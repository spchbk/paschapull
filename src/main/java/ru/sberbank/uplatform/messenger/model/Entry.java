package ru.sberbank.uplatform.messenger.model;

import java.util.Arrays;

public class Entry {

    private long id;
    private long time;
    private Messaging[] messaging;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Messaging[] getMessaging() {
        return messaging;
    }

    public void setMessaging(Messaging[] messaging) {
        this.messaging = messaging;
    }

    @Override
    public String toString() {
        return String.format("Entry [id=%s, time=%s, messaging=%s]", id, time, Arrays.toString(messaging));
    }
    
}
