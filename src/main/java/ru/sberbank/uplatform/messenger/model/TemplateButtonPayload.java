package ru.sberbank.uplatform.messenger.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TemplateButtonPayload extends Payload {

    @SerializedName("template_type")
    private TemplateType templateType = TemplateType.button;
    private String text;
    private List<Button> buttons;

    public TemplateType getTemplateType() {
        return templateType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Button> getButtons() {
        if (buttons == null) {
            buttons = new ArrayList<Button>();
        }
        return buttons;
    }

    public void setButtons(List<Button> buttons) {
        this.buttons = buttons;
    }

    @Override
    public String toString() {
        return String.format("TemplateButtonPayload [templateType=%s, text=%s, buttons=%s]", templateType, text,
                buttons);
    }

}
