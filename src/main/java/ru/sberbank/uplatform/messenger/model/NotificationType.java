package ru.sberbank.uplatform.messenger.model;

public enum NotificationType {
    REGULAR, SILENT_PUSH, NO_PUSH
}
