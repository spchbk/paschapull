package ru.sberbank.uplatform.messenger.model;

public class Message {
    private String mid;
    private Long seq;
    private String text;

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return String.format("Message [mid=%s, seq=%s, text=%s]", mid, seq, text);
    }

}
