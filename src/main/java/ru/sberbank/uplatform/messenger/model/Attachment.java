package ru.sberbank.uplatform.messenger.model;

public class Attachment {
    private String title;
    private String url;
    private AttachmentType type;
    private Payload payload;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public AttachmentType getType() {
        return type;
    }

    public void setType(AttachmentType type) {
        this.type = type;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return String.format("Attachment [title=%s, url=%s, type=%s, payload=%s]", title, url, type, payload);
    }

}
