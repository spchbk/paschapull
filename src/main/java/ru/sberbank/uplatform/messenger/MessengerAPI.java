package ru.sberbank.uplatform.messenger;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;

import ru.sberbank.uplatform.message.Update;
import ru.sberbank.uplatform.message.keyboard.KeyboardButton;
import ru.sberbank.uplatform.message.keyboard.ReplyKeyboardMarkup;
import ru.sberbank.uplatform.message.reply.*;
import ru.sberbank.uplatform.messenger.model.Attachment;
import ru.sberbank.uplatform.messenger.model.AttachmentType;
import ru.sberbank.uplatform.messenger.model.Bubble;
import ru.sberbank.uplatform.messenger.model.Button;
import ru.sberbank.uplatform.messenger.model.ButtonType;
import ru.sberbank.uplatform.messenger.model.ImagePayload;
import ru.sberbank.uplatform.messenger.model.MessageOut;
import ru.sberbank.uplatform.messenger.model.Response;
import ru.sberbank.uplatform.messenger.model.TemplateButtonPayload;
import ru.sberbank.uplatform.messenger.model.TemplateGenericPayload;
import ru.sberbank.uplatform.telegram.UpdateList;

/**
 * Interaction with Messenger Platform API.
 *
 * @author Dmitriy_Gulyaev
 * @see <a href="https://developers.facebook.com/docs/messenger-platform">Site
 *      </a>
 * @since 10 May 2016
 */

public class MessengerAPI {
    private static final Logger logger = LoggerFactory.getLogger(MessengerAPI.class);

    private final static int MESSENGER_MAX_TEXT_LENGTH = 320;
    private final static int MESSENGER_MAX_BUTTONS = 3;
    private final static int MESSENGER_MAX_BUBBLES = 10;

    @Value("${messenger_tx_api_url}")
    private String messengerTxApiUrl;

    @Value("${messenger_access_token}")
    private String messengerAccessToken;

    @Value("${messenger_location_url}")
    private String messengerLocationUrl;

    @Value("${messenger_disable}")
    private String messengerDisable;

    private Gson gson = new Gson();

    /** Pool of the client-side HTTP connections. */
    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    private URI messengerTxApiUri;

    private URI getMessengerTxApiUri() {
        if (messengerTxApiUri == null) {
            messengerTxApiUri = URI.create(messengerTxApiUrl + "?access_token=" + messengerAccessToken);
        }
        return messengerTxApiUri;
    }

    public List<Update> jsonToUpdate(Object json) {
        List<Update> result = gson.fromJson(json.toString(), UpdateList.class).getResult();
        logger.info("Got {} messages.", result.size());
        return result;
    }

    public void send(Reply message) {
        try {
            if (message instanceof TextReply) {
                sendTextReply((TextReply) message);
            } else if (message instanceof LocationReply) {
                sendLocationReply((LocationReply) message);
            } else if (message instanceof PhotoReply) {
                sendPhotoReply((PhotoReply) message);
            } else if (message instanceof FileReply) {

            } else if (message instanceof ChatActionReply) {

            }
        } catch (Exception ex) {
            logger.error("Failed send message", ex);
        }
    }

    private Response createButtonTemplateResponse(String text, long userId, ReplyKeyboardMarkup replyKeyboardMarkup) {
        Response response = new Response(userId, null);
        Attachment attachment = new Attachment();
        attachment.setType(AttachmentType.template);

        TemplateButtonPayload templateButtonPayload = new TemplateButtonPayload();
        templateButtonPayload.setText(text);

        for (List<KeyboardButton> list : replyKeyboardMarkup.getKeyboard()) {
            for (KeyboardButton keyboardButton : list) {
                Button button = createButton(keyboardButton.getText());
                templateButtonPayload.getButtons().add(button);
            }
        }

        attachment.setPayload(templateButtonPayload);
        MessageOut message = new MessageOut();
        message.setAttachment(attachment);
        response.setMessage(message);
        return response;
    }

    private Response createGenericTemplateResponse(long userId, ReplyKeyboardMarkup replyKeyboardMarkup) {
        Response response = new Response(userId, null);
        Attachment attachment = new Attachment();
        attachment.setType(AttachmentType.template);

        TemplateGenericPayload templateGenericPayload = new TemplateGenericPayload();
        Bubble bubble = null;

        int buttonsCounter = 0;
        for (List<KeyboardButton> list : replyKeyboardMarkup.getKeyboard()) {
            for (KeyboardButton keyboardButton : list) {
                if (buttonsCounter < (MESSENGER_MAX_BUBBLES * MESSENGER_MAX_BUTTONS)) {
                    Button button = createButton(keyboardButton.getText());
                    if (button == null) {
                        continue;
                    }

                    if (buttonsCounter % MESSENGER_MAX_BUTTONS == 0) {
                        bubble = new Bubble();
                        templateGenericPayload.getElements().add(bubble);
                    }

                    bubble.setTitle("Меню");
                    bubble.getButtons().add(button);
                    buttonsCounter++;
                }

            }
        }

        attachment.setPayload(templateGenericPayload);
        MessageOut message = new MessageOut();
        message.setAttachment(attachment);
        response.setMessage(message);
        return response;
    }

    private Button createButton(String text) {

        switch (text) {
        case "📹 Видео":
            return null;
        case "😀":
            text = "Советы и факты";
            break;
        case "⭐":
            text = "Акции";
            break;
        case "📞":
            text = "Контакты";
            break;
        case "📊":
            text = "Доходы/Расходы";
            break;

        default:
            break;
        }

        Button button = new Button();
        button.setTitle(text);
        button.setType(ButtonType.postback);
        button.setPayload(text);
        return button;
    }

    private void sendReply(String text, long userId) throws IOException {
        sendReply(text, userId, null);
    }

    private void sendReply(String text, long userId, ReplyKeyboardMarkup replyKeyboardMarkup) throws IOException {
        if (replyKeyboardMarkup == null) {
            sendResponse(new Response(userId, text));
        } else {
            int numOfButtons = getNumOfButtons(replyKeyboardMarkup);
            if (numOfButtons <= MESSENGER_MAX_BUTTONS) {
                sendResponse(createButtonTemplateResponse(text, userId, replyKeyboardMarkup));
            } else {
                sendResponse(new Response(userId, text));
                sendResponse(createGenericTemplateResponse(userId, replyKeyboardMarkup));
            }
        }
    }

    // https://developers.google.com/maps/documentation/static-maps/intro
    private void sendLocationReply(LocationReply locationReply) throws IOException {

        String url = MessageFormat.format(messengerLocationUrl, locationReply.getLongitude().toString(),
                locationReply.getLatitude().toString());

        logger.debug("Map url: {}", url);

        sendResponse(createImageResponse(locationReply.getChatID(), url));
    }

    private void sendPhotoReply(PhotoReply photoReply) throws IOException {
        if (photoReply.isExternalURL()) {
            sendResponse(createImageResponse(photoReply.getChatID(), photoReply.getEntity()));
        } else {
            Response response = createImageResponse(photoReply.getChatID(), null);

            HttpPost post = new HttpPost(getMessengerTxApiUri());
            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);

            entityBuilder.addBinaryBody("filedata", new File(photoReply.getEntity()));
            entityBuilder.addTextBody("message", gson.toJson(response.getMessage()));
            entityBuilder.addTextBody("recipient", gson.toJson(response.getRecipient()));
            post.setEntity(entityBuilder.build());

            sendPost(post);

            if (photoReply.getReplyMarkup() instanceof ReplyKeyboardMarkup) {
                ReplyKeyboardMarkup keyboard = (ReplyKeyboardMarkup) photoReply.getReplyMarkup();
                sendReply("", photoReply.getChatID(), keyboard);
            }
        }
    }

    private void sendPost(HttpPost post) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager)
                .setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.warn(response.getStatusLine().toString());
                    logger.warn(EntityUtils.toString(response.getEntity()));
                } else {
                    if (logger.isTraceEnabled()) {
                        String responseStr = EntityUtils.toString(response.getEntity());
                        logger.trace("Response = {}", responseStr);
                    }
                }
            }
        }
    }

    private Response createImageResponse(long userId, String url) {
        Response response = new Response(userId, null);
        Attachment attachment = new Attachment();
        attachment.setType(AttachmentType.image);
        attachment.setPayload(new ImagePayload(url));

        MessageOut message = new MessageOut();
        message.setAttachment(attachment);
        response.setMessage(message);
        return response;
    }

    private int getNumOfButtons(ReplyKeyboardMarkup replyKeyboardMarkup) {
        int result = 0;
        for (List<KeyboardButton> list : replyKeyboardMarkup.getKeyboard()) {
            result = result + list.size();
        }
        return result;
    }

    private void sendResponse(Response response) throws IOException {
        HttpPost post = new HttpPost(getMessengerTxApiUri());
        String json = gson.toJson(response);

        if (logger.isTraceEnabled()) {
            logger.trace("Request = {}", json);
        }

        post.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
        sendPost(post);
    }

    private void sendTextReply(TextReply textReply) throws IOException {
        ReplyKeyboardMarkup replyKeyboardMarkup = null;
        if (textReply.getReplyMarkup() instanceof ReplyKeyboardMarkup) {
            replyKeyboardMarkup = (ReplyKeyboardMarkup) textReply.getReplyMarkup();
        }

        String text = removeHTMLTags(textReply.getText());
        if (text.length() > MESSENGER_MAX_TEXT_LENGTH) {
            int beginIndex = 0;
            int lastN = -1;
            int lastS = -1;
            int length = text.length() - 1;
            for (int i = 0; i <= length; i++) {
                if (text.charAt(i) == '\n') {
                    lastN = i;
                }

                if (text.charAt(i) == ' ') {
                    lastS = i;
                }

                if (i - beginIndex >= MESSENGER_MAX_TEXT_LENGTH) {
                    if (lastN > 0) {
                        sendReply(text.substring(beginIndex, lastN), textReply.getChatID());
                        beginIndex = lastN + 1;
                        lastN = -1;
                    } else if (lastS > 0) {
                        sendReply(text.substring(beginIndex, lastS), textReply.getChatID());
                        beginIndex = lastS + 1;
                        lastS = -1;
                    }
                } else if (i == length) {
                    sendReply(text.substring(beginIndex, i + 1), textReply.getChatID(), replyKeyboardMarkup);
                    beginIndex = i;
                }
            }
        } else {
            sendReply(text, textReply.getChatID(), replyKeyboardMarkup);
        }
    }

    private String removeHTMLTags(String text) {
        final Document.OutputSettings outputSettings = new Document.OutputSettings().prettyPrint(false);
        return Jsoup.clean(text, "", Whitelist.none(), outputSettings);
    }

    public boolean isDisabled() {
        return "1".equals(messengerDisable) || "true".equals(messengerDisable);
    }
}