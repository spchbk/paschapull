package ru.sberbank.uplatform.telegram;

import java.util.Optional;

/**
 * An empty result, regardless of the success or failure.
 *
 * @author Pavel Tarasov
 * @since 03/03/2016.
 */
public class EmptyResult<T> implements Result<T> {

    /** {@inheritDoc} */
    @Override
    public Optional<T> getResult() {
        return Optional.<T>empty();
    }
}