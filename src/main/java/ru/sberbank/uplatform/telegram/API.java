package ru.sberbank.uplatform.telegram;

import java.util.List;
import java.util.Optional;

import ru.sberbank.uplatform.message.inline.AnswerCallbackQuery;
import ru.sberbank.uplatform.message.inline.AnswerInlineQuery;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.Update;
import ru.sberbank.uplatform.message.inline.editor.EditMessage;
import ru.sberbank.uplatform.message.reply.Reply;

/**
 * Telegram Bot API methods.
 *
 * @author Baryshnikov Alexander.
 * @since 22.11.15.
 */
public interface API {

    /**
     * Use this method to send reply to user. On success, the sent Message is returned.
     *
     * @param reply    Reply object are common to the all types.
     * @return {@link Message} object wrapped on {@link Optional} if on success
     *         or {@link Optional#EMPTY} otherwise.
     */
    Optional<Message> send(Reply reply);

    /**
     * Use this method to receive incoming updates using long polling.
     *
     * @param limits    the number of updates to be retrieved.
     *                  Values between 1—100 are accepted.
     * @return an Array of {@link Update} objects.
     */
    List<Update> getUpdates(int limits);

    /**
     * Use this method to send answers to an inline query. On success, True is returned.
     * No more than 50 results per query are allowed.
     *
     * @param inlineQueryParams    object which represents inline query parameters.
     * @return {@code true} if method on success,
     *         {@code false} otherwise.
     */
    boolean answerInlineQuery(AnswerInlineQuery inlineQueryParams);

    /**
     * Use this method to send answers to callback queries sent from inline keyboards.
     * The answer will be displayed to the user as a notification at the top of the chat
     * screen or as an alert.
     *
     * @param callbackQueryParams    object which represents callback query parameters.
     * @return {@code true} if method on success,
     *         {@code false} otherwise.
     */
    boolean answerCallbackQuery(AnswerCallbackQuery callbackQueryParams);

    /**
     * This method allow you to change an existing message in the message history instead
     * of sending a new one with a result of an action. This is most useful for messages
     * with inline keyboards using callback queries, but can also help reduce clutter in
     * conversations with regular chat bots.
     *
     * Please note, that it is currently only possible to edit messages without
     * {@code reply_markup} or with inline keyboards.
     *
     * @param messageParams    object which represents edited message parameters.
     * @return {@code true} if method on success,
     *         {@code false} otherwise.
     */
    boolean editMessage(EditMessage messageParams);
}