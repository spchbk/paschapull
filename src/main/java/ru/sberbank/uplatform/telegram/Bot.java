package ru.sberbank.uplatform.telegram;

import com.google.gson.Gson;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.APIError;
import ru.sberbank.uplatform.api.UploadCache;
import ru.sberbank.uplatform.message.inline.AnswerCallbackQuery;
import ru.sberbank.uplatform.message.inline.AnswerInlineQuery;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.Update;
import ru.sberbank.uplatform.message.inline.editor.EditMessage;
import ru.sberbank.uplatform.message.reply.ChatActionReply;
import ru.sberbank.uplatform.message.reply.FileReply;
import ru.sberbank.uplatform.message.reply.Reply;

import java.io.IOException;
import java.util.*;

/**
 * Bot core class implementing Telegram bot API methods.
 *
 * @author Baryshnikov Alexander.
 * @since 22.11.15.
 */
public class Bot implements API {
    private static final Logger logger = LoggerFactory.getLogger(Bot.class);

    /** Telegram Bot API URL. */
    @Value("${api_url}")
    private String apiUrl;

    /** Telegram bot token. */
    @Value("${token}")
    private String token;

    /** Cache of the uploaded files. */
    @Autowired
    private UploadCache fileCache;

    /** Pool of the client-side HTTP connections. */
    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    /** Last update‘s unique identifier. */
    private Integer lastUpdateId = null;

    /** {@inheritDoc} */
    @Override
    public Optional<Message> send(Reply message) {
        try {
            if (message instanceof FileReply) return sendFile((FileReply) message);
            else if (message instanceof ChatActionReply) return sendReply(message, true);
            else if (message != null) return sendReply(message, false);
        } catch (Exception ex) {
            logger.error("Failed send message", ex);
        }
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public synchronized List<Update> getUpdates(int limits) {
        String url = getUrl("getUpdates") + "?limit=" + String.valueOf(limits);
        if (lastUpdateId != null) {
            url += "&offset=" + String.valueOf(lastUpdateId + 1);
        }

        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                List<Update> res = new Gson().fromJson(EntityUtils.toString(response.getEntity()), UpdateList.class).getResult();
                if (!res.isEmpty()) {
                    lastUpdateId = res.stream().mapToInt(Update::getUpdateId).max().getAsInt();
                }

                logger.info("New update id {} - size {}", lastUpdateId, res.size());
                return res;
            }
        } catch (Exception ex) {
            logger.error("Failed get updates", ex);
            return new ArrayList<>();
        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean answerInlineQuery(AnswerInlineQuery inlineQueryParams) {
        try {
            HttpPost post = new HttpPost(getUrl("answerInlineQuery"));
            String json = new Gson().toJson(inlineQueryParams);
            post.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            sendRaw(post, true);
            return true;
        } catch (Exception e) {
            logger.error("Failed to send inline query response", e);
            return false;
        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean answerCallbackQuery(AnswerCallbackQuery callbackQueryParams) {
        try {
            HttpPost post = new HttpPost(getUrl("answerCallbackQuery"));
            String json = new Gson().toJson(callbackQueryParams);
            post.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            sendRaw(post, true);
            return true;
        } catch (Exception e) {
            logger.error("Failed to send callback query response", e);
            return false;
        }
    }

    /** {@inheritDoc} */
    @Override
    public boolean editMessage(EditMessage messageParams) {
        try {
            HttpPost post = new HttpPost(getUrl(messageParams.getMethodName()));
            String json = new Gson().toJson(messageParams);
            post.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
            sendRaw(post, true);
            return true;
        } catch (Exception e) {
            logger.error("Failed to edit message", e);
            return false;
        }
    }

    /** URL builder for Telegram API methods. */
    private String getUrl(String action) {
        return apiUrl + "/bot" + token + "/" + action;
    }

    /**
     * Method for sends files. In this method the presence of the file is checked in the
     * cache. Uses cached file if it found and adds to cache if there is not found.
     *
     * @param reply    {@link FileReply} reply.
     * @return {@link Message} object wrapped on {@link Optional} if on success
     *         {@link Optional#EMPTY} otherwise.
     */
    private Optional<Message> sendFile(FileReply reply) throws Exception {
        boolean cached = false;
        if (reply.isFileBased() && !reply.isExternalURL()) {
            Optional<String> id = fileCache.getUploadedId(reply.getEntity());
            if (id.isPresent()) {
                cached = true;
                logger.info("Used cached file {} -> {}", reply.getEntity(), id.get());
                reply.setFileId(id.get());
            }
        }

        Optional<Message> msg = sendReply(reply, false);
        if (!cached && msg.isPresent() && !reply.isExternalURL()) {
            Message message = msg.get();
            logger.info("Message: {}", message);
            String id = reply.getFileIdFromReply(message);
            fileCache.setUploadId(reply.getEntity(), id);
            logger.info("Added to cached file {} -> {}", reply.getEntity(), id);
        }
        return msg;
    }

    /**
     * Method creates POST request to Telegram Bot API for the all types of the replies.
     *
     * @param reply          {@link Reply} reply object.
     * @param emptyResult    {@code true} if the returned result is {@link EmptyResult},
     *                       {@code false} if the returned result is {@link MessageResult}.
     * @return {@link Message} object wrapped on {@link Optional} if on success
     *         or if the specified parameter {@code emptyResult} is {@code false},
     *         {@link Optional#EMPTY} otherwise.
     */
    private Optional<Message> sendReply(Reply reply, boolean emptyResult) throws Exception {
        HttpPost post = new HttpPost(getUrl(reply.getActionName()));
        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
        entityBuilder.setContentType(ContentType.MULTIPART_FORM_DATA);
        reply.serialize(entityBuilder);
        post.setEntity(entityBuilder.build());
        return sendRaw(post, emptyResult);
    }

    /**
     * Execute the request to Telegram Bot API.
     *
     * @param post           HTTP POST method.
     * @param emptyResult    {@code true} if the returned result is {@link EmptyResult},
     *                       {@code false} if the returned result is {@link MessageResult}.
     * @return {@link Message} object wrapped on {@link Optional} if on success.
     *
     * @throws IOException in case of a problem or the connection was aborted.
     * @throws APIError in case if Telegram API method not success.
     */
    @SuppressWarnings("unchecked")
    private Optional<Message> sendRaw(HttpPost post, boolean emptyResult) throws IOException, APIError {
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                Class clazz = (!emptyResult) ? MessageResult.class : EmptyResult.class;
                return ((Result) new Gson().fromJson(EntityUtils.toString(response.getEntity()), clazz)).getResult();
            }
        }
    }
}