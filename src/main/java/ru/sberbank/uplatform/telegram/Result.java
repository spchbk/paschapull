package ru.sberbank.uplatform.telegram;

import java.util.Optional;

/**
 * Result received from Telegram server if on success.
 *
 * @author Pavel Tarasov
 * @since 03/03/2016.
 */
public interface Result<T> {

    /**
     * Get generic result wrapped on {@link Optional} object.
     *
     * @return result wrapped on {@link Optional} if on success
     *         or {@link Optional#EMPTY} otherwise.
     */
    Optional<T> getResult();
}