package ru.sberbank.uplatform.telegram;

import ru.sberbank.uplatform.message.Message;

import java.util.Optional;

/**
 * Message result received from Telegram server if on success.
 *
 * @author Baryshnikov Alexander.
 * @since 23.11.15.
 */
public class MessageResult implements Result<Message> {

    /** Result message. */
    private Message result;

    /** {@inheritDoc} */
    public Optional<Message> getResult() {
        return (result != null) ? Optional.of(result) : Optional.<Message>empty();
    }

    public void setResult(Message result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "MessageResult{" +
                "result=" + result +
                '}';
    }
}