package ru.sberbank.uplatform.telegram;

import ru.sberbank.uplatform.message.Update;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by baryshnikov on 24.12.15.
 */
public class UpdateList {
    private Boolean ok;
    private List<Update> result;

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public List<Update> getResult() {
        if (result == null)
            result = new ArrayList<>();
        return result;
    }

    public void setResult(List<Update> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "UpdateList{" +
                "ok=" + ok +
                ", result=" + result +
                '}';
    }
}
