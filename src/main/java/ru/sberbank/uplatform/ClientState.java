package ru.sberbank.uplatform;

/**
 * Created by baryshnikov on 18.11.15.
 */
public enum ClientState {
    WANT_ATM("get-nearest-atms"),

    WANT_FILIAL("get-nearest-filials"),

    FLOWERS_INFO("flowers-info"),

    FLOWERS_VIEW("flowers-view"),

    FLOWERS_CHOICE("flowers-choice"),

    FLOWERS_PHONE_RECEIVER("flowers-phone-receiver"),

    FLOWERS_ADDRESS("flowers-address"),

    FLOWERS_BOOKING("flowers-booking-view"),

    FLOWER_AGREE_BOOKING("flowers-agree-booking"),

    FLOWER_RECALL("flowers-recall"),

    FLOWER_RATING("flowers-rating"),

    @Deprecated BALANCE_CONSUMPTION("balance-consumption"),

    @Deprecated BALANCE_REVENUE("balance-revenue"),

    STAT_CONSUMPTION,

    STAT_REVENUE,

    BALANCE_CONSUMPTION_CATEGORY("balance-consumption-category"),

    BALANCE_REVENUE_CATEGORY("balance-revenue-category"),

    ACCEPT_OR_REJECT_CONSUMPTION_AGREEMENT("accept-or-reject-consumption-agreement"),

    CR_ADD_CHECK_AGREEMENT_ACCEPT,

    CR_ADD_STEP_1,

    CR_ADD_STEP_2,

    CR_ADD_STEP_2_REVENUE,

    CR_ADD_STEP_3,

    NEWS("news-view"),

    CALL_CENTER("call-center-main"),

    IDLE("returning-client");

    private String id;

    // TODO private
    public final String endpoint;

    ClientState(String id) {
        this.id = id;
        endpoint = "direct:" + id;
    }

    ClientState() {
        this.id = name();
        endpoint = "direct:" + this.id;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getId() {
        return this.id;
    }

}
