package ru.sberbank.uplatform.route;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.RouteDefinition;

import ru.sberbank.uplatform.Client;
import ru.sberbank.uplatform.ClientState;
import ru.sberbank.uplatform.Endpoints;
import ru.sberbank.uplatform.message.Message;

public abstract class CustomRouteBuilder extends RouteBuilder {

    protected final static String KEY_SHOW_CR_AGREEMENT = "showConsumptionAgreement";
    protected final static String KEY_CONS_REQ = "getConsumptionRequest";
    protected final static String KEY_CONS_MOD = "consumptionMode";
    
    protected final static String KEY_CR_BEAN = "consumptionBean";
    protected final static String KEY_WRONG_SUM_FORMAT = "KEY_WRONG_SUM_FORMAT";
    protected final static String KEY_WRONG_CR_CATEGORY = "KEY_WRONG_CR_CATEGORY";
    
    protected final static String BUTTON_TEXT_BACK = "🔙 Назад";
    protected final static String BUTTON_TEXT_CV_MENU = "📊";

    protected RouteDefinition from(Endpoints ep) {
        return super.from(ep.endpoint).id(ep.id);
    }

    protected RouteDefinition from(ClientState cs) {
        return super.from(cs.getEndpoint()).id(cs.getId());
    }

    protected Processor setKeyboardProcessor(Keyboard keyboard) {
        return new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                setKeyboard(exchange, keyboard);
            }
        };
    }

    protected void setKeyboard(Exchange exchange, Keyboard keyboard) {
        exchange.getIn().setHeader("keyboard", keyboard.name());
    }

    protected Processor setTemplateProcessor(String templateName) {
        return new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                setTemplate(exchange, templateName);
            }
        };
    }

    protected void setTemplate(Exchange exchange, String templateName) {
        exchange.getIn().setHeader("template", "templates/" + templateName + ".vm");
    }

    protected Processor setStateProcessor(ClientState clientState) {
        return new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                setState(exchange, clientState);
            }
        };
    }

    protected void setState(Exchange exchange, ClientState clientState) {
        System.out.println("setState " + clientState);
        exchange.getProperty("client", Client.class).setState(clientState);
    }

    protected Processor setTextProcessor(String text) {
        return new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                setText(exchange, text);
            }
        };
    }

    protected void setText(Exchange exchange, String text) {
        exchange.getIn().setBody(text);
    }

    protected Processor setHeaderProcessor(String name, Object value) {
        return new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                setHeader(exchange, name, value);
            }
        };
    }

    protected void setHeader(Exchange exchange, String name, Object value) {
        exchange.getIn().setHeader(name, value);
    }

    protected Long getUserId(Exchange exchange) {
        return exchange.getProperty("chat_id", Long.class);
    }

    protected String getMessageText(Exchange exchange) {
        Message message = exchange.getProperty("message", Message.class);
        return message.getText();
    }

    protected void setMessageText(Exchange exchange, String text) {
        Message message = exchange.getProperty("message", Message.class);
        message.setText(text);
    }

}
