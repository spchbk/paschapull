package ru.sberbank.uplatform.route;

public enum Keyboard {
    main_menu_full,

    main_menu,

    hide_menu,

    help_menu,

    spasibo_menu,

    vote_menu,

    currencies_menu,

    flowers_menu,

    flowers_main_menu,

    flowers_repeat_menu,

    flowers_agree_menu,

    rating_menu,

    cancel_menu,

    consumption_or_revenue_menu_old,

    consumption_or_revenue_menu,

    consumption_or_revenue_sub_menu,

    consumption_category_menu,

    revenue_category_menu,

    news_menu,

    consumption_agreement
}
