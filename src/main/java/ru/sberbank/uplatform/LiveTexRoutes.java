package ru.sberbank.uplatform;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.gson.GsonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import ru.sberbank.uplatform.message.Message;
import ru.sberbank.uplatform.message.Update;
import ru.sberbank.uplatform.message.reply.LiveTexReply;
import ru.sberbank.uplatform.message.reply.TextReply;
import ru.sberbank.uplatform.telegram.API;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Routes for the LiveTex dialog system.
 *
 * @author Pavel Tarasov
 * @since 12/05/2016.
 */
public class LiveTexRoutes extends RouteBuilder {

    /** Object is result of the {@code getUpdates} method. */
    private class UpdateResult<T> {
        /** Successful flag. */
        private final Boolean ok;

        /** Result list of the {@link Update} objects. */
        private final T result;

        @SerializedName("error_code")
        private final Integer errorCode;

        public UpdateResult(boolean ok, T result, Integer errorCode) {
            this.ok = ok;
            this.result = result;
            this.errorCode = errorCode;
        }
    }

    /** So bad, but a problematically return HTTP response from splitter in Telegram Routes. */
    @Autowired
    private API bot;

    /** Queue of the unrecognized updates from users. */
    private final LinkedBlockingQueue<Update> updatesQueue = new LinkedBlockingQueue<>();

    /** List of the updates, which taken and await response. Key is message id. */
    private final ConcurrentMap<Integer, Update> waitList = new ConcurrentHashMap<>();

    /** Default limit of the updates list. */
    private final int defaultLimit = 100;

    /** Data format of the incoming messages. */
    private final GsonDataFormat replyDataFormat = new GsonDataFormat();

    /** Error response for sendMessage method. */
    private final String errorResult;

    public LiveTexRoutes() {
        replyDataFormat.setUnmarshalType(TextReply.class);
        errorResult = new Gson().toJson(new UpdateResult<>(false, null, 400));
    }

    @Override
    public void configure() throws Exception {
        String[] url = new String[] {""};

        from("direct:call-center")
                .id("call-center")
                .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.CALL_CENTER))
                .setBody(simple("Какой вопрос Вас интересует?"))
                .setHeader("keyboard", constant("hide_menu"))
                .to(Endpoints.ADD_TEXT_REPLY.endpoint);

        from(ClientState.CALL_CENTER.endpoint)
                .id(ClientState.CALL_CENTER.getId())
                .choice()
                .when(method(this, "checkCancel"))
                    .process(ex -> ex.getProperty("client", Client.class).setState(ClientState.IDLE))
                    .to(ClientRoutes.epNewClient)
                .otherwise()
                    .to(Endpoints.LIVE_TEX_ADD.endpoint)
                .end();

        // receive incoming updates using long polling.
        from(Endpoints.LIVE_TEX_GET_UPDATES.endpoint)
                .id(Endpoints.LIVE_TEX_GET_UPDATES.id)
                .process(ex -> {
                    Integer limit = ex.getIn().getHeader("limit", Integer.class);
                    limit = (limit != null && limit <= defaultLimit) ? limit : defaultLimit;

                    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
                    List<Update> updates = new ArrayList<>();
                    updatesQueue.drainTo(updates, limit);
                    updates.forEach(u -> waitList.put(u.getMessage().getId(), u));

                    ex.getOut().setHeader(Exchange.CONTENT_TYPE, "text/html; charset=utf-8");
                    ex.getOut().setBody(new Gson().toJson(new UpdateResult<>(true, updates, null)));
                });

        // send reply to unrecognized user's message.
        from(Endpoints.LIVE_TEX_SEND_MESSAGE.endpoint)
                .id(Endpoints.LIVE_TEX_SEND_MESSAGE.id)
                .unmarshal(replyDataFormat)
                .process(ex -> {
                    ex.getOut().setHeader(Exchange.CONTENT_TYPE, "text/html; charset=utf-8");
                    String body = errorResult;

                    LiveTexReply reply = getReply(ex);
                    if (reply != null) {
                        ex.setProperty("chat_id", reply.getChatID());
                        Optional<Message> message = bot.send(reply);
                        if (message != null && message.isPresent()) {
                            body = new Gson().toJson(new UpdateResult<>(true, message.get(), null));
                        }
                    }

                    ex.getOut().setBody(body);
                });

        // send reply sticker to unrecognized user's message.
        from(Endpoints.LIVE_TEX_SEND_STICKER.endpoint)
                .id(Endpoints.LIVE_TEX_SEND_STICKER.id)
                .to("{{api_url}}/bot{{token}}/sendSticker?bridgeEndpoint=true&throwExceptionOnFailure=false");

        // get file info.
        from(Endpoints.LIVE_TEX_GET_FILE.endpoint)
                .id(Endpoints.LIVE_TEX_GET_FILE.id)
                .to("{{api_url}}/bot{{token}}/getFile?bridgeEndpoint=true&throwExceptionOnFailure=false");

        // download file.
        from(Endpoints.LIVE_TEX_DOWNLOAD_FILE.endpoint)
                .id(Endpoints.LIVE_TEX_DOWNLOAD_FILE.id)
                .process(ex -> {
                    String path = ex.getIn().getHeader("CamelHttpPath", String.class);
                    url[0] = (path != null) ? path : "";
                })
                // todo: Hack. Fix later, ${header.CamelHttpPath} incorrectly processed.
                .to("{{api_url}}/file/bot{{token}}" + url[0] + "?bridgeEndpoint=true&throwExceptionOnFailure=false");

        // add unrecognized message to queue
        from(Endpoints.LIVE_TEX_ADD.endpoint)
                .id(Endpoints.LIVE_TEX_ADD.id)
                .process(ex -> {
                    Update update = new Update();
                    update.setMessage(ex.getProperty("message", Message.class));
                    update.setUpdateId(ex.getProperty("update_id", Integer.class));
                    updatesQueue.add(update);
                });
    }

    /**
     * Formed reply from JSON in request body or query parameters.
     *
     * @param ex    Camel exchange.
     * @return {@link LiveTexReply} reply from LiveTex.
     */
    private LiveTexReply getReply(Exchange ex) {
        LiveTexReply reply = ex.getIn().getBody(LiveTexReply.class);
        if (reply == null) {
            String text = ex.getIn().getHeader("text", String.class);
            Long chatId = ex.getIn().getHeader("chat_id", Long.class);
            if (text != null && !text.isEmpty() && chatId != null) {
                Integer replyToMessageId = ex.getIn().getHeader("reply_to_message_id", Integer.class);

                reply = new LiveTexReply(text);
                reply.setChatID(chatId);
                reply.setReplyToMessageID(replyToMessageId);
                reply.setSerializeMarkup(ex.getIn().getHeader("reply_markup", String.class));
            }
        }

        return reply;
    }

    public boolean checkCancel(@ExchangeProperty("message") Message msg) {
        return (msg.getText() != null && msg.getText().trim().toLowerCase().equals("отмена"));
    }
}