package ru.sberbank.uplatform.instant;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class Answer {
  
  @SerializedName("id")
  private Long id;
  @SerializedName("description")
  private String description;
  @SerializedName("created")
  private Date created;

  
  public Long getId() {
      return this.id;
  }

  public void setId(Long id) {
      this.id = id;
  }
  
  public String getDescription() {
      return this.description;
  }

  public void setDescription(String description) {
      this.description = description;
  }
  
  public Date getCreated() {
      return this.created;
  }

  public void setCreated(Date created) {
      this.created = created;
  }
  
}