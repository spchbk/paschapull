package ru.sberbank.uplatform.instant;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Date;

public class AnswerService{
    private Logger logger = LoggerFactory.getLogger(AnswerService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
    private static final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private Type listType = new TypeToken<List<Answer>>() {
    }.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${knowledge_url}")
    private String serviceUrl;

    /**
    * Create new Answer in remote service by POST request. Usually it creates new record in `answer` table.
    * 
    * @param description  Required field  (usually in database it is description field). Can be updated in future 
    * @return New instance of Answer with all generated fields or empty if something goes wrong (see log)
    */
    public Optional<Answer> create(String description){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("description", description);
        HttpPost post = new HttpPost(serviceUrl + "/answers");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to create answer", ex);
            return Optional.<Answer>empty();
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer: " + EntityUtils.toString(response.getEntity()));
                    return Optional.<Answer>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Answer.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to create answer", ex);
            return Optional.<Answer>empty();
        }
    }

    /**
    * Update Answer by remote service by PUT request. Usually it updated record in `answer` table.
    * 
    * @param keyId Primary key (usually in database it is id field) required for record identification
    * @param description New value of description
    * @return true if records updated, otherwise see log
    */
    public boolean update(Long keyId, String description){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("description", description);
        HttpPut post = new HttpPut(serviceUrl + "/answer"  + "/" + encode(keyId));
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to update answer", ex);
            return false;
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to update answer", ex);
            return false;
        }
    }

    /**
    * Remove Answer by remote service by DELETE request. Usually it removes record from `answer` table.
    * 
    * @param id Primary key (usually in database it is id field) required for record identification
    * @return true if record removed, otherwise see log
    */
    public boolean remove(Long id){
        HttpDelete post = new HttpDelete(serviceUrl + "/answer" + "/" + encode(id));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to delete from answer", ex);
            return false;
        }
    }

    /**
    * Get Answer by remote service by GET request by primary keys. Usually it gets record from `answer` table.
    * 
    * @param id Primary key (usually in database it is id field) required for record identification
    * @return Instance of Answer with fields or empty if something goes wrong (see log)
    */
    public Optional<Answer> get(Long id) {
        return one(serviceUrl + "/answer" + "/" + encode(id));
    }

    /**
    * Get list of Answer from remote service by GET request with limits. Usually it gets record from `answer` table.
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Answer or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Answer> getAnswers(long offset, long limit) {
        return list(serviceUrl + "/answers?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    /**
    * Get list of Answer from remote service by GET request filtered by `id` field. Usually it gets record from `answer` table.
    * @param id Value of `id` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Answer or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Answer> getAnswersById(Long id, long offset, long limit) {
        return list(serviceUrl + "/answers/id/" + encode(id) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Answer from remote service by GET request filtered by `description` field. Usually it gets record from `answer` table.
    * @param description Value of `description` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Answer or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Answer> getAnswersByDescription(String description, long offset, long limit) {
        return list(serviceUrl + "/answers/description/" + encode(description) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Answer from remote service by GET request filtered by `created` field. Usually it gets record from `answer` table.
    * @param created Value of `created` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Answer or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Answer> getAnswersByCreated(Date created, long offset, long limit) {
        return list(serviceUrl + "/answers/created/" + encode(created) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Answer from remote service by GET request from custom view
    * @return List with instances of Answer or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Answer> getAnswersScanner(String pattern) {
        return list(serviceUrl + "/answers/scanner"  + "/" + encode(pattern) );
    }
    /**
    * Get list of Answer from remote service by GET request from custom view
    * @return List with instances of Answer or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Answer> getAnswersCollectionScanner(String collection, String pattern) {
        return list(serviceUrl + "/answers/collection-scanner"  + "/" + encode(collection)  + "/" + encode(pattern) );
    }
    /**
    * Get list of Answer from remote service by GET request from custom view
    * @return List with instances of Answer or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Answer> getAnswersInCollection(Long collectionId) {
        return list(serviceUrl + "/answers/in-collection"  + "/" + encode(collectionId) );
    }
    /**
    * Get list of Answer from remote service by GET request from custom view
    * @return List with instances of Answer or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Answer> getAnswersRandom(Long collectionId) {
        return list(serviceUrl + "/answers/random"  + "/" + encode(collectionId) );
    }

    private List<Answer> list(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code list answer " + EntityUtils.toString(response.getEntity()));
                    return Collections.<Answer>emptyList();
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of answer", ex);
            return Collections.<Answer>emptyList();
        }
    }

    private Optional<Answer> one(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code get answer" + EntityUtils.toString(response.getEntity()));
                    return Optional.<Answer>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Answer.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get Answer", ex);
            return Optional.<Answer>empty();
        }
    }

    //For non-spring systems

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    private String encode(Object value) {
        try {
            String s = String.valueOf(value);
            if(value instanceof Date) {
               s = AnswerService.formatter.format((Date)value);
            }
            return URLEncoder.encode(String.valueOf(value), "UTF-8");
        } catch (Throwable t) {
            return "";
        }
    }
}