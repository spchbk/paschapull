package ru.sberbank.uplatform.instant;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class Message {
  public static final String MimeFileVideo = "file/video";
  public static final String MimeFilePhoto = "file/photo";
  public static final String MimeTextText = "text/text";
  public static final String MimeTextLocation = "text/location";
  
  @SerializedName("id")
  private Long id;
  @SerializedName("answer_id")
  private Long answerId;
  @SerializedName("content")
  private String content;
  @SerializedName("created")
  private Date created;
  @SerializedName("mime")
  private String mime;

  
  public Long getId() {
      return this.id;
  }

  public void setId(Long id) {
      this.id = id;
  }
  
  public Long getAnswerId() {
      return this.answerId;
  }

  public void setAnswerId(Long answerId) {
      this.answerId = answerId;
  }
  
  public String getContent() {
      return this.content;
  }

  public void setContent(String content) {
      this.content = content;
  }
  
  public Date getCreated() {
      return this.created;
  }

  public void setCreated(Date created) {
      this.created = created;
  }
  
  public String getMime() {
      return this.mime;
  }

  public void setMime(String mime) {
      this.mime = mime;
  }
  
}