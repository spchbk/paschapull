package ru.sberbank.uplatform.instant;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Date;

public class AnswerCollectionService{
    private Logger logger = LoggerFactory.getLogger(AnswerCollectionService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
    private static final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private Type listType = new TypeToken<List<AnswerCollection>>() {
    }.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${knowledge_url}")
    private String serviceUrl;

    /**
    * Create new AnswerCollection in remote service by POST request. Usually it creates new record in `answer_collection` table.
    * 
    * @param answerId  Required field  (usually in database it is answer_id field). Can be updated in future 
    * @param collectionId  Required field  (usually in database it is collection_id field).
    * @return New instance of AnswerCollection with all generated fields or empty if something goes wrong (see log)
    */
    public Optional<AnswerCollection> create(Long answerId, Long collectionId){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("answer_id", answerId);
        data.put("collection_id", collectionId);
        HttpPost post = new HttpPost(serviceUrl + "/answer-collections");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to create answer_collection", ex);
            return Optional.<AnswerCollection>empty();
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer_collection: " + EntityUtils.toString(response.getEntity()));
                    return Optional.<AnswerCollection>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), AnswerCollection.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to create answer_collection", ex);
            return Optional.<AnswerCollection>empty();
        }
    }

    /**
    * Update AnswerCollection by remote service by PUT request. Usually it updated record in `answer_collection` table.
    * 
    * @param keyAnswerId Primary key (usually in database it is answer_id field) required for record identification
    * @param keyCollectionId Primary key (usually in database it is collection_id field) required for record identification
    * @param answerId New value of answer_id
    * @return true if records updated, otherwise see log
    */
    public boolean update(Long keyAnswerId, Long keyCollectionId, Long answerId){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("answer_id", answerId);
        HttpPut post = new HttpPut(serviceUrl + "/answer-collection"  + "/" + encode(keyAnswerId)  + "/" + encode(keyCollectionId));
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to update answer_collection", ex);
            return false;
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer_collection: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to update answer_collection", ex);
            return false;
        }
    }

    /**
    * Remove AnswerCollection by remote service by DELETE request. Usually it removes record from `answer_collection` table.
    * 
    * @param answerId Primary key (usually in database it is answer_id field) required for record identification
    * @param collectionId Primary key (usually in database it is collection_id field) required for record identification
    * @return true if record removed, otherwise see log
    */
    public boolean remove(Long answerId, Long collectionId){
        HttpDelete post = new HttpDelete(serviceUrl + "/answer-collection" + "/" + encode(answerId) + "/" + encode(collectionId));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer_collection: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to delete from answer_collection", ex);
            return false;
        }
    }

    /**
    * Get AnswerCollection by remote service by GET request by primary keys. Usually it gets record from `answer_collection` table.
    * 
    * @param answerId Primary key (usually in database it is answer_id field) required for record identification
    * @param collectionId Primary key (usually in database it is collection_id field) required for record identification
    * @return Instance of AnswerCollection with fields or empty if something goes wrong (see log)
    */
    public Optional<AnswerCollection> get(Long answerId, Long collectionId) {
        return one(serviceUrl + "/answer-collection" + "/" + encode(answerId) + "/" + encode(collectionId));
    }

    /**
    * Get list of AnswerCollection from remote service by GET request with limits. Usually it gets record from `answer_collection` table.
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of AnswerCollection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<AnswerCollection> getAnswerCollections(long offset, long limit) {
        return list(serviceUrl + "/answer-collections?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    /**
    * Get list of AnswerCollection from remote service by GET request filtered by `answer_id` field. Usually it gets record from `answer_collection` table.
    * @param answerId Value of `answer_id` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of AnswerCollection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<AnswerCollection> getAnswerCollectionsByAnswerId(Long answerId, long offset, long limit) {
        return list(serviceUrl + "/answer-collections/answer-id/" + encode(answerId) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of AnswerCollection from remote service by GET request filtered by `collection_id` field. Usually it gets record from `answer_collection` table.
    * @param collectionId Value of `collection_id` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of AnswerCollection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<AnswerCollection> getAnswerCollectionsByCollectionId(Long collectionId, long offset, long limit) {
        return list(serviceUrl + "/answer-collections/collection-id/" + encode(collectionId) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    

    private List<AnswerCollection> list(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code list answer_collection " + EntityUtils.toString(response.getEntity()));
                    return Collections.<AnswerCollection>emptyList();
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of answer_collection", ex);
            return Collections.<AnswerCollection>emptyList();
        }
    }

    private Optional<AnswerCollection> one(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code get answer_collection" + EntityUtils.toString(response.getEntity()));
                    return Optional.<AnswerCollection>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), AnswerCollection.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get AnswerCollection", ex);
            return Optional.<AnswerCollection>empty();
        }
    }

    //For non-spring systems

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    private String encode(Object value) {
        try {
            String s = String.valueOf(value);
            if(value instanceof Date) {
               s = AnswerCollectionService.formatter.format((Date)value);
            }
            return URLEncoder.encode(String.valueOf(value), "UTF-8");
        } catch (Throwable t) {
            return "";
        }
    }
}