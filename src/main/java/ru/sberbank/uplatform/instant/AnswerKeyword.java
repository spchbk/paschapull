package ru.sberbank.uplatform.instant;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class AnswerKeyword {
  
  @SerializedName("answer_id")
  private Long answerId;
  @SerializedName("keyword_id")
  private Long keywordId;

  
  public Long getAnswerId() {
      return this.answerId;
  }

  public void setAnswerId(Long answerId) {
      this.answerId = answerId;
  }
  
  public Long getKeywordId() {
      return this.keywordId;
  }

  public void setKeywordId(Long keywordId) {
      this.keywordId = keywordId;
  }
  
}