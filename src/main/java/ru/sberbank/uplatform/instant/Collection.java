package ru.sberbank.uplatform.instant;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class Collection {
  
  @SerializedName("id")
  private Long id;
  @SerializedName("label")
  private String label;
  @SerializedName("enable")
  private Boolean enable;
  @SerializedName("created")
  private Date created;

  
  public Long getId() {
      return this.id;
  }

  public void setId(Long id) {
      this.id = id;
  }
  
  public String getLabel() {
      return this.label;
  }

  public void setLabel(String label) {
      this.label = label;
  }
  
  public Boolean getEnable() {
      return this.enable;
  }

  public void setEnable(Boolean enable) {
      this.enable = enable;
  }
  
  public Date getCreated() {
      return this.created;
  }

  public void setCreated(Date created) {
      this.created = created;
  }
  
}