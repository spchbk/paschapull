package ru.sberbank.uplatform.instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.message.reply.*;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by baryshnikov on 18.01.16.
 */
public class Knowledge {
    private static final long messagesLimit = 100;
    @Autowired
    private AnswerService answerService;

    @Autowired
    private MessageService messageService;

    @Value("${facts_collection_id}")
    private Long factsCollectionId;

    @Value("${advises_collection_id}")
    private Long advisesCollectionId;

    private Random random = new Random();


    /**
     * Search for instant answer.
     * @param query
     * @return Messages in answers (multiple type)
     */
    public List<Message> search(String query) {
        List<Answer> answers = answerService.getAnswersScanner(query);
        if (answers.isEmpty()) return Collections.emptyList();
        return messageService.getMessagesByAnswerId(answers.get(random.nextInt(answers.size())).getId(), 0, messagesLimit);
    }

    /**
     * Get random fact
     * @return Messages in answers (multiple type)
     */
    public List<Message> randomFact() {
        List<Answer> answers = answerService.getAnswersRandom(factsCollectionId);
        if (answers.isEmpty()) return Collections.emptyList();
        return messageService.getMessagesByAnswerId(answers.get(random.nextInt(answers.size())).getId(), 0, messagesLimit);
    }

    /**
     * Get random advise
     * @return Messages in answers (multiple type)
     */
    public List<Message> randomAdvise() {
        List<Answer> answers = answerService.getAnswersRandom(advisesCollectionId);
        if (answers.isEmpty()) return Collections.emptyList();
        return messageService.getMessagesByAnswerId(answers.get(random.nextInt(answers.size())).getId(), 0, messagesLimit);
    }

    public static Reply toReply(Message message, Long chatId) {
        switch (message.getMime()) {
            case Message.MimeFilePhoto: {
                PhotoReply reply = new PhotoReply();
                reply.setFile(message.getContent());
                reply.setChatID(chatId);
                return reply;
            }
            case Message.MimeFileVideo: {
                VideoReply reply = new VideoReply();
                reply.setFile(message.getContent());
                reply.setChatID(chatId);
                return reply;
            }
            case Message.MimeTextLocation: {
                LocationReply reply = new LocationReply();
                String[] latlong = message.getContent().split(",");
                try {
                    reply.setLatitude(Float.parseFloat(latlong[0]));
                    reply.setLongitude(Float.parseFloat(latlong[1]));
                    reply.setChatID(chatId);
                    return reply;
                } catch (Throwable t) {
                    //return new TextReply(message.getContent());
                }
            }
            default: {
                TextReply reply = new TextReply(message.getContent());
                reply.setChatID(chatId);
                return reply;
            }
        }
    }

    public static Reply toReply(Message message) {
        return toReply(message, null);
    }
}
