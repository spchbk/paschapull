package ru.sberbank.uplatform.instant.db;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.Consts;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.APIError;
import ru.sberbank.uplatform.util.Utils;
import ru.sberbank.uplatform.api.InstantAnswer;

import java.util.Optional;

/**
 * Created by baryshnikov on 13.12.15.
 */
public class InstantRestDB implements InstantAnswer {
    static final ContentType TEXT_PLAIN = ContentType.create("text/plain", Consts.UTF_8);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

    @Value("${instant_db_url}")
    private String apiUrl;
    private Logger logger = LoggerFactory.getLogger(InstantRestDB.class);

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Optional<String> search(String query) {
        HttpGet get = new HttpGet(apiUrl + "/search?keyword=" + Utils.encode(query));

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Answer.class).getText());
            }
        } catch (Exception ex) {
            logger.error("Failed get instant answer for " + query);
            return Optional.empty();
        }
    }
}