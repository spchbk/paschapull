package ru.sberbank.uplatform.instant.db;

/**
 * Created by baryshnikov on 13.12.15.
 */
public class Answer {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
