package ru.sberbank.uplatform.instant;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Date;

public class AnswerKeywordService{
    private Logger logger = LoggerFactory.getLogger(AnswerKeywordService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
    private static final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private Type listType = new TypeToken<List<AnswerKeyword>>() {
    }.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${knowledge_url}")
    private String serviceUrl;


    public Optional<AnswerKeyword> create(Long answerId, Long keywordId){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("answer_id", answerId);
        data.put("keyword_id", keywordId);
        HttpPost post = new HttpPost(serviceUrl + "/answer-keywords");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to create answer_keyword", ex);
            return Optional.<AnswerKeyword>empty();
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer_keyword: " + EntityUtils.toString(response.getEntity()));
                    return Optional.<AnswerKeyword>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), AnswerKeyword.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to create answer_keyword", ex);
            return Optional.<AnswerKeyword>empty();
        }
    }

    public boolean update(Long keyAnswerId, Long keyKeywordId, Long answerId){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("answer_id", answerId);
        HttpPut post = new HttpPut(serviceUrl + "/answer-keyword/" + encode(keyAnswerId) + "/" + encode(keyKeywordId) + "/");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to update answer_keyword", ex);
            return false;
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer_keyword: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to update answer_keyword", ex);
            return false;
        }
    }

    public boolean remove(Long AnswerId, Long KeywordId){
        HttpDelete post = new HttpDelete(serviceUrl + "/answer-keyword/" + encode(AnswerId) + "/" + encode(KeywordId) + "/");
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status answer_keyword: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to delete from answer_keyword", ex);
            return false;
        }
    }

    public Optional<AnswerKeyword> get(Long answerId, Long keywordId) {
        return one(serviceUrl + "/answer-keyword/" + encode(answerId) + "/" + encode(keywordId) + "/");
    }

    public List<AnswerKeyword> getAnswerKeywords(long offset, long limit) {
        return list(serviceUrl + "/answer-keywords/?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    public List<AnswerKeyword> getAnswerKeywordsByAnswerId(Long answerId, long offset, long limit) {
        return list(serviceUrl + "/answer-keywords/answer-id/" + encode(answerId) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    public List<AnswerKeyword> getAnswerKeywordsByKeywordId(Long keywordId, long offset, long limit) {
        return list(serviceUrl + "/answer-keywords/keyword-id/" + encode(keywordId) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    

    private List<AnswerKeyword> list(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code list answer_keyword " + EntityUtils.toString(response.getEntity()));
                    return Collections.<AnswerKeyword>emptyList();
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of answer_keyword", ex);
            return Collections.<AnswerKeyword>emptyList();
        }
    }

    private Optional<AnswerKeyword> one(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code get answer_keyword" + EntityUtils.toString(response.getEntity()));
                    return Optional.<AnswerKeyword>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), AnswerKeyword.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get AnswerKeyword", ex);
            return Optional.<AnswerKeyword>empty();
        }
    }

    //For non-spring systems

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    private String encode(Object value) {
        try {
            String s = String.valueOf(value);
            if(value instanceof Date) {
               s = AnswerKeywordService.formatter.format((Date)value);
            }
            return URLEncoder.encode(String.valueOf(value), "UTF-8");
        } catch (Throwable t) {
            return "";
        }
    }
}