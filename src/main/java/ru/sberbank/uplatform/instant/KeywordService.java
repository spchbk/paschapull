package ru.sberbank.uplatform.instant;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Date;

public class KeywordService{
    private Logger logger = LoggerFactory.getLogger(KeywordService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
    private static final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private Type listType = new TypeToken<List<Keyword>>() {
    }.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${knowledge_url}")
    private String serviceUrl;

    /**
    * Create new Keyword in remote service by POST request. Usually it creates new record in `keyword` table.
    * 
    * @param answerId  Required field  (usually in database it is answer_id field).
    * @param pattern  Required field  (usually in database it is pattern field). Can be updated in future 
    * @return New instance of Keyword with all generated fields or empty if something goes wrong (see log)
    */
    public Optional<Keyword> create(Long answerId, String pattern){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("answer_id", answerId);
        data.put("pattern", pattern);
        HttpPost post = new HttpPost(serviceUrl + "/keywords");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to create keyword", ex);
            return Optional.<Keyword>empty();
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status keyword: " + EntityUtils.toString(response.getEntity()));
                    return Optional.<Keyword>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Keyword.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to create keyword", ex);
            return Optional.<Keyword>empty();
        }
    }

    /**
    * Update Keyword by remote service by PUT request. Usually it updated record in `keyword` table.
    * 
    * @param keyId Primary key (usually in database it is id field) required for record identification
    * @param pattern New value of pattern
    * @return true if records updated, otherwise see log
    */
    public boolean update(Long keyId, String pattern){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("pattern", pattern);
        HttpPut post = new HttpPut(serviceUrl + "/keyword"  + "/" + encode(keyId));
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to update keyword", ex);
            return false;
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status keyword: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to update keyword", ex);
            return false;
        }
    }

    /**
    * Remove Keyword by remote service by DELETE request. Usually it removes record from `keyword` table.
    * 
    * @param id Primary key (usually in database it is id field) required for record identification
    * @return true if record removed, otherwise see log
    */
    public boolean remove(Long id){
        HttpDelete post = new HttpDelete(serviceUrl + "/keyword" + "/" + encode(id));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status keyword: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to delete from keyword", ex);
            return false;
        }
    }

    /**
    * Get Keyword by remote service by GET request by primary keys. Usually it gets record from `keyword` table.
    * 
    * @param id Primary key (usually in database it is id field) required for record identification
    * @return Instance of Keyword with fields or empty if something goes wrong (see log)
    */
    public Optional<Keyword> get(Long id) {
        return one(serviceUrl + "/keyword" + "/" + encode(id));
    }

    /**
    * Get list of Keyword from remote service by GET request with limits. Usually it gets record from `keyword` table.
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Keyword or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Keyword> getKeywords(long offset, long limit) {
        return list(serviceUrl + "/keywords?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    /**
    * Get list of Keyword from remote service by GET request filtered by `id` field. Usually it gets record from `keyword` table.
    * @param id Value of `id` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Keyword or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Keyword> getKeywordsById(Long id, long offset, long limit) {
        return list(serviceUrl + "/keywords/id/" + encode(id) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Keyword from remote service by GET request filtered by `answer_id` field. Usually it gets record from `keyword` table.
    * @param answerId Value of `answer_id` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Keyword or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Keyword> getKeywordsByAnswerId(Long answerId, long offset, long limit) {
        return list(serviceUrl + "/keywords/answer-id/" + encode(answerId) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Keyword from remote service by GET request filtered by `pattern` field. Usually it gets record from `keyword` table.
    * @param pattern Value of `pattern` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Keyword or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Keyword> getKeywordsByPattern(String pattern, long offset, long limit) {
        return list(serviceUrl + "/keywords/pattern/" + encode(pattern) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Keyword from remote service by GET request filtered by `created` field. Usually it gets record from `keyword` table.
    * @param created Value of `created` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Keyword or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Keyword> getKeywordsByCreated(Date created, long offset, long limit) {
        return list(serviceUrl + "/keywords/created/" + encode(created) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    

    private List<Keyword> list(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code list keyword " + EntityUtils.toString(response.getEntity()));
                    return Collections.<Keyword>emptyList();
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of keyword", ex);
            return Collections.<Keyword>emptyList();
        }
    }

    private Optional<Keyword> one(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code get keyword" + EntityUtils.toString(response.getEntity()));
                    return Optional.<Keyword>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Keyword.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get Keyword", ex);
            return Optional.<Keyword>empty();
        }
    }

    //For non-spring systems

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    private String encode(Object value) {
        try {
            String s = String.valueOf(value);
            if(value instanceof Date) {
               s = KeywordService.formatter.format((Date)value);
            }
            return URLEncoder.encode(String.valueOf(value), "UTF-8");
        } catch (Throwable t) {
            return "";
        }
    }
}