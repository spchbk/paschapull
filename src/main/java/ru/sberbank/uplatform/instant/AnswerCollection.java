package ru.sberbank.uplatform.instant;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class AnswerCollection {
  
  @SerializedName("answer_id")
  private Long answerId;
  @SerializedName("collection_id")
  private Long collectionId;

  
  public Long getAnswerId() {
      return this.answerId;
  }

  public void setAnswerId(Long answerId) {
      this.answerId = answerId;
  }
  
  public Long getCollectionId() {
      return this.collectionId;
  }

  public void setCollectionId(Long collectionId) {
      this.collectionId = collectionId;
  }
  
}