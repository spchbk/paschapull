package ru.sberbank.uplatform.instant.local;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.api.InstantAnswer;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Created by baryshnikov on 30.11.15.
 */
public class Knowledge implements InstantAnswer {

    @Value("${knowledge_index_file}")
    private String indexFile;

    private List<Answer> answers;

    private static final Logger logger = LoggerFactory.getLogger(Knowledge.class);

    @PostConstruct
    public void build() throws IOException {
        try (FileReader fr = new FileReader(indexFile)) {
            answers = new Gson().fromJson(fr, new TypeToken<List<Answer>>() {
            }.getType());
        }
        answers.forEach((x) -> logger.info("{} in {}", x, new File(indexFile).getParent()));
        for (Answer ans : answers) ans.build(new File(indexFile).getParent());
    }


    @Override
    public Optional<String> search(String query) {
        final String normalized = query.trim().toLowerCase();
        Optional<Answer> ans = answers.stream().filter(x -> x.isMatched(normalized)).findFirst();
        if (ans.isPresent()) return Optional.of(ans.get().getText());
        return Optional.empty();
    }

    public String getIndexFile() {
        return indexFile;
    }

    public void setIndexFile(String indexFile) {
        this.indexFile = indexFile;
    }
}
