package ru.sberbank.uplatform.instant.local;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by baryshnikov on 30.11.15.
 */
public class Answer {
    private String contentFile;
    private List<String> keywords;
    private List<Pattern> compiled;
    private String text;

    public String getContentFile() {
        return contentFile;
    }

    public void setContentFile(String contentFile) {
        this.contentFile = contentFile;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public List<Pattern> getCompiled() {
        return compiled;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void build(String parent) throws IOException {
        if (text == null) {
            text = new String(Files.readAllBytes(Paths.get((parent == null || parent.isEmpty() ? "" : parent + "/") + contentFile)), StandardCharsets.UTF_8);
        }
        compiled = keywords.stream().map(Pattern::compile).collect(Collectors.toList());
    }

    public void setCompiled(List<Pattern> compiled) {
        this.compiled = compiled;
    }

    public boolean isMatched(String query) {
        return getCompiled().stream().anyMatch(x -> x.matcher(query).matches());
    }

    @Override
    public String toString() {
        return "Answer{" +
                "contentFile='" + contentFile + '\'' +
                ", keywords=" + keywords +
                ", compiled=" + compiled +
                ", text='" + text + '\'' +
                '}';
    }
}
