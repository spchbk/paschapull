package ru.sberbank.uplatform.instant;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

public class Keyword {
  
  @SerializedName("id")
  private Long id;
  @SerializedName("answer_id")
  private Long answerId;
  @SerializedName("pattern")
  private String pattern;
  @SerializedName("created")
  private Date created;

  
  public Long getId() {
      return this.id;
  }

  public void setId(Long id) {
      this.id = id;
  }
  
  public Long getAnswerId() {
      return this.answerId;
  }

  public void setAnswerId(Long answerId) {
      this.answerId = answerId;
  }
  
  public String getPattern() {
      return this.pattern;
  }

  public void setPattern(String pattern) {
      this.pattern = pattern;
  }
  
  public Date getCreated() {
      return this.created;
  }

  public void setCreated(Date created) {
      this.created = created;
  }
  
}