package ru.sberbank.uplatform.instant;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Date;

public class MessageService{
    private Logger logger = LoggerFactory.getLogger(MessageService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
    private static final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private Type listType = new TypeToken<List<Message>>() {
    }.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${knowledge_url}")
    private String serviceUrl;

    /**
    * Create new Message in remote service by POST request. Usually it creates new record in `message` table.
    * 
    * @param answerId  Required field  (usually in database it is answer_id field).
    * @param content  Required field  (usually in database it is content field). Can be updated in future 
    * @param mime  Required field  (usually in database it is mime field). Can be updated in future 
    * @return New instance of Message with all generated fields or empty if something goes wrong (see log)
    */
    public Optional<Message> create(Long answerId, String content, String mime){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("answer_id", answerId);
        data.put("content", content);
        data.put("mime", mime);
        HttpPost post = new HttpPost(serviceUrl + "/messages");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to create message", ex);
            return Optional.<Message>empty();
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status message: " + EntityUtils.toString(response.getEntity()));
                    return Optional.<Message>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Message.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to create message", ex);
            return Optional.<Message>empty();
        }
    }

    /**
    * Update Message by remote service by PUT request. Usually it updated record in `message` table.
    * 
    * @param keyId Primary key (usually in database it is id field) required for record identification
    * @param content New value of content
    * @param mime New value of mime
    * @return true if records updated, otherwise see log
    */
    public boolean update(Long keyId, String content, String mime){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("content", content);
        data.put("mime", mime);
        HttpPut post = new HttpPut(serviceUrl + "/message"  + "/" + encode(keyId));
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to update message", ex);
            return false;
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status message: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to update message", ex);
            return false;
        }
    }

    /**
    * Remove Message by remote service by DELETE request. Usually it removes record from `message` table.
    * 
    * @param id Primary key (usually in database it is id field) required for record identification
    * @return true if record removed, otherwise see log
    */
    public boolean remove(Long id){
        HttpDelete post = new HttpDelete(serviceUrl + "/message" + "/" + encode(id));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status message: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to delete from message", ex);
            return false;
        }
    }

    /**
    * Get Message by remote service by GET request by primary keys. Usually it gets record from `message` table.
    * 
    * @param id Primary key (usually in database it is id field) required for record identification
    * @return Instance of Message with fields or empty if something goes wrong (see log)
    */
    public Optional<Message> get(Long id) {
        return one(serviceUrl + "/message" + "/" + encode(id));
    }

    /**
    * Get list of Message from remote service by GET request with limits. Usually it gets record from `message` table.
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Message or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Message> getMessages(long offset, long limit) {
        return list(serviceUrl + "/messages?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    /**
    * Get list of Message from remote service by GET request filtered by `id` field. Usually it gets record from `message` table.
    * @param id Value of `id` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Message or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Message> getMessagesById(Long id, long offset, long limit) {
        return list(serviceUrl + "/messages/id/" + encode(id) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Message from remote service by GET request filtered by `answer_id` field. Usually it gets record from `message` table.
    * @param answerId Value of `answer_id` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Message or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Message> getMessagesByAnswerId(Long answerId, long offset, long limit) {
        return list(serviceUrl + "/messages/answer-id/" + encode(answerId) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Message from remote service by GET request filtered by `content` field. Usually it gets record from `message` table.
    * @param content Value of `content` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Message or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Message> getMessagesByContent(String content, long offset, long limit) {
        return list(serviceUrl + "/messages/content/" + encode(content) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Message from remote service by GET request filtered by `created` field. Usually it gets record from `message` table.
    * @param created Value of `created` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Message or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Message> getMessagesByCreated(Date created, long offset, long limit) {
        return list(serviceUrl + "/messages/created/" + encode(created) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Message from remote service by GET request filtered by `mime` field. Usually it gets record from `message` table.
    * @param mime Value of `mime` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Message or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Message> getMessagesByMime(String mime, long offset, long limit) {
        return list(serviceUrl + "/messages/mime/" + encode(mime) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    

    private List<Message> list(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code list message " + EntityUtils.toString(response.getEntity()));
                    return Collections.<Message>emptyList();
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of message", ex);
            return Collections.<Message>emptyList();
        }
    }

    private Optional<Message> one(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code get message" + EntityUtils.toString(response.getEntity()));
                    return Optional.<Message>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Message.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get Message", ex);
            return Optional.<Message>empty();
        }
    }

    //For non-spring systems

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    private String encode(Object value) {
        try {
            String s = String.valueOf(value);
            if(value instanceof Date) {
               s = MessageService.formatter.format((Date)value);
            }
            return URLEncoder.encode(String.valueOf(value), "UTF-8");
        } catch (Throwable t) {
            return "";
        }
    }
}