package ru.sberbank.uplatform.instant;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Date;

public class CollectionService{
    private Logger logger = LoggerFactory.getLogger(CollectionService.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
    private static final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private Type listType = new TypeToken<List<Collection>>() {
    }.getType();

    @Autowired
    private PoolingHttpClientConnectionManager connectionManager;

    @Value("${knowledge_url}")
    private String serviceUrl;

    /**
    * Create new Collection in remote service by POST request. Usually it creates new record in `collection` table.
    * 
    * @param label  Required field  (usually in database it is label field). Can be updated in future 
    * @param enable  (usually in database it is enable field). Can be updated in future 
    * @return New instance of Collection with all generated fields or empty if something goes wrong (see log)
    */
    public Optional<Collection> create(String label, Boolean enable){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("label", label);
        data.put("enable", enable);
        HttpPost post = new HttpPost(serviceUrl + "/collections");
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to create collection", ex);
            return Optional.<Collection>empty();
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status collection: " + EntityUtils.toString(response.getEntity()));
                    return Optional.<Collection>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Collection.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to create collection", ex);
            return Optional.<Collection>empty();
        }
    }

    /**
    * Update Collection by remote service by PUT request. Usually it updated record in `collection` table.
    * 
    * @param keyId Primary key (usually in database it is id field) required for record identification
    * @param label New value of label
    * @param enable New value of enable
    * @return true if records updated, otherwise see log
    */
    public boolean update(Long keyId, String label, Boolean enable){
        java.util.HashMap<String, Object> data = new java.util.HashMap<>();
        data.put("label", label);
        data.put("enable", enable);
        HttpPut post = new HttpPut(serviceUrl + "/collection"  + "/" + encode(keyId));
        post.setHeader("Content-type", "application/json");
        try {
            post.setEntity(new StringEntity(new Gson().toJson(data)));
        } catch (UnsupportedEncodingException ex) {
            logger.error("Failed to update collection", ex);
            return false;
        }

        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status collection: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to update collection", ex);
            return false;
        }
    }

    /**
    * Remove Collection by remote service by DELETE request. Usually it removes record from `collection` table.
    * 
    * @param id Primary key (usually in database it is id field) required for record identification
    * @return true if record removed, otherwise see log
    */
    public boolean remove(Long id){
        HttpDelete post = new HttpDelete(serviceUrl + "/collection" + "/" + encode(id));
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(post)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK status collection: " + EntityUtils.toString(response.getEntity()));
                    return false;
                }
                return true;
            }
        } catch (Exception ex) {
            logger.error("Failed to delete from collection", ex);
            return false;
        }
    }

    /**
    * Get Collection by remote service by GET request by primary keys. Usually it gets record from `collection` table.
    * 
    * @param id Primary key (usually in database it is id field) required for record identification
    * @return Instance of Collection with fields or empty if something goes wrong (see log)
    */
    public Optional<Collection> get(Long id) {
        return one(serviceUrl + "/collection" + "/" + encode(id));
    }

    /**
    * Get list of Collection from remote service by GET request with limits. Usually it gets record from `collection` table.
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Collection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Collection> getCollections(long offset, long limit) {
        return list(serviceUrl + "/collections?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    /**
    * Get list of Collection from remote service by GET request filtered by `id` field. Usually it gets record from `collection` table.
    * @param id Value of `id` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Collection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Collection> getCollectionsById(Long id, long offset, long limit) {
        return list(serviceUrl + "/collections/id/" + encode(id) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Collection from remote service by GET request filtered by `label` field. Usually it gets record from `collection` table.
    * @param label Value of `label` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Collection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Collection> getCollectionsByLabel(String label, long offset, long limit) {
        return list(serviceUrl + "/collections/label/" + encode(label) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Collection from remote service by GET request filtered by `enable` field. Usually it gets record from `collection` table.
    * @param enable Value of `enable` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Collection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Collection> getCollectionsByEnable(Boolean enable, long offset, long limit) {
        return list(serviceUrl + "/collections/enable/" + encode(enable) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Collection from remote service by GET request filtered by `created` field. Usually it gets record from `collection` table.
    * @param created Value of `created` as filter
    * @param offset Count of skipping records
    * @param limit  Count of returning records
    * @return List with instances of Collection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Collection> getCollectionsByCreated(Date created, long offset, long limit) {
        return list(serviceUrl + "/collections/created/" + encode(created) + "?limit=" + String.valueOf(limit) + "&offset=" + String.valueOf(offset));
    }
    
    /**
    * Get list of Collection from remote service by GET request from custom view
    * @return List with instances of Collection or empty collection (not null!) if something goes wrong (see log)
    */
    public List<Collection> getCollectionsInAnswer(Long answerId) {
        return list(serviceUrl + "/collections/in-answer"  + "/" + encode(answerId) );
    }

    private List<Collection> list(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code list collection " + EntityUtils.toString(response.getEntity()));
                    return Collections.<Collection>emptyList();
                }
                return gson.fromJson(EntityUtils.toString(response.getEntity()), listType);
            }
        } catch (Exception ex) {
            logger.error("Failed to get list of collection", ex);
            return Collections.<Collection>emptyList();
        }
    }

    private Optional<Collection> one(String url) {
        HttpGet get = new HttpGet(url);
        try (CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    logger.error("Non-OK return code get collection" + EntityUtils.toString(response.getEntity()));
                    return Optional.<Collection>empty();
                }
                return Optional.of(gson.fromJson(EntityUtils.toString(response.getEntity()), Collection.class));
            }
        } catch (Exception ex) {
            logger.error("Failed to get Collection", ex);
            return Optional.<Collection>empty();
        }
    }

    //For non-spring systems

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    private String encode(Object value) {
        try {
            String s = String.valueOf(value);
            if(value instanceof Date) {
               s = CollectionService.formatter.format((Date)value);
            }
            return URLEncoder.encode(String.valueOf(value), "UTF-8");
        } catch (Throwable t) {
            return "";
        }
    }
}