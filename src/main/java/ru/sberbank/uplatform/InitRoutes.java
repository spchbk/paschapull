package ru.sberbank.uplatform;

import org.apache.camel.builder.RouteBuilder;

/**
 * Created by baryshnikov on 14.11.15.
 */
public class InitRoutes extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        /*
        Here we initializing required resources.
        Only when all steps are done we start main route - incoming-updates
         */

        from("timer:initializer?repeatCount=1")
                .id("initializer")
                .to("direct:init-db")
                .to("direct:init-quotes")
                .to("direct:init-stock")
                .to("direct:init-cbr")
                .to("direct:init-flowers")
                .to(Endpoints.HELP_INIT.endpoint)
                .to(Endpoints.INLINE_INIT.endpoint)
                .to(ClientRoutes.epInitClient)
                .log("initialization complete")
                .process(exchange -> {
                    exchange.getContext().startRoute("incoming-updates");
                });

    }
}
