package ru.sberbank.uplatform;

import org.apache.axis.message.MessageElement;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.builder.RouteBuilder;
import ru.cbr.web.*;
import ru.sberbank.uplatform.cbr.converter.CbrConverterManager;
import ru.sberbank.uplatform.cbr.data.*;
import ru.sberbank.uplatform.message.Message;

import javax.xml.bind.JAXBContext;
import javax.xml.rpc.ServiceException;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Optional;
import java.util.regex.Matcher;

/**
 * @author Pavel Tarasov
 * @since 18/01/2016.
 */
public class CBRRoutes extends RouteBuilder {
    private DailyInfoSoap service;
    private DayCurrenciesRates currenciesRates;
    private BiCurrencyBucket biCurrencyBucket;
    private BiCurrencyValue biCurrencyValue;
    private InternationalReserve reserves;
    private Double refinancingRate;

    public CBRRoutes() {
        try {
            service = new DailyInfoLocator().getDailyInfoSoap();
        } catch (ServiceException e) {
            log.error("Service construct failed" , e);
        }
    }

    @Override
    public void configure() throws Exception {
        from("direct:init-cbr")
                .id("init-cbr")
                .process(exchange -> exchange.getContext().startRoute("cbr-update-cache"));

        from("timer:cbr-update-cache?period={{cbr_update_cache_interval}}")
                .id("cbr-update-cache")
                .autoStartup(false)
                .to("direct:update-currencies-rates")
                .to("direct:update-bi-currency-bucket")
                .to("direct:update-bi-currency-value")
                .to("direct:update-international-reserve")
                .to("direct:update-refinancing-rate")
                .log("CBR Cache Updated");

        // update daily currencies rates cache.
        from("direct:update-currencies-rates")
                .id("update-currencies-rates")
                .process(exchange -> {
                    try {
                        GetCursOnDateXMLResponseGetCursOnDateXMLResult res = service.getCursOnDateXML(Calendar.getInstance());
                        MessageElement element = res.get_any()[0];
                        currenciesRates = (DayCurrenciesRates) JAXBContext.newInstance(DayCurrenciesRates.class)
                                .createUnmarshaller().unmarshal(element);
                        currenciesRates.setDate(element.getAttribute("OnDate"));
                        CbrConverterManager.getInstance().update(currenciesRates);
                    } catch (Throwable t) {
                        log.error("Failed update currencies rates", t);
                    }
                });

        // update bi-currency bucket structure cache.
        from("direct:update-bi-currency-bucket")
                .id("update-bi-currency-bucket")
                .process(exchange -> {
                    try {
                        BiCurBacketXMLResponseBiCurBacketXMLResult res = service.biCurBacketXML();
                        MessageElement element = res.get_any()[0];
                        biCurrencyBucket = (BiCurrencyBucket) JAXBContext.newInstance(BiCurrencyBucket.class)
                                .createUnmarshaller().unmarshal(element);
                        biCurrencyBucket.getBuckets().sort(Comparator.<BiCurrencyBucket.Bucket>reverseOrder());
                    } catch (Throwable t) {
                        log.error("Failed update bi-currency bucket structure", t);
                    }
                });

        // update bi-currency bucket value cache.
        from("direct:update-bi-currency-value")
                .id("update-bi-currency-value")
                .process(exchange -> {
                    try {
                        Calendar prev = Calendar.getInstance();
                        Calendar next = Calendar.getInstance();
                        prev.add(Calendar.DAY_OF_MONTH, -3);
                        next.add(Calendar.DAY_OF_MONTH, 3);
                        BiCurBaseXMLResponseBiCurBaseXMLResult res = service.biCurBaseXML(prev, next);
                        MessageElement element = res.get_any()[0];
                        biCurrencyValue = (BiCurrencyValue) JAXBContext.newInstance(BiCurrencyValue.class)
                                .createUnmarshaller().unmarshal(element);
                        biCurrencyValue.getValues().sort(Comparator.<BiCurrencyValue.BiValue>reverseOrder());
                    } catch (Throwable t) {
                        log.error("Failed update bi-currency bucket value", t);
                    }
                });

        // update international reserves cache.
        from("direct:update-international-reserve")
                .id("update-international-reserve")
                .process(exchange -> {
                    try {
                        Calendar prev = Calendar.getInstance();
                        Calendar next = Calendar.getInstance();
                        prev.add(Calendar.MONTH, -2);
                        next.add(Calendar.MONTH, 2);
                        MrrfXMLResponseMrrfXMLResult res = service.mrrfXML(prev, next);
                        MessageElement element = res.get_any()[0];
                        reserves = (InternationalReserve) JAXBContext.newInstance(InternationalReserve.class)
                                .createUnmarshaller().unmarshal(element);
                        reserves.getReserves().sort(Comparator.<InternationalReserve.ReserveInfo>reverseOrder());
                    } catch (Throwable t) {
                        log.error("Failed update international reserve cache", t);
                    }
                });

        // update refinancing rate cache.
        from("direct:update-refinancing-rate")
                .id("update-refinancing-rate")
                .process(exchange -> {
                    try {
                        MainInfoXMLResponseMainInfoXMLResult res = service.mainInfoXML();
                        MessageElement element = res.get_any()[0];
                        String value = element.getElementsByTagName("stavka_ref").item(0).getFirstChild().getNodeValue();
                        refinancingRate = Double.valueOf(value);
                    } catch (Throwable t) {
                        log.error("Failed update refinancing rate cache", t);
                    }
                });

        // get daily currencies rates.
        from("direct:get-cbr-currencies-rates")
                .id("get-cbr-currencies-rates")
                .process(exchange -> {
                    exchange.getIn().setBody(currenciesRates);
                    exchange.getIn().setHeader("rates", currenciesRates);
                });

        // get bi-currency bucket info.
        from("direct:get-cbr-bi-currency-bucket")
                .id("get-cbr-bi-currency-bucket")
                .process(exchange -> {
                    if (biCurrencyBucket != null && !biCurrencyBucket.getBuckets().isEmpty() &&
                            biCurrencyValue != null && !biCurrencyValue.getValues().isEmpty()) {
                        BiCurrencyBucket.Bucket bucket = biCurrencyBucket.getBuckets().get(0);
                        BiCurrencyValue.BiValue biValue = biCurrencyValue.getValues().get(0);
                        exchange.getIn().setBody(bucket);
                        exchange.getIn().setBody(biValue);
                        exchange.getIn().setHeader("bucket", bucket);
                        exchange.getIn().setHeader("bivalue", biValue);
                    }
                });

        // currency exchanger
        from("direct:cbr-request-exchange-currency")
                .id("cbr-request-exchange-currency")
                .setBody(method(this, "hasCurrencyExchangeRequest"))
                .to("log:cbr-currency?showHeaders=true");

        // get international reserves
        from("direct:get-cbr-international-reserves")
                .id("get-cbr-international-reserves")
                .process(exchange -> {
                    if (reserves != null && !reserves.getReserves().isEmpty()) {
                        exchange.getIn().setBody(reserves.getReserves().get(0));
                        exchange.getIn().setHeader("reserves", reserves.getReserves().get(0));
                    }
                });

        from("direct:get-cbr-refinancing-rate")
                .id("get-cbr-refinancing-rate")
                .process(exchange -> {
                    exchange.getIn().setBody(refinancingRate);
                    exchange.getIn().setHeader("refinancingRate", refinancingRate);
                });
    }

    public boolean hasCurrencyExchangeRequest(@ExchangeProperty("message") Message msg, Exchange exchange) {
        if (msg == null || msg.getText() == null) {
            return false;
        }

        Matcher matcher = CbrConverterManager.getInstance().getPattern().matcher(msg.getText().trim().replace(" ", "").toLowerCase());
        if (!matcher.find() || matcher.groupCount() < 2) {
            Optional<CurrenciesRates> currency = CbrConverterManager.getInstance().findCurrency(msg.getText().trim().toLowerCase());
            if (!currency.isPresent()) {
                return false;
            }
            log.info(">> value:" + currency.get().getNominal());
            log.info("CurRates: " + currency.get());
            exchange.getIn().setHeader("value", Double.valueOf(currency.get().getNominal()));
            exchange.getIn().setHeader("currency", currency.get());
            exchange.getIn().setHeader("amount", currency.get().getRate());
            return true;
        }

        double value;
        try {
            value = Double.valueOf(matcher.group(1).replace(",", "."));
        } catch (NumberFormatException ignored) {
            return false;
        }

        Optional<CurrenciesRates> currency = CbrConverterManager.getInstance().findCurrency(matcher.group(3).trim());
        if (!currency.isPresent()) {
            return false;
        }

        exchange.getIn().setHeader("value", value);
        exchange.getIn().setHeader("currency", currency.get());
        exchange.getIn().setHeader("amount", value * currency.get().getRate() / currency.get().getNominal());
        return true;
    }
}
