package ru.sberbank.uplatform;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;

/**
 * @author Pavel Tarasov
 * @since 23/05/2016.
 */
public class BotRestAPI extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        restConfiguration().component("jetty")
                .host("127.0.0.1")
                .port(8080)
                .bindingMode(RestBindingMode.auto);

        rest()
                .get("/pushNews").to("direct:push-news")
                .post("/pushNews").to("direct:push-news");

        rest("/bot")
                .get("/getUpdates").to(Endpoints.LIVE_TEX_GET_UPDATES.endpoint)
                .get("/getFile").to(Endpoints.LIVE_TEX_GET_FILE.endpoint)
                .get("/sendMessage").to(Endpoints.LIVE_TEX_SEND_MESSAGE.endpoint)
                .post("/sendMessage").to(Endpoints.LIVE_TEX_SEND_MESSAGE.endpoint)
                .get("/sendSticker").to(Endpoints.LIVE_TEX_SEND_STICKER.endpoint)
                .post("/sendSticker").to(Endpoints.LIVE_TEX_SEND_STICKER.endpoint)
                .get("/file").to(Endpoints.LIVE_TEX_DOWNLOAD_FILE.endpoint);

        from("jetty:http://127.0.0.1:8088/file/bot?matchOnUriPrefix=true")
                .to(Endpoints.LIVE_TEX_DOWNLOAD_FILE.endpoint);
    }
}
