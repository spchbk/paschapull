package ru.sberbank.uplatform;

/**
 * Created by baryshnikov on 19.11.15.
 */
public class From {
    public final String type;
    public final String id;
    public final String endpoint;

    public From(String type, String id) {
        this.type = type;
        this.id = id;
        this.endpoint = type + ":" + id;
    }

    @Override
    public String toString() {
        return "From{" +
                "type='" + type + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
