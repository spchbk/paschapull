package ru.sberbank.uplatform;

import ru.sberbank.uplatform.sbol.api.Quote;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Thread-safe (with RW lock) currency holder and converter
 * Created by baryshnikov on 28.11.15.
 */
public class CurrencyManager {
    private Map<Currency, Quote> currency = new HashMap<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Currency base;

    public CurrencyManager(Currency base) {
        this.base = base;
        currency.put(base, new Quote(1.0, 1.0));
    }

    /**
     * Create Currency manager with RUB as base
     */
    public CurrencyManager() {
        this(Currency.Rub);
    }

    /**
     * Set price for foreign currency in base currency units
     * If foreign currency id is same as base and value is not 0 RuntimeException will be thrown
     *
     * @param name  Foreign currency.
     * @param value Price for foreign currency in base units
     */
    public void setPrice(Currency name, Quote value) {
        if (name == base && (value.getBuy() != 1.0d || value.getSell() != 1.0))
            throw new RuntimeException("Can not set price for base currency " + base.name() + " to non one: " + value);
        Lock lck = lock.writeLock();
        lck.lock();
        try {
            currency.put(name, value);
        } finally {
            lck.unlock();
        }
    }

    /**
     * Find price for currency by currency id
     *
     * @param name Foreign or base currency id
     */
    public Optional<Quote> priceFor(Currency name) {
        Lock lck = lock.readLock();
        lck.lock();
        try {
            Quote v = currency.get(name);
            if (v == null) return Optional.empty();
            return Optional.of(v);
        } finally {
            lck.unlock();
        }
    }

    /**
     * Build price list for all known currency in `newBase` units.
     * Means: how much cost each currency in newBase units.
     *
     * @param newBase        Target currency units
     * @param excludeNewBase Flag to exclude price for new basis currency (which is always must be 1)
     */
    public Map<Currency, Quote> priceTable(Currency newBase, boolean excludeNewBase) {
        HashMap<Currency, Quote> table = new HashMap<>();
        Lock lck = lock.readLock();
        lck.lock();
        try {
            if (!currency.containsKey(newBase)) return table;
            Quote qNewBase = currency.get(newBase);
            currency.forEach((c, price) -> table.put(c, new Quote(price.getBuy() / qNewBase.getBuy(), price.getSell() / qNewBase.getSell())));
        } finally {
            lck.unlock();
        }
        if (excludeNewBase) table.remove(newBase);
        return table;
    }

    /**
     * Build price list for all known currency in `newBase` units.
     * Means: how much cost newBase in each currency units.
     *
     * @param newBase        Target currency units
     * @param excludeNewBase Flag to exclude price for new basis currency (which is always must be 1)
     */
    public Map<Currency, Quote> sellTable(Currency newBase, boolean excludeNewBase) {
        HashMap<Currency, Quote> table = new HashMap<>();
        Lock lck = lock.readLock();
        lck.lock();
        try {
            if (!currency.containsKey(newBase)) return table;
            Quote qNewBase = currency.get(newBase);
            currency.forEach((c, price) -> table.put(c, new Quote(qNewBase.getBuy() / price.getBuy(), qNewBase.getSell() / price.getSell())));
        } finally {
            lck.unlock();
        }
        if (excludeNewBase) table.remove(newBase);
        return table;
    }

    /**
     * Get price list with new base currency
     *
     * @see #priceTable(Currency, boolean)
     */
    public Map<Currency, Quote> priceTable(Currency newBase) {
        return priceTable(newBase, true);
    }

    /**
     * Get sell price list with new base currency
     *
     * @see #priceTable(Currency, boolean)
     */
    public Map<Currency, Quote> sellTable(Currency newBase) {
        return sellTable(newBase, true);
    }


    /**
     * Get current base currency id
     */
    public Currency getBase() {
        return base;
    }
}
