package ru.sberbank.uplatform.dbo;

/**
 * @author Pavel Tarasov
 * @since 24/05/2016.
 */
public class UserBean {
    private Long userId;
    private Long chatId;
    private String firstname;
    private String lastname;
    private String username;
    private Integer platform;
    private Double latitude;
    private Double longitude;
    private Character gender;

    public UserBean() {
    }

    public UserBean(Long userId, Long chatId, String firstname, String lastname, String username,
                    Integer platform, Double latitude, Double longitude, Character gender)
    {
        this.userId = userId;
        this.chatId = chatId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.platform = platform;
        this.latitude = latitude;
        this.longitude = longitude;
        this.gender = gender;
    }

    public Object[] getInsertValues() {
        return new Object[] {userId, chatId, firstname, lastname, username, platform, latitude, longitude, gender};
    }

    public Object[] getUpdateValues(long id) {
        return new Object[] {chatId, firstname, lastname, username, id};
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserBean bean = (UserBean) o;

        if (userId != null ? !userId.equals(bean.userId) : bean.userId != null) return false;
        if (chatId != null ? !chatId.equals(bean.chatId) : bean.chatId != null) return false;
        if (firstname != null ? !firstname.equals(bean.firstname) : bean.firstname != null) return false;
        if (lastname != null ? !lastname.equals(bean.lastname) : bean.lastname != null) return false;
        if (username != null ? !username.equals(bean.username) : bean.username != null) return false;
        if (platform != null ? !platform.equals(bean.platform) : bean.platform != null) return false;
        if (latitude != null ? !latitude.equals(bean.latitude) : bean.latitude != null) return false;
        return !(longitude != null ? !longitude.equals(bean.longitude) : bean.longitude != null)
                && !(gender != null ? !gender.equals(bean.gender) : bean.gender != null);

    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (chatId != null ? chatId.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (platform != null ? platform.hashCode() : 0);
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "userId=" + userId +
                ", chatId=" + chatId +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", username='" + username + '\'' +
                ", platform=" + platform +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", gender=" + gender +
                '}';
    }
}