package ru.sberbank.uplatform.dbo;

import com.google.gson.annotations.SerializedName;
import ru.sberbank.uplatform.commerce.flowers.api.FlowerOrder;

import java.util.Date;

/**
 * @author Pavel Tarasov
 * @since 25/02/2016.
 */
public class OrderBean {
    private Long id;
    @SerializedName("user")
    private Long userId;
    private String phone;
    @SerializedName("product")
    private Integer productId;
    @SerializedName("delivery_city")
    private String deliveryCity;
    @SerializedName("billing_city")
    private String billingCity;
    @SerializedName("delivery_date")
    private Date deliveryDate;
    @SerializedName("status")
    private Boolean orderStatus;
    @SerializedName("order")
    private String orderId;

    public OrderBean(FlowerOrder order) {
        userId = order.getUserId();
        phone = order.getPhone();
        productId = order.getFlower().getProductId();
        if (order.getBillingCity().isPresent()) {
            deliveryCity = order.getDeliveryCity().get();
        }
        if (order.getBillingCity().isPresent()) {
            billingCity = order.getBillingCity().get();
        }
        if (order.getDeliveryDate().isPresent()) {
            deliveryDate = order.getDeliveryDate().get();
        }
    }

    public Object[] getValues() {
        return new Object[] {userId, phone, productId, orderStatus, orderId};
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Boolean getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Boolean orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "OrderBean{" +
                "userId=" + userId +
                ", phone='" + phone + '\'' +
                ", productId=" + productId +
                ", deliveryCity='" + deliveryCity + '\'' +
                ", billingCity='" + billingCity + '\'' +
                ", deliveryDate=" + deliveryDate +
                ", orderStatus=" + orderStatus +
                ", orderId='" + orderId + '\'' +
                '}';
    }
}
