package ru.sberbank.uplatform.dbo;

/**
 * Created by baryshnikov on 14.11.15.
 */
public class Dialog {
    private String nextRoute;
    private Long chatID;

    public Dialog(String nextRoute, Long chatID) {
        this.nextRoute = nextRoute;
        this.chatID = chatID;
    }

    public Dialog() {
    }

    public String getNextRoute() {
        return nextRoute;
    }

    public void setNextRoute(String nextRoute) {
        this.nextRoute = nextRoute;
    }

    public Long getChatID() {
        return chatID;
    }

    public void setChatID(Long chatID) {
        this.chatID = chatID;
    }

    @Override
    public String toString() {
        return "Dialog{" +
                "nextRoute='" + nextRoute + '\'' +
                ", chatID=" + chatID +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dialog dialog = (Dialog) o;

        if (nextRoute != null ? !nextRoute.equals(dialog.nextRoute) : dialog.nextRoute != null) return false;
        return !(chatID != null ? !chatID.equals(dialog.chatID) : dialog.chatID != null);

    }

    @Override
    public int hashCode() {
        int result = nextRoute != null ? nextRoute.hashCode() : 0;
        result = 31 * result + (chatID != null ? chatID.hashCode() : 0);
        return result;
    }
}
