package ru.sberbank.uplatform.dbo;

/**
 * @author Pavel Tarasov
 * @since 02/12/2015.
 */
public class MetricBean {
    private Long userId;
    private String command;
    private Boolean answered;

    public MetricBean() {
    }

    public MetricBean(String command, Boolean answered) {
        this.command = command;
        this.answered = answered;
    }

    public Object[] getValues() {
        return new Object[] {userId, command, answered};
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Boolean getAnswered() {
        return answered;
    }

    public void setAnswered(Boolean answered) {
        this.answered = answered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetricBean bean = (MetricBean) o;

        return !(userId != null ? !userId.equals(bean.userId) : bean.userId != null) &&
                !(command != null ? !command.equals(bean.command) : bean.command != null) &&
                !(answered != null ? !answered.equals(bean.answered) : bean.answered != null);
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (command != null ? command.hashCode() : 0);
        result = 31 * result + (answered != null ? answered.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MetricBean{" +
                "userId=" + userId +
                ", command='" + command + '\'' +
                ", answered=" + answered +
                '}';
    }
}