package ru.sberbank.uplatform.dbo;

/**
 * @author Pavel Tarasov
 * @since 25/05/2016.
 */
public class QueueBean {
    private MetricBean metric;
    private UserBean user;

    public QueueBean(MetricBean metric, UserBean user) {
        this.metric = metric;
        this.user = user;
    }

    public MetricBean getMetric() {
        return metric;
    }

    public void setMetric(MetricBean metric) {
        this.metric = metric;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QueueBean queueBean = (QueueBean) o;

        return !(metric != null ? !metric.equals(queueBean.metric) : queueBean.metric != null)
                && !(user != null ? !user.equals(queueBean.user) : queueBean.user != null);
    }

    @Override
    public int hashCode() {
        int result = metric != null ? metric.hashCode() : 0;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "QueueBean{" +
                "metric=" + metric +
                ", user=" + user +
                '}';
    }
}