package ru.sberbank.uplatform.dbo;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * @author Pavel Tarasov
 * @since 26/11/2015.
 */
public class DatabaseBean {
    private static final Logger LOG = Logger.getLogger(DatabaseBean.class);
    private DataSource dataSource;


    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void create() throws Exception {
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        String sqlMetrics = "CREATE TABLE IF NOT EXISTS metrics (\n"
                + "  id BIGSERIAL NOT NULL,\n"
                + "  user_id BIGINT NOT NULL,\n"
                + "  command_text VARCHAR NOT NULL,\n"
                + "  answered BOOLEAN,\n"
                + "  created_at TIMESTAMP WITH TIME ZONE DEFAULT now(),\n"
                + "  CONSTRAINT metrics_pkey PRIMARY KEY (id)\n"
                + ")";
        String sqlUsers = "CREATE TABLE IF NOT EXISTS users (" +
                "  id BIGSERIAL PRIMARY KEY,\n" +
                "  user_id BIGINT NOT NULL,\n" +
                "  chat_id BIGINT,\n" +
                "  firstname VARCHAR DEFAULT '',\n" +
                "  lastname VARCHAR DEFAULT '',\n" +
                "  username VARCHAR DEFAULT '',\n" +
                "  platform INTEGER NOT NULL,\n" +
                "  gender CHAR,\n" +
                "  latitude NUMERIC,\n" +
                "  longitude NUMERIC,\n" +
                "  created TIMESTAMP WITH TIME ZONE DEFAULT now()" +
                ")";
        String sqlFlowers = "CREATE TABLE IF NOT EXISTS flowers (" +
                " id serial NOT NULL," +
                " product_id integer NOT NULL," +
                " name character varying NOT NULL," +
                " description character varying NOT NULL," +
                " photo character varying NOT NULL," +
                " price decimal NOT NULL," +
                " created TIMESTAMP WITH TIME ZONE DEFAULT now()," +
                " CONSTRAINT flowers_pkey PRIMARY KEY (id)" +
                ")";
        String sqlFlowerOrders = "CREATE TABLE IF NOT EXISTS flower_orders (" +
                " id serial NOT NULL," +
                " user_id bigint NOT NULL," +
                " phone character varying NOT NULL," +
                " product_id integer NOT NULL," +
                " delivery_city character varying DEFAULT ''," +
                " billing_city character varying DEFAULT ''," +
                " delivery_date date DEFAULT now()," +
                " status boolean DEFAULT false," +
                " order_id character varying DEFAULT ''," +
                " created TIMESTAMP WITH TIME ZONE DEFAULT now()," +
                " CONSTRAINT flower_orders_pkey PRIMARY KEY (id)" +
                ")";
        String sqlConsumptions = "CREATE TABLE IF NOT EXISTS consumptions (\n"
                + " id serial NOT NULL,\n"
                + " user_id bigint NOT NULL,\n"
                + " value decimal NOT NULL,\n"
                + " purpose character varying,\n"
                + " category_id integer REFERENCES consumption_categories(id),\n"
                + " create_date TIMESTAMP WITH TIME ZONE DEFAULT now(),\n"
                + " is_consumption boolean NOT NULL,\n"
                + " CONSTRAINT consumptions_pkey PRIMARY KEY (id)\n"
                + ")";
        String sqlConsumptionCategories = "CREATE TABLE IF NOT EXISTS consumption_categories (" +
                "id serial NOT NULL," +
                "category_name character varying NOT NULL," +
                "is_consumption boolean NOT NULL," +
                "CONSTRAINT consumption_categories_pkey PRIMARY KEY (id)" +
                ")";
        String indexes = "CREATE INDEX id ON metrics(id);"
                + "CREATE INDEX user_id ON metrics(user_id);"
                + "CREATE INDEX command_text ON metrics(command_text);"
                + "CREATE INDEX created_at ON metrics(created_at);";
        String userIndexes = "CREATE INDEX id ON users(id);"
                + "CREATE INDEX user_id ON users(user_id);"
                + "CREATE INDEX chat_id ON users(chat_id);"
                + "CREATE INDEX firstname ON users(firstname);"
                + "CREATE INDEX lastname ON users(lastname);"
                + "CREATE INDEX username ON users(username);"
                + "CREATE INDEX platform ON users(platform);"
                + "CREATE INDEX latitude ON users(latitude);"
                + "CREATE INDEX longitude ON users(longitude);"
                + "CREATE INDEX gender ON users(gender);"
                + "CREATE INDEX created ON users(created);";
        String indexesConsumptions = "CREATE INDEX consumptions_user_id_idx ON consumptions(user_id);"
                + "CREATE INDEX consumptions_create_date_idx ON consumptions(create_date);"
                + "CREATE INDEX consumptions_purpose_idx ON consumptions(purpose);"
                + "CREATE INDEX consumptions_category_idx ON consumptions(category_id);";


        LOG.info("Creating table 'metrics'");
        try {
            jdbc.execute(sqlMetrics);
            jdbc.execute(indexes);
            LOG.info("Table 'metrics' is created");
        } catch (Throwable t) {
            LOG.info("Database table 'metrics' already exists");
        }
        LOG.info("Creating table 'users'");
        try {
            jdbc.execute(sqlUsers);
            jdbc.execute(userIndexes);
            LOG.info("Table 'users' is created");
        } catch (Throwable t) {
            LOG.info("Database table 'users' already exists");
        }
        LOG.info("Creating table 'flowers'");
        try {
            jdbc.execute(sqlFlowers);
            LOG.info("Table 'flowers' is created");
        } catch (Throwable t) {
            LOG.info("Database table 'flowers' already exists");
        }
        LOG.info("Creating table 'flower_orders'");
        try {
            jdbc.execute(sqlFlowerOrders);
            LOG.info("Table 'flower_orders' is created");
        } catch (Throwable t) {
            LOG.info("Database table 'flower_orders' already exists");
        }
        LOG.info("Creating table 'consumption's categories'");
        try {
            jdbc.execute(sqlConsumptionCategories);
            LOG.info("Table 'consumption's categories' is created");
        } catch (Throwable t) {
            LOG.info("Database table 'consumption's categories' already exists");
        }
        LOG.info("Creating table 'consumptions'");
        try {
            jdbc.execute(sqlConsumptions);
            LOG.info("Table 'consumptions' is created");
        } catch (Throwable t) {
            LOG.info("Database table 'consumptions' already exists");
        }
        LOG.info("Creating table 'indexes'");
        try {
            jdbc.execute(indexesConsumptions);
            LOG.info("Table 'indexes' is created");
        } catch (Throwable t) {
            LOG.info("Database table 'indexes' already exists");
        }
    }
}
