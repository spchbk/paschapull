package ru.sberbank.uplatform.consumption;

import java.math.BigDecimal;

/**
 * Adding consumption or revenue to DB.
 *
 * @author Dmitriy_Gulyaev
 * @since 24-Mar-2016
 */
public class ConsumptionBean {
    private Long id;
    private BigDecimal value;
    private Long userId;
    private String purpose;
    private String category;

    /**
     * true - consumption, false - revenue
     */
    private Boolean consumptionMode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getConsumptionMode() {
        return consumptionMode;
    }

    public void setConsumptionMode(Boolean consumptionMode) {
        this.consumptionMode = consumptionMode;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return String.format("ConsumptionBean [id=%s, value=%s, userId=%s, purpose=%s, category=%s, consumptionMode=%s]", id, value,
                userId, purpose, category, consumptionMode);
    }

}
