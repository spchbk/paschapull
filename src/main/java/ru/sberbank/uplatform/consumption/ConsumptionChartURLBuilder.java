package ru.sberbank.uplatform.consumption;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Build Google Charts API Line Chart URL
 * <p>
 * https://developers.google.com/chart/image/docs/gallery/line_charts
 *
 * @author Dmitriy_Gulyaev
 */

public class ConsumptionChartURLBuilder {

    private static final boolean DEBUG = false;

    private static final Logger logger = LoggerFactory.getLogger(ConsumptionChartURLBuilder.class);

    private static final String CHART_GOOGLE_API_URI = "https://chart.googleapis.com/chart";

    private static final int[] DEMO_DATA = {170, 150, 50, 150, 100, 50};

    /**
     * Extended Encoding Format string.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/data_formats#extended-encoding-format">
     * link</a>
     */
    private static final String EEFS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-.";
    private static final int EEFSL = EEFS.length();

    private static final Locale ruLocale = new Locale("ru", "RU");
    private static final SimpleDateFormat frmM = new SimpleDateFormat("MMM", ruLocale);
    private static final SimpleDateFormat frmY = new SimpleDateFormat("yyyy", ruLocale);

    /**
     * Series Colors.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/chart_params#series-colors-chco-all-charts">
     * link</a>
     */
    private static final String CHART_PARAM_SERIES_COLORS = "chco";

    /**
     * Chart Size.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/chart_params#chart-size-chs-all-charts">
     * link</a>
     */
    private static final String CHART_PARAM_CHART_SIZE = "chs";

    /**
     * Chart data string.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/data_formats#overview">
     * link</a>
     */
    private static final String CHART_PARAM_DATA_STRING = "chd";

    /**
     * Grid Lines.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/gallery/line_charts#grid-lines-chg-line----bar-radar-scatter">
     * link</a>
     */
    private static final String CHART_PARAM_GRID_LINES = "chg";

    /**
     * Chart Types.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/gallery/line_charts#chart-types-cht">
     * link</a>
     */
    private static final String CHART_PARAM_CHART_TYPE = "cht";

    /**
     * Custom Axis Labels.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/gallery/line_charts#custom-axis-labels-chxl">
     * link</a>
     */
    private static final String CHART_PARAM_AXIS_LABELS = "chxl";

    /**
     * Visible Axes.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/gallery/line_charts#visible-axes-chxt">
     * link</a>
     */
    private static final String CHART_PARAM_VISIBLE_AXES = "chxt";

    /**
     * Axis Label Positions.
     *
     * @see <a href=
     * "https://developers.google.com/chart/image/docs/gallery/line_charts#axis-label-positions-chxp">
     * link</a>
     */
    private static final String CHART_PARAM_AXIS_LABEL_POS = "chxp";

    /**
     * Line Styles.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/gallery/line_charts#line-styles-chls-line------radar">
     * link</a>
     */
    private static final String CHART_PARAM_LINE_STYLES = "chls";

    /**
     * Shape Markers.
     *
     * @see<a href=
     * "https://developers.google.com/chart/image/docs/chart_params#line-fills-chm-line----radar">
     * link</a>
     */
    private static final String CHART_PARAM_LINE_FILLS = "chm";

    private static List<SumPerPeriod> generateDemoList(Boolean consumptionMode) {
        int daysBefore = DEMO_DATA.length;
        List<SumPerPeriod> newList = new ArrayList<>(daysBefore);

        for (int day = 0; day < daysBefore; day++) {
            Calendar calendar = Calendar.getInstance(ruLocale);
            calendar.add(Calendar.DAY_OF_YEAR, -day);

            SumPerPeriod sumPerPeriod = new SumPerPeriod();

            sumPerPeriod.setCreateDate(calendar.getTime());
            sumPerPeriod.setSumValue(new BigDecimal(DEMO_DATA[day]));

            newList.add(0, sumPerPeriod);
        }
        return newList;
    }

    public static String buildURLForDemo(Boolean consumptionMode) {
        return buildURL(generateDemoList(consumptionMode), consumptionMode);
    }

    /**
     * Round number to first two digits.
     *
     * @param value
     * @return
     */
    private static int roundSum(int value) {
        int tempValue = Math.abs(value);
        String str = String.valueOf(tempValue);
        if (str.length() > 1) {
            int t = Integer.parseInt(str.substring(0, 2)) + 1;
            t = (int) (t * (int) Math.pow(10, str.length() - 2));
            if (value < 0) {
                t = t * -1;
            }
            return t;
        } else {
            return value;
        }
    }

    public static String buildURL(List<SumPerPeriod> sumList, Boolean consumptionMode) {

        // Max. value in chart
        int maxValue = 0;

        // Min. value in chart
        int minValue = 0;

        // Middle value in chart
        int mdlValue = 0;

        // Accumulated value.
        int accValue = 0;

        for (SumPerPeriod sumPerPeriod : sumList) {

            if (DEBUG) {
                System.out.println(sumPerPeriod);
            }

            // No consumptions or revenues at this day.
            if (sumPerPeriod.getSumValue() == null) {
                continue;
            }

            int currentValue = sumPerPeriod.getSumValue().intValue();
            accValue = accValue + currentValue;

            if (accValue > maxValue) {
                maxValue = accValue;
            }

            if (accValue < minValue) {
                minValue = accValue;
            }
        }

        maxValue = roundSum(maxValue);
        minValue = roundSum(minValue);

        if (minValue == 0) {
            mdlValue = maxValue / 2;
        }

        int range = Math.abs(maxValue - minValue);

        if (DEBUG) {
            System.out.println("------------------------------------------");
            System.out.println("maxValue " + maxValue);
            System.out.println("minValue " + minValue);
            System.out.println("range    " + range);
        }

        boolean firstMonthOnChart = true;
        int minValueAbs = Math.abs(minValue);
        int totalDays = sumList.size();
        boolean byYear = totalDays > 650;
        int daysInXAxis = 0;

        if (totalDays <= 31) {
            daysInXAxis = 1;
        } else if (totalDays <= 31 * 2) {
            daysInXAxis = 5;
        }

        StringBuilder chd = new StringBuilder();

        StringBuilder chxl = new StringBuilder();
        StringBuilder chxl0 = new StringBuilder();
        StringBuilder chxl1 = new StringBuilder();

        StringBuilder chxp0 = new StringBuilder();
        StringBuilder chxp1 = new StringBuilder();
        StringBuilder chxp2 = new StringBuilder();

        if (daysInXAxis == 0) {
            chxp0.append("0");
            chxp1.append("1");
        }

        chxl0.append("0:|");
        chxl1.append("1:|");
        chd.append("e:");

        int accumValue = 0;
        for (int currentDay = 0; currentDay < sumList.size(); currentDay++) {
            SumPerPeriod sumPerPeriod = sumList.get(currentDay);

            int currValue = 0;
            if (sumPerPeriod.getSumValue() != null) {
                currValue = sumPerPeriod.getSumValue().intValue();
            }

            accumValue = accumValue + currValue;

            int value4096based = getRelatedValue(accumValue + minValueAbs, 4096 - 1, range);

            if (DEBUG) {
                System.out.println("a = " + accumValue + ", v = " + currValue + ", r=" + range + ", m = " + minValueAbs
                        + ",t = " + value4096based);
            }

            char c1 = EEFS.charAt(value4096based / EEFSL);
            char c2 = EEFS.charAt(value4096based % EEFSL);

            chd.append(c1);
            chd.append(c2);

            Date createDate = sumPerPeriod.getCreateDate();
            Calendar tempCalendar = Calendar.getInstance();
            tempCalendar.setTime(sumPerPeriod.getCreateDate());

            int dayOfTheMonth = tempCalendar.get(Calendar.DATE);
            int month = tempCalendar.get(Calendar.MONTH);

            int xPos = (currentDay) * 100 / totalDays;

            if (dayOfTheMonth == 1 || (currentDay == 0 && totalDays < 30)) {

                if (!byYear) {
                    chxl0.append(frmM.format(createDate));
                }

                addCHXP(chxp0, xPos);
                chxl0.append('|');

                if (month == 0 || firstMonthOnChart) {
                    chxl1.append(frmY.format(createDate));
                    chxl1.append('|');
                    addCHXP(chxp1, xPos);
                }
                firstMonthOnChart = false;
            } else if (daysInXAxis != 0) {
                if (dayOfTheMonth % daysInXAxis == 0) {
                    chxl0.append(dayOfTheMonth);
                    chxl0.append('|');
                    addCHXP(chxp0, xPos);
                }
            }
        }

        chxl.append(chxl0);
        chxl.append(chxl1);

        chxl.append("2:");
        addCHXL(chxl, minValue);
        addCHXL(chxl, mdlValue);
        addCHXL(chxl, maxValue);

        int relMin = getRelatedValue(minValue + minValueAbs, 100, range);
        int relMdl = getRelatedValue(mdlValue + minValueAbs, 100, range);
        int relMax = getRelatedValue(maxValue + minValueAbs, 100, range);

        chxp2.append('2');

        chxp2.append(',');
        chxp2.append(relMin);

        chxp2.append(',');
        chxp2.append(relMdl);

        chxp2.append(',');
        chxp2.append(relMax);

        URIBuilder uriBuilder = null;
        try {
            uriBuilder = new URIBuilder(CHART_GOOGLE_API_URI);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String colorLine = Boolean.TRUE.equals(consumptionMode) ? "0066FF" : "7dc244";
        String translucency = "80";
        uriBuilder.addParameter(CHART_PARAM_VISIBLE_AXES, "x,x,y");
        uriBuilder.addParameter(CHART_PARAM_AXIS_LABELS, chxl.toString());
        uriBuilder.addParameter(CHART_PARAM_CHART_TYPE, "lc");
        uriBuilder.addParameter(CHART_PARAM_GRID_LINES, "0,10");
        uriBuilder.addParameter(CHART_PARAM_DATA_STRING, chd.toString());
        uriBuilder.addParameter(CHART_PARAM_CHART_SIZE, "700x400");
        uriBuilder.addParameter(CHART_PARAM_LINE_STYLES, "4");
        uriBuilder.addParameter(CHART_PARAM_SERIES_COLORS, colorLine);
        if (consumptionMode != null) {
            uriBuilder.addParameter(CHART_PARAM_LINE_FILLS, "B," + colorLine + translucency + ",0,0,0");
        }

        uriBuilder.addParameter(CHART_PARAM_AXIS_LABEL_POS,
                chxp0.append('|').append(chxp1).append('|').append(chxp2).toString());

        String result = uriBuilder.toString();

        if (DEBUG) {
            for (NameValuePair nameValuePair : uriBuilder.getQueryParams()) {
                System.out.format("%-5s = %s\n", nameValuePair.getName(), nameValuePair.getValue());
            }
            System.out.println("url   = " + result);
            System.out.println("length= " + result.length());
        }

        logger.debug("URL = %s", result);

        return result;

    }

    private static int getRelatedValue(int value, int base, int maxValue) {
        if (maxValue == 0) {
            return 0;
        }
        long intermediateLong = (long) value * (long) base;
        return (int) (intermediateLong / (long) maxValue);
    }

    private static void addCHXL(StringBuilder chxl, int value) {
        chxl.append('|');
        chxl.append(value);
    }

    private static void addCHXP(StringBuilder chxp, int value) {
        if (chxp.length() > 0) {
            chxp.append(',');
        }
        chxp.append(value);
    }

}
