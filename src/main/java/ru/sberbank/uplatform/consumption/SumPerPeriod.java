package ru.sberbank.uplatform.consumption;

import java.math.BigDecimal;
import java.util.Date;

public class SumPerPeriod {

    private BigDecimal sumValue;
    private Date createDate;

    public BigDecimal getSumValue() {
        return sumValue;
    }

    public void setSumValue(BigDecimal sumValue) {
        this.sumValue = sumValue;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return String.format("SumPerPeriod [sumValue=%s, createDate=%s]", sumValue, createDate);
    }

}
