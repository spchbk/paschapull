package ru.sberbank.uplatform.consumption;

import java.util.regex.Pattern;

public class ConsumptionStrConv {

    private final static String[] regexMonths = {"январ.", "феврал.", "март.?", "апрел.", "ма[й|я|е]", "июн.", "июл.", "август.?",
            "сентябр.?", "октябр.", "ноябр.", "декабр."};

    private final static String[] regexConsumption = {"расход.{0,2}", "потратил.?", "трат.?"};

    public static Boolean getConsumptionMode(String periodString) {
        String periodStringLower = periodString.toLowerCase();
        if (periodStringLower.startsWith("баланс")) {
            return null;
        }
        boolean isConsumption = checkRegex(periodString, regexConsumption) != -1;
        return isConsumption;
    }

    public static boolean getConsumptionModeSimple(String mode) {
        return mode.toLowerCase().contains("расход");
    }

    public static boolean isYearStr(String periodString) {
        return periodString != null && periodString.toLowerCase().startsWith("год");
    }

    public static boolean isMonthStr(String periodString) {
        return periodString != null && periodString.toLowerCase().startsWith("месяц");
    }

    public static boolean isWeekStr(String periodString) {
        return periodString != null && periodString.toLowerCase().startsWith("недел");
    }

    public static boolean isTodayStr(String periodString) {
        return periodString != null && periodString.toLowerCase().startsWith("сегодня");
    }

    public static boolean isYesterdayStr(String periodString) {
        return periodString != null && periodString.toLowerCase().startsWith("вчера");
    }

    public static boolean isTwoDayBeforeStr(String periodString) {
        return periodString != null && periodString.toLowerCase().startsWith("позавчера");
    }

    public static int getMonth(String monthName) {
        return checkRegex(monthName, regexMonths);
    }

    private static int checkRegex(String monthName, String[] arrayRegex) {
        int flags = Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE;
        for (int indexMonth = 0; indexMonth < arrayRegex.length; ++indexMonth) {
            String regex = arrayRegex[indexMonth];
            boolean find = Pattern.compile(regex, flags).matcher(monthName).matches();
            if (find) {
                return indexMonth;
            }
        }

        return -1;
    }
}
