package ru.sberbank.uplatform.consumption;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Request, obtain consumption or revenue info from DB.
 *
 * @author Dmitriy_Gulyaev
 * @since 24-Mar-2016
 */
public class GetConsumptionRequest {

    private static final SimpleDateFormat simpleDateFormatDDMMYYYY = new SimpleDateFormat("dd.MM.yyyy");

    /** true - consumption, false - revenue, null - both, balance */
    private Boolean consumptionMode;

    /**
     * Period string in source query, e.g. "2 дня", "10 дней", "неделю",
     * "месяц".
     */
    private String originalPeriodStr;

    /**
     * Purpose of consumption or revenue, e.g. "ашан", "оплата заказа"
     */
    private String purpose;

    /**
     * Interval's preposition
     */
    private String preposition;

    private Date fromDate;

    private Date toDate;

    /**
     * true - get max() value, false - get sum() value from DB.
     */
    private boolean maxMode;

    private boolean demoMode;

    public GetConsumptionRequest() {

    }

    public GetConsumptionRequest(Boolean consumptionMode) {
        this.consumptionMode = consumptionMode;
    }

    public GetConsumptionRequest(Boolean consumptionMode, boolean demoMode) {
        this.consumptionMode = consumptionMode;
        this.demoMode = demoMode;
    }

    /*
     * public GetConsumptionRequest(Boolean perMonth, boolean consumptionMode) {
     * this.perMonth = perMonth; this.consumptionMode = consumptionMode; }
     */
    public GetConsumptionRequest(String type, String purpose, String preposition, String interval, String days, boolean maxMode) {
        this.consumptionMode = ConsumptionStrConv.getConsumptionMode(type);
        this.preposition = preposition;

        Calendar calendarFrom = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        Calendar calendarTo = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        clearTime(calendarFrom);
        clearTime(calendarTo);

        if (days != null) {
            int perDays = Integer.parseInt(days);
            calendarFrom.add(Calendar.DAY_OF_MONTH, perDays * -1);
            setFromDate(calendarFrom.getTime());
        }
        if (interval != null) {
            this.setOriginalPeriodStr(interval);

            if (ConsumptionStrConv.isYearStr(interval)) {
                calendarFrom.add(Calendar.YEAR, -1);
                setFromDate(calendarFrom.getTime());
            } else if (ConsumptionStrConv.isMonthStr(interval)) {
                calendarFrom.add(Calendar.MONTH, -1);
                setFromDate(calendarFrom.getTime());
            } else if (ConsumptionStrConv.isWeekStr(interval)) {
                calendarFrom.add(Calendar.WEEK_OF_YEAR, -1);
                setFromDate(calendarFrom.getTime());
            } else if (ConsumptionStrConv.isTodayStr(interval)) {
                setFromDate(calendarFrom.getTime());
            } else if (ConsumptionStrConv.isYesterdayStr(interval)) {
                calendarFrom.add(Calendar.DATE, -1);
                setFromDate(calendarFrom.getTime());
                setToDate(calendarTo.getTime());
            } else if (ConsumptionStrConv.isTwoDayBeforeStr(interval)) {
                calendarFrom.add(Calendar.DATE, -2);
                calendarTo.add(Calendar.DATE, -1);
                setFromDate(calendarFrom.getTime());
                setToDate(calendarTo.getTime());
            } else {
                int month = ConsumptionStrConv.getMonth(interval);
                if (0 <= month) {
                    int year = calendarFrom.get(Calendar.YEAR);
                    int currentMonth = calendarFrom.get(Calendar.MONTH);
                    calendarFrom.set(year, month, 1);
                    calendarTo.set(year, month, 1);
                    calendarTo.add(Calendar.MONTH, 1);
                    boolean lastYear = currentMonth < month;
                    if (lastYear) {
                        calendarFrom.add(Calendar.YEAR, -1);
                        calendarTo.add(Calendar.YEAR, -1);
                    }
                    setFromDate(calendarFrom.getTime());
                    setToDate(calendarTo.getTime());
                } else {
                    try {
                        interval = interval.replaceAll("[.,/-]", ".");
                        setFromDate(simpleDateFormatDDMMYYYY.parse(interval));
                        calendarTo.setTime(getFromDate());
                        calendarTo.add(Calendar.DAY_OF_MONTH, 1);
                        setToDate(calendarTo.getTime());
                    } catch (ParseException ignore) {
                    }
                }
            }
        }

        this.setPurpose(purpose);
        this.maxMode = maxMode;
    }

    private void clearTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public Boolean isConsumptionMode() {
        return consumptionMode;
    }

    public void setConsumptionMode(Boolean consumptionMode) {
        this.consumptionMode = consumptionMode;
    }

    public String getOriginalPeriodStr() {
        return originalPeriodStr;
    }

    public void setOriginalPeriodStr(String originalPeriodStr) {
        this.originalPeriodStr = originalPeriodStr;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public boolean isMaxMode() {
        return maxMode;
    }

    public void setMaxMode(boolean maxMode) {
        this.maxMode = maxMode;
    }

    public boolean isDemoMode() {
        return demoMode;
    }

    public void setDemoMode(boolean demoMode) {
        this.demoMode = demoMode;
    }

    public String getPreposition() {
        return preposition;
    }

    public void setPreposition(String preposition) {
        this.preposition = preposition;
    }

    @Override
    public String toString() {
        return String.format(
                "GetConsumptionRequest [consumptionMode=%s, originalPeriodStr=%s, purpose=%s, fromDate=%s, toDate=%s, maxMode=%s, demoMode=%s]",
                consumptionMode, originalPeriodStr, purpose, fromDate, toDate, maxMode, demoMode);
    }

}
