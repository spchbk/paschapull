package ru.sberbank.uplatform.consumption;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SumPerPeriodRowMapper implements RowMapper<SumPerPeriod> {

    @Override
    public SumPerPeriod mapRow(ResultSet rs, int rowNum) throws SQLException {
        SumPerPeriod sumPerPeriod = new SumPerPeriod();
        sumPerPeriod.setCreateDate(rs.getDate("createdate"));
        sumPerPeriod.setSumValue(rs.getBigDecimal("sumValue"));

        return sumPerPeriod;
    }

}
