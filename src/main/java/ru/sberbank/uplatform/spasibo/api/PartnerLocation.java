package ru.sberbank.uplatform.spasibo.api;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 26/11/2015.
 */
public class PartnerLocation {
    private Integer id;
    private String title;
    private Partner partner;
    private String location;
    private City city;
    private String address;
    @SerializedName("short_address")
    private String shortAddress;
    private Integer distance; // in meters
    @SerializedName("is_fuzzy_location")
    private Boolean fuzzyLocation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShortAddress() {
        return shortAddress;
    }

    public void setShortAddress(String shortAddress) {
        this.shortAddress = shortAddress;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Boolean getFuzzyLocation() {
        return fuzzyLocation;
    }

    public void setFuzzyLocation(Boolean fuzzyLocation) {
        this.fuzzyLocation = fuzzyLocation;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (partner != null ? partner.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (shortAddress != null ? shortAddress.hashCode() : 0);
        result = 31 * result + (distance != null ? distance.hashCode() : 0);
        result = 31 * result + (fuzzyLocation != null ? fuzzyLocation.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PartnerLocation partnerLocation = (PartnerLocation) o;
        if (!id.equals(partnerLocation.id)) return false;
        if (title != null ? !title.equals(partnerLocation.title) : partnerLocation.title != null) return false;
        if (partner != null ? !partner.equals(partnerLocation.partner) : partnerLocation.partner != null) return false;
        if (location != null ? !location.equals(partnerLocation.location) : partnerLocation.location != null) return false;
        if (city != null ? !city.equals(partnerLocation.city) : partnerLocation.city != null) return false;
        if (address != null ? !address.equals(partnerLocation.address) : partnerLocation.address != null) return false;
        if (shortAddress != null ? !shortAddress.equals(partnerLocation.shortAddress) : partnerLocation.shortAddress != null) return false;
        if (distance != null ? !distance.equals(partnerLocation.distance) : partnerLocation.distance != null) return false;
        return !(fuzzyLocation != null ? !fuzzyLocation.equals(partnerLocation.fuzzyLocation) : partnerLocation.fuzzyLocation != null);
    }

    @Override
    public String toString() {
        return "PartnerLocation{" +
                "id='" + id + '\'' +
                ", title=" + title +
                ", partner=" + partner +
                ", location=" + location +
                ", city=" + city +
                ", address=" + address +
                ", shortAddress=" + shortAddress +
                ", distance=" + distance +
                ", fuzzyLocation=" + fuzzyLocation +
                '}';
    }
}
