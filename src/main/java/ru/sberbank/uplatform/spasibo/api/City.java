package ru.sberbank.uplatform.spasibo.api;

/**
 * @author Pavel Tarasov
 * @since 26/11/2015.
 */
public class City {
    private Integer id;
    private String title;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;
        return id.equals(city.id) && !(title != null ? !title.equals(city.title) : city.title != null);
    }

    @Override
    public String toString() {
        return "City{" +
                "id='" + id + '\'' +
                ", title=" + title +
                '}';
    }
}
