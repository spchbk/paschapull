package ru.sberbank.uplatform.spasibo.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 26/11/2015.
 */
public class Category {
    private Integer id;
    private String title;
    @SerializedName("image")
    private String imageLink;
    @SerializedName("new_partners_ids")
    private List<Integer> newPartnersIds;
    @SerializedName("new_partners_count")
    private Integer newPartnersCount;
    @SerializedName("total_partners_count")
    private Integer totalPartnersCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public List<Integer> getNewPartnersIds() {
        return newPartnersIds;
    }

    public void setNewPartnersIds(List<Integer> newPartnersIds) {
        this.newPartnersIds = newPartnersIds;
    }

    public Integer getNewPartnersCount() {
        return newPartnersCount;
    }

    public void setNewPartnersCount(Integer newPartnersCount) {
        this.newPartnersCount = newPartnersCount;
    }

    public Integer getTotalPartnersCount() {
        return totalPartnersCount;
    }

    public void setTotalPartnersCount(Integer totalPartnersCount) {
        this.totalPartnersCount = totalPartnersCount;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (imageLink != null ? imageLink.hashCode() : 0);
        result = 31 * result + (newPartnersIds != null ? newPartnersIds.hashCode() : 0);
        result = 31 * result + (newPartnersCount != null ? newPartnersCount.hashCode() : 0);
        result = 31 * result + (totalPartnersCount != null ? totalPartnersCount.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;
        if (!id.equals(category.id)) return false;
        if (title != null ? !title.equals(category.title) : category.title != null) return false;
        if (imageLink != null ? !imageLink.equals(category.imageLink) : category.imageLink != null) return false;
        if (newPartnersIds != null ? !newPartnersIds.equals(category.newPartnersIds) : category.newPartnersIds != null) return false;
        if (newPartnersCount != null ? !newPartnersCount.equals(category.newPartnersCount) : category.newPartnersCount != null) return false;
        return !(totalPartnersCount != null ? !totalPartnersCount.equals(category.totalPartnersCount) : category.totalPartnersCount != null);
    }

    @Override
    public String toString() {
        return "Category{" +
                "id='" + id + '\'' +
                ", title=" + title +
                ", imageLink=" + imageLink +
                ", newPartnersIds=" + newPartnersIds +
                ", newPartnersCount=" + newPartnersCount +
                ", totalPartnersCount=" + totalPartnersCount +
                '}';
    }
}
