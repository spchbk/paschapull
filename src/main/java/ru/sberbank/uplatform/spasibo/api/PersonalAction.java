package ru.sberbank.uplatform.spasibo.api;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 27/11/2015.
 */
public class PersonalAction extends Action {
    @SerializedName("want_agree")
    private Boolean wantAgree;
    @SerializedName("action_agreement")
    private Boolean actionAgreement;

    public Boolean getWantAgree() {
        return wantAgree;
    }

    public void setWantAgree(Boolean wantAgree) {
        this.wantAgree = wantAgree;
    }

    public Boolean getActionAgreement() {
        return actionAgreement;
    }

    public void setActionAgreement(Boolean actionAgreement) {
        this.actionAgreement = actionAgreement;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (wantAgree != null ? wantAgree.hashCode() : 0);
        result = 31 * result + (actionAgreement != null ? actionAgreement.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PersonalAction action = (PersonalAction) o;
        return !(wantAgree != null ? !wantAgree.equals(action.wantAgree) : action.wantAgree != null) &&
                !(actionAgreement != null ? !actionAgreement.equals(action.actionAgreement) : action.actionAgreement != null);
    }

    @Override
    public String toString() {
        return "PersonalAction{" +
                "id='" + getId() + '\'' +
                ", title=" + getTitle() +
                ", shortDescription=" + getShortDescription() +
                ", description=" + getDescription() +
                ", url=" + getUrl() +
                ", image=" + getImage() +
                ", rate=" + getRate() +
                ", startDate=" + getStartDate() +
                ", endDate=" + getEndDate() +
                ", image=" + getViewType() +
                ", acceptsPoints=" + getPartner() +
                ", favorite=" + getFavorite() +
                ", wantAgree=" + wantAgree +
                ", actionAgreement=" + actionAgreement +
                '}';
    }
}
