package ru.sberbank.uplatform.spasibo.api;

import com.google.gson.annotations.SerializedName;

/**
 * @author Pavel Tarasov
 * @since 27/11/2015.
 */
public class Action {
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    private Integer id;
    private String title;
    @SerializedName("short_description")
    private String shortDescription;
    private String description;
    private String url;
    private String image;
    private String rate;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("view_type")
    private String viewType;
    private Partner partner;
    @SerializedName("is_favorite")
    private Boolean favorite;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortDescription != null ? shortDescription.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (rate != null ? rate.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (viewType != null ? viewType.hashCode() : 0);
        result = 31 * result + (partner != null ? partner.hashCode() : 0);
        result = 31 * result + (favorite != null ? favorite.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Action action = (Action) o;
        if (!id.equals(action.id)) return false;
        if (title != null ? !title.equals(action.title) : action.title != null) return false;
        if (shortDescription != null ? !shortDescription.equals(action.shortDescription) : action.shortDescription != null) return false;
        if (description != null ? !description.equals(action.description) : action.description != null) return false;
        if (url != null ? !url.equals(action.url) : action.url != null) return false;
        if (image != null ? !image.equals(action.image) : action.image != null) return false;
        if (rate != null ? !rate.equals(action.rate) : action.rate != null) return false;
        if (startDate != null ? !startDate.equals(action.startDate) : action.startDate != null) return false;
        if (endDate != null ? !endDate.equals(action.title) : action.title != null) return false;
        if (viewType != null ? !viewType.equals(action.viewType) : action.viewType != null) return false;
        if (partner != null ? !partner.equals(action.partner) : action.partner != null) return false;
        return !(favorite != null ? !favorite.equals(action.favorite) : action.favorite != null);
    }

    @Override
    public String toString() {
        return "Action{" +
                "id='" + id + '\'' +
                ", title=" + title +
                ", shortDescription=" + shortDescription +
                ", description=" + description +
                ", url=" + url +
                ", image=" + image +
                ", rate=" + rate +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", viewType=" + viewType +
                ", partner=" + partner +
                ", favorite=" + favorite +
                '}';
    }
}
