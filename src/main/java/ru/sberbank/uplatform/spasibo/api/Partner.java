package ru.sberbank.uplatform.spasibo.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 26/11/2015.
 */
public class Partner {
    private Integer id;
    private String title;
    private String description;
    private List<Category> category;
    @SerializedName("purchase_type")
    private Integer purchaseType;
    private String rate;
    @SerializedName("web_site")
    private String site;
    private String url;
    private String image;
    @SerializedName("is_accepts_points")
    private Boolean acceptsPoints;
    @SerializedName("is_gives_points")
    private Boolean givesPoints;
    @SerializedName("is_new")
    private Boolean isNew;
    @SerializedName("locations_count")
    private Integer locationsCount;
    @SerializedName("is_favorite")
    private Boolean favorite;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public Integer getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(Integer purchaseType) {
        this.purchaseType = purchaseType;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getAcceptsPoints() {
        return acceptsPoints;
    }

    public void setAcceptsPoints(Boolean acceptsPoints) {
        this.acceptsPoints = acceptsPoints;
    }

    public Boolean getGivesPoints() {
        return givesPoints;
    }

    public void setGivesPoints(Boolean givesPoints) {
        this.givesPoints = givesPoints;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Integer getLocationsCount() {
        return locationsCount;
    }

    public void setLocationsCount(Integer locationsCount) {
        this.locationsCount = locationsCount;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (purchaseType != null ? purchaseType.hashCode() : 0);
        result = 31 * result + (rate != null ? rate.hashCode() : 0);
        result = 31 * result + (site != null ? site.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (acceptsPoints != null ? acceptsPoints.hashCode() : 0);
        result = 31 * result + (givesPoints != null ? givesPoints.hashCode() : 0);
        result = 31 * result + (isNew != null ? isNew.hashCode() : 0);
        result = 31 * result + (locationsCount != null ? locationsCount.hashCode() : 0);
        result = 31 * result + (favorite != null ? favorite.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Partner partner = (Partner) o;
        if (!id.equals(partner.id)) return false;
        if (title != null ? !title.equals(partner.title) : partner.title != null) return false;
        if (description != null ? !description.equals(partner.description) : partner.description != null) return false;
        if (category != null ? !category.equals(partner.category) : partner.category != null) return false;
        if (purchaseType != null ? !purchaseType.equals(partner.purchaseType) : partner.purchaseType != null) return false;
        if (rate != null ? !rate.equals(partner.rate) : partner.rate != null) return false;
        if (site != null ? !site.equals(partner.site) : partner.site != null) return false;
        if (url != null ? !url.equals(partner.url) : partner.url != null) return false;
        if (image != null ? !image.equals(partner.image) : partner.image != null) return false;
        if (acceptsPoints != null ? !acceptsPoints.equals(partner.acceptsPoints) : partner.acceptsPoints != null) return false;
        if (givesPoints != null ? !givesPoints.equals(partner.givesPoints) : partner.givesPoints != null) return false;
        if (isNew != null ? !isNew.equals(partner.isNew) : partner.isNew != null) return false;
        if (locationsCount != null ? !locationsCount.equals(partner.locationsCount) : partner.locationsCount != null) return false;
        return !(favorite != null ? !favorite.equals(partner.favorite) : partner.favorite != null);
    }

    @Override
    public String toString() {
        return "Partner{" +
                "id='" + id + '\'' +
                ", title=" + title +
                ", description=" + description +
                ", category=" + category +
                ", purchaseType=" + purchaseType +
                ", rate=" + rate +
                ", site=" + site +
                ", url=" + url +
                ", image=" + image +
                ", acceptsPoints=" + acceptsPoints +
                ", givesPoints=" + givesPoints +
                ", isNew=" + isNew +
                ", locationCount=" + locationsCount +
                ", favorite=" + favorite +
                '}';
    }
}
