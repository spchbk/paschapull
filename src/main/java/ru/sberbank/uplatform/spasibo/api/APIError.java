package ru.sberbank.uplatform.spasibo.api;

/**
 * @author Pavel Tarasov
 * @since 02/12/2015.
 */
public class APIError extends Exception {
    public APIError(String message) {
        super(message);
    }
}
