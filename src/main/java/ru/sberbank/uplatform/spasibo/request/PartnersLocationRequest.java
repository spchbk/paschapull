package ru.sberbank.uplatform.spasibo.request;

import com.google.gson.reflect.TypeToken;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.sberbank.uplatform.spasibo.api.PartnerLocation;
import ru.sberbank.uplatform.spasibo.response.SpasiboResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 27/11/2015.
 */
//todo add sort field
public class PartnersLocationRequest implements Request {
    private NameValuePair category;
    private NameValuePair bounds;
    private NameValuePair cityId;
    private NameValuePair acceptsPoints;
    private NameValuePair givesPoints;
    private NameValuePair purchaseType;
    private NameValuePair location;
    private NameValuePair distance;
    private NameValuePair partner;
    private NameValuePair partnerLocation;
    private NameValuePair hasActions;
    private NameValuePair pageSize;

    @Override
    public String getMethod() {
        return "GET";
    }

    @Override
    public String getPath() {
        return "/partners-locations";
    }

    @Override
    public List<NameValuePair> getQueryParameters() {
        List<NameValuePair> params = new ArrayList<>();
        if (category != null) params.add(category);
        if (bounds != null) params.add(bounds);
        if (cityId != null) params.add(cityId);
        if (acceptsPoints != null) params.add(acceptsPoints);
        if (givesPoints != null) params.add(givesPoints);
        if (purchaseType != null) params.add(purchaseType);
        if (location != null) params.add(location);
        if (distance != null) params.add(distance);
        if (partner != null) params.add(partner);
        if (partnerLocation != null) params.add(partnerLocation);
        if (hasActions != null) params.add(hasActions);
        if (pageSize != null) params.add(pageSize);
        return params;
    }

    @Override
    public Type getResponseType() {
        return new TypeToken<SpasiboResponse<PartnerLocation>>(){}.getType();
    }

    public PartnersLocationRequest setCategory(int categoryId) {
        this.category = new BasicNameValuePair("category", String.valueOf(categoryId));
        return this;
    }

    public PartnersLocationRequest setBounds(String leftUpperLat, String leftUpperLon, String rightBottomLat, String rightBottomLon) {
        StringBuilder sb = new StringBuilder("(").append(leftUpperLat).append(", ").append(leftUpperLon)
                .append(", ").append(rightBottomLat).append(", ").append(rightBottomLon).append(")");
        this.bounds = new BasicNameValuePair("bounds", sb.toString());
        return this;
    }

    public PartnersLocationRequest setCityId(int cityId) {
        this.cityId = new BasicNameValuePair("city", String.valueOf(cityId));
        return this;
    }

    public PartnersLocationRequest setAcceptsPoints(boolean acceptsPoints) {
        this.acceptsPoints = new BasicNameValuePair("is_accepts_points", acceptsPoints ? "True" : "False");
        return this;
    }

    public PartnersLocationRequest setGivesPoints(boolean givesPoints) {
        this.givesPoints = new BasicNameValuePair("is_gives_points", givesPoints ? "True" : "False");
        return this;
    }

    public PartnersLocationRequest setPurchaseType(boolean purchaseType) {
        this.purchaseType = new BasicNameValuePair("purchase_type", purchaseType ? "1" : "0");
        return this;
    }

    public PartnersLocationRequest setPageSize(int pageSize) {
        this.pageSize = new BasicNameValuePair("page_size", String.valueOf(pageSize));
        return this;
    }

    public PartnersLocationRequest setHasActions() {
        this.hasActions = new BasicNameValuePair("has_actions", "True");
        return this;
    }

    public PartnersLocationRequest setPartnerLocation(int partnerLocationId) {
        this.partnerLocation = new BasicNameValuePair("partner_location", String.valueOf(partnerLocationId));
        return this;
    }

    public PartnersLocationRequest setPartner(int partnerId) {
        this.partner = new BasicNameValuePair("partner", String.valueOf(partnerId));
        return this;
    }

    // distance in meters
    public PartnersLocationRequest setDistance(int distance) {
        this.distance = new BasicNameValuePair("distance", String.valueOf(distance));
        return this;
    }

    public PartnersLocationRequest setLocation(String lat, String lon) {
        this.location = new BasicNameValuePair("location", lat + "," + lon);
        return this;
    }
}
