package ru.sberbank.uplatform.spasibo.request;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 27/11/2015.
 */
public interface Request {

    String getMethod();

    String getPath();

    List<NameValuePair> getQueryParameters();

    Type getResponseType();

    default List<NameValuePair> getHeaderParams() {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("Content-type", "application/json"));
        params.add(new BasicNameValuePair("Device-ID", "1"));
        params.add(new BasicNameValuePair("Push-ID", "1"));
        params.add(new BasicNameValuePair("Device-OS", "1"));
        params.add(new BasicNameValuePair("Device-Model", "1"));
        params.add(new BasicNameValuePair("Device-OS-Version", "1"));
        params.add(new BasicNameValuePair("Device-Screen-Size", "640x480"));
        return params;
    }
}
