package ru.sberbank.uplatform.spasibo.request;

import com.google.gson.reflect.TypeToken;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.sberbank.uplatform.spasibo.api.Partner;
import ru.sberbank.uplatform.spasibo.response.SpasiboResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 27/11/2015.
 */
public class PartnersRequest implements Request {
    private NameValuePair title;
    private NameValuePair cityId;
    private NameValuePair acceptsPoints;
    private NameValuePair givesPoints;
    private NameValuePair purchaseType;

    @Override
    public String getMethod() {
        return "GET";
    }

    @Override
    public String getPath() {
        return "/partners";
    }

    @Override
    public List<NameValuePair> getQueryParameters() {
        List<NameValuePair> params = new ArrayList<>();
        if (title != null) params.add(title);
        if (cityId != null) params.add(cityId);
        if (acceptsPoints != null) params.add(acceptsPoints);
        if (givesPoints != null) params.add(givesPoints);
        if (purchaseType != null) params.add(purchaseType);
        return params;
    }

    @Override
    public Type getResponseType() {
        return new TypeToken<SpasiboResponse<Partner>>(){}.getType();
    }

    public PartnersRequest setTitle(String title) {
        this.title = new BasicNameValuePair("title", title);
        return this;
    }

    public PartnersRequest setCityId(int cityId) {
        this.cityId = new BasicNameValuePair("city", String.valueOf(cityId));
        return this;
    }

    public PartnersRequest setAcceptsPoints(boolean acceptsPoints) {
        this.acceptsPoints = new BasicNameValuePair("is_accepts_points", acceptsPoints ? "True" : "False");
        return this;
    }

    public PartnersRequest setGivesPoints(boolean givesPoints) {
        this.givesPoints = new BasicNameValuePair("is_gives_points", givesPoints ? "True" : "False");
        return this;
    }

    public PartnersRequest setPurchaseType(boolean purchaseType) {
        this.purchaseType = new BasicNameValuePair("purchase_type", purchaseType ? "1" : "0");
        return this;
    }
}
