package ru.sberbank.uplatform.spasibo.request;

import com.google.gson.reflect.TypeToken;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.sberbank.uplatform.spasibo.api.Action;
import ru.sberbank.uplatform.spasibo.response.SpasiboResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 02/12/2015.
 */
public class ActionRequest implements Request {
    private String path = "/actions";
    private NameValuePair partner;
    private NameValuePair city;

    @Override
    public String getMethod() {
        return "GET";
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public List<NameValuePair> getQueryParameters() {
        List<NameValuePair> params = new ArrayList<>();
        if (partner != null) params.add(partner);
        if (city != null) params.add(city);
        return params;
    }

    @Override
    public Type getResponseType() {
        return new TypeToken<SpasiboResponse<Action>>(){}.getType();
    }

    public ActionRequest setPartner(int partnerId) {
        this.partner = new BasicNameValuePair("partner", String.valueOf(partnerId));
        return this;
    }

    public ActionRequest setCity(int cityId) {
        this.city = new BasicNameValuePair("city", String.valueOf(cityId));
        return this;
    }
}
