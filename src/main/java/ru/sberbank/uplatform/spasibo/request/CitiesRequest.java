package ru.sberbank.uplatform.spasibo.request;

import com.google.gson.reflect.TypeToken;
import org.apache.http.NameValuePair;
import ru.sberbank.uplatform.spasibo.api.City;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 27/11/2015.
 */
public class CitiesRequest implements Request {

    @Override
    public String getMethod() {
        return "GET";
    }

    @Override
    public String getPath() {
        return "/cities";
    }

    @Override
    public List<NameValuePair> getQueryParameters() {
        return Collections.<NameValuePair>emptyList();
    }

    @Override
    public Type getResponseType() {
        return new TypeToken<List<City>>(){}.getType();
    }
}
