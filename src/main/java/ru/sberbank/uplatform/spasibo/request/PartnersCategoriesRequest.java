package ru.sberbank.uplatform.spasibo.request;

import com.google.gson.reflect.TypeToken;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.sberbank.uplatform.spasibo.api.Category;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pavel Tarasov
 * @since 27/11/2015.
 */
public class PartnersCategoriesRequest implements Request {
    private NameValuePair cityId;
    private NameValuePair acceptsPoints;
    private NameValuePair givesPoints;
    private NameValuePair purchaseType;

    @Override
    public String getMethod() {
        return "GET";
    }

    @Override
    public String getPath() {
        return "/partners-categories";
    }

    @Override
    public List<NameValuePair> getQueryParameters() {
        List<NameValuePair> params = new ArrayList<>();
        if (cityId != null) params.add(cityId);
        if (acceptsPoints != null) params.add(acceptsPoints);
        if (givesPoints != null) params.add(givesPoints);
        if (purchaseType != null) params.add(purchaseType);
        return params;
    }

    @Override
    public Type getResponseType() {
        return new TypeToken<List<Category>>(){}.getType();
    }

    public PartnersCategoriesRequest setCityId(int cityId) {
        this.cityId = new BasicNameValuePair("city", String.valueOf(cityId));
        return this;
    }

    public PartnersCategoriesRequest setAcceptsPoints(boolean acceptsPoints) {
        this.acceptsPoints = new BasicNameValuePair("is_accepts_points", acceptsPoints ? "True" : "False");
        return this;
    }

    public PartnersCategoriesRequest setGivesPoints(boolean givesPoints) {
        this.givesPoints = new BasicNameValuePair("is_gives_points", givesPoints ? "True" : "False");
        return this;
    }

    public PartnersCategoriesRequest setPurchaseType(boolean purchaseType) {
        this.purchaseType = new BasicNameValuePair("purchase_type", purchaseType ? "1" : "0");
        return this;
    }
}
