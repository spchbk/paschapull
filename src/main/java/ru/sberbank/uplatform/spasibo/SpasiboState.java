package ru.sberbank.uplatform.spasibo;

/**
 * @author Pavel Tarasov
 * @since 03/12/2015.
 */
public enum SpasiboState {
    FIND_ACTIONS("direct", "get-nearest-actions"),
    FIND_PARTNERS("direct", "get-nearest-partners");

    private String id;
    private String type;
    private final String endpoint;

    SpasiboState(String type, String id) {
        this.id = id;
        this.type = type;
        endpoint = type + ":" + id;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }
}
