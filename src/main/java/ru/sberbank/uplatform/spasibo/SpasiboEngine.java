package ru.sberbank.uplatform.spasibo;

import com.google.gson.Gson;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import ru.sberbank.uplatform.spasibo.api.APIError;
import ru.sberbank.uplatform.spasibo.response.Response;
import ru.sberbank.uplatform.spasibo.request.Request;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;

/**
 * @author Pavel Tarasov
 * @since 26/11/2015.
 */
public class SpasiboEngine {
    private final Logger log = LoggerFactory.getLogger(SpasiboEngine.class);
    private PoolingHttpClientConnectionManager connectionManager;
    @Value("${spasibo_api_url}")
    private String urlAPI;
    @Value("${spasibo_token}")
    private String token;

    public PoolingHttpClientConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public void setConnectionManager(PoolingHttpClientConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public <T> Response<T> sendRequest(Request request) throws IOException, APIError {
        String paramString = URLEncodedUtils.format(request.getQueryParameters(), "utf-8");
        StringBuilder urlBuilder = new StringBuilder(urlAPI).append(request.getPath());
        if (!paramString.isEmpty()) {
            urlBuilder.append("?").append(paramString);
        }

        HttpRequestBase requestBase = request.getMethod().equals("GET") ? new HttpGet() : new HttpPost();
        requestBase.setURI(URI.create(urlBuilder.toString()));
        requestBase.setHeader("Authorization", token);
        request.getHeaderParams().forEach(param -> requestBase.setHeader(param.getName(), param.getValue()));
        Response<T> resp = new Response<>();
        resp.setResponse(executeRequest(requestBase, request.getResponseType()));
        return resp;
    }

    private <T> T executeRequest(HttpRequestBase base, Type type) throws IOException, APIError {
        try (CloseableHttpClient client = HttpClients.custom().setConnectionManager(connectionManager).setConnectionManagerShared(true).build()) {
            try (CloseableHttpResponse response = client.execute(base)) {
                if (response.getStatusLine().getStatusCode() != 200) {
                    throw new APIError(EntityUtils.toString(response.getEntity()));
                }

                return new Gson().fromJson(EntityUtils.toString(response.getEntity()), type);
            }
        }
    }
}
