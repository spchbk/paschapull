package ru.sberbank.uplatform;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.sberbank.uplatform.message.keyboard.ReplyKeyboard;
import ru.sberbank.uplatform.message.reply.LocationReply;
import ru.sberbank.uplatform.message.reply.PhotoReply;
import ru.sberbank.uplatform.message.reply.Reply;
import ru.sberbank.uplatform.message.reply.VideoReply;

/**
 * Created by baryshnikov on 23.11.15.
 */
public class Attachment {
    public static final String HEADER = "attachment";
    private List<Reply> replies;
    private String keyboard;
    private List<Reply> future = new ArrayList<>();
    public final VirtualDirectory videos, instVideos, photos, documents;
    private Map<String, ReplyKeyboard> keyBoardsMarkup;

    public Attachment(VirtualDirectory videos, VirtualDirectory instVideos,
                      VirtualDirectory photos, VirtualDirectory documents, List<Reply> replies,
                      Map<String, ReplyKeyboard> keyBoardsMarkup) {
        this.videos = videos;
        this.instVideos = instVideos;
        this.photos = photos;
        this.documents = documents;
        this.replies = replies;
        this.keyBoardsMarkup = keyBoardsMarkup;
    }

    public VirtualDirectory getDocuments() {
        return documents;
    }

    public VirtualDirectory getPhotos() {
        return photos;
    }

    public VirtualDirectory getVideos() {
        return videos;
    }

    public VirtualDirectory getInstVideos() {
        return instVideos;
    }

    public void appendInstantVideo(String caption, String file) {
        addVideo(caption, file, future, null, instVideos);
    }

    public void appendVideo(String caption, String file) {
        addVideo(caption, file, future, null, videos);
    }

    public void appendVideo(String caption, String file, String keyboard) {
        addVideo(caption, file, future, keyboard, videos);
    }

    public void prependVideo(String caption, String file) {
        addVideo(caption, file, replies, null, videos);
    }

    public void appendLocation(Float lat, Float lon) {
        addLocation(lat, lon, future, null);
    }

    public void appendLocation(Float lat, Float lon, String keyboard) {
        addLocation(lat, lon, future, keyboard);
    }

    public void prependLocation(Float lat, Float lon) {
        addLocation(lat, lon, replies, null);
    }

    public void appendPhoto(String caption, String file, Integer botType) {
        addPhoto(caption, file, future, null, botType);
    }

    public void appendPhoto(String caption, String file, String keyboard, Integer botType) {
        addPhoto(caption, file, future, keyboard, botType);
    }

    public void prependPhoto(String caption, String file, Integer botType) {
        addPhoto(caption, file, replies, null, botType);
    }

    public void prependPhoto(String caption, String file, String keyboard, Integer botType) {
        addPhoto(caption, file, replies, keyboard, botType);
    }

    private void addPhoto(String caption, String file, List<Reply> dest, String keyboard, Integer botType) {
        PhotoReply r = new PhotoReply();
        r.setBotType(botType);
        r.setCaption(caption);
        r.setFile(photos.getRealName(file));
        if (keyboard != null) r.setReplyMarkup(keyBoardsMarkup.get(keyboard));
        dest.add(r);
    }

    private void addVideo(String caption, String file, List<Reply> dest, String keyboard, VirtualDirectory dir) {
        VideoReply r = new VideoReply();
        r.setCaption(caption);
        r.setFile(dir.getRealName(file));
        if (keyboard != null) r.setReplyMarkup(keyBoardsMarkup.get(keyboard));
        dest.add(r);
    }

    private void addLocation(Float lat, Float lon, List<Reply> dest, String keyboard) {
        LocationReply r = new LocationReply();
        r.setLatitude(lat);
        r.setLongitude(lon);
        if (keyboard != null) r.setReplyMarkup(keyBoardsMarkup.get(keyboard));
        dest.add(r);
    }

    public String getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(String keyboard) {
        this.keyboard = keyboard;
    }

    public List<Reply> getReplies() {
        return replies;
    }

    public void end() {
        replies.addAll(future);
    }
}