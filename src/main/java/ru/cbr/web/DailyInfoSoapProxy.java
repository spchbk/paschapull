package ru.cbr.web;

public class DailyInfoSoapProxy implements ru.cbr.web.DailyInfoSoap {
  private String _endpoint = null;
  private ru.cbr.web.DailyInfoSoap dailyInfoSoap = null;
  
  public DailyInfoSoapProxy() {
    _initDailyInfoSoapProxy();
  }
  
  public DailyInfoSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initDailyInfoSoapProxy();
  }
  
  private void _initDailyInfoSoapProxy() {
    try {
      dailyInfoSoap = (new ru.cbr.web.DailyInfoLocator()).getDailyInfoSoap();
      if (dailyInfoSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)dailyInfoSoap)._setProperty("javax.xml.rpc.web.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)dailyInfoSoap)._getProperty("javax.xml.rpc.web.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (dailyInfoSoap != null)
      ((javax.xml.rpc.Stub)dailyInfoSoap)._setProperty("javax.xml.rpc.web.endpoint.address", _endpoint);
    
  }
  
  public ru.cbr.web.DailyInfoSoap getDailyInfoSoap() {
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap;
  }
  
  public ru.cbr.web.SaldoXMLResponseSaldoXMLResult saldoXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.saldoXML(fromDate, toDate);
  }
  
  public ru.cbr.web.ROISfixXMLResponseROISfixXMLResult ROISfixXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.ROISfixXML(fromDate, toDate);
  }
  
  public ru.cbr.web.RuoniaXMLResponseRuoniaXMLResult ruoniaXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.ruoniaXML(fromDate, toDate);
  }
  
  public ru.cbr.web.OstatDepoXMLResponseOstatDepoXMLResult ostatDepoXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.ostatDepoXML(fromDate, toDate);
  }
  
  public ru.cbr.web.OstatDepoResponseOstatDepoResult ostatDepo(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.ostatDepo(fromDate, toDate);
  }
  
  public ru.cbr.web.ROISfixResponseROISfixResult ROISfix(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.ROISfix(fromDate, toDate);
  }
  
  public ru.cbr.web.RuoniaResponseRuoniaResult ruonia(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.ruonia(fromDate, toDate);
  }
  
  public ru.cbr.web.Mrrf7DResponseMrrf7DResult mrrf7D(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.mrrf7D(fromDate, toDate);
  }
  
  public ru.cbr.web.Mrrf7DXMLResponseMrrf7DXMLResult mrrf7DXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.mrrf7DXML(fromDate, toDate);
  }
  
  public ru.cbr.web.RepoDebtUSDResponseRepoDebtUSDResult repoDebtUSD(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.repoDebtUSD(fromDate, toDate);
  }
  
  public ru.cbr.web.RepoDebtUSDXMLResponseRepoDebtUSDXMLResult repoDebtUSDXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.repoDebtUSDXML(fromDate, toDate);
  }
  
  public ru.cbr.web.MrrfResponseMrrfResult mrrf(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.mrrf(fromDate, toDate);
  }
  
  public ru.cbr.web.MrrfXMLResponseMrrfXMLResult mrrfXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.mrrfXML(fromDate, toDate);
  }
  
  public ru.cbr.web.SaldoResponseSaldoResult saldo(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.saldo(fromDate, toDate);
  }
  
  public ru.cbr.web.NewsInfoXMLResponseNewsInfoXMLResult newsInfoXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.newsInfoXML(fromDate, toDate);
  }
  
  public ru.cbr.web.OmodInfoXMLResponseOmodInfoXMLResult omodInfoXML() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.omodInfoXML();
  }
  
  public ru.cbr.web.XVolResponseXVolResult XVol(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.XVol(fromDate, toDate);
  }
  
  public ru.cbr.web.XVolXMLResponseXVolXMLResult XVolXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.XVolXML(fromDate, toDate);
  }
  
  public ru.cbr.web.MainInfoXMLResponseMainInfoXMLResult mainInfoXML() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.mainInfoXML();
  }
  
  public ru.cbr.web.AllDataInfoXMLResponseAllDataInfoXMLResult allDataInfoXML() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.allDataInfoXML();
  }
  
  public ru.cbr.web.NewsInfoResponseNewsInfoResult newsInfo(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.newsInfo(fromDate, toDate);
  }
  
  public ru.cbr.web.SwapDynamicXMLResponseSwapDynamicXMLResult swapDynamicXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.swapDynamicXML(fromDate, toDate);
  }
  
  public ru.cbr.web.SwapInfoSellUSDVolXMLResponseSwapInfoSellUSDVolXMLResult swapInfoSellUSDVolXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.swapInfoSellUSDVolXML(fromDate, toDate);
  }
  
  public ru.cbr.web.SwapInfoSellUSDVolResponseSwapInfoSellUSDVolResult swapInfoSellUSDVol(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.swapInfoSellUSDVol(fromDate, toDate);
  }
  
  public ru.cbr.web.SwapInfoSellUSDResponseSwapInfoSellUSDResult swapInfoSellUSD(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.swapInfoSellUSD(fromDate, toDate);
  }
  
  public ru.cbr.web.SwapInfoSellUSDXMLResponseSwapInfoSellUSDXMLResult swapInfoSellUSDXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.swapInfoSellUSDXML(fromDate, toDate);
  }
  
  public ru.cbr.web.BiCurBaseXMLResponseBiCurBaseXMLResult biCurBaseXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.biCurBaseXML(fromDate, toDate);
  }
  
  public ru.cbr.web.BiCurBaseResponseBiCurBaseResult biCurBase(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.biCurBase(fromDate, toDate);
  }
  
  public ru.cbr.web.BiCurBacketXMLResponseBiCurBacketXMLResult biCurBacketXML() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.biCurBacketXML();
  }
  
  public ru.cbr.web.BiCurBacketResponseBiCurBacketResult biCurBacket() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.biCurBacket();
  }
  
  public ru.cbr.web.SwapDynamicResponseSwapDynamicResult swapDynamic(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.swapDynamic(fromDate, toDate);
  }
  
  public ru.cbr.web.SwapMonthTotalResponseSwapMonthTotalResult swapMonthTotal(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.swapMonthTotal(fromDate, toDate);
  }
  
  public ru.cbr.web.SwapMonthTotalXMLResponseSwapMonthTotalXMLResult swapMonthTotalXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.swapMonthTotalXML(fromDate, toDate);
  }
  
  public ru.cbr.web.MKRResponseMKRResult MKR(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.MKR(fromDate, toDate);
  }
  
  public ru.cbr.web.MKRXMLResponseMKRXMLResult MKRXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.MKRXML(fromDate, toDate);
  }
  
  public ru.cbr.web.DVResponseDVResult DV(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.DV(fromDate, toDate);
  }
  
  public ru.cbr.web.DVXMLResponseDVXMLResult DVXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.DVXML(fromDate, toDate);
  }
  
  public ru.cbr.web.Repo_debtResponseRepo_debtResult repo_debt(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.repo_debt(fromDate, toDate);
  }
  
  public ru.cbr.web.Repo_debtXMLResponseRepo_debtXMLResult repo_debtXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.repo_debtXML(fromDate, toDate);
  }
  
  public ru.cbr.web.Coins_baseResponseCoins_baseResult coins_base(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.coins_base(fromDate, toDate);
  }
  
  public ru.cbr.web.Coins_baseXMLResponseCoins_baseXMLResult coins_baseXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.coins_baseXML(fromDate, toDate);
  }
  
  public ru.cbr.web.FixingBaseResponseFixingBaseResult fixingBase(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.fixingBase(fromDate, toDate);
  }
  
  public ru.cbr.web.FixingBaseXMLResponseFixingBaseXMLResult fixingBaseXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.fixingBaseXML(fromDate, toDate);
  }
  
  public ru.cbr.web.OvernightResponseOvernightResult overnight(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.overnight(fromDate, toDate);
  }
  
  public ru.cbr.web.OvernightXMLResponseOvernightXMLResult overnightXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.overnightXML(fromDate, toDate);
  }
  
  public ru.cbr.web.BauctionResponseBauctionResult bauction(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.bauction(fromDate, toDate);
  }
  
  public ru.cbr.web.BauctionXMLResponseBauctionXMLResult bauctionXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.bauctionXML(fromDate, toDate);
  }
  
  public ru.cbr.web.DepoDynamicXMLResponseDepoDynamicXMLResult depoDynamicXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.depoDynamicXML(fromDate, toDate);
  }
  
  public ru.cbr.web.DepoDynamicResponseDepoDynamicResult depoDynamic(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.depoDynamic(fromDate, toDate);
  }
  
  public ru.cbr.web.OstatDynamicXMLResponseOstatDynamicXMLResult ostatDynamicXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.ostatDynamicXML(fromDate, toDate);
  }
  
  public ru.cbr.web.OstatDynamicResponseOstatDynamicResult ostatDynamic(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.ostatDynamic(fromDate, toDate);
  }
  
  public ru.cbr.web.DragMetDynamicXMLResponseDragMetDynamicXMLResult dragMetDynamicXML(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.dragMetDynamicXML(fromDate, toDate);
  }
  
  public ru.cbr.web.DragMetDynamicResponseDragMetDynamicResult dragMetDynamic(java.util.Calendar fromDate, java.util.Calendar toDate) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.dragMetDynamic(fromDate, toDate);
  }
  
  public java.util.Calendar getLatestDateTime() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getLatestDateTime();
  }
  
  public java.lang.String getLatestDate() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getLatestDate();
  }
  
  public java.util.Calendar getLatestDateTimeSeld() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getLatestDateTimeSeld();
  }
  
  public java.lang.String getLatestDateSeld() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getLatestDateSeld();
  }
  
  public ru.cbr.web.EnumValutesXMLResponseEnumValutesXMLResult enumValutesXML(boolean seld) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.enumValutesXML(seld);
  }
  
  public ru.cbr.web.EnumValutesResponseEnumValutesResult enumValutes(boolean seld) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.enumValutes(seld);
  }
  
  public java.util.Calendar getLatestReutersDateTime() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getLatestReutersDateTime();
  }
  
  public ru.cbr.web.EnumReutersValutesXMLResponseEnumReutersValutesXMLResult enumReutersValutesXML() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.enumReutersValutesXML();
  }
  
  public ru.cbr.web.EnumReutersValutesResponseEnumReutersValutesResult enumReutersValutes() throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.enumReutersValutes();
  }
  
  public ru.cbr.web.GetReutersCursOnDateXMLResponseGetReutersCursOnDateXMLResult getReutersCursOnDateXML(java.util.Calendar on_date) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getReutersCursOnDateXML(on_date);
  }
  
  public ru.cbr.web.GetReutersCursOnDateResponseGetReutersCursOnDateResult getReutersCursOnDate(java.util.Calendar on_date) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getReutersCursOnDate(on_date);
  }
  
  public ru.cbr.web.GetReutersCursDynamicXMLResponseGetReutersCursDynamicXMLResult getReutersCursDynamicXML(java.util.Calendar fromDate, java.util.Calendar toDate, int numCode) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getReutersCursDynamicXML(fromDate, toDate, numCode);
  }
  
  public ru.cbr.web.GetReutersCursDynamicResponseGetReutersCursDynamicResult getReutersCursDynamic(java.util.Calendar fromDate, java.util.Calendar toDate, int numCode) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getReutersCursDynamic(fromDate, toDate, numCode);
  }
  
  public ru.cbr.web.GetCursDynamicXMLResponseGetCursDynamicXMLResult getCursDynamicXML(java.util.Calendar fromDate, java.util.Calendar toDate, java.lang.String valutaCode) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getCursDynamicXML(fromDate, toDate, valutaCode);
  }
  
  public ru.cbr.web.GetCursDynamicResponseGetCursDynamicResult getCursDynamic(java.util.Calendar fromDate, java.util.Calendar toDate, java.lang.String valutaCode) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getCursDynamic(fromDate, toDate, valutaCode);
  }
  
  public ru.cbr.web.GetCursOnDateXMLResponseGetCursOnDateXMLResult getCursOnDateXML(java.util.Calendar on_date) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getCursOnDateXML(on_date);
  }
  
  public ru.cbr.web.GetSeldCursOnDateXMLResponseGetSeldCursOnDateXMLResult getSeldCursOnDateXML(java.util.Calendar on_date) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getSeldCursOnDateXML(on_date);
  }
  
  public ru.cbr.web.GetSeldCursOnDateResponseGetSeldCursOnDateResult getSeldCursOnDate(java.util.Calendar on_date) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getSeldCursOnDate(on_date);
  }
  
  public ru.cbr.web.GetCursOnDateResponseGetCursOnDateResult getCursOnDate(java.util.Calendar on_date) throws java.rmi.RemoteException{
    if (dailyInfoSoap == null)
      _initDailyInfoSoapProxy();
    return dailyInfoSoap.getCursOnDate(on_date);
  }
  
  
}