#!/bin/bash

HOME="/home/$USER/telegram"

SUDO="sudo"

if [ "$(id -u)" == "0" ]; then
   SUDO=""
   HOME="/root/telegram"
fi

BACKUPS="/var/backups/telegram"
BINDIR="/opt/telegram"
CONFDIR="/etc/telegram"
LOGSDIR="/var/log/telegram"
SHARE_VIDEODIR="/usr/share/telegram/video"
SHARE_PHOTODIR="/usr/share/telegram/photo"
SERVICEDIR="/etc/init"
CERTPASS="qwe123"

echo "Check requirements..."
if ! java -version; then
  $SUDO apt-get update && $SUDO apt-get install -y software-properties-common
  $SUDO add-apt-repository -y ppa:webupd8team/java
  $SUDO apt-get update
  $SUDO apt-get install -y oracle-java8-installer
fi

$SUDO apt-get install -y gdebi-core
$SUDO gdebi -n /tmp/sber-telegram*.deb

# Create symlinks
echo "Creating symlinks"
$SUDO ln -s "$BINDIR" "$HOME/bin"                            || exit 1
$SUDO ln -s "$LOGSDIR" "$HOME/logs"                          || exit 1
$SUDO ln -s "$SHARE_VIDEODIR" "$HOME/video"                  || exit 1
$SUDO ln -s "$SHARE_PHOTODIR" "$HOME/photo"                  || exit 1
$SUDO ln -s "$CONFDIR" "$HOME/config"                        || exit 1
$SUDO ln -s "$BACKUPS" "$HOME/backups"                       || exit 1
$SUDO ln -s "$SERVICEDIR/telegram.conf" "$HOME/service.conf" || exit 1


cat << EOL
===========================================================
                          DONE

Installation success, but instance is not running yet.

Start service

    sudo service telegram start

===========================================================
EOL
