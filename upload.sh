#!/bin/bash
#
# Requires configuration in ~/.ssh/config telegram-$ENV or telegram-dev item
#
# Starts in project home
ST_BEGIN=$(date +%s)
DIR="~/telegram"
TARGET="target/"
ENV=$1
if [ -z $1 ]; then
  ENV="dev"
fi
mvn --version || ( echo "Maven not installed" && exit 1 )
mvn clean install

# Create temp dir
ssh telegram-$ENV rm -rf /tmp/telegram
ssh telegram-$ENV mkdir -p /tmp/telegram/bin
ssh telegram-$ENV mkdir -p /tmp/telegram/conf
ssh telegram-$ENV mkdir -p /tmp/telegram/video
ssh telegram-$ENV mkdir -p /tmp/telegram/photo

scp "$TARGET"/sber-telegram-bot-*-SNAPSHOT-"$ENV"_all.deb            telegram-$ENV:"/tmp/"

scp -r src/main/resources/setup.sh telegram-$ENV:"/tmp/telegram/setup.sh"
ssh telegram-$ENV chmod +x /tmp/telegram//setup.sh
ssh telegram-$ENV /tmp/telegram/setup.sh || ( exit 1 && notify-send "Deploy to $ENV failed!" )
ssh telegram-$ENV rm "/tmp/*.deb"
ssh telegram-$ENV 'echo manual | sudo tee /etc/init/telegram-checker.override'

if [ "start" == "$2" ]; then
# ssh telegram-$ENV telegram-begin-listen
  ssh telegram-$ENV sudo service telegram restart
fi
if [ "start-slave" == "$2" ]; then
  ssh telegram-$ENV sudo rm /etc/init/telegram-checker.override
  ssh telegram-$ENV sudo service telegram-checker restart
  ssh telegram-$ENV sudo service telegram restart
fi
ST_END=$(date +%s)
notify-send "Deploy to $ENV success" "It takes $(($ST_END-$ST_BEGIN)) seconds"
