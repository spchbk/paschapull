#!/bin/bash

URL="https://ec2-54-76-154-1.eu-west-1.compute.amazonaws.com:443/"
MODES=( currency )
THREADS=( 1 4 8 16 32 64 )
COUNTS=( 100 1000 10000 )

echo "timestamp,mode,threads,count,requests_per_second" > result.csv
for mode in "${MODES[@]}"; do
  for thread in "${THREADS[@]}"; do
     for count in "${COUNTS[@]}"; do
       echo "mode = $mode, threads = $thread, count = $count"
       rp=$(ab -n $count -c $thread -k -H "Content-Type: application/json" -p perf_$mode.json "$URL" | grep "Requests per second" | cut   -d ':' -f 2 | cut -d ' ' -s -f 5 )
       echo "$(date --rfc-3339=seconds),$mode,$thread,$count,$rp" >> result.csv
     done
  done
done
